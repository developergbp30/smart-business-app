import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart'
;
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/widgets/change_user_widget.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_left.dart';
import 'package:smart_business/src/presentation/widgets/modals/modal_users.dart';
import 'package:smart_business/src/presentation/widgets/widget_time_line_burble.dart';

import 'create_programing_page.dart';

class ExampleData {
  String title;
  String subtitle;
  String date;
  Color color;
  ExampleData({this.title, this.subtitle, this.date, this.color});
}

class ProgramingPage extends StatefulWidget {
  static const String routeName = 'programing_page';
  @override
  _ProgramingPageState createState() => _ProgramingPageState();
}

class _ProgramingPageState extends State<ProgramingPage> {
  ThemeApp _themeApp = ThemeApp();
  List<ExampleData> example = [
    new ExampleData(title: "Bitácora 12 Nov 2019", subtitle: "Coca-Cola Inc", date: "12 Nov 2019", color: new ThemeApp().colorPink),
    new ExampleData(title: "Bitácora 12 Nov 2019", subtitle: "Nike", date: "12 Nov 2019", color: new ThemeApp().colorPink),
    new ExampleData(title: "Acordar diseño de personajes", subtitle: "Estudios Ghibli", date: "12 Nov 2019", color: new ThemeApp().colorPink),
    new ExampleData(title: "Bitácora 12 Nov 2019", subtitle: "Coca-Cola Inc", date: "12 Nov 2019", color: new ThemeApp().colorPink),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: DrawerLeft(),
      ),
      appBar: AppBar(
        backgroundColor: _themeApp.colorPink,
        leading: Builder(builder: (context) => IconButton(icon: Icon(Icons.menu), onPressed: () => Scaffold.of(context).openDrawer()),),
        title: Text("Por programar"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(12),
            )
        ),
        actions: <Widget>[
          InkWell(
            child: AspectRatio(
                aspectRatio: 1.4,
                child: ChangeUserWidget()),
            onTap: (){
                  _showUserModal(context);
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Container(child: ListView.builder(
          itemCount: example.length,
          itemBuilder: (context, i ) => _itemCard(example[i])),),
    );
  }

  Widget _itemCard(ExampleData example) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: InkWell(
        onTap: (){
          _ModalDetail();
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      //margin: const EdgeInsets.only(top: 10),
                      //color: Colors.red,
                      width: 50,
                      height: 85,
                        child: TimeLineWidget(color: example.color,heightCylinder: 30,)),
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, top: 0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(example.title, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                            Container(
                                margin: const EdgeInsets.only(top: 5),
                                child: Text(example.subtitle, style: TextStyle(color: Colors.grey[700]),)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Chip(
                                  avatar: CircleAvatar(
                                    backgroundImage: new NetworkImage("https://image.shutterstock.com/image-photo/high-contrast-black-white-portrait-260nw-768973558.jpg"),
                                    // child: ,
                                  ),
                                  label:Text("Reynaldo José"),
                                ),
                                Text(example.date,style: TextStyle(fontSize: 12, color: Colors.grey[250])),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10,)
              ],
            ),
          ),),
      ),
    );
  }

  _showUserModal(BuildContext context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return ModalUsersWidget();
        }
    );
  }

  Widget _ModalDetail(){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
           // height: 350,
            padding: const EdgeInsets.all(20),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.close, color: Colors.grey,),
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ),
                Text("Bitácora 12 Nov 2019", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                Text("Coca-Cola inc"),
                Text("12 Nov 2019"),
                Chip(
                  avatar: CircleAvatar(
                    backgroundImage: new NetworkImage("https://image.shutterstock.com/image-photo/high-contrast-black-white-portrait-260nw-768973558.jpg"),
                    // child: ,
                  ),
                  label:Text("Estefania Ardilla"),
                ),
                SizedBox(height: 20,),
                Text("Description", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                Text("is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy "
                    "text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"),

                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {

                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Programar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => CreatePrograming()));

                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }
}
