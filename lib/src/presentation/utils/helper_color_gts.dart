 import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:smart_business/src/models/invitations.dart';

/*Color helperColorGts(GtsTodos gts){
      if(gts.idPqr != null){
        return Color(0xFFE9B567);
      }

      if(gts.idEvento != null){
        return Color(0xFF2A3971);
      }

      if(gts.idCliente != null){
        return Color(0xFF594EF1);
      }

      if(gts.idOportunidad != null && gts.idOportunidad == 1){
        return Color(0xFF079CE5);
      }

      if(gts.idOportunidad != null && gts.idOportunidad == 2){
        return Color(0xFF439569);
      }

      if(gts.idOportunidad != null && gts.idOportunidad == 3){
        return Color(0xFF9F9F9F);
      }

      return Color(0xFFEF2A2A);

 }*/
Color helperColorGts(GtsTodos gts){

      if(gts.gtsTipos != null && gts.gtsTipos.idPadre == null){
        var response = Color(0xFFEF2A2A);

        switch(gts.gtsTipos.tipoPadre){
          case 1: // oportunidades vigentes
              response = Color(0xFF079CE5);
            break;
          case 2: // oportunidades concretas
              response = Color(0xFF439569);
            break;
          case 3: // oportunidades perdidas
            response = Color(0xFF9F9F9F);
            break;
          case 4: // Pqr
              response = Color(0xFFE9B567);
            break;
          case 5: // Actas
            response = Color(0xFF2AC1CC);
            break;
          case 6: // Competencias
            response = Color(0xFFF86767);
            break;
          case 7: // Eventos
              response = Color(0xFF2A3971);
            break;
          case 8: // Proyectos
            response = Color(0xFFAB9910);
            break;
        }
        return response;
      }else {
        return Color(0xFF888787);
      }

 }

Color helperColorGtsId(int id){

        var response = Color(0xFFEF2A2A);

        switch(id){
          case 1: // oportunidades vigentes
              response = Color(0xFF079CE5);
            break;
          case 2: // oportunidades concretas
              response = Color(0xFF439569);
            break;
          case 3: // oportunidades perdidas
            response = Color(0xFF9F9F9F);
            break;
          case 4: // Pqr
              response = Color(0xFFE9B567);
            break;
          case 5: // Actas
            response = Color(0xFF2AC1CC);
            break;
          case 6: // Competencias
            response = Color(0xFFF86767);
            break;
          case 7: // Eventos
              response = Color(0xFF2A3971);
            break;
          case 8: // Proyectos
            response = Color(0xFFAB9910);
            break;
        }
        return response;
 }