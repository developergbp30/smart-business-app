
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:smart_business/src/blocs/contacts/contact_event.dart';
import 'package:smart_business/src/blocs/contacts/contact_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/contacts/contacts_repository.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState>{
   final ContactsRepository _contactsRepository;
   final AuthRepository _authRepository = AuthRepository();
   ContactBloc({@required ContactsRepository contactsRepository}) : assert(contactsRepository != null),
   _contactsRepository =  contactsRepository;

  @override
  ContactState get initialState => UnLoadedContactsState();

  @override
  Stream<ContactState> mapEventToState(ContactEvent event) async* {
    if(event is LoadContactsEvent){
     yield* _mapLoadContactsToState(event);
    }
    else if(event is SearchContactsEvent){
     yield* _mapSearchContactsState(event);
    }
    if(event is SearchUserEvent){
     yield*  _mapSearchUserState(event);
    }
    else if(event is ToSelectUserEvent){
     yield* _mapSelectUserState(event);
    }
    if(event is SearchClientContactsEvent){
      yield* _mapSearchClientContactsState(event);
    }
    else if(event is ToSelectClientContactEvent){
      yield* _mapSelectClientContactsState(event);
    }
  }
   Stream<ContactState> _mapLoadContactsToState(LoadContactsEvent event) async*{
      try{
        var token =  await _authRepository.getToken();

        if(event.type == "contact-client" && event.idContact != null){
          var res  =  await _contactsRepository.contactsClient(event.idContact);
          yield LoadedClientContactState(clientContact: res, tokenUser: token);

        }
        else if(event.type == "users"){
          var res = await _contactsRepository.users();
          yield  LoadedUsersState(users: res, tokenUser: token);
        }

        else {
          final res = await _contactsRepository.getContactsExample();
          yield LoadedContactsState(res);
        }

      }catch (error){

      }
   }

   Stream<ContactState> _mapSearchContactsState(SearchContactsEvent event) async*{
      var list =  event.contacts;
      var filter = list.where((item) => item.name.contains(event.query));
     yield SearchContactsState(query: event.query, contacts: event.contacts, contactFilters: filter.toList());
   }

   Stream<ContactState> _mapSearchUserState(SearchUserEvent event) async*{
     var token =  await _authRepository.getToken();
     var list =  event.contacts;
     var filter = list.where((item) => item.nombre.contains(event.query)).toList();
     yield  LoadedUsersState(users: filter, tokenUser: token, previous: event.contacts);
   }

   Stream<ContactState> _mapSelectUserState(ToSelectUserEvent event) async*{
     var token =  await _authRepository.getToken();
     var list =  event.contacts;
     list.forEach((i){
       if(i.id == event.id) i.selected = event.selected;
     });
     print(list);
     yield  LoadedUsersState(users: list, tokenUser: token,);
   }

   Stream<ContactState> _mapSearchClientContactsState(SearchClientContactsEvent event) async*{
     var token =  await _authRepository.getToken();
     var list =  event.contacts;
     var filter = list.where((item) => item.contacto.nombre.contains(event.query)).toList();
     yield  LoadedClientContactState(clientContact: filter, tokenUser: token, previous: event.contacts);
   }

   Stream<ContactState> _mapSelectClientContactsState(ToSelectClientContactEvent event) async*{
     var token =  await _authRepository.getToken();
     var list =  event.contacts;
     list.forEach((i){
       if(i.id == event.id) i.selected = event.selected;
     });
     print(list);
     yield  LoadedClientContactState(clientContact: list, tokenUser: token,);
   }
}