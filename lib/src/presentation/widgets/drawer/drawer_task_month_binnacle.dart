import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/blocs/task/task_event.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/widgets/drawer/sub_drawer_task/widget_events.dart';
import 'package:smart_business/src/presentation/widgets/drawer/sub_drawer_task/widget_opportunities.dart';
import 'package:smart_business/src/presentation/widgets/drawer/sub_drawer_task/widget_pqr.dart';
import 'package:smart_business/src/presentation/widgets/drawer/sub_drawer_task/widget_project.dart';
import 'package:smart_business/src/presentation/widgets/drawer/sub_drawer_task/widget_task_defeated.dart';
import 'package:smart_business/src/presentation/widgets/drawer/sub_drawer_task/widget_task_valid.dart';
class DrawerBinnacleTask extends StatefulWidget {
  /*-------------------------
  | 1) type => 1 tasks for WorkPlane
  | 2) type => 2 tasks for Binnacle
  |
  ----------------------------
   */
  int type;
  int userId;
  DrawerBinnacleTask({this.type, this.userId});

  @override
  _DrawerBinnacleTaskState createState() => _DrawerBinnacleTaskState();
}

class _DrawerBinnacleTaskState extends State<DrawerBinnacleTask>  {
  ThemeApp _themeApp = ThemeApp();


  @override
  void initState() {
    BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskDefeatedEvent(typeTask: widget.type));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: _themeApp.colorPink,
          leading: Builder(builder: (context) => IconButton(icon: Icon(FontAwesomeIcons.calendarCheck), onPressed: () => Scaffold.of(context).openDrawer()),),
          title: Text("Tareas ${DateFormat.MMMM('es').format(DateTime.now().toLocal())}"),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(12),
              )
          ),
        ),
        body: Column(
          children: <Widget>[
            _tabBar(),
            Expanded(
              child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                TaskDefeated(),
                TaskValid(),
                PqrList(),
                OpportunitiesWidgetList(),
                EventsWidgetList(),
                ProjectList()
              ]),
            )
          ],
        ),
      ),
    );
  }
  
  Widget _tabBar() {
    return Container(
      child: TabBar(
        isScrollable: false,
          tabs: <Widget>[
              Tab(
                icon: Icon(Icons.library_books,),
              ),
              Tab(
              icon: Icon(FontAwesomeIcons.clipboardList),
            ),
              Tab(
                icon: Icon(FontAwesomeIcons.phoneSquareAlt),
              ),

              Tab(
                icon: Icon(FontAwesomeIcons.rocket),
              ),
              Tab(
                icon: Icon(FontAwesomeIcons.calendar),
              ),
              Tab(
                icon: Icon(FontAwesomeIcons.briefcase),
              ),
          ],
        indicatorColor: Colors.transparent,
        labelColor: _themeApp.colorPink,
        indicatorSize: TabBarIndicatorSize.label,
        unselectedLabelColor: Colors.grey,
        onTap: tap,
      ),
    );
  }

  void tap(int index){
    switch(index){
      case 0:
        BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskDefeatedEvent(typeTask: widget.type));
        break;
      case 1:
        BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskValidEvent(typeTask: widget.type));
        break;
      case 2:
        BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskPqrEvent(typeTask: widget.type, userId: widget.userId));

        break;
      case 3: //
        BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskOpsEvent(typeTask: widget.type));
        break;
      case 4: //
        BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskEventsEvent(typeTask: widget.type));
        break;
      case 5: //
        BlocProvider.of<TaskBloc>(context).dispatch(LoadTaskProjectEvent(typeTask: widget.type));
        break;
    }
  }
}
