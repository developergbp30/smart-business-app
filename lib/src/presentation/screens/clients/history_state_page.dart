import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
class HistoryStatePage extends StatefulWidget {
  @override
  _HistoryStatePageState createState() => _HistoryStatePageState();
}

class _HistoryStatePageState extends State<HistoryStatePage> {
  final Color greenColor =  Color(0xFF11BA19);
  TextEditingController descriptionCtrl = TextEditingController();

  String exampleDate = "2029-03-10";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(icon: Icon(Icons.clear, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Historial de estado", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: Colors.green,), onPressed: (){

          })
        ],
      ),
      body: Container(child: ListView.builder(
          itemCount: 4,
          itemBuilder: (context, i) => _item(context)),),
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 1),
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[

              InkWell(
                onTap: () async {
                    _modalEdit(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.pencilAlt),
                    Text("Editar estado")
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget _item(BuildContext context){
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 10),
      child: InkWell(
        onTap: (){
          _modalDetail(context);
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left:2.0),
                      child: Text("Estado actual:", style: TextStyle(fontWeight: FontWeight.w600, color: Color(0xFF5D5D5D)),),
                    ),
                  ],
                ),
                Row(children: <Widget>[
                  Icon(Icons.check_box, color: greenColor,),
                  SizedBox(width: 8,),
                  Text("Productivo (Activo)", style: TextStyle(color: greenColor),),

                ],),
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Chip(
                        avatar: CircleAvatar(
                          backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814__340.jpg"),
                          // child: ,
                        ),
                        backgroundColor: Color(0xFFDBDBDB),//Color(0xFFFFFFFF),
                        shadowColor:Color(0xFFDBDBDB),
                        label:Text("Juan Camilo"),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.calendar_today, color: Colors.green,),
                          Padding(
                            padding: const EdgeInsets.only(left:3.0),
                            child: Text("${DateFormat.yMMMd('es').format(DateTime.parse(exampleDate).toLocal())}",
                              style: TextStyle(color: Color(0xFF5D5D5D), fontSize: 12),),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _modalEdit(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Editar estado", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Seleccionar estado", style: TextStyle(color: Colors.grey[700]),),
                        ),
                        InkWell(
                          child:  Container(
                            height: 40,
                            decoration: new BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Color(0xFFDBDBDB), width: 1),
                                borderRadius: new BorderRadius.circular(5.0)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(""),
                                  Icon(Icons.expand_more)
                                ],
                              ),
                            ),
                          ),
                          onTap: (){
                            Navigator.of(context).pop();
                            _modalStates(context);
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Justificación", style: TextStyle(color: Colors.grey[700]),),
                        ),
                        SizedBox(height: 6),
                        TextFormField(
                          controller: descriptionCtrl,
                          keyboardType: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 5,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFFDBDBDB),
                                    width: 5.0
                                )
                            ),
                            hintText: 'Ingresa la justificación',
                            // labelText: 'Descripcion'
                          ),
                          validator: (value){
                            if(value.isEmpty){
                              return 'Ingrese la descripción';
                            }
                            return null;
                          },
                          onSaved: (value){

                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: greenColor),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {

                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          })
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }

  void _modalStates(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Selección de estados", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Intermitente (activo)"),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Intermitente (inactivo)"),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("En mora (inactivo)", style: TextStyle(color: greenColor),),
                        ),
                      ],
                    ),
                  ),


                ],
              ),
            ),
          );
        }
    );
  }

  void _modalDetail(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(width: 60,height: 3, color: Colors.grey.withOpacity(0.6), margin: EdgeInsets.only(top: 15),),
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Descripción estado", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Chip(
                          avatar: CircleAvatar(
                            backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814__340.jpg"),
                            // child: ,
                          ),
                          backgroundColor: Color(0xFFFFFFFF),
                          shadowColor:Color(0xFFDBDBDB),
                          label:Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                            Text("Juan Camilo", style: TextStyle(color: Color(0xFF5D5D5D), fontWeight: FontWeight.w600),),
                            Text("Ejecutivo comercial")
                          ],),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.calendar_today, color: Colors.green,),
                            Padding(
                              padding: const EdgeInsets.only(left:3.0),
                              child: Text("${DateFormat.yMMMd('es').format(DateTime.parse(exampleDate).toLocal())}",
                                style: TextStyle(color: Color(0xFF5D5D5D), fontSize: 12),),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 30,),
                  ListTile(
                    title: Text("Descripción", style: TextStyle(color: Colors.black),),
                    subtitle: Text("of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,",textAlign: TextAlign.justify,),
                  ),
                  SizedBox(height: 30,)
                ],
              ),
            ),
          );
        }
    );
  }
}
