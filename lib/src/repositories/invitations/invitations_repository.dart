
import 'package:smart_business/src/api/services/invitations_http_service.dart';
import 'package:smart_business/src/models/approved_user_model.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/guest_user_model.dart';
import 'package:smart_business/src/models/invitations.dart';

class InvitationsRepository {
    InvitationsHttp _invitationsHttp = InvitationsHttp();

  Future<List<InvitationReceivedModel>> received(int idUser) async {
    try{
        return await _invitationsHttp.getReceived(idUser);
    }catch (error){
       return Future.error(error);
    }
  }

  Future<List<InvitationReceivedModel>> send(int idUser) async {
    try{
        return await _invitationsHttp.getSend(idUser);
    }catch (error){
     return Future.error(error);
    }
  }

  Future approved(ApprovedInvitationModel inv) async {
    try{
      return await _invitationsHttp.approved(inv);
    }catch (error){
     return Future.error(error);
    }
  }

  Future reject(ApprovedInvitationModel inv) async {
    try{
      return await _invitationsHttp.reject(inv);
    }catch (error){
      return Future.error(error);
    }
  }

  Future cancel(ApprovedInvitationModel inv) async {
    try{
        return await _invitationsHttp.cancel(inv);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<RequestContactModel>> contacts(int idGts) async{
    try{
      return await _invitationsHttp.contacts(idGts);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<GuestUserModel>> guestUsers(int idGts) async{
    try{
        return await _invitationsHttp.guestUsers(idGts);
    }catch (error){
      Future.error(error);
    }
  }

  Future<List<GuestUserModel>> notificationActividadEmail(int idGts) async{
    try{
        return await _invitationsHttp.notificationActividadEmail(idGts);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<ApprovedUserActivityModel> userApprovedActivity(int idGts) async{
    try{
      return await _invitationsHttp.approvedUser(idGts);
    }catch (error){
      return Future.error(error);
    }
  }
}