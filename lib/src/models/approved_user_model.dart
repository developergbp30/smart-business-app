class ApprovedUserActivityModel {
  int ptAprobado;
  int idUserAprobado;
  String ptAprobadoFecha;
  DatosUsuario datosUsuario;

  ApprovedUserActivityModel(
      {this.ptAprobado,
        this.idUserAprobado,
        this.ptAprobadoFecha,
        this.datosUsuario});

  ApprovedUserActivityModel.fromJson(Map<String, dynamic> json) {
    ptAprobado = json['pt_aprobado'];
    idUserAprobado = json['id_user_aprobado'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    datosUsuario = json['datos_usuario'] != null
        ? new DatosUsuario.fromJson(json['datos_usuario'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pt_aprobado'] = this.ptAprobado;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    if (this.datosUsuario != null) {
      data['datos_usuario'] = this.datosUsuario.toJson();
    }
    return data;
  }
}

class DatosUsuario {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  DatosUsuario(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  DatosUsuario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}
