import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/contacts/contact_bloc.dart';
import 'package:smart_business/src/blocs/contacts/contact_event.dart';
import 'package:smart_business/src/blocs/contacts/contact_state.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/repositories/contacts/contacts_repository.dart';
class ContactsPage extends StatefulWidget {
  final int idContact;
  final String type;

  ContactsPage({this.idContact, this.type});

  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  ContactBloc contactBloc;
  List<dynamic> selectedContacts = [];
  List<ClientesContacto> selectedClientContacts = [];
  List<UserModel> selectedUsers = [];

  String avatarUrl = "https://cdn.pixabay.com/photo/2016/03/09/15/10/man-1246508_960_720.jpg";

  List<ContactModel> contactList;

  String tokenUser;

  @override
  void initState() {
    contactBloc = ContactBloc(contactsRepository: ContactsRepository());
    contactBloc.dispatch(LoadContactsEvent(idContact: widget.idContact, type: widget.type));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(icon: Icon(Icons.clear, color: Colors.black,), onPressed: (){
            Navigator.pop(context);
        }),
        title: _boxSearch(context),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: Colors.black,), onPressed: (){
            BackToPageContactsModel result;
            print("widget.type  ${widget.type}");
            if(widget.type == "contact-client"){
              result = new BackToPageContactsModel(clientContacts: selectedClientContacts);
            }
             if(widget.type == "users"){
               result = new BackToPageContactsModel(users: selectedUsers);
             }
            else if(widget.type == "default"){
              result = new BackToPageContactsModel(contacts: selectedContacts);
            }

            Navigator.pop(context, result);
          })
        ],
      ),
      body: BlocListener(
        bloc: contactBloc,
        listener: (context, state){

        },
        child: Container(
          child: Column(
            children: <Widget>[
              selectedContacts.length > 0 || selectedClientContacts.length > 0 || selectedUsers.length > 0?
              _listSelectedContacts(context) :
              Container(),
              Divider(),
              Expanded(
                child: _listContacts(context),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _boxSearch(BuildContext context){
    return Container(
      height: 50,
      margin: const EdgeInsets.only(bottom: 7, top: 7),
     // padding: const EdgeInsets.all(10.0),
      child: TextFormField(
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.grey,
                    width: 5.0
                ),
              borderRadius: BorderRadius.all(Radius.circular(10))
            ),
            hintText: 'Buscar contacto',
            hintStyle: TextStyle(fontStyle: FontStyle.italic),

          // labelText: 'Actividad'
        ),
        onChanged: (value) {
          if(contactBloc.currentState is LoadedUsersState){
            var list;
            if(contactBloc.currentState.props[2] != null){
              list =  contactBloc.currentState.props[2]; //previous
            }else{
              list =  contactBloc.currentState.props[0];
            }

            contactBloc.dispatch(SearchUserEvent(
              query: value,
              contacts: list,
            ));
          }

          if(contactBloc.currentState is LoadedClientContactState){
            var list;
            if(contactBloc.currentState.props[2] != null){
              list =  contactBloc.currentState.props[2]; //previous
            }else{
              list =  contactBloc.currentState.props[0];
            }

            contactBloc.dispatch(SearchClientContactsEvent(
              query: value,
              contacts: list,
            ));
          }

        },
      ),
    );
  }
  
  Widget _listSelectedContacts(BuildContext context){
    if(widget.type == "contact-client"){
      return Container(
        padding: const EdgeInsets.only(top: 10, left: 10,bottom: 5),
        height: 75,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: selectedClientContacts.length,
          itemBuilder: (context, i) => InkWell(
            onTap: (){

              if(contactBloc.currentState is LoadedClientContactState){
                contactBloc.dispatch(ToSelectClientContactEvent(
                    contacts: contactBloc.currentState.props[0],
                    id: selectedClientContacts[i].id,
                    selected: false
                ));
              }
              selectedClientContacts.removeWhere((item) => item.id == selectedClientContacts[i].id);
              setState(() {});
            },
            child: Container(
              margin: EdgeInsets.only(right: 10),
              child: new  Column(
                children: <Widget>[
                  Badge(
                    padding: EdgeInsets.all(0),
                    position: BadgePosition(bottom: 1, left: 30),
                    badgeColor: Colors.white,
                    badgeContent: Icon(Icons.cancel, color: Colors.grey.withOpacity(0.6),),
                    child: new CircleAvatar(
                        backgroundImage: loadImageApi(selectedClientContacts[i].contacto.foto, tokenUser)
                    ),
                  ),
                  Text(_splitName(selectedClientContacts[i].contacto.nombre))

                ],
              ),
            ),
          ),
        ),
      );
    }
    if(widget.type == "users"){
      return Container(
        padding: const EdgeInsets.only(top: 10, left: 10,bottom: 5),
        height: 75,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: selectedUsers.length,
          itemBuilder: (context, i) => InkWell(
            onTap: (){

              if(contactBloc.currentState is LoadedUsersState){
                contactBloc.dispatch(ToSelectUserEvent(
                  contacts: contactBloc.currentState.props[0],
                  id: selectedUsers[i].id,
                  selected: false
                ));
              }
              selectedUsers.removeWhere((item) => item.id == selectedUsers[i].id);
              setState(() {});
            },
            child: Container(
              margin: EdgeInsets.only(right: 10, left: 5),
              child: new  Column(
                children: <Widget>[
                  Badge(
                    padding: EdgeInsets.all(0),
                    position: BadgePosition(bottom: 1, left: 30),
                    badgeColor: Colors.white,
                    badgeContent: Icon(Icons.cancel, color: Colors.grey.withOpacity(0.6),),
                    child: new CircleAvatar(
                        backgroundImage: loadImageApi(selectedUsers[i].foto, tokenUser)
                    ),
                  ),
                  Text(_splitName(selectedUsers[i].nombre))

                ],
              ),
            ),
          ),
        ),
      );
    }
    return SizedBox();
  }
  
  String _splitName(String name){
   var sp =  name.split(" ");
   return sp[0];
  }
  
  Widget _listContacts(BuildContext context){
    return BlocBuilder(
      bloc: contactBloc,
      builder: (context, state){
        if(state is LoadedClientContactState){
          tokenUser = state.tokenUser;
          return  ListView.builder(itemBuilder: (context, i) => new ListTile(
            leading: state.clientContact[i].selected ?
            Badge(
              padding: EdgeInsets.all(0),
              position: BadgePosition(bottom: 1, left: 30),
              badgeColor: Colors.white,
              badgeContent: Icon(Icons.check_circle, color: Colors.green,),
              child: new CircleAvatar(
                  backgroundImage: loadImageApi(state.clientContact[i].contacto.foto, tokenUser)
              ),
            )
                :
            new CircleAvatar(
              backgroundImage: loadImageApi(state.clientContact[i].contacto.foto, state.tokenUser),
            ),
            title: Text(state.clientContact[i].contacto.nombre),
            subtitle: Text(""),
            onLongPress: (){

              if(state.clientContact[i].selected){
                state.clientContact[i].selected = false;
                selectedClientContacts.removeWhere((item) => item.idContacto == state.clientContact[i].idContacto);
                setState(() {});
              }else {
                state.clientContact[i].selected = true;
                selectedClientContacts.add(state.clientContact[i]);
                setState(() {});
              }

            },
          ),
            itemCount: state.clientContact.length,);
        }
        if(state is LoadedUsersState){
          return  ListView.builder(itemBuilder: (context, i) => new ListTile(
            leading: state.users[i].selected ?
            Badge(
              padding: EdgeInsets.all(0),
              position: BadgePosition(bottom: 1, left: 30),
              badgeColor: Colors.white,
              badgeContent: Icon(Icons.check_circle, color: Colors.green,),
              child: new CircleAvatar(
                  backgroundImage: loadImageApi(state.users[i].foto, tokenUser)
              ),
            )
                :
            new CircleAvatar(
              backgroundImage: loadImageApi(state.users[i].foto, state.tokenUser),
            ),
            title: Text(state.users[i].nombre),
            subtitle: Text(""),
            onLongPress: (){

              if(state.users[i].selected){
                state.users[i].selected = false;
                selectedUsers.removeWhere((item) => item.id == state.users[i].id);
                setState(() {});
              }else {
                state.users[i].selected = true;
                selectedUsers.add(state.users[i]);
                setState(() {});
              }

            },
          ),
            itemCount: state.users.length,);
        }
        return Container(child: Text("No hay datos"),);
      },
    );
  }
}
