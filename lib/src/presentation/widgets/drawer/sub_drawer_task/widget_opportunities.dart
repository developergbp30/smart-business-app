import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/blocs/task/task_state.dart';
import 'package:smart_business/src/models/for_views/data_screen_create_activity.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/presentation/screens/bitacora/create_bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/workplan/create_activity.dart';

import '../../widget_time_line_burble.dart';
class OpportunitiesWidgetList extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "Oportunidades",
          style: TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.bold),
        ),
        BlocBuilder<TaskBloc, TaskState>(builder: (context, state) {
          if (state is LoadedTaskOpsState) {
            return  Expanded(
              child: ListView.builder(
                  itemCount: state.task.length,
                  itemBuilder: (context, i) =>
                      _itemCard(context, state.task[i])),
            );
          }

          if (state is LoadedTaskOpsBinnacleState) {
            return  Expanded(
              child: ListView.builder(
                  itemCount: state.task.length,
                  itemBuilder: (context, i) =>
                      _itemCard(context, state.task[i], typeNav: 2)),
            );
          }

          if(state is SendingRequestTaskState){
            return Container(
              //color: Colors.red,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height/2,
                child: Center(child: CircularProgressIndicator(),));
          }
          return Container(
            height: 50,
            width: 50,
          );
        })
      ],
    );
  }

  String _typeOpportunity(state) {
    var title;
    switch (state) {
      case 1:
        title = 'Oportunidades Ganadas';
        break;
      case 2:
        title = 'Oportunidades Perdidas';
        break;
      default:
        title = 'Oportunidades Vigentes';
        break;
    }

    return title;
  }

  Widget _itemCard(BuildContext context, TaskOpportunityModel example, {typeNav = 1}) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: InkWell(
        onTap: () async {
          var values = new SetFormActivity(
              id: example.id,
              idClient: example.idCliente,
              title: example.titulo,
              description: '',
              typeActivity: _typeOpportunity(example.estadoFinal),
              idTypeActivity: example.id,
              idOpportunity: example.id,
              disabledModalAct: true,
              disabledModalDynamic: true);
          if(typeNav == 1){
            await Navigator.push(
                context,
                new MaterialPageRoute<bool>(
                    builder: (context) => CreateActivity(
                      setFormActivity: values,
                    )));
          }else {
            await Navigator.push(
                context,
                new MaterialPageRoute<bool>(
                    builder: (context) => CreateBitacoraPage(setFormActivity: values,)));
          }

        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                        //margin: const EdgeInsets.only(top: 10),
                        child: TimeLineWidget(
                      color: Colors.lightBlue,
                    )),
                    Flexible(
                      child: Container(
                        margin:
                            const EdgeInsets.only(left: 10, right: 10, top: 0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              example.clientes.nombreCorto,
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.flagCheckered,
                                      size: 15, color: Colors.grey[700]),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    example.precioTotal,
                                    style: TextStyle(color: Colors.grey[700]),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.dotCircle,
                                      size: 15, color: Colors.grey[700]),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    example.titulo,
                                    style: TextStyle(color: Colors.grey[700]),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
