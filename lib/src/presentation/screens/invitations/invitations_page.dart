import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/invitations/invitations_bloc.dart';
import 'package:smart_business/src/blocs/invitations/invitations_event.dart';
import 'package:smart_business/src/blocs/invitations/invitations_state.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/models/user_session_model.dart';
import 'package:smart_business/src/presentation/screens/invitations/detail_invitation_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/helper_color_gts.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/change_user_widget.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_left.dart';
import 'package:smart_business/src/presentation/widgets/modals/conventions.dart';
import 'package:smart_business/src/presentation/widgets/modals/modal_users.dart';
import 'package:smart_business/src/presentation/widgets/widget_time_line_burble.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
class InvitationsPage extends StatefulWidget {
  static const String routeName = 'invitations_page';
  @override
  _InvitationsPageState createState() => _InvitationsPageState();
}

class _InvitationsPageState extends State<InvitationsPage> {
  final Map<int, Widget> _menuSegment = const<int, Widget>{
    0: Padding(padding: const EdgeInsets.all(10),
    child: Text("Recibidas"),),
    1: Padding(padding: const EdgeInsets.all(10),
      child: Text("Enviadas"),),
  };
  ThemeApp _themeApp = ThemeApp();

  int theriGroupValue = 0;

  int userID = 0;

  InvitationsBloc _invitationsBloc;

  AuthRepository   _authRepository = AuthRepository();

  UserSessionModel userInSession;

  String tokenUser;

  @override
  void initState() {
     _userSession();
    _invitationsBloc = InvitationsBloc(invitationsRepository: InvitationsRepository());
    _invitationsBloc.dispatch(LoadInvitationsEvent(type: theriGroupValue, idUser: userID));

    super.initState();
  }

  _userSession() async {
    var user = await _authRepository.userData();
    var token = await _authRepository.getToken();
    setState(() {
      userInSession =  user;
      tokenUser = token;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: DrawerLeft(),
      ),
      appBar: AppBar(
        backgroundColor: _themeApp.colorPink,
        leading: Builder(builder: (context) => IconButton(icon: Icon(Icons.menu), onPressed: () => Scaffold.of(context).openDrawer()),),
        title: Text("Invitaciones"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(12),
            )
        ),
          actions: <Widget>[
            InkWell(
              child: AspectRatio(
                  aspectRatio: 1.4,
                  child: ChangeUserWidget()),
              onTap: (){
                _showUserModal(context);
              },
            )
          ]
      ),
      body: RefreshIndicator(
        child: BlocListener<InvitationsBloc, InvitationsState>(
          bloc: _invitationsBloc,
          listener: (context, state){

          },
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: CupertinoSegmentedControl(
                  groupValue: theriGroupValue,
                  children: _menuSegment,
                  borderColor: Colors.transparent,
                  selectedColor: _themeApp.colorPink,
                  unselectedColor: Colors.grey[300],
                  onValueChanged: (change) {
                    setState(() {
                      theriGroupValue=  change;
                    });

                    _invitationsBloc.dispatch(LoadInvitationsEvent(type: theriGroupValue, idUser: userID));
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width/2,
                    padding: const EdgeInsets.all(0),
                    child: FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.info_outline,color: Colors.black54),
                            Text(' Convenciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                         // _settingModalBottomSheetConventions(context);
                          modalConventions(context);
                        }),
                  ),
                ],
              ),

              BlocBuilder<InvitationsBloc, InvitationsState>(
                bloc: _invitationsBloc,
                builder: (context, state){
                  if(state is LoadedInvitationsState){
                    return  Expanded(child: ListView.builder(
                        itemCount: state.invitations.length,
                        itemBuilder: (context, i) => _itemCard(state.invitations[i], state.type)));
                  }
                  if(state is LoadingInvitationsState){
                    return CircularProgressIndicator();
                  }
                  else if(state is ErrorInvitationsState){
                    return Container(
                      child: Text(state.message, style: TextStyle(color: Colors.red),),
                    );
                  }
                  return Container(child: Text("Sin Resultados"),);
                },
              ),
            ],
          ),
        ),
        onRefresh: () async{
          await Future.delayed(Duration(seconds: 3));
          _invitationsBloc.dispatch(LoadInvitationsEvent(type: theriGroupValue, idUser: userID));
        },
      ),
    );
  }


  Widget _itemCard(InvitationReceivedModel invitation, int type) {
    Color _colorHelper =  helperColorGts(invitation.gtsTodos);
    return InkWell(
      child: Container(
        margin: const EdgeInsets.all(10),
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                      //margin: const EdgeInsets.only(top: 10),
                      child: TimeLineWidget(color: _colorHelper,)),
                  Flexible(
                    child: Container(
                      margin: const EdgeInsets.only(left: 10, right: 10, top: 0),
                      padding: const EdgeInsets.all(0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(child: Text("${invitation.gtsTodos.titulo}", style: TextStyle(color: _colorHelper, fontWeight: FontWeight.bold),)),
                              type == 2 && invitation.gtsTodos.ptAprobado == 1 ? Icon(Icons.check_circle, color: _themeApp.colorPink,) :
                              type == 2 && invitation.gtsTodos.ptAprobado == 2 ? Icon(Icons.cancel,color: Colors.grey.withOpacity(0.7))  :
                              type == 2 && invitation.gtsTodos.ptAprobado == null ?   Icon(FontAwesomeIcons.minusCircle, color: Colors.grey.withOpacity(0.7),) :
                                  Container()
                            ],
                          ),
                          Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Text("${invitation.gtsTodos.descripcion}", style: TextStyle(color: Colors.grey[700]),)),
                          Container(
                            margin: const EdgeInsets.only(top: 5),
                            child: Text(invitation.gtsTodos.gtsTipos.nombre, style: TextStyle(color: Colors.grey[700]),),
                          ),

                        ],
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Chip(
                    avatar: CircleAvatar(
                      backgroundImage: loadImageApi(invitation.datosUsuarios.foto, tokenUser),
                      // child: ,
                    ),
                    label:Text(invitation.datosUsuarios.nombre),
                  ),
                  Flexible(
                    child: Container(
                      margin: const EdgeInsets.only(left: 5),
                      child: Column(
                        children: <Widget>[
                          Text(DateFormat("dd MMMM yyyy").format(DateTime.parse(invitation.invitadoFecha).toLocal()),style: TextStyle(fontSize: 12)),
                          Text("${invitation.gtsTodos.ptHoraInicio} - ${invitation.gtsTodos.ptHoraFin}", style: TextStyle(fontSize: 12),)
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
        ),
          ),),
      ),
      onTap: () async {
        await Navigator.push(context, MaterialPageRoute(builder: (context) => InvitationDetailPage(args: invitation, typeInvitation: type, userInSession: userInSession,)));
      },
    );
  }

  selectUser(UserModel val){
    print("USUARIO SELECCIONADO");
    print(val.toJson());
    userID = val.id;
    _invitationsBloc.dispatch(LoadInvitationsEvent(type: theriGroupValue, idUser: userID));
    Navigator.of(context).pop();
  }

  _showUserModal(BuildContext context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return ModalUsersWidget(function: selectUser,);
        }
    );
  }

}
