
import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/login_request_model.dart';

abstract class AuthState extends Equatable{
  AuthState([List props = const []]) : super(props);
}

class UnAuthenticate extends AuthState{}

class StatusAuthState  extends AuthState {}

class Authenticated extends AuthState {
  final LoginUserModel user;
  Authenticated(this.user) : super([user]);

  @override
  String toString() => '$runtimeType {${user.token} }';
}

class SuccessVerificationCodeCompany extends AuthState {
  final SessionCompanyModel companyModel;
  SuccessVerificationCodeCompany(this.companyModel) : super([companyModel]);

  @override
  String toString() => '$runtimeType {${companyModel.idEmpresa} : ${companyModel.toJson()} }';
}
class SendingCodeVerifyCompanyState extends AuthState{}

class SendingLoginState extends AuthState{}

class ErrorCompanyState extends AuthState {
  final String status;
  final String message;
  ErrorCompanyState({this.status, this.message}) : super([status, message]);

  @override
  String toString() => '$runtimeType {$status  : $message} }';
}

class ErrorLoginState extends AuthState {
  final String status;
  final String message;
  ErrorLoginState({this.status, this.message}) : super([status, message]);

  @override
  String toString() => '$runtimeType {$status  : $message} }';
}

class CloseSessionState extends AuthState{}
