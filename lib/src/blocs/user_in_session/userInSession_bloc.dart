import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/user_in_session/userInSession_event.dart';
import 'package:smart_business/src/blocs/user_in_session/userInSession_state.dart';
import 'package:meta/meta.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';

class UserInSessionBloc extends Bloc<UserInSessionEvent, UserInSessionState>{
  final AuthRepository _authRepository;
  UserInSessionBloc({@required AuthRepository authRepository}) : assert(authRepository != null), _authRepository = authRepository;
  @override
  UserInSessionState get initialState => UnloadedDataUserInSession();

  @override
  Stream<UserInSessionState> mapEventToState(UserInSessionEvent event) async* {
   if(event is LoadDataUserInSessionEvent){
    yield* _mapLoadDataToState();
   }
  }

  Stream<UserInSessionState> _mapLoadDataToState() async*{
    try{
      final res =  await _authRepository.userData();
      var token =  await _authRepository.getToken();

      yield LoadedDataUserInSession(res, token: token);
    }catch (error){
      print(error);
    }
  }
}