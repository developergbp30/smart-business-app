
import 'package:equatable/equatable.dart';

abstract class GtsInvitationEvent extends Equatable {
  GtsInvitationEvent([List props = const []]) : super(props);
}

class LoadGtsGuestUsersEvent extends GtsInvitationEvent {
  final int idGts;
  LoadGtsGuestUsersEvent(this.idGts) : super([idGts]);

  @override
  String toString() => '$runtimeType { idGts : $idGts }';
}