import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/client_model.dart';
import 'package:smart_business/src/models/invitations.dart' as px;

class ClientsHttp extends ApiProvider {

  Future<List<ClientModel>> getClients() async {
    try{
      Response response = await dio.get("/cliente/lista", options: Options(headers: {"requirestoken": true}));
      return (response.data["clientes"] as List)
          .map((json) => new ClientModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future<dynamic> changeClient(int id) async {
    try{
      Response response = await dio.get("/cliente/actualizar-cliente/$id", options: Options(headers: {"requirestoken": true}));
      return response.statusCode;
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseClientModel>> listClientUser(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Clientes/$idUser/Action-Load-Select-Dinamic", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseClientModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<px.ClientesContacto>> contacts(int idClient) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Clientes-Contactos/$idClient/Action-Load-Clientes-Contactos", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new px.ClientesContacto.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }
}