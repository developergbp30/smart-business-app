import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora_detail/bitacora_detail_bloc.dart';
import 'package:smart_business/src/blocs/bitacora_detail/bitacora_detail_event.dart';
import 'package:smart_business/src/blocs/bitacora_detail/bitacora_detail_state.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_invitation_bloc.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/presentation/screens/bitacora/segment_detail/segment_detail.dart';
import 'package:smart_business/src/presentation/screens/bitacora/segment_detail/segment_expenses.dart';
import 'package:smart_business/src/presentation/screens/bitacora/segment_detail/segment_pending.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
class DetailBitacoraPage extends StatefulWidget {
  static const String routeName = 'detail_bitacora_page';
  ResponseBitacoraModel args;
  DetailBitacoraPage({@required this.args});
  @override
  _DetailBitacoraPageState createState() => _DetailBitacoraPageState();
}

class _DetailBitacoraPageState extends State<DetailBitacoraPage> {
  final Map<int, Widget> _menuSegment = const<int, Widget>{
    0: Padding(padding: const EdgeInsets.all(10),
      child: Text("Detalles"),),
    1: Padding(padding: const EdgeInsets.all(10),
      child: Text("Pendientes"),),
    2: Padding(padding: const EdgeInsets.all(10),
      child: Text("Gastos"),)
  };
  ThemeApp _themeApp = ThemeApp();
  int theriGroupValue = 0;
  BitacoraDetailBloc _bitacoraDetailBloc;
  @override
  void initState() {
    _bitacoraDetailBloc = BitacoraDetailBloc(bitacoraRepository: BitacoraRepository());
    _bitacoraDetailBloc.dispatch(FindDetailBitacoraEvent(id: widget.args.id));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(1.0),
      appBar:  AppBar(
        iconTheme: IconThemeData(
          color: _themeApp.colorPink, //change your color here
        ),
        leading: IconButton(icon:Icon( Icons.arrow_back_ios),
          onPressed: (){
            Navigator.pop(context);
          },),
        backgroundColor: Colors.grey[100],
        title: Text("Bitácora", style: TextStyle(color: _themeApp.colorPink),),),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(top: 20, bottom: 20),
              child: CupertinoSegmentedControl(
                groupValue: theriGroupValue,
                children: _menuSegment,
                borderColor: Colors.transparent,
                selectedColor: _themeApp.colorPink,
                unselectedColor: Colors.grey[300],
                onValueChanged: (change) {
                  setState(() {
                    theriGroupValue=  change;
                  });
                },
              ),
            ),
            Expanded(child: _builderSegmentPage())
          ],
        ),
      ),
    );
  }
  _builderSegmentPage(){
    if(theriGroupValue == 0){
    return  BlocBuilder<BitacoraDetailBloc, BitacoraDetailState>(
        bloc: _bitacoraDetailBloc,
        builder: (context, state){
          if(state is DetailBitacoraState){
            //return Text("${state.detail.toJson()}");
            return MultiBlocProvider(
              providers: [
                BlocProvider<BitacoraBloc>(builder: (context) => BitacoraBloc(bitacoraRepository: BitacoraRepository()),),
                BlocProvider<GtsInvitationBloc>(builder: (context) => GtsInvitationBloc(invitationsRepository: InvitationsRepository()),)
              ],
                child: SegmentDetail(args: state.detail, token: state.token,));
          }

          return Container(child: Center(child: CircularProgressIndicator()),);
        },
      );

    }
    else if(theriGroupValue == 1){
      return  BlocBuilder<BitacoraDetailBloc, BitacoraDetailState>(
        bloc: _bitacoraDetailBloc,
        builder: (context, state){
          if(state is DetailBitacoraState){
            return BlocProvider(
                builder: (context) => GtsInvitationBloc(invitationsRepository: InvitationsRepository()),
                child: SegmentPending(args: state.pending, idBitacora: widget.args.id,token: state.token,));
          }

          return Container(child: Center(child: CircularProgressIndicator()),);
        },
      );

    }
    if(theriGroupValue == 2){
      return  BlocBuilder<BitacoraDetailBloc, BitacoraDetailState>(
        bloc: _bitacoraDetailBloc,
        builder: (context, state){
          if(state is DetailBitacoraState){
            return BlocProvider(
                builder: (context) => GtsInvitationBloc(invitationsRepository: InvitationsRepository()),
                child: SegmentExpenses(args: state.expenses, balance: state.balanceExpenses,  idBitacora: widget.args.id,token: state.token,));
          }

          return Container(child: Center(child: CircularProgressIndicator()),);
        },
      );
    }
  }
}
