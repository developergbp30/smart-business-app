import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_business/src/api/services/auth_http_service.dart';
import 'package:smart_business/src/database/database_helpers.dart';
import 'package:smart_business/src/models/login_request_model.dart';
import 'package:smart_business/src/models/user_session_model.dart';

class AuthRepository {
   AuthHttp _authHttp = AuthHttp();
   DatabaseHelper _databaseHelper = DatabaseHelper.instance;
   UserSessionModel _userSessionModel;


   Future<SessionCompanyModel> checkCodeCompany(String code) async {
     try{
       var res = await _authHttp.checkCodeCompany(code);
       await setCompanyID(code/*res.idEmpresa.toString()*/);
       return res;
     }catch (error){
       print(error);
       return Future.error(error);
     }
   }

  Future<LoginUserModel> login(LoginRequestModel data) async {
    try {
      var res = await _authHttp.loginApi(data);
      res.username = data.username;
      await setToken(res.token);
      saveUserDB(res);
      return res;
    }catch (error){
      return Future.error(error);
    }
  }

  Future<UserSessionModel> userData() async {
     if(_userSessionModel == null){
       try{
         _userSessionModel =  await _authHttp.userData();
         return _userSessionModel;
       }catch (error){
         return Future.error(error);
       }
     }else{
       return _userSessionModel;
     }

  }

  Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.get("token");
  }



  Future<void> setToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("token", token);
  }
  // return code id of company
   Future<String> getCompanyID()  async {
     final prefs = await SharedPreferences.getInstance();
     return prefs.get("codeCompany");
   }
   // save code company id
  Future<void> setCompanyID(String code)  async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("codeCompany", code);
  }

  void saveUserDB(LoginUserModel user) async {
    await _databaseHelper.rawInsertOrReplace("User",
        "Username, Token, Status, ExpiresIn",
        [user.username, user.token, 1, user.expiresIn]);
  }

  Future<LoginUserModel> currentSessionDB() async {
    final res = await _databaseHelper.selectQuery("User",
        [],
        "Status = ?", [1]);
    if(res != null){
      print(res);
      final data =  res[0];
      return new LoginUserModel(
        username: data["Username"],
        token: data["Token"],
       // expiresIn: data["ExpiresIn"]
      );
    }
    return null;
  }

  void updateStatusSession(String username) async {
    Map<String, dynamic> value = {"Status": 0};
    await _databaseHelper.update("User",
        value, "Username = ? AND Status = ?", [username, 1]);
  }

  Future<void> clearSession() async{
     var userSession = await currentSessionDB();
     updateStatusSession(userSession.username);
     final  prefs = await SharedPreferences.getInstance();
     await prefs.remove("token");
     await prefs.remove("codeCompany");
  }

}