import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/presentation/screens/clients/admin_social%20_network.dart';
import 'package:smart_business/src/presentation/screens/clients/history_state_page.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/campus_segment/create_edit_campus.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/contacts_segment.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/headquarters_segment.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/information_general_segment.dart';

import 'create_edit_client_page.dart';

class Menu {
  int id;
  String title;
  IconData icon;
  Menu({this.id, this.title, this.icon});
}



class DetailClientPage extends StatefulWidget {
  @override
  _DetailClientPageState createState() => _DetailClientPageState();
}

class _DetailClientPageState extends State<DetailClientPage> {

  List<bool> _focusInput = List(9);

  ScrollController _scrollController;
  double _scrollPosition;

  double heightScreen;

  Color focusButtonColor =  Color(0xFF11BA19);

   List<Menu> choices =  <Menu>[
    new Menu(id: 0, title: 'Editar', icon: Icons.directions_car),
    new Menu(id: 1, title: 'Eliminar', icon: Icons.directions_bike),
    new Menu(id: 2, title: 'Administrar redes', icon: Icons.directions_boat),
    new Menu(id: 3, title: 'Historial de estado', icon: Icons.directions_bus),
  ];

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });

    //print("$_scrollPosition  | $heightScreen");
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    focusInput(0);
    super.initState();
  }

  void _select(Menu choice) async {
    // Causes the app to rebuild with the new _selectedChoice.
    switch(choice.id){
      case 0:
         await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateClientPage()));
        break;
      case 2:
        await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => AdminSocialNetworkPage()));
        break;
      case 3:
        await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => HistoryStatePage()));
        break;

    }
    setState(() {
    //  _selectedChoice = choice;
    });
  }

  @override
  Widget build(BuildContext context) {
    heightScreen = MediaQuery.of(context).size.height/3;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),

        title: Text("Apple", style: TextStyle(color: Colors.green),),
        actions: <Widget>[
          PopupMenuButton<Menu>(
            icon: Icon(Icons.more_vert, color: Colors.green,),
            onSelected: _select,
            itemBuilder: (BuildContext context) {
              return choices.map((Menu choice) {
                return PopupMenuItem<Menu>(
                  value: choice,
                  child: Text(choice.title),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          _scrollPosition!= null && _scrollPosition > heightScreen ? _horizontalMenu() : SizedBox(height: 0,),
          Expanded(
            child: ListView(
              controller: _scrollController,
              children: <Widget>[
                _containerImages(),
                SizedBox(height: 5,),
                _itemStatus(),
                _horizontalMenu(),
                _switchSegment(),
                Container(
                  height: 300,
                ),
                Container(
                  height: 300,
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: _switchFloatingActionButton(),
    );
  }

  Widget _containerImages(){
    return Container(

      //color: Colors.red,
      height: 110,
      margin: EdgeInsets.only(top: 20, bottom: 20),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          _imageCoverPage(),
          Positioned(
              top: 20,
              child: _imageProfile()),
        ],
      ),
    );
  }

  Widget _imageCoverPage({double heightContainer = 70}){
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          height: heightContainer,
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.2),
              image: DecorationImage(
                image:  new NetworkImage("https://cdn.pixabay.com/photo/2015/06/25/17/21/smart-watch-821557_960_720.jpg"),
                fit: BoxFit.cover,
              ),
            )
        ),
        Positioned(
          right: 20,
            bottom: -50,
            child: _socialNetwork())
      ],
    );
  }

  Widget _socialNetwork(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Badge(
          badgeColor: Colors.green,
          badgeContent: Icon(FontAwesomeIcons.twitter, color: Colors.white,size: 15,),),
        SizedBox(width: 5,),
        Badge(
          badgeColor: Colors.green,
          badgeContent: Icon(FontAwesomeIcons.instagram,color: Colors.white,size: 15),),
        SizedBox(width: 5,),
        Badge(
          badgeColor: Colors.green,
          badgeContent: Icon(FontAwesomeIcons.linkedin, color: Colors.white,size: 15),),
        SizedBox(width: 5,),
        Badge(
          badgeColor: Colors.green,
          badgeContent: Icon(FontAwesomeIcons.twitch, color: Colors.white,size: 15),),
        SizedBox(width: 5,),
        Badge(
          badgeColor: Colors.green,
          badgeContent: Icon(FontAwesomeIcons.facebook, color: Colors.white,size: 15),),
      ],
    );
  }

  Widget _imageProfile(){
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          width: 80,
          height: 80,
          child: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: NetworkImage(
              'https://image.freepik.com/iconos-gratis/mac-os_318-10374.jpg',
            ),
          ),
        ),
      ],
    );
  }

 Widget _itemStatus(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        _itemBox(color: Color(0xFFFF9800), label: "2/95"),
        _itemBox(color: Color(0xFF6FC98A)),
        _itemBox(
            icon: FontAwesomeIcons.chartBar,
            color:Color(0xFFFF9191), label: "Gold")
      ],
    );
  }

  Widget _itemBox({Color color = Colors.green, IconData icon: Icons.check_box, label = "Productivo"}){
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: color)
      ),
      child: Row(
        children: <Widget>[
          Container(
            color: color,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Icon(icon, color: Colors.white,),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Text(label),
          )
        ],
      ),
    );
  }


  Widget _horizontalMenu(){
    return Container(
      height: 50,
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Color(0xFFDBDBDB),
        borderRadius: BorderRadius.all(Radius.circular(10.0))
      ),
      child: ListView(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: _focusInput[0] ? focusButtonColor : Colors.transparent,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), bottomLeft: Radius.circular(10.0))
            ),
            child: IconButton(icon: Icon(Icons.info, color: _focusInput[0] ? Colors.white : Color(0xFF5D5D5D),), onPressed: (){
              focusInput(0);
            }),
          ),
          Container(
            color: _focusInput[1] ? focusButtonColor : Colors.transparent,
            child: IconButton(icon: Icon(FontAwesomeIcons.building, color: _focusInput[1] ? Colors.white : Color(0xFF5D5D5D),), onPressed: (){
              focusInput(1);
            }),
          ),
          Container(
            color: _focusInput[2] ? focusButtonColor : Colors.transparent,
            child: IconButton(icon: Icon(FontAwesomeIcons.solidUser,color: _focusInput[2] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(2);
            }),
          ),
          Container(
            color: _focusInput[3] ? focusButtonColor : Colors.transparent,
            child: IconButton(icon: Icon(FontAwesomeIcons.calendar,color: _focusInput[3] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(3);
            }),
          ),
          Container(
            color: _focusInput[4] ? focusButtonColor : Colors.transparent,
            child: IconButton(icon: Icon(FontAwesomeIcons.image,color: _focusInput[4] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(4);
            }),
          ),
          Container(
            color: _focusInput[5] ? focusButtonColor : Colors.transparent,
            child: IconButton(icon: Icon(FontAwesomeIcons.chartBar,color: _focusInput[5] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(5);
            }),
          ),
          Container(
            color: _focusInput[6] ? focusButtonColor : Colors.transparent,
            child: IconButton(
                icon: Icon(FontAwesomeIcons.chessKing,color: _focusInput[6] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(6);
            }),
          ),
          Container(
            color: _focusInput[7] ? focusButtonColor : Colors.transparent,
            child: IconButton(icon: Icon(Icons.info,color: _focusInput[7] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(7);
            }),
          ),
          Container(
            decoration: BoxDecoration(
                color: _focusInput[8] ? focusButtonColor : Colors.transparent,
                borderRadius: BorderRadius.only(topRight: Radius.circular(10.0), bottomRight: Radius.circular(10.0))
            ),
            child: IconButton(icon: Icon(Icons.info,color: _focusInput[8] ? Colors.white : Color(0xFF5D5D5D)), onPressed: (){
              focusInput(8);
            }),
          ),
        ],
      ),
    );
  }

  focusInput(int index){
    print(index);
    for(var i =0; i< 9; i++) {
      if(i == index) _focusInput[i] = true;
      else _focusInput[i] = false;
    }
    setState(() {});
  }

  Widget _switchSegment(){
    if(_focusInput[0])  return InformationGeneralSegment();
    if(_focusInput[1])  return HeadQuartersSegment();
    if(_focusInput[2])  return ContactsSegment();

    return SizedBox();
  }

  Widget _switchFloatingActionButton(){
    if(_focusInput[1]) return FloatingActionButton(
      backgroundColor: focusButtonColor,
      onPressed:() async {
        var res =  await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateCampusPage()));

      },
      tooltip: 'Crear',
      child: Icon(Icons.add),
    );
    return null;
  }
}
