import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart' as pdfLibrary;
import 'package:pdf/widgets.dart' as pdfLibraryWidget;
import 'package:permission_handler/permission_handler.dart';

class PdfPage extends StatefulWidget {
  static const String routeName = 'pdf_page';
  @override
  _PdfPageState createState() => _PdfPageState();
}

class _PdfPageState extends State<PdfPage> {
  pdfLibraryWidget.Document pdf  = pdfLibraryWidget.Document();

  File imageFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Crear Pdf"),),
      body: Container(
        child: Column(
          children: <Widget>[
            FlatButton(
              onPressed: _generateDocument,
              child: Text("Generar PDF"),
            ),
            FlatButton(
              onPressed: _cropImage,
              child: Text("Editar imagen"),
            ),
            FlatButton(
              onPressed: _pickImage,
              child: Text("Subir imagen"),
            ),
          ],
        ),
      ),
    );
  }

  void _generateDocument() async {
    print("Genrando PDF");
    pdf.addPage(pdfLibraryWidget.Page(
        pageFormat: pdfLibrary.PdfPageFormat.a4,
        build: (pdfLibraryWidget.Context context) {
          return pdfLibraryWidget.Center(
            child: pdfLibraryWidget.Text("Hello World"),
          ); // Center
        }));
    
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);

    if(permission.value == 2){
      String dir = ( await getExternalStorageDirectory()).absolute.path+"/";
      final file = File(dir+"example.pdf");
      await file.writeAsBytes(pdf.save());
    }

  }

  Future<Null> _pickImage() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      setState(() {

      });
    }
  }


  _cropImage() async {
    //String dir = ( await getExternalStorageDirectory()).absolute.path+"/example.jpg";
    String dir = "/storage/emulated/0/ing.jpg";
    print("paht: $dir");
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        )
    );
  }
}
