import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_event.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_state.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_invitation_bloc.dart';
import 'package:smart_business/src/blocs/invitations/invitations_bloc.dart';
import 'package:smart_business/src/blocs/invitations/invitations_event.dart';
import 'package:smart_business/src/blocs/invitations/invitations_state.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_session_model.dart';
import 'package:smart_business/src/presentation/screens/comments/comments_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
class InvitationDetailPage extends StatefulWidget {
  final InvitationReceivedModel args;
  final int typeInvitation;
  UserSessionModel userInSession;
  InvitationDetailPage({this.args, this.typeInvitation, this.userInSession});
  @override
  _InvitationDetailPageState createState() => _InvitationDetailPageState();
}

class _InvitationDetailPageState extends State<InvitationDetailPage> {
  ThemeApp _themeApp = ThemeApp();
  InvitationsBloc  _invitationsBloc;
  GtsInvitationBloc _gtsInvitationBloc;

  String tokenUser;

  @override
  void initState() {
    getToken();
    _invitationsBloc =  InvitationsBloc(invitationsRepository: InvitationsRepository());
    _gtsInvitationBloc = GtsInvitationBloc(invitationsRepository: InvitationsRepository());
    _gtsInvitationBloc.dispatch(LoadGtsGuestUsersEvent(widget.args.idGts));
    super.initState();
  }

   getToken() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      tokenUser =  prefs.get("token");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:  AppBar(
        iconTheme: IconThemeData(
          color: _themeApp.colorPink, //change your color here
        ),
        leading: IconButton(icon:Icon( Icons.arrow_back_ios),
          onPressed: (){
            Navigator.pop(context);
          },),
        backgroundColor: Colors.white,
        title: Text("Detalle de la invitación", style: TextStyle(color: _themeApp.colorPink),),),
      body: BlocListener(
        bloc: _invitationsBloc,
        listener: (context, state){
          if(state is SuccessApprovedInvitationsState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Invitacion Aprobada",style: TextStyle(color: Colors.black),), backgroundColor: Colors.green,));
          }

          if(state is SuccessRejectInvitationsState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Invitacion Rechazada",style: TextStyle(color: Colors.black),), backgroundColor: Colors.green,));
          }

          if(state is SuccessCancelInvitationsState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Invitacion Cancelada",style: TextStyle(color: Colors.black),), backgroundColor: Colors.green,));
          }

          if(state is ErrorInvitationsState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("a corrido un error",style: TextStyle(color: Colors.black),), backgroundColor: Colors.red,));
          }
        },
        child: BlocBuilder<InvitationsBloc, InvitationsState>(
          bloc: _invitationsBloc,
          builder: (context, state) {
            return Container(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    _cardInfo(),
                    ListTile(
                      title: RichText(text: TextSpan(
                          text: "Tipo: ",
                          style: TextStyle(color: Colors.black),
                          children: <TextSpan>[
                            TextSpan(
                              text: widget.args.tipoPadre,
                              style: TextStyle(color: Colors.grey[600]),)]),),

                      subtitle: Padding(padding: const EdgeInsets.only(left: 30),
                        child: Text(widget.args.gtsTodos.miPqr != null ? widget.args.gtsTodos.miPqr.titulo : "" , textAlign: TextAlign.left,),),
                    ),
                    ListTile(
                      title: RichText(text: TextSpan(
                          text: "Subtipo: ",
                          style: TextStyle(color: Colors.black),
                          children: <TextSpan>[
                            TextSpan(
                              text: widget.args.gtsTodos.gtsTipos.nombre,
                              style: TextStyle(color: Colors.grey[600]),
                            )
                          ]
                      ),),
                    ),
                    // < Section Client -->
                    widget.args.gtsTodos.cliente != null ? ListTile(
                      leading:  new CircleAvatar(
                        backgroundImage: loadImageApi(widget.args.gtsTodos.cliente.logo, tokenUser),
                      ),
                      title: Text("${widget.args.gtsTodos.cliente.nombreCorto}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
                      subtitle: Text("Cliente", style: TextStyle(fontStyle: FontStyle.italic),),
                    ) : Container(),
                    // < finish Section Client -->
                    ListTile(
                      title: Text("Descripción", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
                      subtitle: Text(widget.args.gtsTodos.descripcion, style: TextStyle(fontStyle: FontStyle.italic),),
                    ),
                    Divider(),
                    // Row Contacts
                    Center(
                      child: Badge(
                          elevation: 0,
                          badgeColor: Colors.grey[200],
                          shape: BadgeShape.square,
                          borderRadius: 20,
                          toAnimate: false,
                          badgeContent:
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.person,color: Colors.black54),
                              Text(' Contactos', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                            ],)
                      ),
                    ),
                    // <------- list of contacts --->
                    widget.args.gtsTodos.gtsContactos != null && widget.args.gtsTodos.gtsContactos.length > 0
                        ? _renderListContacts(widget.args.gtsTodos.gtsContactos): SizedBox(height: 20,),
                    // Row invitations
                    Center(
                      child: Badge(
                          elevation: 0,
                          badgeColor: Colors.grey[200],
                          shape: BadgeShape.square,
                          borderRadius: 20,
                          toAnimate: false,
                          badgeContent:
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.email,color: Colors.black54),
                              Text(' Invitaciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                            ],)
                      ),
                    ),
                    // <------- list of invitations --->
                     _guestUsers(context),
                    // Row Notification activity
                    Center(
                      child: Badge(
                          elevation: 0,
                          badgeColor: Colors.grey[200],
                          shape: BadgeShape.square,
                          borderRadius: 20,
                          toAnimate: false,
                          badgeContent:
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.email,color: Colors.black54),
                              Text('! Notificación de actividad', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                            ],)
                      ),
                    ),
                    // <------- list of Notification activity --->
                    _notificationEmail(context),
                    _activityApproved(),
                  ],
                ));
          }),
      ),
        bottomNavigationBar: _bottomNavigationBar()
    );
  }

  Widget _bottomNavigationBar(){
    if(widget.typeInvitation == 1){// INVITACIONES RECIBIDAS
      return  widget.args.idUserRecibio == widget.userInSession.id ? BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 1),
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.typeInvitation == 1 ? <Widget>[
              InkWell(
                onTap: () async {
                  _ModalReject();
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.ban),
                    Text("Rechazar")
                  ],
                ),
              ),
              SizedBox(width: 20,),
              InkWell(
                onTap: () async {
                  _ModalApproved();
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.checkCircle),
                    Text("Aprobar")
                  ],
                ),
              ),
            ] : <Widget>[
              InkWell(
                onTap: () async {
                  _ModalCancel();
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.ban),
                    Text("Cancelar")
                  ],
                ),
              ),
            ],
          ),
        ),
      ) : null;
    } else if(widget.typeInvitation == 2){// INVITACIONES ENVIADAS

      if(widget.args.respuesta != null) return null; // SI YA SE AHA RESPONDIDO LA INVITACION NO SE PUEDE CANCELAR

      return  widget.args.idUserEnvio == widget.userInSession.id ? BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 1),
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children:  <Widget>[
              InkWell(
                onTap: () async {
                  _ModalCancel();
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.ban),
                    Text("Cancelar")
                  ],
                ),
              ),
            ],
          ),
        ),
      ) : null;
    }
     return null;
    }

  Widget _cardInfo() {
    return Container(
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(10.0),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomLeft,
            stops: [0.3, 1],
            colors: [Color(0xCC079CE5),  Color(0xff079CE5)],


          )
      ),
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.only(top: 8, left: 10, right: 10, bottom: 8),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Flexible(child: Text(widget.args.gtsTodos.titulo, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),))
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text("${DateFormat("dd MMMM yyyy").format(DateTime.parse(widget.args.invitadoFecha).toLocal()) }, "
                  "${widget.args.gtsTodos.ptHoraInicio} - ${widget.args.gtsTodos.ptHoraFin}", style: TextStyle(color: Colors.white))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              /*Badge(
                badgeContent: Text('3', style: TextStyle(color: Colors.white)),
                child: Icon(FontAwesomeIcons.clock, color: Colors.white),
              ),
              SizedBox(
                width: 20,
              ),*/
              InkWell(
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context) => CommentsPage(idGts: widget.args.idGts,)));
                },
                child: Badge(
                  badgeContent: Text('9', style: TextStyle(color: Colors.white),),
                  child: Icon(Icons.message, color: Colors.white),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _renderListContacts(List<GtsContactsModel> contact) {
      return ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: contact.length,
          itemBuilder: (context, i) => ListTile(
            leading:  new CircleAvatar(
              backgroundImage: loadImageApi(contact[i].clientesContacto.contacto.foto, tokenUser)
            ),
            title: Text("${contact[i].clientesContacto.contacto.nombreCorto}"),
          ));
  }

  Widget _guestUsers(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.users.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                   position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.white,
                  badgeContent: _iconStatusGuestUser(state.users[i].respuesta),
                  child: new CircleAvatar(
                      backgroundImage: loadImageApi(state.users[i].datosInvitados.foto, tokenUser)
                  ),
                ),
                title: Text("${state.users[i].datosInvitados.nombre}"),
              ));
        }
        return SizedBox(height: 20,);
      },
    );
  }

  Widget _notificationEmail(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.email.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                   position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.transparent,
                  badgeContent: _iconStatusGuestUser(state.email[i].respuesta),
                  child:  state.email[i].datosInvitados == null ?
                    new CircleAvatar(
                    child: Icon(FontAwesomeIcons.envelope, color: Colors.grey,),
                    backgroundColor: Colors.grey.withOpacity(0.2))
                  : new CircleAvatar(
                      backgroundImage: loadImageApi(state.email[i].datosInvitados.foto, tokenUser)
                  ) ,
                ),
                title: Text("${state.email[i].datosInvitados != null ? state.email[i].datosInvitados.nombre : state.email[i].correoExterno}"),
                subtitle: state.email[i].datosInvitados == null ? Text("correo electronico externo") : Text(state.email[i].datosInvitados.cargo),
              ));
        }
        return Container();
      },
    );
  }

  Widget _activityApproved(){
   return BlocBuilder(
       bloc: _gtsInvitationBloc,
       builder: (context, state){
     if(state is LoadedGtsGuestUsers){
       if(state.userApproved.datosUsuario != null){
         return Container(
           padding: const EdgeInsets.all(8),
           margin: const EdgeInsets.all(10),
           decoration: new BoxDecoration(
               color: Colors.grey[250],
               border: Border.all(color: Colors.grey[350], width: 1),
               borderRadius: new BorderRadius.circular(5.0)
           ),
           child: Column(
             children: <Widget>[
               Container(
                   width: MediaQuery.of(context).size.width,
                   child: Text("Actividad aprobada", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
               Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Chip(
                     avatar: CircleAvatar(
                       backgroundImage: loadImageApi(state.userApproved.datosUsuario.foto, tokenUser),
                       // child: ,
                     ),
                     label:Text(state.userApproved.datosUsuario.nombre),
                   ),
                   Row(
                     children: <Widget>[
                       Icon(Icons.calendar_today, color: Colors.green,),
                       Text("${DateFormat("dd MMMM yyyy").format(DateTime.parse(state.userApproved.ptAprobadoFecha).toLocal())}", style: TextStyle(color: Colors.grey[600]),)
                     ],
                   )
                 ],
               )
             ],
           ),
         );
       }
       return Container(
         padding: const EdgeInsets.all(8),
         margin: const EdgeInsets.all(10),
         decoration: new BoxDecoration(
             color: Colors.grey[250],
             border: Border.all(color: Colors.grey[350], width: 1),
             borderRadius: new BorderRadius.circular(5.0)
         ),
         child: Column(
           children: <Widget>[
             Container(
                 width: MediaQuery.of(context).size.width,
                 child: Text("Actividad pendiente por aprobación", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: <Widget>[],
             )
           ],
         ),
       );
     }
     return Container();
   });
  }

  Widget _ModalApproved(){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            height: 200,
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(20),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Aceptar invitación", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  padding: EdgeInsets.all(8.0),
                  child: Text("¿Estas seguro que quiere  aceptar esta invitación?"),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                           var data = ApprovedInvitationModel(idInvitation:  widget.args.id,
                           date: DateFormat("yyyy-MM-dd").format(DateTime.now()),
                           response:1
                           );
                          _invitationsBloc.dispatch(ApprovedInvitationsEvent(data));
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }

  Widget _ModalReject(){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            height: 200,
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(20),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Rechazar invitación", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  padding: EdgeInsets.all(8.0),
                  child: Text("¿Estas seguro que quiere  rechazar esta invitación?"),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {

                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          var data = ApprovedInvitationModel(idInvitation:  widget.args.id,
                              date: DateFormat("yyyy-MM-dd").format(DateTime.now()),
                              response:2
                          );
                          _invitationsBloc.dispatch(RejectInvitationsEvent(data));
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }

  Widget _ModalCancel(){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            height: 200,
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(20),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Cancelar invitación", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  padding: EdgeInsets.all(8.0),
                  child: widget.args.respuesta == null ? Text("¿Estas seguro que quiere  cancelar esta invitación?") :
                  Text("Ya no se puede cancelar", style: TextStyle(color: Colors.red),)
                  ,
                ),

                widget.args.respuesta == null ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {

                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          var data = ApprovedInvitationModel(idInvitation:  widget.args.id,
                              date: DateFormat("yyyy-MM-dd").format(DateTime.now()),
                              response:3
                          );
                          _invitationsBloc.dispatch(CancelInvitationsEvent(data));
                          Navigator.of(context).pop();
                        })
                  ],
                )
                    : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {

                          Navigator.pop(context);
                        }),
                  ],
                ),
              ],
            ),
          );
        }
    );
  }

  Widget _iconStatusGuestUser(var value){
    if(value == null){
      return Container();
    }
    else if(value == 1) return Icon(Icons.check_circle, color: _themeApp.colorPink,);
    if(value == 2 || value == 3 ) return Icon(Icons.cancel, color: Colors.grey.withOpacity(0.6),);

    return Container();
  }

}
