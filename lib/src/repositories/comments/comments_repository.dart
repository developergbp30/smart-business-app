import 'package:smart_business/src/api/services/comments_http_service.dart';
import 'package:smart_business/src/models/comments_model.dart';

class CommentsRepository {
  CommentHttp _commentHttp = CommentHttp();

  Future<List<CommentModel>> getComments(int idGts) async {
    try{
      return  await _commentHttp.getComments(idGts);
    }catch (error){
      Future.error(error);
    }
  }

  Future<CommentModel> createComment(CommentCreateModel create) async {
    try{
      return await  _commentHttp.createComment(create);
    }catch (error){
      Future.error(error);
    }
  }
}