import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/actas_model.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/models/overdue_task_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

class TaskHttp extends ApiProvider{

  Future<List<OverdueTaskModel>> overdue(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/$idUser/Gts-PlanTrabajo-Tareas-vencidas", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new OverdueTaskModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOverdueBinnacleModel>> overdueBinnacle(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Bitacora-Vencidas?id_user=$idUser", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskOverdueBinnacleModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOverdueBinnacleModel>> validBinnacle(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Bitacora-Vigentes?id_user=$idUser", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskOverdueBinnacleModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskPqrModel>> pqr(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/Pqrs/$idUser/Action-List-Pqrs", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskPqrModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskPqrBinnacleModel>> pqrBinnacle(int idUser) async {
    print("/GestionTiempos/Bitacora/Action-Bitacora-Pqr?id_user=$idUser");
    try{

      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Bitacora-Pqr?id_user=$idUser", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskPqrBinnacleModel.fromJson(json))
          .toList();
    }catch (error){

      return Future.error(error);
    }
  }

  Future<List<TaskOpportunityModel>> ops(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/Ops/$idUser/Action-Listar-Ops", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskOpportunityModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOpportunityModel>> opsBinnacle(int idUser) async {
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Bitacora-Ops?id_user=$idUser", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskOpportunityModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskProjectModel>> project() async {
    try{
      Response response = await dio.get("/GestionTiempos/Proyectos/Action-Listar", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskProjectModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskProjectModel>> projectBinnacle() async {
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Bitacora-Proyectos", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskProjectModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskEventModel>> event() async {
    try{
      Response response = await dio.get("/GestionTiempos/Eventos/Action-Listar", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskEventModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskEventModel>> eventBinnacle() async {
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Bitacora-Event", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskEventModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskActaModel>> actas(int idUser) async {
    try{//
      Response response = await dio.get("/GestionTiempos/Actas/$idUser/Action-List-Actas", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new TaskActaModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<ActaParcialModel> detailActa(int idActa) async {
    try{//
      Response response = await dio.get("/GestionTiempos/Actas/$idActa/Action-Actividad-Parcial", options: Options(headers: {"requirestoken": true}));
      return new  ActaParcialModel.fromJson(response.data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<WorkPlaneModel> detailTask(int idGts) async {
    try{//
      Response response = await dio.get("/GestionTiempos/modalDetalleTarea/$idGts/consultar-modal-detalle-tarea", options: Options(headers: {"requirestoken": true}));
      return new WorkPlaneModel.fromJsonDetailTask(response.data);
    }catch (error){
      return Future.error(error);
    }
  }


}