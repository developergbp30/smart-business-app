import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:smart_business/src/blocs/auth/auth_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_event.dart';
import 'package:smart_business/src/blocs/auth/auth_state.dart';
import 'package:smart_business/src/blocs/my_bloc_delegate.dart';
import 'package:smart_business/src/presentation/app.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';

void main() {

  initializeDateFormatting().then((_){});
  BlocSupervisor.delegate = MyBlocDelegate();
  final authRepository = AuthRepository();
  final authBloc = AuthBloc(authRepository: authRepository);
  authBloc.dispatch(StatusAuthEvent());

  authBloc.state
  .firstWhere((state) => state is UnAuthenticate || state is Authenticated)
  .then((_) => runApp(App(authRepository: authRepository, authBloc: authBloc,)));

}

