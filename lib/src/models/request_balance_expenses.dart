class ReqBalanceExpensesModel {
  String maximo;
  List<Detalle> detalle;

  ReqBalanceExpensesModel({this.maximo, this.detalle});

  ReqBalanceExpensesModel.fromJson(Map<String, dynamic> json) {
    maximo = json['maximo'];
    if (json['detalle'] != null) {
      detalle = new List<Detalle>();
      json['detalle'].forEach((v) {
        detalle.add(new Detalle.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['maximo'] = this.maximo;
    if (this.detalle != null) {
      data['detalle'] = this.detalle.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Detalle {
  int idTipo;
  String valor;
  Tipo tipo;

  Detalle({this.idTipo, this.valor, this.tipo});

  Detalle.fromJson(Map<String, dynamic> json) {
    idTipo = json['id_tipo'];
    valor = json['valor'];
    tipo = json['tipo'] != null ? new Tipo.fromJson(json['tipo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_tipo'] = this.idTipo;
    data['valor'] = this.valor;
    if (this.tipo != null) {
      data['tipo'] = this.tipo.toJson();
    }
    return data;
  }
}

class Tipo {
  int id;
  String nombre;

  Tipo({this.id, this.nombre});

  Tipo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    return data;
  }
}
