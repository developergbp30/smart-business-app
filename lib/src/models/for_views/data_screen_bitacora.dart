class SetFormBitacora {
  int id;
  int idClient;
  int idActa;
  String title;
  String subTypeActivity;
  int idSubTypeActivity;
  String nameActivity;
  String nameTask;
  String results;
  String description;
  String typeActivity;
  String date;
  String startTime;
  String endTime;
  int idTypeActivity;
  int idOpportunity;
  bool disabledModalAct;
  bool disabledModalDynamic;

  SetFormBitacora({this.id, this.idClient,this.title, this.description,
    this.typeActivity, this.idTypeActivity, this.idOpportunity,
    this.disabledModalAct, this.disabledModalDynamic, this.idActa,
    this.subTypeActivity,this.idSubTypeActivity,this.nameActivity, this.nameTask, this.results, this.date, this.endTime, this.startTime
  });
}