import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/info_work_plane_binnacle_model.dart';
import 'package:smart_business/src/models/select_binnacle_model.dart';

abstract class SelectBinnacleState extends Equatable {
  SelectBinnacleState([List props = const []]) : super(props);
}

class EmptySelectBinnacleState extends SelectBinnacleState{}

class LoadingDataSelectBinnacleState extends SelectBinnacleState{}

class ListSelectBinnacleState extends SelectBinnacleState{
  final List<SelectBinnacleModel> items;
  final SelectBinnacleModel selected;
  final InfoWorkPlaneBinnacleModel infoItem;

  ListSelectBinnacleState({this.items, this.selected, this.infoItem}) : super([items, selected, infoItem]);
  @override
  String toString()  => '$runtimeType { items: $items }';
}

class ErrorSelectBinnacleState extends SelectBinnacleState{
  final String message;
  ErrorSelectBinnacleState({this.message}) : super([message]);

  @override
  String toString()  => '$runtimeType { message: $message }';
}