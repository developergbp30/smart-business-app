import 'package:smart_business/src/models/opportunity_model.dart';

class OpportunityRepository{

  List<OpportunityModel> opports = [
    new OpportunityModel(
      name: "PO 8757- Intalacion de la nueva tienda de NY"
    ),
    new OpportunityModel(
      name: "OP 6754 - Crecimiento de mercado"
    ),
  ];

  Future<List<OpportunityModel>> getListExample() async {
    Future.delayed(Duration(seconds: 2));
      return opports;
  }
}