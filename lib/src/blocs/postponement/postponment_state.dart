import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/postponement_model.dart';

abstract class PostponementState extends Equatable {
  PostponementState([List props = const []]) : super(props);
}


class UnloadPostponementState extends PostponementState{}

class LoadedHistoryPostponementState extends PostponementState{
  final List<PostponementModel> history;
  final String token;
  LoadedHistoryPostponementState({this.history, this.token}) : super([history, token]);

  @override
  String toString()  => '$runtimeType { history: $history }';
}

class ErrorPostponementState extends PostponementState{
  final String message;
  ErrorPostponementState({this.message}) : super([message]);
  @override
  String toString()  => '$runtimeType { message: $message }';
}