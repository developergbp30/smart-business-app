import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/request_balance_expenses.dart';

abstract class BitacoraDetailState extends Equatable {
  BitacoraDetailState([List props = const []]) : super(props);
}


class UnloadBitacoraDetailState extends BitacoraDetailState{}

class  DetailBitacoraState extends  BitacoraDetailState{
  final ResponseDetailBitacoraModel detail;
  final List<ResponsePendingModel> pending;
  final List<ExpenseBinnacleModel> expenses;
  final ReqBalanceExpensesModel balanceExpenses;
  final String token;
  
  DetailBitacoraState({this.detail, this.pending, this.expenses,this.balanceExpenses,  this.token})
      : super([detail, pending, expenses, balanceExpenses, token]);
}