import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class AdminSocialNetworkPage extends StatefulWidget {
  @override
  _AdminSocialNetworkPageState createState() => _AdminSocialNetworkPageState();
}

class _AdminSocialNetworkPageState extends State<AdminSocialNetworkPage> {
  TextEditingController linkCtrl = TextEditingController();
  final Color greenColor =  Color(0xFF11BA19);

  List<String> listExternalEmail = [];

  @override
  void initState() {
    listExternalEmail.add("https://www.facebook.com/pg/Gregory-Iscala-1975413289420297/videos/");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(icon: Icon(Icons.clear, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Administar redes", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: Colors.green,), onPressed: (){

          })
        ],
      ),
      body: Container(child:
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: <Widget>[
              // add network
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Formulario", style: TextStyle(color: Colors.grey[700]),),
              ),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(""),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){

                },
              ),
              // Link
              _addLink()
            ],
          ),
        ),),
    );
  }

  Widget _addLink(){
    return Container(
      height: 300,
      margin: const EdgeInsets.only(top: 5),
      child: Column(
        children: <Widget>[
          Text("Añadir correo electronico externo"),
          SizedBox(height: 5,),
          Row(
            children: <Widget>[
              Flexible(
                child: TextFormField(
                  controller: linkCtrl,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey,
                            width: 5.0
                        )
                    ),
                    hintText: 'Preparar presentación',
                    // labelText: 'Actividad'
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.add_box, color: greenColor, size: 30,),
                onPressed: (){
                  if(linkCtrl.text!= null && linkCtrl.text.length > 0){
                    listExternalEmail.add(linkCtrl.text);
                    linkCtrl.clear();
                    setState(() {});

                  }
                },
              )
            ],
          ),
          Expanded(
            child:  ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: listExternalEmail.length,
                itemBuilder: (context, i) {
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      child: Chip(
                        onDeleted:(){
                          listExternalEmail.remove(listExternalEmail[i]);
                          setState(() {});
                        },
                        avatar: CircleAvatar(
                            backgroundColor: Colors.white,
                            child: Icon(_iconSocialNetwork(listExternalEmail[i]), color: Colors.black54,)),
                        label: Text(listExternalEmail[i]),));
                }),
          )
        ],
      ),
    );
  }

  IconData _iconSocialNetwork(String link){
    if(link.toLowerCase().contains("facebook")) return FontAwesomeIcons.facebook;
    if(link.toLowerCase().contains("instagram")) return FontAwesomeIcons.instagram;
    if(link.toLowerCase().contains("twitter")) return FontAwesomeIcons.twitter;
    if(link.toLowerCase().contains("twitch")) return FontAwesomeIcons.twitch;
    if(link.toLowerCase().contains("linkedin")) return FontAwesomeIcons.linkedin;

    return FontAwesomeIcons.networkWired;
  }
}
