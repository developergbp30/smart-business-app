import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/comments_model.dart';

class CommentHttp extends ApiProvider{

  Future<List<CommentModel>> getComments(int idGts) async{
   try{
     print(idGts);
     Response response = await dio.get("/GestionTiempos/comentarios-bitacora/$idGts/action-listar-comentarios", options: Options(headers: {"requirestoken": true}));
     return (response.data["data"] as List)
         .map((json) => new CommentModel.fromJson(json))
         .toList();
   }catch (error){
     Future.error(error);
   }
  }

  Future<CommentModel> createComment(CommentCreateModel create) async{
    try{
      Response response = await dio.post("/GestionTiempos/comentarios-bitacora/crear-comentario",
          options: Options(headers: {"requirestoken": true}),
      data: create.toJson());
      return  new CommentModel.fromJson(response.data);

    }catch (error){
      Future.error(error);
    }
  }
}