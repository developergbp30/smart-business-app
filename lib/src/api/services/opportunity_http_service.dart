import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/opportunity_model.dart';

class OpportunityHttp extends ApiProvider{

  Future<List<ResponseOpportunityModel>> listOpsUser(int idUser) async{
    try{
      Response response = await dio.get("/GestionTiempos/Ops/$idUser/Action-Load-Select-Dinamic-Ops", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseOpportunityModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}