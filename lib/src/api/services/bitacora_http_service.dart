import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/create_expenses.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/info_work_plane_binnacle_model.dart';
import 'package:smart_business/src/models/select_binnacle_model.dart';

class BitacoraHttp extends ApiProvider{

  Future<List<ResponseBitacoraModel>> getCalendar(int idUser, String startDate, String endDate, String year) async{
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/Action-Calendario-Semanas?year=$year&inicio=$startDate&fin=$endDate&id_user_principal=$idUser",
          options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => ResponseBitacoraModel.fromJson(json))
          .toList();
    }catch (error){
      print("---------------_______> Error $error");
      return Future.error(error);
    }
  }

  Future<ResponseDetailBitacoraModel> detail(int id) async {
    try{
      Response response = await dio.get("/GestionTiempos/detalle-bitacora/$id/action-ver-detalle-bitacora",
          options: Options(headers: {"requirestoken": true}));

      return new ResponseDetailBitacoraModel.fromJson(response.data);

    }catch (error){
      return Future.error(error);
    }
  }

  Future create(CreateBitacoraModel data) async {
    try{
      Response response = await dio.post("/GestionTiempos/Bitacora/modalBitacora/crear-nueva-bitacora",
          data: data.toJson(),
          options: Options(headers: {"requirestoken": true}));

      if(response.statusCode == 201){ // Success Full
        return new ResponseRegisterActivity( message: 'registro exitoso', type: 1);
      }else {
        if(response.data['id'] != null) return new ResponseRegisterActivity( message: 'Actualizacion exitosa', type: 1);
        return ResponseRegisterActivity(message: response.data['mensaje'], type: 2);
      }

    }catch (error){
      return Future.error(error);
    }
  }

  Future createPending(CreatePendingModel data) async {
    try{
      Response response = await dio.post("/GestionTiempos/pendientes-bitacora/clonar-bitacora",
          data: data.toJson(),
          options: Options(headers: {"requirestoken": true}));

      if(response.statusCode == 201){ // Success Full
        return new ResponseRegisterActivity( message: 'registro exitoso', type: 1);
      }else {
        if(response.data['id'] != null) return new ResponseRegisterActivity( message: 'Actualizacion exitosa', type: 1);
        return ResponseRegisterActivity(message: response.data['mensaje'], type: 2);
      }

    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponsePendingModel>> listPending(int id)  async{
    try{
      Response response = await dio.get("/GestionTiempos/pendientes-bitacora/$id/listar-pendientes",
          options: Options(headers: {"requirestoken": true}));

      return (response.data as List)
          .map((json) => new ResponsePendingModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ExpenseBinnacleModel>> listExpenses(int id) async {
    try{
      Response response = await dio.get("/presupuestos_generales/$id/listar_datos",
          options: Options(headers: {"requirestoken": true}));

      if(response.data['error'] !=  null) return Future.error(response.data['error']);

      return (response.data["datos"]["data"] as List)
          .map((json) => new ExpenseBinnacleModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future createExpense(CreateExpensesModel data) async{
    print(data.toJson());
    try{
      Response response = await dio.post("/presupuestos_generales/guardar",
          data: data.toJson(),
          options: Options(headers: {"requirestoken": true}));

      print("response.statusCode: ${response.statusCode}");
      print("response.data: ${response.data}");
      return response.statusCode;

    }catch (error){
      return Future.error(error);
    }
  }

  Future updateExpense(CreateExpensesModel data) async{
    print(data.toJson());
    try{
      Response response = await dio.put("/presupuestos_generales/guardar",
          data: data.toJson(),
          options: Options(headers: {"requirestoken": true}));

      print("response.statusCode: ${response.statusCode}");
      print("response.data: ${response.data}");
      return response.statusCode;

    }catch (error){
      return Future.error(error);
    }
  }

  Future deleteExpense(int id) async {
    try{
      Response response = await dio.delete("/presupuestos_generales/$id/eliminar",
          options: Options(headers: {"requirestoken": true}));
      print("response.statusCode: ${response.statusCode}");
      print("response.data: ${response.data}");
      return response.statusCode;
    }catch (error){
      return Future.error(error);
    }
  }

  Future deleteBinnacle(int id) async {
    try{
      Response response = await dio.delete("/GestionTiempos/detalle-bitacora/$id/eliminar-gts",
          options: Options(headers: {"requirestoken": true}));

      print("response.statusCode: ${response.statusCode}");
      print("response.data: ${response.data}");

      return response.statusCode;
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<SelectBinnacleModel>> listForSelect(int idUser) async{
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/modalBitacora/actividad-plan-de-trabajo-bitacora?id_user=$idUser",
          options: Options(headers: {"requirestoken": true}));

      return (response.data as List)
          .map((json) => new SelectBinnacleModel.fromJson(json))
          .toList();

    }catch (error){
      return  Future.error(error);
    }
  }

  Future<InfoWorkPlaneBinnacleModel> infoWorkPlane(int id) async{
    try{
      Response response = await dio.get("/GestionTiempos/Bitacora/modalBitacora/$id/informacion-plan-trabajo",
          options: Options(headers: {"requirestoken": true}));

      return new InfoWorkPlaneBinnacleModel.fromJson(response.data);

    }catch (error){
      return Future.error(error);
    }
  }
}