class ResponsePqrModel {
  int value;
  String label;
  int idCliente;
  int estado;
  Clientes clientes;

  ResponsePqrModel(
      {this.value, this.label, this.idCliente, this.estado, this.clientes});

  ResponsePqrModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    label = json['label'];
    idCliente = json['id_cliente'];
    estado = json['estado'];
    clientes = json['clientes'] != null
        ? new Clientes.fromJson(json['clientes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['label'] = this.label;
    data['id_cliente'] = this.idCliente;
    data['estado'] = this.estado;
    if (this.clientes != null) {
      data['clientes'] = this.clientes.toJson();
    }
    return data;
  }
}

class Clientes {
  int id;
  String nombreCorto;
  String logo;

  Clientes({this.id, this.nombreCorto, this.logo});

  Clientes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombreCorto = json['nombre_corto'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre_corto'] = this.nombreCorto;
    data['logo'] = this.logo;
    return data;
  }
}

class TaskPqrModel {
  int id;
  int idPqr;
  String fecha;
  String horaFin;
  String horaInicio;
  Pqrs pqrs;

  TaskPqrModel(
      {this.id,
        this.idPqr,
        this.fecha,
        this.horaFin,
        this.horaInicio,
        this.pqrs});

  TaskPqrModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idPqr = json['id_pqr'];
    fecha = json['fecha'];
    horaFin = json['hora_fin'];
    horaInicio = json['hora_inicio'];
    pqrs = json['pqrs'] != null ? new Pqrs.fromJson(json['pqrs']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_pqr'] = this.idPqr;
    data['fecha'] = this.fecha;
    data['hora_fin'] = this.horaFin;
    data['hora_inicio'] = this.horaInicio;
    if (this.pqrs != null) {
      data['pqrs'] = this.pqrs.toJson();
    }
    return data;
  }
}

class Pqrs {
  int id;
  String titulo;
  String descripcion;
  int idCliente;
  int idOportunidad;
  Clientes clientes;

  Pqrs(
      {this.id,
        this.titulo,
        this.descripcion,
        this.idCliente,
        this.idOportunidad,
        this.clientes});

  Pqrs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idCliente = json['id_cliente'];
    idOportunidad = json['id_oportunidad'];
    clientes = json['clientes'] != null
        ? new Clientes.fromJson(json['clientes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_cliente'] = this.idCliente;
    data['id_oportunidad'] = this.idOportunidad;
    if (this.clientes != null) {
      data['clientes'] = this.clientes.toJson();
    }
    return data;
  }
}

class TaskPqrBinnacleModel {
  int id;
  int estado;
  Null finJustificacion;
  Null finFecha;
  String titulo;
  String descripcion;
  int idTipo;
  int idCliente;
  int idOportunidad;
  Null idClienteContacto;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  Clientes clientes;

  TaskPqrBinnacleModel(
      {this.id,
        this.estado,
        this.finJustificacion,
        this.finFecha,
        this.titulo,
        this.descripcion,
        this.idTipo,
        this.idCliente,
        this.idOportunidad,
        this.idClienteContacto,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.clientes});

  TaskPqrBinnacleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    finJustificacion = json['fin_justificacion'];
    finFecha = json['fin_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idTipo = json['id_tipo'];
    idCliente = json['id_cliente'];
    idOportunidad = json['id_oportunidad'];
    idClienteContacto = json['id_cliente_contacto'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    clientes = json['clientes'] != null
        ? new Clientes.fromJson(json['clientes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['fin_justificacion'] = this.finJustificacion;
    data['fin_fecha'] = this.finFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_tipo'] = this.idTipo;
    data['id_cliente'] = this.idCliente;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente_contacto'] = this.idClienteContacto;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.clientes != null) {
      data['clientes'] = this.clientes.toJson();
    }
    return data;
  }
}




