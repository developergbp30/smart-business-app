class BackDataThirdSelect {
  int type;
  String label;
  dynamic data;

  BackDataThirdSelect({this.type, this.data});
}


class ConfigActivity {
  int id;
  bool showButtonContacts =  false;
  ConfigActivity({this.id, this.showButtonContacts});
}
// class for fill Form
class SetFormActivity {
  int id;
  int idClient;
  int idActa;
  String title;
  String description;
  String typeActivity;
  int idTypeActivity;
  int idOpportunity;
  bool disabledModalAct;
  bool disabledModalDynamic;
  SetFormActivity({this.id, this.idClient,this.title, this.description,
  this.typeActivity, this.idTypeActivity, this.idOpportunity,
  this.disabledModalAct, this.disabledModalDynamic, this.idActa
  });
}