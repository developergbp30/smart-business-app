import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/presentation/screens/contacts/contacts_page.dart';
class LinkContactPage extends StatefulWidget {
  @override
  _LinkContactPageState createState() => _LinkContactPageState();
}

class _LinkContactPageState extends State<LinkContactPage> {
  final Color greenColor =  Color(0xFF11BA19);
  TextEditingController cargoCtrl = TextEditingController();
  TextEditingController descriptionCtrl= TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(icon: Icon(Icons.clear, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Vincular contacto", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: Colors.green,), onPressed: (){

          })
        ],
      ),
      body: Container(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            //--> Name
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Nombre", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            //--> Area
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Área", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            //--> Rol
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Rol", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            //--> Cargo
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Cargo", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              controller: cargoCtrl,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'ingrese ..',
                // labelText: 'Actividad'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese el nombre corto';
                }
                return null;
              },
              onSaved: (value){

              },
            ),
            // Date Link
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Fecha de vinculación", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Decision
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Toma de desición", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Description
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Descripción", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              controller: descriptionCtrl,
              keyboardType: TextInputType.multiline,
              minLines: 1,
              maxLines: 5,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'Ingresa la descipción',
                // labelText: 'Descripcion'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese la descripción';
                }
                return null;
              },
              onSaved: (value){
              },
            ),
            //email corporate
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Correo Corporativo", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              controller: cargoCtrl,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'ingrese ..',
                // labelText: 'Actividad'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese el nombre corto';
                }
                return null;
              },
              onSaved: (value){

              },
            ),
            // Phone corporate
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Teléfono corporativo", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              keyboardType: TextInputType.phone,
              controller: cargoCtrl,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'ingrese ..',
                // labelText: 'Actividad'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese el nombre corto';
                }
                return null;
              },
              onSaved: (value){

              },
            ),
            // Campus
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Sede", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Boss
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Jefe", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Add Subordinate
            Container(
              width: MediaQuery.of(context).size.width/2,
              margin: const EdgeInsets.only(left: 20, right: 20),
              color: Colors.transparent,
              padding: const EdgeInsets.all(8),
              child: FlatButton(
                  padding: const EdgeInsets.all(0),
                  color: Color(0xFFF5F5F5),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(50.0),
                      side: BorderSide(color: Color(0xFFDBDBDB))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(FontAwesomeIcons.userPlus,color: greenColor),
                      SizedBox(width: 10,),
                      Text(" Vincular subordinados", style: TextStyle(color: greenColor),),
                    ],
                  ),
                  onPressed: () async {
                    // Todo: add id of contact and Type contact for screen ContactsPage working
                    await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => ContactsPage()));
                  }),
            ),
          ],
        ),
      ),),
    );
  }
}
