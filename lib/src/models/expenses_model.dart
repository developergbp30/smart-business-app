class ExpenseBinnacleModel {
  int id;
  String titulo;
  String descripcion;
  String fecha;
  String adjunto;
  String adjuntoNombre;
  int idTipo;
  int valorSolicitado;
  int valorAprobado;
  dynamic rechazoJustificacion;
  int idGts;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  int porcentajeAprobado;
  CreadoPor creadoPor;
  String rechazadoAt;
  CreadoPor aporbadoPor;//rechazado_por
  CreadoPor rechazadoPor;//
  String aprobadoAt;
  Tipo tipo;

  ExpenseBinnacleModel(
      {this.id,
        this.titulo,
        this.descripcion,
        this.fecha,
        this.adjunto,
        this.adjuntoNombre,
        this.idTipo,
        this.valorSolicitado,
        this.valorAprobado,
        this.rechazoJustificacion,
        this.idGts,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.porcentajeAprobado,
        this.creadoPor,
        this.rechazadoAt,
        this.aporbadoPor,
        this.aprobadoAt,
        this.tipo});

  ExpenseBinnacleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    fecha = json['fecha'];
    adjunto = json['adjunto'];
    adjuntoNombre = json['adjunto_nombre'];
    idTipo = json['id_tipo'];
    valorSolicitado = json['valor_solicitado'];
    valorAprobado = json['valor_aprobado'];
    rechazoJustificacion = json['rechazo_justificacion'];
    idGts = json['id_gts'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    porcentajeAprobado = json['porcentaje_aprobado'];
    creadoPor = json['creado_por'] != null
        ? new CreadoPor.fromJson(json['creado_por'])
        : null;
    rechazadoAt = json['rechazado_at'];
    aporbadoPor = json['aporbado_por'] != null
        ? new CreadoPor.fromJson(json['aporbado_por'])
        : null;
    aprobadoAt = json['aprobado_at'];
    rechazadoPor = json['rechazado_por'] != null
        ? new CreadoPor.fromJson(json['rechazado_por'])
        : null;
    tipo = json['tipo'] != null ? new Tipo.fromJson(json['tipo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['fecha'] = this.fecha;
    data['adjunto'] = this.adjunto;
    data['adjunto_nombre'] = this.adjuntoNombre;
    data['id_tipo'] = this.idTipo;
    data['valor_solicitado'] = this.valorSolicitado;
    data['valor_aprobado'] = this.valorAprobado;
    data['rechazo_justificacion'] = this.rechazoJustificacion;
    data['id_gts'] = this.idGts;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['porcentaje_aprobado'] = this.porcentajeAprobado;
    if (this.creadoPor != null) {
      data['creado_por'] = this.creadoPor.toJson();
    }
    data['rechazado_at'] = this.rechazadoAt;
    if (this.aporbadoPor != null) {
      data['aporbado_por'] = this.aporbadoPor.toJson();
    }
    data['aprobado_at'] = this.aprobadoAt;
    if (this.tipo != null) {
      data['tipo'] = this.tipo.toJson();
    }
    return data;
  }
}

class CreadoPor {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  CreadoPor(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  CreadoPor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}

class Tipo {
  int id;
  int estado;
  String nombre;
  int tipo;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  Tipo(
      {this.id,
        this.estado,
        this.nombre,
        this.tipo,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Tipo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    nombre = json['nombre'];
    tipo = json['tipo'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['nombre'] = this.nombre;
    data['tipo'] = this.tipo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}


class ExpenseTypeModel {
  int id;
  int estado;
  String nombre;
  int tipo;
  int createdBy;
  int updatedBy;

  ExpenseTypeModel(
      {this.id,
        this.estado,
        this.nombre,
        this.tipo,
        this.createdBy,
        this.updatedBy});

  ExpenseTypeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    nombre = json['nombre'];
    tipo = json['tipo'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['nombre'] = this.nombre;
    data['tipo'] = this.tipo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}
