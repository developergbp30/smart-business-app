import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_event.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_state.dart';
import 'package:smart_business/src/models/for_views/data_screen_create_activity.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/workplan/workplan_repository.dart';
import 'package:flutter/material.dart';
class SelectActivityBloc extends Bloc<SelectActivityEvent, SelectActivityState> {
  WorkPlanRepository _workPlanRepository;
  AuthRepository _authRepository = AuthRepository();

  SelectActivityBloc({@required WorkPlanRepository workPlanRepository}) :
  assert(workPlanRepository !=  null), _workPlanRepository = workPlanRepository;

  @override
  SelectActivityState get initialState => EmptySelectsState();

  @override
  Stream<SelectActivityState> mapEventToState(SelectActivityEvent event) async*{
    if(event is LoadDataForSelectsEvent){
     yield* _mapLoadDataForSelectsToState();
    }
    else if(event is FindSubTypeActivity){
      yield* _mapLoadSubTypesActivity(event);
    }
  }

  Stream<SelectActivityState> _mapLoadDataForSelectsToState() async*{
    try{// find all data for selects [Activity, Activity SubType, Opportunity ]
      var sActivities =  await _workPlanRepository.getTypeActivity();
      // find params of validations times in screen create_activity
      var pTimes =  await _workPlanRepository.getTimeParams();
      yield DataForSelectState(activities: sActivities, timeParams: pTimes);
    }catch (error){
     // yield ErrorRegisterActivityState(message: error.toString());
    }
  }

  Stream<SelectActivityState> _mapLoadSubTypesActivity(FindSubTypeActivity event) async*{
    try { // find data of activity selected for id, and return new state whit data
      // [ List Type Activities and List of sub Activities ]
      bool showThirdSelect;
      var subActivities;
      BackDataThirdSelect thirdSelect;
      if(event.selected.id != null){// if id is null find type activity father ( Personalized )
        showThirdSelect = false;//disable select
        subActivities =  await _workPlanRepository.getSubTypeActivityFather(event.selected.id);
      }else{
        showThirdSelect =  true;// active select
        var id  =  _findIdActivityForName(event.selected.nombre); // find id for name
        subActivities =  await _workPlanRepository.getSubTypeActivity(id);// find sub types of Activity
        var user = await _authRepository.userData();
        thirdSelect =  await _loadDataThirdSelect(id, user.id);// find data for select (opportunities, events, Pqr etc)
      }

      yield DataForSelectState(activities:event.activities, subTypeActivities: subActivities,
        showThirdSelect: showThirdSelect,
        thirdSelect:  thirdSelect,);
    }catch (error){
      print(error);
    }
  }

  int _findIdActivityForName(String name) {
    int value;
    switch (name){
      case 'Oportunidades Vigentes':

        value = 1;
        break;
      case 'Oportunidades Perdidas':

        value = 2;
        break;
      case 'Oportunidades Ganadas':

        value = 3;
        break;
      case 'Pqr':
        value = 4;
        break;
      case 'Evento':
        value = 5;
        break;
      case 'Cliente':
        value = 6;
        break;
      case 'Competencia':
        value = 7;
        break;
      case 'Proyectos':
        value = 8;
        break;
      default:
        value = null;
    }
    return value;
  }

  _loadDataThirdSelect(int idSubTAct, int idUser) async {
    BackDataThirdSelect values = BackDataThirdSelect();
    switch(idSubTAct){
      case 1:
        values.type = 1;
        values.label = 'Oportunidades';
        values.data =  await _workPlanRepository.listOpsUser(idUser);
        break;
      case 2:
        values.type = 2;
        values.label = 'Oportunidades';
        values.data =  await _workPlanRepository.listOpsUser(idUser);
        break;
      case 3:
        values.type = 3;
        values.label = 'Oportunidades';
        values.data =  await _workPlanRepository.listOpsUser(idUser);
        break;
      case 4:
        values.type = 4;
        values.label = 'Pqr';
        values.data =  await _workPlanRepository.listPqrUser(idUser);
        break;
      case 5:
        values.type = 5;
        values.label = 'Eventos';
        values.data =  await _workPlanRepository.listEventUser(idUser);
        break;
      case 6:
        values.type = 6;
        values.label = 'Cliente';
        values.data =  await _workPlanRepository.listClientUser(idUser);
        break;
      case 7:
        values.type = 7;
        values.label = 'Competencias';
        values.data =  await _workPlanRepository.listCompetitionUser(idUser);
        break;
      case 8:
        values.type = 8;
        values.label = 'Proyectos';
        values.data =  await _workPlanRepository.listProjectUser(idUser);
        break;
    }

    return values;
  }

}