import 'package:smart_business/src/models/invitations.dart';

class WorkPlaneModel {
  int id;
  int idUserPrincipal;
  String titulo;
  String descripcion;
  String fecha;
  String horaInicio;
  String horaFin;
  String bHoraInicio;
  String bHoraFin;
  int ptTipo;
  int ptAtiempo;
  int idSubtipoActividad;
  int idPqr;
  int idActa;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idProyecto;
  String bFecha;
  NombreCodigo nombreCodigo;
  GtsTipos gtsTipos;
  Cliente cliente;
  int gtsComantaryCount;
  int gtsAaplazamientosCount;
  int gtsPendientesCount;
  String tipoActividad;

  WorkPlaneModel(
      {this.id,
        this.idUserPrincipal,
        this.titulo,
        this.descripcion,
        this.fecha,
        this.horaInicio,
        this.horaFin,
        this.ptTipo,
        this.ptAtiempo,
        this.idSubtipoActividad,
        this.idPqr,
        this.idActa,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idProyecto,
        this.bFecha,
        this.bHoraInicio,
        this.bHoraFin,
        this.nombreCodigo,
        this.gtsTipos,
        this.cliente,
        this.gtsComantaryCount,
        this.gtsAaplazamientosCount,
        this.gtsPendientesCount,
        this.tipoActividad,
      });

  WorkPlaneModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    fecha = json['fecha'];
    horaInicio = json['hora_inicio'];
    horaFin = json['hora_fin'];
    ptTipo = json['pt_tipo'];
    ptAtiempo = json['pt_atiempo'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idProyecto = json['id_proyecto'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    gtsComantaryCount = json['gts_comentarios_count'];
    gtsAaplazamientosCount = json['gts_aplazamientos_count'];
    gtsPendientesCount = json['gts_pendientes_count'];
    tipoActividad = json['tipo_actividad'];
    nombreCodigo = json['nombre_codigo'] != null
        ? new NombreCodigo.fromJson(json['nombre_codigo'])
        : null;
    gtsTipos = json['gts_tipos'] != null
        ? new GtsTipos.fromJson(json['gts_tipos'])
        : null;
    cliente =
    json['cliente'] != null ? new Cliente.fromJson(json['cliente']) : null;
  }

  WorkPlaneModel.fromJsonDetailTask(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    fecha = json['pt_fecha'];
    horaInicio = json['pt_hora_inicio'];
    horaFin = json['pt_hora_fin'];
    ptTipo = json['pt_tipo'];
    ptAtiempo = json['pt_atiempo'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idProyecto = json['id_proyecto'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    gtsComantaryCount = json['gts_comentarios_count'];
    gtsAaplazamientosCount = json['gts_aplazamientos_count'];
    gtsPendientesCount = json['gts_pendientes_count'];
    tipoActividad = json['tipo_padre'];
    nombreCodigo = json['nombre_codigo'] != null
        ? new NombreCodigo.fromJson(json['nombre_codigo'])
        : null;
    gtsTipos = json['gts_tipos'] != null
        ? new GtsTipos.fromJson(json['gts_tipos'])
        : null;
    cliente =
    json['cliente'] != null ? new Cliente.fromJson(json['cliente']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['fecha'] = this.fecha;
    data['hora_inicio'] = this.horaInicio;
    data['hora_fin'] = this.horaFin;
    data['pt_tipo'] = this.ptTipo;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_proyecto'] = this.idProyecto;
    data['b_fecha'] = this.bFecha;
    if (this.nombreCodigo != null) {
      data['actividad_gts'] = this.nombreCodigo.toJson();
    }
    if (this.gtsTipos != null) {
      data['gts_tipos'] = this.gtsTipos.toJson();
    }
    if (this.cliente != null) {
      data['cliente'] = this.cliente.toJson();
    }
    return data;
  }
}

class NombreCodigo {
  String nombre;
  int id;
  String codigo;

  NombreCodigo({this.nombre});

  NombreCodigo.fromJson(Map<String, dynamic> json) {
    nombre = json['nombre'];
    id = json['id'];
    codigo = json['codigo'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nombre'] = this.nombre;
    return data;
  }
}



class Cliente {
  int id;
  int kpiValor;
  String kpiUpdated;
  String nombreLegal;
  String nombreCorto;
  String logo;
  String logoMini;
  String banner1;
  String banner2;
  int tamano;
  String fechaInauguracion;
  int fechaExacta;
  int utilidadEstado;
  int creditoDias;
  int creditoCupo;
  int idmCiudad;
  int idSedePrincipal;
  int idFormulario;
  int idOrigen;
  int idEvento;
  int idUserIdentifico;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  Cliente(
      {this.id,
        this.kpiValor,
        this.kpiUpdated,
        this.nombreLegal,
        this.nombreCorto,
        this.logo,
        this.logoMini,
        this.banner1,
        this.banner2,
        this.tamano,
        this.fechaInauguracion,
        this.fechaExacta,
        this.utilidadEstado,
        this.creditoDias,
        this.creditoCupo,
        this.idmCiudad,
        this.idSedePrincipal,
        this.idFormulario,
        this.idOrigen,
        this.idEvento,
        this.idUserIdentifico,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Cliente.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    kpiValor = json['kpi_valor'];
    kpiUpdated = json['kpi_updated'];
    nombreLegal = json['nombre_legal'];
    nombreCorto = json['nombre_corto'];
    logo = json['logo'];
    logoMini = json['logo_mini'];
    banner1 = json['banner_1'];
    banner2 = json['banner_2'];
    tamano = json['tamano'];
    fechaInauguracion = json['fecha_inauguracion'];
    fechaExacta = json['fecha_exacta'];
    utilidadEstado = json['utilidad_estado'];
    creditoDias = json['credito_dias'];
    creditoCupo = json['credito_cupo'];
    idmCiudad = json['idm_ciudad'];
    idSedePrincipal = json['id_sede_principal'];
    idFormulario = json['id_formulario'];
    idOrigen = json['id_origen'];
    idEvento = json['id_evento'];
    idUserIdentifico = json['id_user_identifico'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['kpi_valor'] = this.kpiValor;
    data['kpi_updated'] = this.kpiUpdated;
    data['nombre_legal'] = this.nombreLegal;
    data['nombre_corto'] = this.nombreCorto;
    data['logo'] = this.logo;
    data['logo_mini'] = this.logoMini;
    data['banner_1'] = this.banner1;
    data['banner_2'] = this.banner2;
    data['tamano'] = this.tamano;
    data['fecha_inauguracion'] = this.fechaInauguracion;
    data['fecha_exacta'] = this.fechaExacta;
    data['utilidad_estado'] = this.utilidadEstado;
    data['credito_dias'] = this.creditoDias;
    data['credito_cupo'] = this.creditoCupo;
    data['idm_ciudad'] = this.idmCiudad;
    data['id_sede_principal'] = this.idSedePrincipal;
    data['id_formulario'] = this.idFormulario;
    data['id_origen'] = this.idOrigen;
    data['id_evento'] = this.idEvento;
    data['id_user_identifico'] = this.idUserIdentifico;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}


/*
"id": 120,
    "justificacion": "porque quiero puedo y no me da miedo.",
    "pt_aprobado": 2
 */
class ApprovedRejectModel {
  int id;
  String justification;
  int ptApproved;

  ApprovedRejectModel({this.id, this.justification, this.ptApproved});

  Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
      data["id"] =  this.id;
      data["justificacion"] =  this.justification;
      data["pt_aprobado"] =  this.ptApproved;
      return data;
  }
}

/*
{
    "descripcion": "me aburrri de madrugar todos los dias",
    "cancelada": 1
}
 */

class CancelWorkPlaneModel {
  int id;
  int cancel;
  String description;
  CancelWorkPlaneModel({this.id, this.cancel, this.description});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["cancelada"] =  this.cancel;
    data["descripcion"] =  this.description;
    return data;
  }
}

/*
{
    "hora_inicio": "13:30:06",
    "hora_fin": "14:30:06",
    "justificacion": "Architecto ut quos quasi aut deleniti eum assumenda eveniet",
    "fecha_nueva": "2020-03-11",
    "fecha_anterior": "2020-03-09"
}
 */

class RequestPostponementModel {
  int id;
  String startTime;
  String endTime;
  String justification;
  String newDate;
  String beforeDate;

  RequestPostponementModel({this.id, this.startTime, this.endTime, this.justification, this.newDate, this.beforeDate});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["hora_inicio"] =  this.startTime;
    data["hora_fin"] =  this.endTime;
    data["fecha_nueva"] =  this.newDate;
    data["fecha_anterior"] =  this.beforeDate;
    data["justificacion"] =  this.justification;
    return data;
  }
}
