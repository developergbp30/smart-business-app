import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/invitations/invitations_event.dart';
import 'package:smart_business/src/blocs/invitations/invitations_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
import 'package:meta/meta.dart';

class InvitationsBloc extends Bloc<InvitationsEvent, InvitationsState> {
  final InvitationsRepository _invitationsRepository;
  final AuthRepository _authRepository = AuthRepository();

  InvitationsBloc({@required InvitationsRepository invitationsRepository}) :
      assert(invitationsRepository != null), _invitationsRepository = invitationsRepository;

  @override
  InvitationsState get initialState => UnloadInvitationsState();

  @override
  Stream<InvitationsState> mapEventToState(InvitationsEvent event) async* {

   if(event is LoadInvitationsEvent){
     switch(event.type){
       case 0: { yield* _mapLoadInvitationsReceivedToState(event.idUser); }
       break;
       case 1: { yield* _mapLoadInvitationsSendToState(event.idUser); }
     }
   }
   else if(event  is ApprovedInvitationsEvent){
     yield* _mapApprovedInvitationToState(event);
   }

   if(event is RejectInvitationsEvent){
    yield* _mapRejectInvitationToState(event);
   }

   else if(event is CancelInvitationsEvent){
     yield* _mapCancelInvitationToState(event);
   }

  }

  Stream<InvitationsState> _mapLoadInvitationsReceivedToState(int idUser) async* {
    yield LoadingInvitationsState();
    try{
      var value;
      if(idUser == 0){ // if is 0 find for users in session.
        var userSession =  await _authRepository.userData();
        value = userSession.id;
      }else {
        value = idUser;
      }

       var res =  await _invitationsRepository.received(value);
       yield LoadedInvitationsState(invitations: res, type: 1);

    }catch (error){
      yield ErrorInvitationsState(message: error.toString());
    }
  }

  Stream<InvitationsState> _mapLoadInvitationsSendToState(int idUser) async* {
    yield LoadingInvitationsState();
    try{
       var value;

       if(idUser == 0){ // if is 0 find for users in session.
         var userSession =  await _authRepository.userData();
         value = userSession.id;
       }else {
         value = idUser;
       }

       var res =  await _invitationsRepository.send(value);
       yield LoadedInvitationsState(invitations: res, type: 2);

    }catch (error){
      yield ErrorInvitationsState(message: error.toString());
    }
  }

  Stream<InvitationsState> _mapApprovedInvitationToState(ApprovedInvitationsEvent event) async* {
    try{
        var res =  await _invitationsRepository.approved(event.approvedInvitation);
        yield SuccessApprovedInvitationsState("Exitos");
    }catch (error){
      yield ErrorInvitationsState(message: error.toString());
    }
  }

  Stream<InvitationsState> _mapRejectInvitationToState(RejectInvitationsEvent event) async* {
    try{
        var res =  await _invitationsRepository.reject(event.rejectInvitation);
        yield SuccessRejectInvitationsState("Exitos");
    }catch (error){
      yield ErrorInvitationsState(message: error.toString());
    }
  }

  Stream<InvitationsState> _mapCancelInvitationToState(CancelInvitationsEvent event) async* {
    try{
        var res =  await _invitationsRepository.cancel(event.rejectInvitation);
        yield SuccessCancelInvitationsState("Exitos");
    }catch (error){
      yield ErrorInvitationsState(message: error.toString());
    }
  }
}