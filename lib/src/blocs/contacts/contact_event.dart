import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';

abstract class ContactEvent extends Equatable {
  ContactEvent([List props = const []]) : super(props);
}

class LoadContactsEvent extends ContactEvent{
 final int idContact;
 final String type;
 LoadContactsEvent({this.idContact, this.type}) : super([idContact, type]);
}

class SearchContactsEvent extends ContactEvent{
 final String query;
 final List<ContactModel> contacts;
  SearchContactsEvent({this.query, this.contacts}) : super([query, contacts]);

  @override
  String toString() => '$runtimeType {$query ${contacts.length} }';
}

class SearchUserEvent extends ContactEvent{
 final String query;
 final List<UserModel> contacts;
  SearchUserEvent({this.query, this.contacts}) : super([query, contacts]);

  @override
  String toString() => '$runtimeType {$query ${contacts.length} }';
}

class SearchClientContactsEvent extends ContactEvent{
 final String query;
 final List<ClientesContacto> contacts;
  SearchClientContactsEvent({this.query, this.contacts}) : super([query, contacts]);
  @override
  String toString() => '$runtimeType {$query ${contacts.length} }';
}

class ToSelectUserEvent extends ContactEvent{
 final bool selected;
 final int id;
 final List<UserModel> contacts;
  ToSelectUserEvent({this.selected, this.contacts, this.id}) : super([selected, contacts, id]);

  @override
  String toString() => '$runtimeType {$selected ${contacts.length} }';
}

class ToSelectClientContactEvent extends ContactEvent{
 final bool selected;
 final int id;
 final List<ClientesContacto> contacts;
  ToSelectClientContactEvent({this.selected, this.contacts, this.id}) : super([selected, contacts, id]);

  @override
  String toString() => '$runtimeType {$selected ${contacts.length} }';
}