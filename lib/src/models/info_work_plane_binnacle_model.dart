import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/workplane_model.dart';

class InfoWorkPlaneBinnacleModel {
  int id;
  int idUserPrincipal;
  dynamic ptTipo;
  int idPqr;
  int idActa;
  int idActaTema;
  int idBitacora;
  String ptFecha;
  String ptHoraInicio;
  String ptHoraFin;
  dynamic ptAtiempo;
  dynamic ptAprobado;
  String idUserAprobado;
  dynamic ptRechazoJustificacion;
  dynamic ptCancelada;
  dynamic ptCancelaJustificacion;
  String ptAprobadoFecha;
  String titulo;
  String descripcion;
  int idSubtipoActividad;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idPqrRelacionada;
  int idProyecto;
  String bResultados;
  String bFecha;
  String bHoraInicio;
  String bHoraFin;
  dynamic bAtiempo;
  dynamic bManoObra;
  int gtsInformeInfo;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  dynamic tipoActividad;
  String nombreSelect;
  GtsTipos gtsTipos;
  Cliente cliente;

  InfoWorkPlaneBinnacleModel(
      {this.id,
        this.idUserPrincipal,
        this.ptTipo,
        this.idPqr,
        this.idActa,
        this.idActaTema,
        this.idBitacora,
        this.ptFecha,
        this.ptHoraInicio,
        this.ptHoraFin,
        this.ptAtiempo,
        this.ptAprobado,
        this.idUserAprobado,
        this.ptRechazoJustificacion,
        this.ptCancelada,
        this.ptCancelaJustificacion,
        this.ptAprobadoFecha,
        this.titulo,
        this.descripcion,
        this.idSubtipoActividad,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idPqrRelacionada,
        this.idProyecto,
        this.bResultados,
        this.bFecha,
        this.bHoraInicio,
        this.bHoraFin,
        this.bAtiempo,
        this.bManoObra,
        this.gtsInformeInfo,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.tipoActividad,
        this.gtsTipos,
        this.cliente, this.nombreSelect});

  InfoWorkPlaneBinnacleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    ptTipo = json['pt_tipo'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idActaTema = json['id_acta_tema'];
    idBitacora = json['id_bitacora'];
    ptFecha = json['pt_fecha'];
    ptHoraInicio = json['pt_hora_inicio'];
    ptHoraFin = json['pt_hora_fin'];
    ptAtiempo = json['pt_atiempo'];
    ptAprobado = json['pt_aprobado'];
    idUserAprobado = json['id_user_aprobado'];
    ptRechazoJustificacion = json['pt_rechazo_justificacion'];
    ptCancelada = json['pt_cancelada'];
    ptCancelaJustificacion = json['pt_cancela_justificacion'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idPqrRelacionada = json['id_pqr_relacionada'];
    idProyecto = json['id_proyecto'];
    bResultados = json['b_resultados'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    bAtiempo = json['b_atiempo'];
    bManoObra = json['b_mano_obra'];
    gtsInformeInfo = json['gts_informe_info'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    tipoActividad = json['tipo_actividad'];
    nombreSelect = json['nombre_select'];
    gtsTipos = json['gts_tipos'] !=  null ?  new GtsTipos.fromJson(json['gts_tipos']) : null;
    cliente = json['cliente'] !=  null ? new Cliente.fromJson(json['cliente']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['pt_tipo'] = this.ptTipo;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_acta_tema'] = this.idActaTema;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['pt_hora_inicio'] = this.ptHoraInicio;
    data['pt_hora_fin'] = this.ptHoraFin;
    data['pt_atiempo'] = this.ptAtiempo;
    data['pt_aprobado'] = this.ptAprobado;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_rechazo_justificacion'] = this.ptRechazoJustificacion;
    data['pt_cancelada'] = this.ptCancelada;
    data['pt_cancela_justificacion'] = this.ptCancelaJustificacion;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_pqr_relacionada'] = this.idPqrRelacionada;
    data['id_proyecto'] = this.idProyecto;
    data['b_resultados'] = this.bResultados;
    data['b_fecha'] = this.bFecha;
    data['b_hora_inicio'] = this.bHoraInicio;
    data['b_hora_fin'] = this.bHoraFin;
    data['b_atiempo'] = this.bAtiempo;
    data['b_mano_obra'] = this.bManoObra;
    data['gts_informe_info'] = this.gtsInformeInfo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['tipo_actividad'] = this.tipoActividad;
    data['gts_tipos'] = this.gtsTipos;
    data['cliente'] = this.cliente;
    return data;
  }
}
