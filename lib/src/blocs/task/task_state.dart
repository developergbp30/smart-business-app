import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/actas_model.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/models/overdue_task_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

abstract class TaskState extends Equatable {
  TaskState([List props = const []]) : super(props);
}

class UnloadedTaskState extends TaskState{}

class SendingRequestTaskState extends TaskState{}

class LoadedTaskDefeatedState extends TaskState{
  final List<OverdueTaskModel> task;
  LoadedTaskDefeatedState({this.task}) :  super([task]);

  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskDefeatedBinnacleState extends TaskState{
  final List<TaskOverdueBinnacleModel> task;
  LoadedTaskDefeatedBinnacleState({this.task}) :  super([task]);

  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskValidBinnacleState extends TaskState{
  final List<TaskOverdueBinnacleModel> task;
  LoadedTaskValidBinnacleState({this.task}) :  super([task]);

  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskPqrState extends TaskState {
  final List<TaskPqrModel> task;
  LoadedTaskPqrState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskPqrBinnacleState extends TaskState {
  final List<TaskPqrBinnacleModel> task;
  LoadedTaskPqrBinnacleState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskProjectState extends TaskState {
  final List<TaskProjectModel> task;
  LoadedTaskProjectState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskProjectBinnacleState extends TaskState {
  final List<TaskProjectModel> task;
  LoadedTaskProjectBinnacleState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskOpsState extends TaskState {
  final List<TaskOpportunityModel> task;
  LoadedTaskOpsState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskOpsBinnacleState extends TaskState {
  final List<TaskOpportunityModel> task;
  LoadedTaskOpsBinnacleState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskEventState extends TaskState {
  final List<TaskEventModel> task;
  LoadedTaskEventState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskEventBinnacleState extends TaskState {
  final List<TaskEventModel> task;
  LoadedTaskEventBinnacleState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskActasState extends TaskState {
  final List<TaskActaModel> task;
  LoadedTaskActasState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class LoadedTaskActasBinnacleState extends TaskState {
  final List<TaskActaModel> task;
  LoadedTaskActasBinnacleState({this.task}) : super([task]);
  @override
  String toString() => '$runtimeType { task $task}';
}

class ErrorTaskState extends TaskState{
  final String message;
  ErrorTaskState({this.message}) : super([message]);
}

class DetailTaskState extends TaskState{
  final WorkPlaneModel detail;
  DetailTaskState({this.detail}) : super([detail]);

  @override
  String toString() => '$runtimeType { detail: $detail}';
}

class  DetailActaState extends TaskState{
  final ActaParcialModel detail;
  DetailActaState({this.detail}) : super([detail]);

  @override
  String toString() => '$runtimeType { detail: $detail}';
}

