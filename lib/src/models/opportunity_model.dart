class OpportunityModel {
  int sid;
  String name;

  OpportunityModel({this.sid, this.name});
}

class ResponseOpportunityModel {
  int value;
  String label;
  String precioTotal;
  dynamic estadoFinal;
  int idCliente;
  Clientes clientes;
 // List<Null> opsCiclos;

  ResponseOpportunityModel(
      {this.value,
        this.label,
        this.precioTotal,
        this.estadoFinal,
        this.idCliente,
        this.clientes,
       // this.opsCiclos
      });

  ResponseOpportunityModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    label = json['label'];
    precioTotal = json['precio_total'];
    estadoFinal = json['estado_final'];
    idCliente = json['id_cliente'];
    clientes = json['clientes'] != null
        ? new Clientes.fromJson(json['clientes'])
        : null;
    /*if (json['ops_ciclos'] != null) {
      opsCiclos = new List<Null>();
      json['ops_ciclos'].forEach((v) {
        opsCiclos.add(new Null.fromJson(v));
      });
    }*/
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['label'] = this.label;
    data['precio_total'] = this.precioTotal;
    data['estado_final'] = this.estadoFinal;
    data['id_cliente'] = this.idCliente;
    if (this.clientes != null) {
      data['clientes'] = this.clientes.toJson();
    }
    /*if (this.opsCiclos != null) {
      data['ops_ciclos'] = this.opsCiclos.map((v) => v.toJson()).toList();
    }*/
    return data;
  }
}

class Clientes {
  int id;
  String nombreCorto;
  String logo;

  Clientes({this.id, this.nombreCorto, this.logo});

  Clientes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombreCorto = json['nombre_corto'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre_corto'] = this.nombreCorto;
    data['logo'] = this.logo;
    return data;
  }
}


class TaskOpportunityModel {
  int id;
  String titulo;
  String precioTotal;
  Null estadoFinal;
  int idCliente;
  Clientes clientes;
  List<OpsCiclos> opsCiclos;

  TaskOpportunityModel(
      {this.id,
        this.titulo,
        this.precioTotal,
        this.estadoFinal,
        this.idCliente,
        this.clientes,
        this.opsCiclos});

  TaskOpportunityModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titulo = json['titulo'];
    precioTotal = json['precio_total'];
    estadoFinal = json['estado_final'];
    idCliente = json['id_cliente'];
    clientes = json['clientes'] != null
        ? new Clientes.fromJson(json['clientes'])
        : null;
    if (json['ops_ciclos'] != null) {
      opsCiclos = new List<OpsCiclos>();
      json['ops_ciclos'].forEach((v) {
        opsCiclos.add(new OpsCiclos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['titulo'] = this.titulo;
    data['precio_total'] = this.precioTotal;
    data['estado_final'] = this.estadoFinal;
    data['id_cliente'] = this.idCliente;
    if (this.clientes != null) {
      data['clientes'] = this.clientes.toJson();
    }
    if (this.opsCiclos != null) {
      data['ops_ciclos'] = this.opsCiclos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OpsCiclos {
  int id;
  int idCiclo;
  int estado;
  String justificacion;
  int idOportunidad;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  OportunidadesCiclos oportunidadesCiclos;

  OpsCiclos(
      {this.id,
        this.idCiclo,
        this.estado,
        this.justificacion,
        this.idOportunidad,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.oportunidadesCiclos});

  OpsCiclos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idCiclo = json['id_ciclo'];
    estado = json['estado'];
    justificacion = json['justificacion'];
    idOportunidad = json['id_oportunidad'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    oportunidadesCiclos = json['oportunidades_ciclos'] != null
        ? new OportunidadesCiclos.fromJson(json['oportunidades_ciclos'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_ciclo'] = this.idCiclo;
    data['estado'] = this.estado;
    data['justificacion'] = this.justificacion;
    data['id_oportunidad'] = this.idOportunidad;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.oportunidadesCiclos != null) {
      data['oportunidades_ciclos'] = this.oportunidadesCiclos.toJson();
    }
    return data;
  }
}

class OportunidadesCiclos {
  int id;
  String nombre;

  OportunidadesCiclos({this.id, this.nombre});

  OportunidadesCiclos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    return data;
  }
}
