class CreateExpensesModel {
  Unique unique;
  Values values;
  int idUser;

  CreateExpensesModel({this.unique, this.values, this.idUser});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.unique != null) {
      data['unique'] = this.unique.toJson();
    }
    if (this.values != null) {
      data['values'] = this.values.toJson();
    }
    data['id_user'] = this.idUser;
    return data;
  }
}

class Unique {
  int id;

  Unique({this.id});

  Unique.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    return data;
  }
}

class Values {
  String titulo;
  String descripcion;
  String fecha;
  String adjunto;
  String adjuntoNombre;
  int idTipo;
  int valorSolicitado;
  int idGts;

  Values(
      {this.titulo,
        this.descripcion,
        this.fecha,
        this.adjunto,
        this.adjuntoNombre,
        this.idTipo,
        this.valorSolicitado, this.idGts});


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['fecha'] = this.fecha;
    data['adjunto'] = this.adjunto;
    data['adjunto_nombre'] = this.adjuntoNombre;
    data['id_tipo'] = this.idTipo;
    data['valor_solicitado'] = this.valorSolicitado;
    data['id_gts'] = this.idGts;
    return data;
  }
}
