import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_bloc.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_event.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_state.dart';
import 'package:smart_business/src/blocs/workplan/workplan_bloc.dart';
import 'package:smart_business/src/blocs/workplan/workplan_event.dart';
import 'package:smart_business/src/blocs/workplan/workplan_state.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/client_model.dart';
import 'package:smart_business/src/models/competitions_model.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/guest_user_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';
import 'package:smart_business/src/models/time_params_model.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';
import 'package:smart_business/src/presentation/screens/contacts/contacts_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/repositories/workplan/workplan_repository.dart';
class EditActivityPage extends StatefulWidget {
  WorkPlaneModel args;
  List<RequestContactModel> listContacts;
  List<GuestUserModel> listGuest;
  List<GuestUserModel> listEmails;

  EditActivityPage({@required this.args, this.listContacts, this.listGuest, this.listEmails});

  @override
  _EditActivityPageState createState() => _EditActivityPageState();
}

class _EditActivityPageState extends State<EditActivityPage> {
  ThemeApp _themeApp = ThemeApp();
  List<ClientesContacto> addContacts;
  final _formKey =  GlobalKey<FormState>();
  //Blocs
  WorkPlanBloc _workPlanBloc;
  SelectActivityBloc _selectActivityBloc;


  var dateActivity;

  String optionTypeActivity;

  String timeInitial;

  ResponseActivityModel typeActivity;

  ResponseActivityModel subTypeActivity;

  String labelThirdSelect;

  CreateActivityModel createActivityModel;
  TextEditingController titleCtrl;
  TextEditingController descriptionCtrl;
  TextEditingController emailExternalCtrl;

  String endTime;

  List<String> listExternalEmail = [];

  bool showButtonAddContact =  false;

  int idClientContact;

  List<UserModel> userGuest;

  List<UserModel> informativeEmail;

  TimeParamsModel _timeParamsModel;

  int idPqr;
  int idOpp;
  int idActa;
  int idEvent;
  int idComp;
  int idProject;
  int idClient;

  DateTime maximumDate; //
  DateTime toTime; //

  bool banSetSubtypes =  false;

  String labelSelectDynamic;

  bool disabledModalDynamic =  false;

  @override
  void initState() {
    createActivityModel = CreateActivityModel();
    createActivityModel.time = new List<String>(2);
    createActivityModel.myContact = [];

    titleCtrl  = new TextEditingController();
    descriptionCtrl  = new TextEditingController();
    emailExternalCtrl  = new TextEditingController();

    setValues();

    _workPlanBloc = WorkPlanBloc(workPlanRepository: WorkPlanRepository());
    //_workPlanBloc.dispatch(DataForSelectsEvent());
    // --> Bloc for selects (Type Activity, SubType Activity, Dynamic Select{ Opportunities, Pqr, Events, Projects, etc}
    _selectActivityBloc =
        SelectActivityBloc(workPlanRepository: WorkPlanRepository());
    _selectActivityBloc.dispatch(LoadDataForSelectsEvent());


    super.initState();
  }

  void setValues(){//setting values of default
    titleCtrl.value = TextEditingValue(text: widget.args.titulo);// title
    descriptionCtrl.value = TextEditingValue(text: widget.args.descripcion); //description
    //Set Sub Activity
    if(widget.args.gtsTipos !=  null){
      subTypeActivity =  new ResponseActivityModel(nombre: widget.args.gtsTipos.nombre, id: widget.args.gtsTipos.id);
      createActivityModel.activitySubType = widget.args.gtsTipos.id;
    }

    labelSelectDynamic = widget.args.nombreCodigo !=  null ? widget.args.nombreCodigo.nombre : "";
    //Set Date in var and model.
    dateActivity = DateTime.parse(widget.args.fecha);//Date of Activity
    createActivityModel.date = DateTime.parse(widget.args.fecha);// Date of Activity in model
    // times in model and vars for screen
    createActivityModel.time = [widget.args.horaInicio, widget.args.horaFin];
    timeInitial = widget.args.horaInicio;
    endTime = widget.args.horaFin;
    // fin set  times
    // Set ids
    createActivityModel.idUser = widget.args.idUserPrincipal;
    idClient = widget.args.idCliente;
    idPqr = widget.args.idPqr;
    idOpp =  widget.args.idOportunidad;
    idActa = widget.args.idActa;
    idEvent = widget.args.idEvento;
    idComp = widget.args.idCompetencia;
    idProject = widget.args.idProyecto;

    print(widget.args.toJson());
    if(widget.args.idCliente !=  null){
      showButtonAddContact =  true;
      idClientContact = widget.args.idCliente;
    }
    // Set list emails external
    informativeEmail = [];
    if(widget.listEmails !=  null){
      widget.listEmails.forEach((item){
        if(item.datosInvitados == null) listExternalEmail.add(item.correoExterno);
        else if(item.datosInvitados != null){
          var value  =  UserModel(id: item.id, nombre: item.datosInvitados.nombre, foto: item.datosInvitados.foto, fotoMiniatura: item.datosInvitados.fotoMiniatura);
          informativeEmail.add(value);
        }
      });
    }
    // Set Guest users
    userGuest = [];
    if( widget.listGuest !=  null ){
      widget.listGuest.forEach((item) {
        var value  =  UserModel(id: item.id, nombre: item.datosInvitados.nombre, foto: item.datosInvitados.foto, fotoMiniatura: item.datosInvitados.fotoMiniatura);
        userGuest.add(value);
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(icon: Icon(Icons.clear, color: _themeApp.colorPink,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Crear Actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: _themeApp.colorPink),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: _themeApp.colorPink,), onPressed: (){
          _registerActivity();
          })
        ],
      ),
      body: MultiBlocListener(
        listeners: [
          BlocListener(
              bloc: _workPlanBloc,
              listener: (context, state){

                if(state is SuccessRegisterActivityState){
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text("${state.message}"), backgroundColor: Colors.green,));
                  Future.delayed(Duration(seconds: 2)).then((time){
                    Navigator.of(context).pop(true);
                  });

                }
                if(state is WarningActivityState){
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text("${state.message}", style: TextStyle(color: Colors.black),), backgroundColor: Colors.yellowAccent,));
                }
                if(state is ErrorRegisterActivityState){
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text("Ha ocurrido un error"), backgroundColor: Colors.red,));
                }
              }),
          BlocListener(
            bloc: _selectActivityBloc,
            listener: (context, state){

              if(state is DataForSelectState){
                if(!banSetSubtypes){
                  banSetSubtypes =  true;
                  var selected =  state.activities.where((item)=> item.nombre == widget.args.tipoActividad).toList().first;
                  typeActivity = ResponseActivityModel(nombre:  selected.nombre, id: selected.id);
                  createActivityModel.activityType = selected.nombre;
                  setState(() {});
                  _selectActivityBloc.dispatch(FindSubTypeActivity(activities: state.activities, selected: selected));
                }

                if(state.timeParams !=  null){
                  _timeParamsModel = state.timeParams;
                  maximumDate = DateTime.now().add(Duration(days: _timeParamsModel.ptPermitido));
                  toTime =  DateTime.now().add(Duration(days: _timeParamsModel.ptTiempo));
                }

              }
            },
          ),
        ],
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: <Widget>[
                  SizedBox(height: 20,),
                  Text("Actividad", style: TextStyle(color: Colors.grey[700]),),
                  TextFormField(
                    controller: titleCtrl,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey,
                              width: 5.0
                          )
                      ),
                      hintText: 'Ingrese el titulo de la actividad.',
                      // labelText: 'Actividad'
                    ),
                    validator: (value){
                      if(value.isEmpty){
                        return 'Ingrese el titulo';
                      }
                      return null;
                    },
                    onSaved: (value){
                      // createActivityModel.title = value;
                    },
                  ),
                  SizedBox(height: 20,),
                  Text("Descripción", style: TextStyle(color: Colors.grey[700]),),
                  TextFormField(
                    controller: descriptionCtrl,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey,
                              width: 5.0
                          )
                      ),
                      hintText: 'Ingresa la descipción',
                      // labelText: 'Descripcion'
                    ),
                    validator: (value){
                      if(value.isEmpty){
                        return 'Ingrese la descripción';
                      }
                      return null;
                    },
                    onSaved: (value){
                      // createActivityModel.description =  value;
                    },
                  ),
                  SizedBox(height: 20,),
                  Divider(),
                  SizedBox(height: 20,),
                  Text("Tipo de Actividad", style: TextStyle(color: Colors.grey[700]),),
                  InkWell(
                    child:  Container(
                      height: 40,
                      decoration: new BoxDecoration(
                          color: Colors.grey[350],
                          border: Border.all(color: Colors.grey[500], width: 1),
                          borderRadius: new BorderRadius.circular(5.0)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${typeActivity != null ? typeActivity.nombre :""}"),
                            Icon(Icons.expand_more)
                          ],
                        ),
                      ),
                    ),
                    onTap: (){

                      _modalTypeActivities(context);
                      /*showCupertinoModalPopup(context: context, builder: (context){
                          return actionSheetActivity(context);
                        });*/
                    },
                  ),
                  SizedBox(height: 20,),
                  Text("Subtipo de Actividad", style: TextStyle(color: Colors.grey[700]),),
                  InkWell(
                    child:  Container(
                      height: 40,
                      decoration: new BoxDecoration(
                          color: Colors.grey[350],
                          border: Border.all(color: Colors.grey[500], width: 1),
                          borderRadius: new BorderRadius.circular(5.0)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${subTypeActivity !=  null ?  subTypeActivity.nombre : ""}"),
                            Icon(Icons.expand_more)
                          ],
                        ),
                      ),
                    ),
                    onTap: (){

                      _modalSubTypeActivities(context);
                      /*showCupertinoModalPopup(context: context, builder: (context){
                          return actionSheetActivity(context);
                        });*/
                    },
                  ),
                  SizedBox(height: 20,),
                  _renderSelectDynamic(),
                  _dividerAndBox(),
                  Text("Fecha", style: TextStyle(color: Colors.grey[700]),),
                  InkWell(
                    child:  Container(
                      height: 40,
                      decoration: new BoxDecoration(
                          color: Colors.grey[350],
                          border: Border.all(color: Colors.grey[500], width: 1),
                          borderRadius: new BorderRadius.circular(5.0)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${dateActivity !=  null ? DateFormat.yMd().format(dateActivity) : ""}"),
                            Icon(Icons.expand_more)
                          ],
                        ),
                      ),
                    ),
                    onTap: (){

                      _showDatePicker(context, (value){
                        setState(() {
                          dateActivity = value;
                          createActivityModel.date = value;
                        });
                      });
                    },
                  ),
                  SizedBox(height: 20,),
                  Text("Hora de inicio", style: TextStyle(color: Colors.grey[700]),),
                  InkWell(
                    child:  Container(
                      height: 40,
                      decoration: new BoxDecoration(
                          color: Colors.grey[350],
                          border: Border.all(color: Colors.grey[500], width: 1),
                          borderRadius: new BorderRadius.circular(5.0)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${timeInitial !=  null ?  timeInitial : widget.args.horaInicio}"),
                            Icon(Icons.expand_more)
                          ],
                        ),
                      ),
                    ),
                    onTap: (){

                      _showTimePicker(context, (value){
                        print(value);
                        timeInitial = DateFormat.Hm().format(value);
                        createActivityModel.time[0] = DateFormat.Hms().format(value);
                        setState(() {});
                      });
                    },
                  ),
                  SizedBox(height: 20,),
                  Text("Hora de finalización", style: TextStyle(color: Colors.grey[700]),),
                  InkWell(
                    child:  Container(
                      height: 40,
                      decoration: new BoxDecoration(
                          color: Colors.grey[350],
                          border: Border.all(color: Colors.grey[500], width: 1),
                          borderRadius: new BorderRadius.circular(5.0)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("${endTime !=  null ? endTime : widget.args.horaFin}"),
                            Icon(Icons.expand_more)
                          ],
                        ),
                      ),
                    ),
                    onTap: (){
                      _showTimePicker(context,  (value){
                        print(value);
                        endTime = DateFormat.Hm().format(value);
                        createActivityModel.time[1] = DateFormat.Hms().format(value);
                        setState(() {});
                      });
                    },
                  ),
                  _dividerAndBox(),
                  showButtonAddContact ? Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 20, right: 20),
                    color: Colors.transparent,
                    padding: const EdgeInsets.all(8),
                    child: FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.person_add,color: _themeApp.colorPink),
                            Text(" Añadir contacto", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          var res = await Navigator.push(context, new MaterialPageRoute<BackToPageContactsModel>(builder: (context) => ContactsPage(idContact: idClientContact,type: "contact-client",)));
                          print("contactos $res");
                          if(res !=  null && res.clientContacts != null){
                            print("contactos");
                            print(res.clientContacts.first);
                           // if(createActivityModel.myContact !=  null && createActivityModel.myContact.length > 0) createActivityModel.myContact.clear();
                            res.clientContacts.forEach((item) {
                              addContacts.removeWhere((i) => i.id == item.id);
                              addContacts.add(item);

                              createActivityModel.myContact.add(item.id);
                            });
                            setState(() {

                            });
                          }
                        }),
                  ) : SizedBox(),
                  Wrap(
                      children:  _buildContactsSelected()
                  ),
                  // add invitations
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 20, right: 20),
                    color: Colors.transparent,
                    padding: const EdgeInsets.all(8),
                    child: FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(FontAwesomeIcons.solidEnvelope,color: _themeApp.colorPink),
                            Text(" Añadir Enviar invitaciones", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          var res = await Navigator.push(context, new MaterialPageRoute<BackToPageContactsModel>(builder: (context) => ContactsPage(type: "users",)));
                          if(res != null){
                            res.users.forEach((item){
                              userGuest.removeWhere((i) => i.id == item.id);
                              userGuest.add(item);
                            });
                            setState(() {

                            });
                          }
                        }),
                  ),
                  Wrap(
                      children:  _buildInvitationsSelected()
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(left: 20, right: 20),
                    color: Colors.transparent,
                    padding: const EdgeInsets.all(8),
                    child: FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(FontAwesomeIcons.solidEnvelope,color: _themeApp.colorPink),
                            Text("! Enviar correo informativo", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          var res = await Navigator.push(context, new MaterialPageRoute<BackToPageContactsModel>(builder: (context) => ContactsPage(type: "users",)));
                          if(res != null){
                            res.users.forEach((item){
                              informativeEmail.removeWhere((i) => i.id == item.id);
                              informativeEmail.add(item);
                            });
                            setState(() {
                            });
                          }
                        }),
                  ),
                  Wrap(
                      children:  _buildNotificationEmailSelected()
                  ),
                  _addExternalEmailWidget()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildContactsSelected(){
    List<Widget> _widget = [];
    if(addContacts != null && addContacts.length > 0) {
      for(var i = 0; i <addContacts.length; i++){
        _widget.add(Chip(
          onDeleted:(){
            addContacts.remove(addContacts[i]);
            setState(() {});
          },
          avatar: CircleAvatar(
            backgroundImage: loadImageApi(addContacts[i].contacto.foto, ""),
            // child: ,
          ),
          label:Text("${addContacts[i].contacto.nombre}"),
        ));
      }
    }
    // if exist contacts
    else if(widget.listContacts != null && widget.listContacts.length > 0){
      for(var i = 0; i <widget.listContacts.length; i++){
        _widget.add(Chip(

          avatar: CircleAvatar(
            backgroundImage: loadImageApi(widget.listContacts[i].foto, ""),
            // child: ,
          ),
          label:Text("${widget.listContacts[i].nombre}"),
        ));
      }
    }


    return _widget;
  }

  List<Widget> _buildInvitationsSelected(){
    List<Widget> _widget = [];
    if(userGuest != null && userGuest.length > 0) {
      for(var i = 0; i <userGuest.length; i++){
        _widget.add(Chip(
            onDeleted:(){
              userGuest.remove(userGuest[i]);
              setState(() {});
            },
          avatar: CircleAvatar(
            backgroundImage: loadImageApi(userGuest[i].foto, ""),
            // child: ,
          ),
          label:Text("${userGuest[i].nombre}"),
        ));
      }
    }
    return _widget;
  }


  List<Widget> _buildNotificationEmailSelected(){
    List<Widget> _widget = [];
    if(informativeEmail != null && informativeEmail.length > 0) {
      for(var i = 0; i <informativeEmail.length; i++){
        _widget.add(Chip(
          onDeleted:(){
            informativeEmail.remove(informativeEmail[i]);
            setState(() {});
          },
          avatar: CircleAvatar(
            backgroundImage: loadImageApi(informativeEmail[i].foto, ""),
            // child: ,
          ),
          label:Text("${informativeEmail[i].nombre}"),
        ));
      }
    }
    return _widget;
  }

  Widget _addExternalEmailWidget(){
    return Container(
      height: 300,
      margin: const EdgeInsets.only(top: 5),
      child: Column(
        children: <Widget>[
          Text("Añadir correo electronico externo"),
          SizedBox(height: 5,),
          Row(
            children: <Widget>[
              Flexible(
                child: TextFormField(
                  controller: emailExternalCtrl,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey,
                            width: 5.0
                        )
                    ),
                    hintText: 'Preparar presentación',
                    // labelText: 'Actividad'
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.add_box, color: _themeApp.colorPink, size: 30,),
                onPressed: (){
                  if(emailExternalCtrl.text!= null && emailExternalCtrl.text.length > 0){
                    listExternalEmail.add(emailExternalCtrl.text);
                    emailExternalCtrl.clear();
                    setState(() {});

                  }
                },
              )
            ],
          ),
          Expanded(
            child:  ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: listExternalEmail.length,
                itemBuilder: (context, i) {
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      child: Chip(
                          onDeleted:(){
                            listExternalEmail.remove(listExternalEmail[i]);
                            setState(() {});
                          },
                        label: Text(listExternalEmail[i]),));
                }),
          )
        ],
      ),
    );
  }

  Widget _dividerAndBox() {
    return Column(
      children: <Widget>[
        SizedBox(height: 20,),
        Divider(),
        SizedBox(height: 20,),
      ],
    );
  }

  Color _itemSelected(firstItem, selectItem){
    if(selectItem == null) return Colors.black;
    else {
      if(firstItem == selectItem) return ThemeApp().colorPink;
      else return Colors.black;
    }
  }

  void _modalTypeActivities(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                        "Tipo de actividad",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      )),
                ),
                Expanded(
                  child: BlocBuilder<SelectActivityBloc, SelectActivityState>(
                    bloc: _selectActivityBloc,
                    builder: (context, state) {
                      if (state is DataForSelectState) {
                        return ListView.builder(
                            itemCount: state.activities.length,
                            itemBuilder: (context, i) => ListTile(
                              title: Text(state.activities[i].nombre),
                              onTap: () {
                                typeActivity = state.activities[i];
                                subTypeActivity = null;
                                labelSelectDynamic = null;
                                setState(() {});
                                _selectActivityBloc.dispatch(
                                    FindSubTypeActivity(
                                        activities: state.activities,
                                        selected: state.activities[i]));
                                Navigator.of(context).pop();
                              },
                            ));
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  void _modalSubTypeActivities(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                        "Subtipo de actividad",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      )),
                ),
                Expanded(
                  child: BlocBuilder<SelectActivityBloc, SelectActivityState>(
                    bloc: _selectActivityBloc,
                    builder: (context, state) {
                      if (state is DataForSelectState) {
                        if (state.subTypeActivities == null) return SizedBox();
                        return ListView.builder(
                            itemCount: state.subTypeActivities.length,
                            itemBuilder: (context, i) => ListTile(
                              title:
                              Text(state.subTypeActivities[i].nombre),
                              onTap: () {
                                subTypeActivity =
                                state.subTypeActivities[i];
                                setState(() {});
                                Navigator.of(context).pop();
                              },
                            ));
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _renderSelectDynamic() {
    return BlocBuilder<SelectActivityBloc, SelectActivityState>(
      bloc: _selectActivityBloc,
      builder: (context, state) {
        if (state is DataForSelectState) {
          return state.showThirdSelect != null &&
              state.showThirdSelect &&
              state.thirdSelect != null
              ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                "${state.thirdSelect.label}",
                style: TextStyle(color: Colors.grey[700]),
              ),
              InkWell(
                child: Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border:
                      Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Text(
                              "${labelSelectDynamic != null ? labelSelectDynamic : ""}"),
                        ),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  if (!disabledModalDynamic) _modalSelectDynamic(context);
                },
              ),
            ],
          )
              : Container(
            height: 20,
          );
        }

        return Container(
          height: 20,
        );
      },
    );
  }

  void _modalSelectDynamic(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                        "Subtipo de actividad",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      )),
                ),
                Expanded(
                  child: BlocBuilder<SelectActivityBloc, SelectActivityState>(
                    bloc: _selectActivityBloc,
                    builder: (context, state) {
                      if (state is DataForSelectState) {
                        if (state.showThirdSelect &&
                            state.thirdSelect != null) {
                          print(state.thirdSelect.type);
                          if (state.thirdSelect.type == 1 ||
                              state.thirdSelect.type == 2 ||
                              state.thirdSelect.type == 3) {
                            print(state.thirdSelect.data);
                            return _subRenderListOpportunities(
                                state.thirdSelect.data);
                          } else if (state.thirdSelect.type == 4) {
                            return _subRenderListPqr(state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 5) {
                            return _subRenderListEvents(state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 6) {
                            return _subRenderListClient(state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 7) {
                            return _subRenderListCompetition(
                                state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 8) {
                            return _subRenderListProject(
                                state.thirdSelect.data);
                          }
                          return SizedBox();
                        }
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  _subRenderListOpportunities(List<ResponseOpportunityModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
          title: Text(item[i].label),
          onTap: () {
            labelSelectDynamic = item[i].label;
            _setIds(item[i].value, 'ops');
            setState(() {});
            Navigator.of(context).pop();
          },
        ));
  }

  _subRenderListPqr(List<ResponsePqrModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
          title: Text(item[i].label),
          onTap: () {
            labelSelectDynamic = item[i].label;
            _setIds(item[i].value, 'pqr');
            if (item[i].clientes != null) {
              showButtonAddContact = true;
              idClientContact = item[i].clientes.id;
              //_setIds(item[i].clientes.id, 'client');
              _setIds(item[i].value, 'pqr');
            } else {
              showButtonAddContact = false;
              idClientContact = null;
            }
            setState(() {});
            Navigator.of(context).pop();
          },
        ));
  }

  _subRenderListEvents(List<ResponseEventModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
          title: Text(item[i].label),
          onTap: () {
            labelSelectDynamic = item[i].label;
            _setIds(item[i].value, 'event');
            setState(() {});
            Navigator.of(context).pop();
          },
        ));
  }

  _subRenderListClient(List<ResponseClientModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
          title: Text(item[i].label),
          onTap: () {
            labelSelectDynamic = item[i].label;
            _setIds(item[i].value, 'client');
            setState(() {});
            Navigator.of(context).pop();
          },
        ));
  }

  _subRenderListCompetition(List<ResponseCompetitionModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
          title: Text(item[i].label),
          onTap: () {
            labelSelectDynamic = item[i].label;
            _setIds(item[i].value, 'comp');
            setState(() {});
            Navigator.of(context).pop();
          },
        ));
  }

  _subRenderListProject(List<ResponseProjectModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
          title: Text(item[i].label),
          onTap: () {
            labelSelectDynamic = item[i].label;
            _setIds(item[i].value, 'project');
            setState(() {});
            Navigator.of(context).pop();
          },
        ));
  }

  void _showDatePicker(context, fChangeTime){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar fecha", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    maximumDate: maximumDate,
                    onDateTimeChanged: (value){

                      fChangeTime(value);

                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }

  void _showTimePicker(context, fChangeTime){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar hora", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.time,
                    onDateTimeChanged: (value){
                      fChangeTime(value);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          setState(() {});
                          Navigator.of(context).pop();
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          setState(() {});
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }



  void _setIds(int value, type){
    for(var i = 0 ; i< 5; i++ ){
      if(type == "client"){
        idClient =  value;
      }else{
        idClient =  null;
      }
      if(type == "ops"){
        idOpp =  value;
      }else{
        idOpp =  null;
      }
      if(type == "pqr"){
        idPqr =  value;
      }else{
        idPqr =  null;
      }
      if(type == "event"){
        idEvent =  value;
      }else{
        idEvent =  null;
      }
      if(type == "project"){
        idProject =  value;
      }else{
        idProject =  null;
      }
      if(type == "comp"){
        idComp =  value;
      }else{
        idComp =  null;
      }
      if(type == "acta"){
        idActa =  value;
      }else{
        idActa =  null;
      }
    }
  }

  void _registerActivity(){
    if(_formKey.currentState.validate()){
      _formKey.currentState.save();
      createActivityModel.id = widget.args.id;
      createActivityModel.title =  titleCtrl.text;
      createActivityModel.description =  descriptionCtrl.text;
      createActivityModel.myInformed = informativeEmail;
      createActivityModel.myGuest = userGuest;
      createActivityModel.myExternal = listExternalEmail;
      createActivityModel.idActa =  idActa;
      createActivityModel.idProject =  idProject;
      createActivityModel.idPqr =  idPqr;
      createActivityModel.idCompetitions =  idComp;
      createActivityModel.idEvents =  idEvent;
      createActivityModel.idOpportunity =  idOpp;
      createActivityModel.idClient =  idClientContact !=  null ? idClientContact : idClient != null ? idClient :  null;
      if(addContacts !=  null){
        addContacts.forEach((item) => createActivityModel.myContact.add(item.id));
      }
      // validate diff date
      var dif = toTime.difference(createActivityModel.date).inDays;
      if(dif >= _timeParamsModel.ptTiempo){
        createActivityModel.ptAtiempo = 1;
      }else {
        createActivityModel.ptAtiempo = 0;
      }
      createActivityModel.log();
      print(createActivityModel.toJson());
      _workPlanBloc.dispatch(SendRegisterActivityEvent(data: createActivityModel));
    }
  }
}
