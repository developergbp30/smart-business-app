class ClientModel {
  int id;
  String nombreCorto;
  String nombreLegal;
  String ciudad;
  int tamano;
  String formulario;
  int oportunidades;
  String valorOportunidades;
  int pqrs;
  int antiguedad;
  String tipoCliente;

  ClientModel(
      {this.id,
        this.nombreCorto,
        this.nombreLegal,
        this.ciudad,
        this.tamano,
        this.formulario,
        this.oportunidades,
        this.valorOportunidades,
        this.pqrs,
        this.antiguedad,
        this.tipoCliente});

  ClientModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombreCorto = json['nombre_corto'];
    nombreLegal = json['nombre_legal'];
    ciudad = json['ciudad'];
    tamano = json['tamano'];
    formulario = json['formulario'];
    oportunidades = json['oportunidades'];
    valorOportunidades = json['valor_oportunidades'];
    pqrs = json['pqrs'];
    antiguedad = json['antiguedad'];
    tipoCliente = json['Tipo Cliente'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre_corto'] = this.nombreCorto;
    data['nombre_legal'] = this.nombreLegal;
    data['ciudad'] = this.ciudad;
    data['tamano'] = this.tamano;
    data['formulario'] = this.formulario;
    data['oportunidades'] = this.oportunidades;
    data['valor_oportunidades'] = this.valorOportunidades;
    data['pqrs'] = this.pqrs;
    data['antiguedad'] = this.antiguedad;
    data['Tipo Cliente'] = this.tipoCliente;
    return data;
  }
}

class ResponseClientModel {
  int value;
  String label;

  ResponseClientModel({this.value, this.label});

  ResponseClientModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['label'] = this.label;
    return data;
  }
}

