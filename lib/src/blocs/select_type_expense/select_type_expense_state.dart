import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/expenses_model.dart';

abstract class SelectExpenseState extends Equatable {
  SelectExpenseState([List props = const []]) : super(props);
}

class UnloadDataSelectExpenseState extends SelectExpenseState{}

class ListTypeExpenseState extends SelectExpenseState{
   final List<ExpenseTypeModel> types;
   ListTypeExpenseState({this.types}) : super([types]);
}

class SelectedTypeExpenseState extends SelectExpenseState{

}