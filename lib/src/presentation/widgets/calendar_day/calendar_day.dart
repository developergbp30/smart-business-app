import 'package:flutter/material.dart';
class CalendarDay extends StatelessWidget {
  final String month;
  final String day;
  final Color headerColor;
  CalendarDay({this.month, this.day, this.headerColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
                color: headerColor !=  null ? headerColor: Colors.red,
                borderRadius: new BorderRadius.circular(3.0)
            ),
            width: 35,
            padding: const EdgeInsets.all(1),
            child: Text("${ month == null ? "Feb" :  month}", style: TextStyle(color: Colors.white),),),
          Container(
            padding: const EdgeInsets.all(1),
            width: 35,
            decoration: new BoxDecoration(
                color: Colors.grey.withOpacity(0.2),
                borderRadius: new BorderRadius.only(bottomLeft:Radius.circular(2.0) , bottomRight: Radius.circular(2.0))
            ),
            child: Text("${ day == null ? "21" : day}", textAlign: TextAlign.center, style: TextStyle(color: Colors.black54, fontSize: 20, fontWeight: FontWeight.bold),),),
        ],
      ),
    );
  }
}
