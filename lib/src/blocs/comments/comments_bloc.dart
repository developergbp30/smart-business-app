import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/comments/comments_event.dart';
import 'package:smart_business/src/blocs/comments/comments_state.dart';
import 'package:smart_business/src/repositories/comments/comments_repository.dart';
import 'package:meta/meta.dart';

class CommentBloc extends Bloc<CommentsEvent, CommentsState> {
  final CommentsRepository _commentsRepository;
  CommentBloc({@required CommentsRepository commentsRepository}) :
      assert(commentsRepository != null), _commentsRepository = commentsRepository;

  @override
  CommentsState get initialState => UnloadCommentsState();

  @override
  Stream<CommentsState> mapEventToState(CommentsEvent event) async*{
    if(event  is LoadCommentsEvent){
      yield* _mapLoadCommentsState(event);
    }
    else if(event is CreateCommentsEvent){
     yield* _mapCreateCommentsState(event);
    }
  }

  Stream<CommentsState> _mapLoadCommentsState(LoadCommentsEvent event) async* {
    try{

      final res =  await _commentsRepository.getComments(event.idGts);
      yield LoadedCommentsState(comments: res);

    }catch (error){
     yield ErrorCommentState(message: error.toString(), dateTime: DateTime.now());
    }
   }

  Stream<CommentsState> _mapCreateCommentsState(CreateCommentsEvent event) async* {
    try{

      final res =  await _commentsRepository.createComment(event.create);
      dispatch(LoadCommentsEvent(event.create.idGts));
    }catch (error){
     yield ErrorCommentState(message: error.toString(), dateTime: DateTime.now());
    }
   }
}