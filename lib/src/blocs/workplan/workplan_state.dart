import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/for_views/data_screen_create_activity.dart';
import 'package:smart_business/src/models/time_params_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

abstract class WorkPlanState extends Equatable {
  WorkPlanState([List props = const []]) : super(props);
}
class UnloadWorkPlaneState extends WorkPlanState{}

class LoadedWorkPlaneState extends WorkPlanState{
  final List<WorkPlaneModel> list;
  final Map<DateTime, List> events;
  LoadedWorkPlaneState({this.list, this.events}) : super([list, events]);

  @override
  String toString() => '$runtimeType {$list $events} ';
}

class ErrorWorkPlaneState extends WorkPlanState{
  final String message;
  ErrorWorkPlaneState({this.message});
}

/*class DataForSelectState extends WorkPlanState{
  final List<ResponseActivityModel> activities;
  final List<ResponseActivityModel> subTypeActivities;
  final BackDataThirdSelect thirdSelect;
  final bool showThirdSelect;
  final TimeParamsModel timeParams;


  DataForSelectState({this.activities, this.subTypeActivities,
    this.showThirdSelect, this.thirdSelect, this.timeParams})
      : super([activities, subTypeActivities,
    showThirdSelect, thirdSelect, timeParams]);
}*/

class SendingRegisterState extends WorkPlanState{}

class SendingRequestToApiSate extends WorkPlanState{}

class ErrorRegisterActivityState extends  WorkPlanState{
    final String message;
    ErrorRegisterActivityState({this.message}) : super([message]);
}

class SuccessRegisterActivityState extends WorkPlanState{
  final String message;
  SuccessRegisterActivityState({this.message}) : super([message]);
}

class WarningActivityState extends WorkPlanState{
  final String message;
  WarningActivityState({this.message}) : super([message]);
}

class SuccessRejectWorkPlaneState extends WorkPlanState{}

class SuccessApprovedWorkPlaneState extends WorkPlanState{}

class SuccessCancelWorkPlaneState extends WorkPlanState{}
class SuccessPostponementPlaneState extends WorkPlanState{
  final String message;
  SuccessPostponementPlaneState({this.message}) : super([message]);
  @override
  String toString() => '$runtimeType { message: $message }';
}