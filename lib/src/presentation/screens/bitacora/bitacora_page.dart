import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/blocs/bitacora_detail/bitacora_detail_bloc.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_bloc.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_event.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_state.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/presentation/screens/bitacora/create_bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/bitacora/detail_bitacora_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/helper_color_gts.dart';
import 'package:smart_business/src/presentation/widgets/change_user_widget.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_left.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_task_month_binnacle.dart';
import 'package:smart_business/src/presentation/widgets/modals/conventions.dart';
import 'package:smart_business/src/presentation/widgets/modals/modal_users.dart';
import 'package:smart_business/src/presentation/widgets/widget_circle_color.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:table_calendar/table_calendar.dart';

class DateBitacora {
  DateTime dateNow;
  DateTime startDate;
  DateTime endDate;
  DateBitacora({this.dateNow, this.startDate, this.endDate});
}

class BitacoraPage extends StatefulWidget {
  static const String routeName = 'bitacora_page';
  @override
  _BitacoraPageState createState() => _BitacoraPageState();
}

class _BitacoraPageState extends State<BitacoraPage> {
  ThemeApp _themeApp = ThemeApp();
  CalendarController _calendarController;
  Map<DateTime, List> _events;
  DateTime _headerDate;
  BitacoraBloc _bitacoraBloc;
  BitacoraDetailBloc _bitacoraDetailBloc;
  EventsCalendarBloc _eventsCalendarBloc;
  DateTime _selectedDay;

  int userID = 0;

  @override
  void initState() {
    _calendarController = CalendarController();
    super.initState();
    _headerDate = DateTime.now();
    // this code example of events
    _selectedDay = DateTime.parse(DateFormat("yyyy-MM-dd").format(DateTime.now()));
    // initialize Bloc
    _eventsCalendarBloc = EventsCalendarBloc();
    _bitacoraBloc =  BitacoraBloc(bitacoraRepository: BitacoraRepository());
    //_bitacoraDetailBloc = BitacoraDetailBloc(bitacoraRepository: BitacoraRepository());
    // dates
    DateBitacora dateBitacora =  _datesBitacore();
    _bitacoraBloc.dispatch(LoadBitacoraEvent(idUser: 0,
        year: dateBitacora.dateNow.year.toString(),
        startDate: dateBitacora.startDate.toString(), endDate: dateBitacora.endDate.toString()));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(1.0),
      drawer: Drawer(
        child: DrawerLeft(),
      ),
      endDrawer: Drawer(
        child: DrawerBinnacleTask(type: 2, userId: userID),
      ),
      appBar: AppBar(
        backgroundColor: _themeApp.colorPink,
        leading: Builder(builder: (context) => IconButton(icon: Icon(Icons.menu), onPressed: () => Scaffold.of(context).openDrawer()),),
        title: Text("Bitácora"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(12),
            )
        ),
          actions: <Widget>[
            InkWell(
              child: AspectRatio(
                  aspectRatio: 1.4,
                  child: ChangeUserWidget()),
              onTap: (){
                  _showUserModal(context);
              },
            )
          ]
      ),
      body: Column(
        children: <Widget>[
          customCalendarHeader(context),
          _buildCalendar(),
          SizedBox(height: 30,),
          Container(
            color: Colors.transparent,
            width: MediaQuery.of(context).size.width/2,
            padding: const EdgeInsets.all(0),
            child: FlatButton(
                padding: const EdgeInsets.all(0),
                color: Colors.transparent,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(50.0),
                    side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.info_outline,color: Colors.black54),
                    Text(' Convenciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                  ],
                ),
                // color: Color(0xff5D5D5D), //Colors.pink,
                onPressed: () async {
                  modalConventions(context);
                }),
          ),
          SizedBox(height: 10,),
          Expanded(child: _buildEventList(),)
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: _themeApp.colorPink,
        onPressed:() async {
          var res = await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateBitacoraPage()));
          if(res !=  null && res){
            DateBitacora dateBitacora =  _datesBitacore();
            _bitacoraBloc.dispatch(LoadBitacoraEvent(idUser: userID,
                year: dateBitacora.dateNow.year.toString(),
                startDate: dateBitacora.startDate.toString(), endDate: dateBitacora.endDate.toString()));
          }
        },
        tooltip: 'Crear',
        child: Icon(Icons.add),
      ),
    );
  }
  Widget customCalendarHeader(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
                child: InkWell(
                  child: Text("${DateFormat.y().format(_headerDate)} ${DateFormat.MMMM('es').format(_headerDate)}", textAlign: TextAlign.center, style: TextStyle(fontSize: 20),),
                  onTap: (){
                    _settingModalBottomSheetMonth(context);
                  },
                )
            ),

            Builder(
                builder: (context) =>
                    Container(
                      width: 50,
                      height: 50,
                      child: Center(
                        child: FlatButton(
                            color: _themeApp.colorPink,
                            child: Icon(FontAwesomeIcons.calendarCheck, color: Colors.white,),
                            onPressed: () => Scaffold.of(context).openEndDrawer()),
                      ),
                    )
            ),

          ],
        )
    );

  }

  Widget _buildCalendar(){
    return BlocBuilder<BitacoraBloc, BitacoraState>(
      bloc: _bitacoraBloc,
      builder: (context, state){
        if(state is LoadedBitacoraState){

          _eventsCalendarBloc.dispatch(ChangeDayEventsCalendarEvent(state.events[_selectedDay]));

          return TableCalendar(
            locale: 'es_CO',
            events: state.events,
            headerVisible: false,
            calendarController: _calendarController,
            calendarStyle: CalendarStyle(
                todayColor: Colors.grey.withOpacity(0.5),
                selectedColor:_themeApp.colorPink
            ),
            headerStyle: HeaderStyle(
                centerHeaderTitle: true,
                formatButtonVisible: false
            ),
            builders: CalendarBuilders(
              markersBuilder: (context, date, events, holidays) {
                final children = <Widget>[];

                if (events.isNotEmpty) {
                  children.add(
                    Positioned(
                      right: 1,
                      bottom: 1,
                      child: _buildEventsMarker(date, events),
                    ),
                  );
                }

                if (holidays.isNotEmpty) {
                  children.add(
                    Positioned(
                      right: -2,
                      top: -2,
                      child: _buildHolidaysMarker(),
                    ),
                  );
                }

                return children;
              },
            ),
            onDaySelected: (date, events) {
              _onDaySelected(date, events);
            },
            onVisibleDaysChanged: (_, __, ___) {
              setState(() {
                _headerDate = _calendarController.focusedDay;
              });

             //  var yMonth =  DateFormat("yyyy-MM").format(_headerDate);
           //   _workPlanBloc.dispatch(LoadWorkPlanEvent(idUser: 0, yearMonth: yMonth));

            },
          );
        }

        return Container();
      },
    );

  }

  DateBitacora _datesBitacore(){
    DateTime now = DateTime.now();
    int lastday = DateTime(now.year, now.month + 1, 0).day;
    var fehcaI = DateTime.now().subtract(Duration(days: now.day-1));
    var fechFin = fehcaI.add(Duration(days: lastday-1));
    print("$now | $fehcaI | $fechFin | $lastday");

    return new DateBitacora(dateNow: now, startDate: fehcaI, endDate: fechFin);
  }

  void _onDaySelected(DateTime day, List events) {
    _eventsCalendarBloc.dispatch(ChangeDayEventsCalendarEvent(events));
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: _calendarController.isSelected(date)
            ? Colors.grey.withOpacity(0.6)
            : _calendarController.isToday(date) ? Colors.grey.withOpacity(0.6) : Colors.grey.withOpacity(0.6),
      ),
      width: 16.0,
      height: 16.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.black54,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildEventList() {
    return BlocBuilder(
      bloc: _eventsCalendarBloc,
      builder: (context, state){
        if(state is ChangeDayEventsCalendarState){
          if(state.events == null) return Container();
          return ListView.builder(
              itemCount: state.events.length,
              itemBuilder: (context, i) =>  _cardItem(context, state.events[i]));
        }
        return Container();
      },
    );
  }

  Widget _cardItem(BuildContext context, ResponseBitacoraModel event) {
    return Container(
      margin: const EdgeInsets.all(5),
      child: InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => DetailBitacoraPage(args: event,)));
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        WidgetCircleColor(color: helperColorGts(GtsTodos(gtsTipos: event.gtsTipos)),),
                        Container(
                            margin: const EdgeInsets.only(left: 5),
                            width: MediaQuery.of(context).size.width/2,
                            child: Text("${event.titulo}", textAlign: TextAlign.left, style: TextStyle(color: helperColorGts(GtsTodos(gtsTipos: event.gtsTipos)), fontWeight: FontWeight.bold),)),

                      ],
                    ),

                    Badge(
                      badgeContent: Text('', style: TextStyle(color: Colors.white),),
                      child: Icon(Icons.message, color: Colors.black54),
                    )
                  ],
                ),
                Container(
                    margin: const EdgeInsets.only(top: 5, left: 30),
                    width: MediaQuery.of(context).size.width,
                    child: Text(event.descripcion, style: TextStyle(color: Colors.grey.withOpacity(1.0)),)),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(top: 5, left: 30),
                  child: Text("${event.gtsTipos != null ? event.gtsTipos.nombre : ""}", textAlign: TextAlign.left, style: TextStyle(color: Colors.grey.withOpacity(1.0))),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Container(
                        margin: const EdgeInsets.only(top: 5, left: 40),
                        child: Text("${event.ptHoraInicio} - ${event.ptHoraFin}",style: TextStyle(color: Colors.grey.withOpacity(1.0)))),
                    _activityToTimeIcon(event.ptAtiempo, event.bFecha)
                  ],

                )
              ],
            ),
          ),
        ),
      ),
    );
  }


  void _settingModalBottomSheetMonth(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            padding: const EdgeInsets.only(bottom: 10),
            child: new Wrap(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar el mes", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
              ],
            ),
          );
        }
    );
  }

  selectUser(UserModel val){
    userID = val.id;
    DateBitacora dateBitacora =  _datesBitacore();
    _bitacoraBloc.dispatch(LoadBitacoraEvent(idUser: userID,
        year: dateBitacora.dateNow.year.toString(),
        startDate: dateBitacora.startDate.toString(), endDate: dateBitacora.endDate.toString()));
    setState(() {});
    Navigator.of(context).pop();
  }

  _showUserModal(BuildContext context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return ModalUsersWidget(function: selectUser,);
        }
    );
  }

  Widget _activityToTimeIcon(int toTime, String dateFinalized){
    print("P A tiempo ${toTime} $dateFinalized");
    if(dateFinalized == null){
      if(toTime == 1){
        return  Icon(FontAwesomeIcons.solidClock, color: Colors.green);
      }else if(toTime == 0){
        return Icon(FontAwesomeIcons.clock, color: Colors.orangeAccent,);
      }
      return SizedBox();
    }

    return Icon(Icons.check_circle, color: Colors.green,);
  }
}
