import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_event.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_state.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_invitation_bloc.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
class DetailPending extends StatefulWidget {
  ResponsePendingModel args;
  String token;
  DetailPending({@required this.args, this.token});
  @override
  _DetailPendingState createState() => _DetailPendingState();
}

class _DetailPendingState extends State<DetailPending> {
  ThemeApp _themeApp = ThemeApp();
  GtsInvitationBloc _gtsInvitationBloc;


  @override
  void initState() {
    _gtsInvitationBloc = GtsInvitationBloc(invitationsRepository: InvitationsRepository());
    _gtsInvitationBloc.dispatch(LoadGtsGuestUsersEvent(widget.args.idBitacora));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: _themeApp.colorPink,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(
          "Detalle del pendiente",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: _themeApp.colorPink),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              _cardInfo(context),
              Positioned(
                bottom: -10,
                left: 20,
                child: Chip(
                  avatar: CircleAvatar(
                    backgroundImage: loadImageApi(widget.args.datosUsuario.foto, widget.token),
                    // child: ,
                  ),
                  label:Text(widget.args.datosUsuario.nombre),
                ),
              )
            ],
          ),
           Container(
             margin: EdgeInsets.only(top: 20, left: 10, right: 10),
             child: _renderStatus(),
           ),

          ListTile(
            title: Text("Descripción", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
            subtitle: Text(widget.args.descripcion, style: TextStyle(fontStyle: FontStyle.italic),),
          ),
          SizedBox(height: 20,),
          Divider(),
          ListTile(
            title: Text("Resultados", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
            subtitle: Text("${widget.args.bResultados}", style: TextStyle(fontStyle: FontStyle.italic),),
          ),
          _activityApproved(),
          // Row Contacts
          Center(
            child: Badge(
                elevation: 0,
                badgeColor: Colors.grey[200],
                shape: BadgeShape.square,
                borderRadius: 20,
                toAnimate: false,
                badgeContent:
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.person,color: Colors.black54),
                    Text(' Contactos', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                  ],)
            ),
          ),
          // <------- list of contacts --->
          _contacts(context),
          Center(
            child: Badge(
                elevation: 0,
                badgeColor: Colors.grey[200],
                shape: BadgeShape.square,
                borderRadius: 20,
                toAnimate: false,
                badgeContent:
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.email,color: Colors.black54),
                    Text(' Invitaciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                  ],)
            ),
          ),
          // <------- list of invitations --->
          _guestUsers(context),
          // <-- finish list of invitations -->
          // Row Notification activity
          Center(
            child: Badge(
                elevation: 0,
                badgeColor: Colors.grey[200],
                shape: BadgeShape.square,
                borderRadius: 20,
                toAnimate: false,
                badgeContent:
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.email,color: Colors.black54),
                    Text('! Notificación de actividad', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                  ],)
            ),
          ),
          // <------- list of Notification activity --->
          _notificationEmail(context),
          // <-- finish list of Notification activity -->

        ],
      ),
    );
  }
  _cardInfo(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(10.0),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomLeft,
            stops: [0.3, 1],
            colors: [Color(0xCC079CE5),  Color(0xff079CE5)],


          )
      ),
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.only(top: 8, left: 10, right: 10, bottom: 8),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Flexible(child: Text("${widget.args.titulo}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),))
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
             // Text("${DateFormat("dd MMM yyyy").format(DateTime.parse(widget.args.bFecha).toLocal()) }, ${widget.args.bHoraInicio} - ${widget.args.bHoraFin}", style: TextStyle(color: Colors.white))
            ],
          ),
        ],
      ),
    );
  }

  _renderStatus(){
    if(widget.args.ptAprobado !=  null) return  Container(
      margin: const EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.green),
          SizedBox(width: 5,),
          Text(DateFormat.yMMMd('es').format(DateTime.parse(widget.args.ptAprobadoFecha)))
        ],
      ),
    );
    if(widget.args.ptCancelada !=  null) return  Container(
      margin: const EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.red),
          SizedBox(width: 5,),
          Text(DateFormat.yMMMd('es').format(DateTime.parse(widget.args.ptFecha)))
        ],
      ),
    );

    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.grey),
          SizedBox(width: 5,),
          Text(DateFormat.yMMMd('es').format(DateTime.parse(widget.args.ptFecha)))
        ],
      ),
    );
  }

  Widget _contacts(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          if(state.contacts == null || state.contacts.length == 0) return Container(width:MediaQuery.of(context).size.width, height: 20);
         // listContacts =  state.contacts;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.contacts.length,
              itemBuilder: (context, i) => ListTile(
                leading:  new CircleAvatar(
                    backgroundImage: loadImageApi(state.contacts[i].foto, widget.token)
                ),
                title: Text("${state.contacts[i].nombre}"),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _guestUsers(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          if(state.users == null || state.users.length == 0) return Container(width:MediaQuery.of(context).size.width, height: 20);
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.users.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                  position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.white,
                  badgeContent: _iconStatusGuestUser(state.users[i].respuesta),
                  child: new CircleAvatar(
                      backgroundImage: loadImageApi(state.users[i].datosInvitados.foto, widget.token)
                  ),
                ),
                title: Text("${state.users[i].datosInvitados.nombre}"),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _notificationEmail(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.email.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                  position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.transparent,
                  badgeContent: _iconStatusGuestUser(state.email[i].respuesta),
                  child:  state.email[i].datosInvitados == null ?
                  new CircleAvatar(
                      child: Icon(FontAwesomeIcons.envelope, color: Colors.grey,),
                      backgroundColor: Colors.grey.withOpacity(0.2))
                      : new CircleAvatar(
                      backgroundImage: loadImageApi(state.email[i].datosInvitados.foto, widget.token)
                  ) ,
                ),
                title: Text("${state.email[i].datosInvitados != null ? state.email[i].datosInvitados.nombre : state.email[i].correoExterno}"),
                subtitle: state.email[i].datosInvitados == null ? Text("correo electronico externo") : Text(state.email[i].datosInvitados.cargo),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _iconStatusGuestUser(var value){
    if(value == null){
      return Container();
    }
    else if(value == 1) return Icon(Icons.check_circle, color: _themeApp.colorPink,);
    if(value == 2 || value == 3 ) return Icon(Icons.cancel, color: Colors.grey.withOpacity(0.6),);

    return Container();
  }

  Widget _activityApproved(){
    return BlocBuilder(
        bloc: _gtsInvitationBloc,
        builder: (context, state){
          if(state is LoadedGtsGuestUsers){
            if(state.userApproved.datosUsuario != null){
              return Container(
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.all(10),
                decoration: new BoxDecoration(
                    color: Colors.grey[250],
                    border: Border.all(color: Colors.grey[350], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text("Actividad aprobada", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Chip(
                          avatar: CircleAvatar(
                            backgroundImage: loadImageApi(state.userApproved.datosUsuario.foto, widget.token),
                            // child: ,
                          ),
                          label:Text(state.userApproved.datosUsuario.nombre),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.calendar_today, color: Colors.green,),
                            Text("${DateFormat("dd MMMM yyyy").format(DateTime.parse(state.userApproved.ptAprobadoFecha).toLocal())}", style: TextStyle(color: Colors.grey[600]),)
                          ],
                        )
                      ],
                    )
                  ],
                ),
              );
            }
            return Container(
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.all(10),
              decoration: new BoxDecoration(
                  color: Colors.grey[250],
                  border: Border.all(color: Colors.grey[350], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Column(
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text("Actividad pendiente por aprobación", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[],
                  )
                ],
              ),
            );
          }
          return Container();
        });
  }
}
