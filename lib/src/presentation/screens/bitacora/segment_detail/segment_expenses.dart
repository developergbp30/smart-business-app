import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/request_balance_expenses.dart';
import 'package:smart_business/src/presentation/screens/bitacora/segment_detail/expenses/detail_expense.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/widget_circle_color.dart';
import 'package:smart_business/src/presentation/widgets/widget_time_line_burble.dart';

import 'create_expenses_binnacle.dart';

class SegmentExpenses extends StatelessWidget {
  ThemeApp _themeApp = ThemeApp();
  int idBitacora;
  List<ExpenseBinnacleModel> args;
  String token;
  ReqBalanceExpensesModel balance;
  SegmentExpenses({@required this.idBitacora, this.args, this.balance, this.token});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height/2,
            child: ListView.builder(
                itemCount: args.length,
                itemBuilder: (context, i) => _itemCard(context, args[i])),
          ),
          Container(
              height: MediaQuery.of(context).size.height/2,
              child: _expensesBalance(context))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: _themeApp.colorPink,
        onPressed:() async {
         await Navigator.push(context, new MaterialPageRoute(builder: (context) => CreateBinnacleExpenses(idBitacora: idBitacora,)));
        },
        tooltip: 'Crear',
        child: Icon(Icons.add),
      ),
    );
  }
  Widget _itemCard(BuildContext context, ExpenseBinnacleModel example) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: InkWell(
        onTap: () async {//  DetailExpense
          await Navigator.push(context, new MaterialPageRoute(builder: (context) => DetailExpense(args: example, token: token)));
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      //margin: const EdgeInsets.only(top: 10),
                        child: TimeLineWidget(color: new ThemeApp().colorPink,)),
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, top: 0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Flexible(child: Text("${example.titulo}", style: TextStyle(color: Colors.grey[700]),)),
                              ],

                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Text("${example.tipo.nombre}")
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("\$${example.valorSolicitado}"),
                                  example.porcentajeAprobado !=  null ?
                                  Container(

                                    padding: EdgeInsets.all(8.0),
                                    decoration: new BoxDecoration(
                                        color: Colors.green,
                                      borderRadius: new BorderRadius.only(topLeft: Radius.circular(5.0), bottomLeft: Radius.circular(5.0))
                                    ),
                                    child: Text("${example.porcentajeAprobado} % ", style: TextStyle(color: Colors.white),),
                                  ) :
                                      SizedBox(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Divider(),
                Row(children: <Widget>[
                  example.valorAprobado != null ?  _approvedValue(context, example.valorAprobado) :
                  Text("Pendiente")
                  ,
                ],)
              ],
            ),
          ),),
      ),
    );
  }

  Widget _approvedValue(BuildContext context, value){
    return Flexible(
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(Icons.check_circle, color: Colors.green,),
            Text("Aprobado"),
             Flexible(
               child: Container(
                color: _themeApp.colorPink,
                height: 2,
                width: MediaQuery.of(context).size.width/2,
            ),
             ),
            Flexible(child: Text("\$$value"))
          ],
        ),
      ),
    );
  }

  Widget _expensesBalance(BuildContext context){
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[Text("Balance de gastos")],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("\$0"),
              Text("\$${balance.maximo}"),
            ],

          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            height: 2,
            color: Colors.blueGrey,
            width: MediaQuery.of(context).size.width,
          ),
         Wrap(
           children:  _renderProgress(context)
         ),

        ],
      ),
    );
  }

  _renderProgress(BuildContext context){
    List<Widget> items = [];
    print(balance.detalle);
    balance.detalle.forEach((i) {
      print("${i.valor } / ${ MediaQuery.of(context).size.width}");
      print(int.parse(i.valor) / MediaQuery.of(context).size.width);
      items.add(_barrProgress(context, cylinderTopHeight: 7.0, label: "${i.tipo.nombre} (\$${i.valor})",
          cylinderBottomHeight: 10.0, progressValue: int.parse(i.valor) / MediaQuery.of(context).size.width));
    });

    return items;
  }

  _barrProgress(BuildContext context,{label = "label", cylinderTopWidth = 2.0, cylinderTopHeight=3.0, cylinderBottomWidth = 2.0, cylinderBottomHeight=3.0, progressValue = 100.0, Color colorCircle = Colors.grey}){
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          Container(
            color: Colors.blueGrey,
            width: cylinderTopWidth,
            height: cylinderTopHeight,
          ),
          WidgetCircleColor(color: colorCircle,),
          Container(
            color: Colors.blueGrey,
            width: cylinderBottomWidth,
            height: cylinderBottomHeight,
          )
        ],),
        SizedBox(width: 5,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: cylinderTopHeight+10,),
            
            Container(
              width: progressValue,//MediaQuery.of(context).size.width/1.1,
              height: 10,
              decoration: BoxDecoration(
                  color: ThemeApp().colorPink,
                borderRadius: BorderRadius.only(topRight: Radius.circular(5), bottomRight: Radius.circular(5))
              ),
            ),
            Text(label, style: TextStyle(fontStyle: FontStyle.italic),)
          ],
        )
      ],
    );
  }


}
