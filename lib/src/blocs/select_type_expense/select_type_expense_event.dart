import 'package:equatable/equatable.dart';

abstract class SelectExpenseEvent extends Equatable {
  SelectExpenseEvent([List props = const []]) : super(props);
}

class GetTypeExpenseEvent extends SelectExpenseEvent{}

class SelectedTypeExpenseEvent extends SelectExpenseEvent{}