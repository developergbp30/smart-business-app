import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/for_views/data_screen_edit_expense.dart';
import 'package:smart_business/src/presentation/screens/comments/comments_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';

import '../create_expenses_binnacle.dart';

class TypeFileWidget{
  IconData icon;
  Color color;
  TypeFileWidget({this.icon, this.color});
}

class DetailExpense extends StatefulWidget {
  final ExpenseBinnacleModel args;
  final String token;
  DetailExpense({@required this.args, this.token});
  @override
  _DetailExpenseState createState() => _DetailExpenseState();
}

class _DetailExpenseState extends State<DetailExpense> {
  ThemeApp _themeApp = ThemeApp();
  final _formKey = GlobalKey<FormState>();
  BitacoraBloc _bitacoraBloc;
  @override
  void initState() {
    _bitacoraBloc = BitacoraBloc(bitacoraRepository: BitacoraRepository());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: _themeApp.colorPink,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(
          "Detalle gasto",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: _themeApp.colorPink),
        ),
      ),
      body: BlocListener(
          bloc: _bitacoraBloc,
          listener: (context,state){
            if(state is SuccessDeleteExpenseState){
              Scaffold.of(context).showSnackBar(SnackBar(content: Text("Gasto eliminado"),backgroundColor: Colors.green,));
              Future.delayed(Duration(seconds: 2)).then((time){
                Navigator.of(context).pop();
              });
            }
            if(state is ErrorBitacoraState){
              Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.message),backgroundColor: Colors.red,));
            }
          },
          child: Padding(
            padding: EdgeInsets.only(top: 8.0, left: 10.0, right: 10.0),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 10),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                        Text(widget.args.titulo),
                        InkWell(
                          onTap: () async {
                            await Navigator.push(context, MaterialPageRoute(builder: (context) => CommentsPage(idGts: widget.args.id,)));
                          },
                          child: Badge(
                            badgeContent: Text("0", style: TextStyle(color: Colors.white),),
                            child: Icon(Icons.message, color: Colors.black54),
                          ),
                        )
                      ],),
                      SizedBox(height: 5,),
                      Row(
                        children: <Widget>[
                          Text(widget.args.tipo.nombre)
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        children: <Widget>[
                          Text("\$${widget.args.valorSolicitado}")
                        ],
                      )
                    ],
                  ),
                ),
                 _renderStatus(),
                ListTile(
                  title: Text("Descripción", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
                  subtitle: Text(widget.args.descripcion, style: TextStyle(fontStyle: FontStyle.italic),),
                ),
                Center(
                  child: Badge(
                      elevation: 0,
                      badgeColor: Colors.grey[200],
                      shape: BadgeShape.square,
                      borderRadius: 20,
                      toAnimate: false,
                      badgeContent:
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.insert_drive_file,color: Colors.black54),
                          Text(' Archivo', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                        ],)
                  ),
                ),
                _renderDocuments(),
              ],
            ),
          )),
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 1),
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: (){
                  _modalDelete(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.trash),
                    Text("Eliminar")
                  ],
                ),
              ),
              SizedBox(width: 10,),
              InkWell(
                onTap: () async {
                  var data  =  new SetFormExpense(
                    id: widget.args.id,
                    idBinnacle: widget.args.idGts,
                    type: widget.args.idTipo,
                    title: widget.args.titulo,
                    description: widget.args.descripcion,
                    value:  widget.args.valorSolicitado,
                  );

                  await Navigator.push(context, new MaterialPageRoute(builder:
                      (context) => CreateBinnacleExpenses(idBitacora: widget.args.idGts,setFormExpense: data,)));

                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.pencilAlt),
                    Text("Editar")
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _containerApproved({title, colorTitle = Colors.green , CreadoPor user}){
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(10),
      decoration: new BoxDecoration(
          color: Colors.grey[250],
          border: Border.all(color: Colors.grey[350], width: 1),
          borderRadius: new BorderRadius.circular(5.0)
      ),
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              child: Text(title, style: TextStyle(color: colorTitle, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Chip(
                avatar: CircleAvatar(
                  backgroundImage: loadImageApi(user.foto, widget.token),
                  // child: ,
                ),
                label:Text(user.nombre),
              ),
              widget.args.porcentajeAprobado !=  null &&  widget.args.porcentajeAprobado != 0?  Container(
                  padding: EdgeInsets.all(8.0),
                  decoration: new BoxDecoration(
                  color: Colors.green,
                  borderRadius: new BorderRadius.only(topLeft: Radius.circular(5.0), bottomLeft: Radius.circular(5.0))
                  ),
                  child: Text("${widget.args.porcentajeAprobado} % ", style: TextStyle(color: Colors.white),),
              ) : SizedBox()
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("${widget.args.aprobadoAt !=  null ?  DateFormat.yMMMd('es').format(DateTime.parse(widget.args.aprobadoAt)) : DateFormat.yMMMd('es').format(DateTime.parse(widget.args.rechazadoAt))}"),
              Row(
                children: <Widget>[
                  widget.args.valorAprobado !=  null ?
                  Icon(Icons.check_circle, color: Colors.green,)
                      : Icon(Icons.cancel, color: Colors.red,)    ,
                  Text("\$${widget.args.valorAprobado !=  null ? widget.args.valorAprobado : widget.args.valorSolicitado}")
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  _renderStatus(){
    if(widget.args.aporbadoPor !=  null ) return    _containerApproved(title: "Presupuesto aprobado", user: widget.args.aporbadoPor);
    if(widget.args.rechazadoAt !=  null ) return    _containerApproved(title: "Presupuesto cancelado",user: widget.args.rechazadoPor, colorTitle: Colors.red);
    return  _containerPending();
  }

  Widget _renderDocuments(){
    if(widget.args.adjuntoNombre ==  null) return SizedBox();
    TypeFileWidget typeFileWidget =  _typeFileIcon(widget.args.adjuntoNombre);
    return ListTile(
      leading: Icon(typeFileWidget.icon, color: typeFileWidget.color,),
      title: Text(widget.args.adjuntoNombre),
    );
  }

  TypeFileWidget _typeFileIcon(String name){
    if(name.contains(".jpg") || name.contains(".png")){
      return new TypeFileWidget(icon: Icons.image, color: Colors.green);
    }
    if(name.contains(".pdf")){

      return new TypeFileWidget(icon: FontAwesomeIcons.solidFilePdf, color: Colors.red);
    }

    return new TypeFileWidget(icon: FontAwesomeIcons.file, color: Colors.grey);
  }

  _containerPending(){
    return  Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(10),
      decoration: new BoxDecoration(
          color: Colors.grey[250],
          border: Border.all(color: Colors.grey[350], width: 1),
          borderRadius: new BorderRadius.circular(5.0)
      ),
      child: Column(
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              child: Text("Presupuesto pendiente", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[],
          )
        ],
      ),
    );
  }
  void _modalDelete(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Eliminar gasto", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20),
                  Text("¿Esta seguro que desea eliminar este gasto?"),
                  SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: new ThemeApp().colorPink),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            _bitacoraBloc.dispatch(DeleteExpenseEvent(id: widget.args.id));
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          );
        }
    );
  }
}
