import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/activity_model.dart';

abstract class SelectActivityEvent extends Equatable {
  SelectActivityEvent([List props = const []]) : super(props);
}

class LoadDataForSelectsEvent extends SelectActivityEvent{}

class FindSubTypeActivity extends SelectActivityEvent{
  final List<ResponseActivityModel> activities;
  final ResponseActivityModel selected;

  FindSubTypeActivity({this.activities, this.selected}) : super([activities]);
}

class SelectDynamicEvent extends SelectActivityEvent{}