import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_event.dart';
import 'package:smart_business/src/blocs/auth/auth_state.dart';
import 'package:smart_business/src/presentation/utils/navigation.dart';
class CodeCompanyPage extends StatefulWidget {
  static const String routeName = 'code_company_page';
  @override
  _CodeCompanyPageState createState() => _CodeCompanyPageState();
}

class _CodeCompanyPageState extends State<CodeCompanyPage> {
   double topContainer = 100.0;
   final _formKey = GlobalKey<FormState>();
   String codeCompany;

   @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    topContainer = MediaQuery.of(context).size.height/10;
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<AuthBloc, AuthState>(
        listener: (BuildContext context, AuthState state){
          if(state is Authenticated){
            navigateFromLoginToHome(context);
          }
          else if(state is SuccessVerificationCodeCompany){
            navigateToLogin(context);
          }
          if(state is SendingCodeVerifyCompanyState){
            Scaffold.of(context).showSnackBar(SnackBar(content: LinearProgressIndicator(),));
          }
          if(state is ErrorCompanyState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("${state.message}",), backgroundColor: Colors.red,));
          }
        },
        child: SafeArea(
          child: BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state){
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      Center(
                        child: Container(
                          padding: const EdgeInsets.all(20),
                          decoration: new BoxDecoration(
                              color: Colors.white.withOpacity(0.9),
                              borderRadius: new BorderRadius.circular(10.0)
                          ),
                          margin:  EdgeInsets.only(left: 20, right: 20, top: topContainer, bottom: 50),
                          // color: Colors.red,
                          height: MediaQuery.of(context).size.height/1.5,
                          alignment: Alignment(0.0, 0.0),
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 70.0,
                                child: Image.asset("assets/images/logo_sb.png", fit: BoxFit.contain,),
                              ),
                              SizedBox(height: 10,),
                              Text("Bienvenido a Smartbusiness",textAlign: TextAlign.center, style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold)),
                              SizedBox(height: 10,),
                              Text("Ingrese su código empresarial de acceso",textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                              SizedBox(height: 20,),
                              TextFormField(
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.blueGrey,
                                      width: 5.0
                                    )
                                  ),
                                  hintText: 'Código',
                                prefixIcon: Icon(Icons.verified_user),
                                //labelText: 'Codigo'
                              ),
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Ingrese el codigo de compañia";
                                    }
                                    return null;
                                  },
                                  onSaved: (value){
                                    setState(() {
                                      codeCompany = value;
                                    });
                                  }
                            ),

                              SizedBox(height: 10,),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 39.68,
                                child: RaisedButton(
                                    shape: new RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(5.0),
                                        side: BorderSide(color: Color(0xffEF2A2A))),
                                    child: Text("Continuar", style: TextStyle(color: Colors.white),),
                                    color: Color(0xffEF2A2A),//Colors.pink,
                                    onPressed: () {
                                      if(_formKey.currentState.validate()){
                                        _formKey.currentState.save();
                                        BlocProvider.of<AuthBloc>(context).dispatch(VerificationCodeCompanyEvent(codeCompany));
                                      }
                                    }),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 42.0,
                        child: Image.asset("assets/images/logo_gbp.png", fit: BoxFit.contain,),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
