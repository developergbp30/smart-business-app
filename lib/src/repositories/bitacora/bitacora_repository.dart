import 'package:smart_business/src/api/services/bitacora_http_service.dart';
import 'package:smart_business/src/api/services/expense_http_service.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/create_expenses.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/info_work_plane_binnacle_model.dart';
import 'package:smart_business/src/models/request_balance_expenses.dart';
import 'package:smart_business/src/models/select_binnacle_model.dart';

class BitacoraRepository {
  BitacoraHttp _bitacoraHttp = BitacoraHttp();
  ExpenseHttp _expenseHttp = ExpenseHttp();

  Future<List<ResponseBitacoraModel>> getCalendar(int idUser, String startDate, String endDate, String year) async{
    try{
      return _bitacoraHttp.getCalendar(idUser, startDate, endDate, year);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<ResponseDetailBitacoraModel> detail(int id) async{
    try{
        return _bitacoraHttp.detail(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponsePendingModel>> listPending(int id) async{
    try{
        return _bitacoraHttp.listPending(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future create(CreateBitacoraModel data) async{
    try{
        return _bitacoraHttp.create(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future createPending(CreatePendingModel data) async{
    try{
        return _bitacoraHttp.createPending(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future  deleteBinnacle(int id) async {
    try{
        return await _bitacoraHttp.deleteBinnacle(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ExpenseBinnacleModel>> listExpenses(int id) async {
    try{
      return await _bitacoraHttp.listExpenses(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future createExpense(CreateExpensesModel data) async{
    try{
      return await _bitacoraHttp.createExpense(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future updateExpense(CreateExpensesModel data) async{
    try{
      return await _bitacoraHttp.updateExpense(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<ReqBalanceExpensesModel> balanceExpense(int id ) async{
    try{
      return await _expenseHttp.balanceExpenses(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future deleteExpense(int id) async{
    try{
      return await _bitacoraHttp.deleteExpense(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<SelectBinnacleModel>> getSelectBinnacle(int idUser) async{
    try{
        return await _bitacoraHttp.listForSelect(idUser);
    }catch (error){
      return  Future.error(error);
    }
  }

  Future<InfoWorkPlaneBinnacleModel> getInfoWorkPlane(int id) async{
    try{
      return await _bitacoraHttp.infoWorkPlane(id);
    }catch (error){
      return  Future.error(error);
    }
  }

}