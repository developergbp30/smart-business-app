import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
class CreateCampusPage extends StatefulWidget {
  @override
  _CreateCampusPageState createState() => _CreateCampusPageState();
}

class _CreateCampusPageState extends State<CreateCampusPage> {
  final Color greenColor =  Color(0xFF11BA19);

  TextEditingController descriptionCtrl = TextEditingController();

  TextEditingController buyMonthCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(icon: Icon(Icons.clear, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Crear/editar cliente", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: Colors.green,), onPressed: (){

          })
        ],
      ),
      body: Container(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(children: <Widget>[
          _addPicture(),
          //--/ Country
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("País", style: TextStyle(color: Colors.grey[700]),),
          ),
          InkWell(
            child:  Container(
              height: 40,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey[500], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(""),
                    Icon(Icons.expand_more)
                  ],
                ),
              ),
            ),
            onTap: (){

            },
          ),
          //--/ State
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Estado", style: TextStyle(color: Colors.grey[700]),),
          ),
          InkWell(
            child:  Container(
              height: 40,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey[500], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(""),
                    Icon(Icons.expand_more)
                  ],
                ),
              ),
            ),
            onTap: (){

            },
          ),
          //-- City
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Ciudad", style: TextStyle(color: Colors.grey[700]),),
          ),
          InkWell(
            child:  Container(
              height: 40,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey[500], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(""),
                    Icon(Icons.expand_more)
                  ],
                ),
              ),
            ),
            onTap: (){

            },
          ),
          //--/ Direction
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Dirección", style: TextStyle(color: Colors.grey[700]),),
          ),
          InkWell(
            child:  Container(
              height: 40,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey[500], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(""),
                    Icon(Icons.expand_more)
                  ],
                ),
              ),
            ),
            onTap: (){

            },
          ),
          //--/ Description
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Descripción", style: TextStyle(color: Colors.grey[700]),),
          ),
          TextFormField(
            controller: descriptionCtrl,
            keyboardType: TextInputType.multiline,
            minLines: 1,
            maxLines: 5,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Colors.grey,
                      width: 5.0
                  )
              ),
              hintText: 'Ingresa la descipción',
              // labelText: 'Descripcion'
            ),
            validator: (value){
              if(value.isEmpty){
                return 'Ingrese la descripción';
              }
              return null;
            },
            onSaved: (value){},
          ),
          /**-----------------------------------------
           *
           *  Section Map Todo: Implement Google Maps
           *
           * ------------------------------------------*/

          /**-----------------------------
           * Additional Info
           */
          Container(
            margin: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 10),
            child: Text("Informatión adicional", style: TextStyle(color: greenColor, fontSize: 16, fontWeight: FontWeight.w600),),
          ),
          //--/ Campus Type
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Tipo de sede", style: TextStyle(color: Colors.grey[700]),),
          ),
          InkWell(
            child:  Container(
              height: 40,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey[500], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(""),
                    Icon(Icons.expand_more)
                  ],
                ),
              ),
            ),
            onTap: (){

            },
          ),
          // Buy Month
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Compras mesuales", style: TextStyle(color: Colors.grey[700]),),
          ),
          TextFormField(
            controller: buyMonthCtrl,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Colors.grey,
                      width: 5.0
                  )
              ),
              hintText: 'ingrese ...',
              // labelText: 'Actividad'
            ),
            validator: (value){
              if(value.isEmpty){
                return 'Ingrese el nombre largo';
              }
              return null;
            },
            onSaved: (value){

            },
          ),
        ],),
      ),),
    );
  }

  Widget _addPicture(){
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height/3,
          decoration: new BoxDecoration(
              color: Color(0xFFF5F5F5),//Colors.white,
              border: Border.all(color: Colors.grey[500], width: 1),
              borderRadius: new BorderRadius.circular(5.0)
          ),
        ),
        Positioned.fill(
          bottom: 10,
          right: 10,
          child: Align(
            alignment: Alignment.bottomRight,
            child: Badge(
              badgeColor: greenColor,
              badgeContent: Icon(Icons.add_photo_alternate, color: Colors.white,),
            ),
          ),
        ),
      ],
    );
  }
}
