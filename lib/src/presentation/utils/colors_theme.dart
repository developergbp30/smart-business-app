
import 'package:flutter/material.dart';
class ThemeApp {
  final colorPink = Color(0xffFE4881);
  final colorGrey = Color(0xff0000005C);
  final colorGrey2 = Color(0xff00000029);
  final colorWhite = Color(0xffF5F5F5);
}