
import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/user_model.dart';

abstract class UsersEvent extends Equatable {
  UsersEvent([List props = const []]) : super(props);
}

class LoadUsersEvent extends UsersEvent{}

class FilterUsersEvent extends UsersEvent {
 final List<UserModel> usersList;
 final String query;
  FilterUsersEvent({this.usersList, this.query}) : super([usersList, query]);
}