import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/opportunity_model.dart';

abstract class OpportunityEvent extends Equatable {
  OpportunityEvent([List props = const []]) : super(props);
}

class LoadOpportunitiesEvent extends OpportunityEvent{}

class SearchOpportunityEvent extends OpportunityEvent{
  String query;
  List<OpportunityModel> opportunity;
  SearchOpportunityEvent({this.query, this.opportunity}) : super([opportunity]);
}