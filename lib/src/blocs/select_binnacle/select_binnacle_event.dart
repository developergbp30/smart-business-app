import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/select_binnacle_model.dart';

abstract class SelectBinnacleEvent extends Equatable {
  SelectBinnacleEvent([List props = const []]) : super(props);
}

class GetDataSelectBinnacleEvent extends SelectBinnacleEvent{}

class SelectedBinnacleEvent extends SelectBinnacleEvent{
  final SelectBinnacleModel selected;
  final List<SelectBinnacleModel> items;
  SelectedBinnacleEvent({this.selected, this.items}) : super([selected, items]);

  @override
  String toString() =>  '$runtimeType { id:  ${selected.value} }';
}