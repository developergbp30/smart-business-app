import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/presentation/screens/contacts/contacts_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/widgets/box_search.dart';

class CreatePrograming extends StatefulWidget {
  @override
  _CreateProgramingState createState() => _CreateProgramingState();
}

class _CreateProgramingState extends State<CreatePrograming> {
  List<ContactModel> addContacts;
  ThemeApp _themeApp = ThemeApp();
  String optionTypeActivity;
  DateTime timeInitial;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(icon: Icon(Icons.close, color:  _themeApp.colorPink,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Programar",style: TextStyle(color: _themeApp.colorPink), textAlign: TextAlign.center,),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color:  _themeApp.colorPink,), onPressed: (){

          })
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(child: ListView(
            children: <Widget>[

              SizedBox(height: 20,),
              Text("Actividad", style: TextStyle(color: Colors.grey[700]),),
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey,
                          width: 5.0
                      )
                  ),
                  hintText: 'Preparar presentación',
                  // labelText: 'Actividad'
                ),
              ),
              SizedBox(height: 20,),
              Text("Descripción", style: TextStyle(color: Colors.grey[700]),),
              TextFormField(
                keyboardType: TextInputType.multiline,
                minLines: 1,
                maxLines: 5,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey,
                          width: 5.0
                      )
                  ),
                  hintText: 'Ingresa la descipción',
                  // labelText: 'Descripcion'
                ),
              ),
              SizedBox(height: 20,),
              Divider(),
              SizedBox(height: 20,),
              Text("Tipo de Actividad", style: TextStyle(color: Colors.grey[700]),),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("$optionTypeActivity"),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){

                  _settingModalBottomSheetSubTypeActivities(context);
                  /*showCupertinoModalPopup(context: context, builder: (context){
                    return actionSheetActivity(context);
                  });*/
                },
              ),
              SizedBox(height: 20,),
              Text("Subtipo de Actividad", style: TextStyle(color: Colors.grey[700]),),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("$optionTypeActivity"),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){

                  _settingModalBottomSheetSubTypeActivities(context);
                  /*showCupertinoModalPopup(context: context, builder: (context){
                    return actionSheetActivity(context);
                  });*/
                },
              ),
              SizedBox(height: 20,),
              Text("Oportunidades", style: TextStyle(color: Colors.grey[700]),),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("$optionTypeActivity"),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){

                  _settingModalBottomSheetOpportunities(context);
                  /*showCupertinoModalPopup(context: context, builder: (context){
                    return actionSheetActivity(context);
                  });*/
                },
              ),
              _dividerAndBox(),
              Text("Fecha", style: TextStyle(color: Colors.grey[700]),),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){

                  _showDatePicker(context);
                },
              ),
              SizedBox(height: 20,),
              Text("Hora de inicio", style: TextStyle(color: Colors.grey[700]),),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("$timeInitial"),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){

                  _showTimePicker(context);
                },
              ),
              SizedBox(height: 20,),
              Text("Hora de finalización", style: TextStyle(color: Colors.grey[700]),),
              InkWell(
                child:  Container(
                  height: 40,
                  decoration: new BoxDecoration(
                      color: Colors.grey[350],
                      border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("$timeInitial"),
                        Icon(Icons.expand_more)
                      ],
                    ),
                  ),
                ),
                onTap: (){
                  _showTimePicker(context);
                },
              ),
              _dividerAndBox(),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 20, right: 20),
                color: Colors.transparent,
                padding: const EdgeInsets.all(8),
                child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    color: Colors.transparent,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0),
                        side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.person_add,color: _themeApp.colorPink),
                        Text(" Añadir contacto", style: TextStyle(color: _themeApp.colorPink),),
                      ],
                    ),
                    // color: Color(0xff5D5D5D), //Colors.pink,
                    onPressed: () async {
                      var res = await Navigator.push(context, new MaterialPageRoute<BackToPageContactsModel>(builder: (context) => ContactsPage()));
                      if(res != null){
                        print(res.contacts.first.name);
                        setState(() {
                          addContacts = res.contacts;
                        });
                      }
                    }),
              ),
              Wrap(
                  children:  _buildContactsSelected()
              ),
              // add invitations
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 20, right: 20),
                color: Colors.transparent,
                padding: const EdgeInsets.all(8),
                child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    color: Colors.transparent,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0),
                        side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(FontAwesomeIcons.solidEnvelope,color: _themeApp.colorPink),
                        Text(" Añadir Enviar invitaciones", style: TextStyle(color: _themeApp.colorPink),),
                      ],
                    ),
                    // color: Color(0xff5D5D5D), //Colors.pink,
                    onPressed: () async {
                      var res = await Navigator.push(context, new MaterialPageRoute<BackToPageContactsModel>(builder: (context) => ContactsPage()));
                      if(res != null){
                        print(res.contacts.first.name);
                        setState(() {
                          addContacts = res.contacts;
                        });
                      }
                    }),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 20, right: 20),
                color: Colors.transparent,
                padding: const EdgeInsets.all(8),
                child: FlatButton(
                    padding: const EdgeInsets.all(0),
                    color: Colors.transparent,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0),
                        side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(FontAwesomeIcons.solidEnvelope,color: _themeApp.colorPink),
                        Text("! Enviar correo informativo", style: TextStyle(color: _themeApp.colorPink),),
                      ],
                    ),
                    // color: Color(0xff5D5D5D), //Colors.pink,
                    onPressed: () async {
                      var res = await Navigator.push(context, new MaterialPageRoute<BackToPageContactsModel>(builder: (context) => ContactsPage()));
                      if(res != null){
                        print(res.contacts.first.name);
                        setState(() {
                          addContacts = res.contacts;
                        });
                      }
                    }),
              ),
              _addExternEmailWidget()
            ],
          )),
        ),
      ),
    );
  }

  List<Widget> _buildContactsSelected(){
    List<Widget> widget = [];
    if(addContacts != null && addContacts.length > 0) {
      for(var i = 0; i <addContacts.length; i++){
        widget.add(Chip(
          avatar: CircleAvatar(
            backgroundImage: new NetworkImage(addContacts[i].avatarUrl),
            // child: ,
          ),
          label:Text("${addContacts[i].name}"),
        ));
      }
    }


    return widget;
  }

  Widget _dividerAndBox() {
    return Column(
      children: <Widget>[
        SizedBox(height: 20,),
        Divider(),
        SizedBox(height: 20,),
      ],
    );
  }

  Widget actionSheetActivity(BuildContext context){
    return CupertinoActionSheet(
      title: Text("Tipo de actividad", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
      actions: <Widget>[
        CupertinoActionSheetAction(
          child: Text("Oportunidad ganada"),
          onPressed: (){

          },
        ),
        CupertinoActionSheetAction(
          child: Text("Oportunidad Vencida"),
          onPressed: (){

          },
        ),
        CupertinoActionSheetAction(
          child: Text("PQR"),
          onPressed: (){

          },
        ),
        CupertinoActionSheetAction(
          child: Text("Evento"),
          onPressed: (){

          },
        ),
        CupertinoActionSheetAction(
          child: Text("Cliente"),
          onPressed: (){

          },
        ),
        CupertinoActionSheetAction(
          child: Text("Personalizado"),
          onPressed: (){

          },
        ),
      ],
    );
  }

  void _settingModalBottomSheetActivities(context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Wrap(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Tipo de actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Oportunidad ganada'),
                  onTap: () => _selectedItemActivity("Oportunidad ganada"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Oportunidad vencida'),
                  onTap: () => _selectedItemActivity("Oportunidad vencida"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('PQR'),
                  onTap: () => _selectedItemActivity("PQR"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Evento'),
                  onTap: () => _selectedItemActivity("Evento"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Cliente'),
                  onTap: () => _selectedItemActivity("Cliente"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Personalizado'),
                  onTap: () => _selectedItemActivity("Personalizado"),
                ),
              ],
            ),
          );
        }
    );
  }

  void _settingModalBottomSheetSubTypeActivities(context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Wrap(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Tipo de actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Oportunidad ganada'),
                  onTap: () => _selectedItemActivity("Oportunidad ganada"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Oportunidad vencida'),
                  onTap: () => _selectedItemActivity("Oportunidad vencida"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('PQR'),
                  onTap: () => _selectedItemActivity("PQR"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Evento'),
                  onTap: () => _selectedItemActivity("Evento"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Cliente'),
                  onTap: () => _selectedItemActivity("Cliente"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Personalizado'),
                  onTap: () => _selectedItemActivity("Personalizado"),
                ),
              ],
            ),
          );
        }
    );
  }

  void _settingModalBottomSheetOpportunities(context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Wrap(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Oportunidades", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: globalBoxSearch(context,hintText: "Buscar oportunidad", fOnchange: (value){})),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Oportunidad ganada'),
                  onTap: () => _selectedItemActivity("Oportunidad ganada"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Oportunidad vencida'),
                  onTap: () => _selectedItemActivity("Oportunidad vencida"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('PQR'),
                  onTap: () => _selectedItemActivity("PQR"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Evento'),
                  onTap: () => _selectedItemActivity("Evento"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Cliente'),
                  onTap: () => _selectedItemActivity("Cliente"),
                ),
                new ListTile(
                  leading: new SizedBox(width: 20,),
                  title: new Text('Personalizado'),
                  onTap: () => _selectedItemActivity("Personalizado"),
                ),
              ],
            ),
          );
        }
    );
  }

  void _showDatePicker(context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar fecha", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (value){
                      setState(() {

                      });
                    },
                  ),
                ),
                /* Row(children: <Widget>[
                 Container(
                   width: 200,
                   height: 200,
                   child: CupertinoDatePicker(
                     mode: CupertinoDatePickerMode.date,
                     onDateTimeChanged: (value){
                       setState(() {
                         print(date);
                         date = value;
                       });
                     },
                   ),
                 ),
                 Container(
                   width: 200,
                   height: 200,
                   child: CupertinoDatePicker(
                     mode: CupertinoDatePickerMode.date,
                     onDateTimeChanged: (value){
                       setState(() {
                         print(date);
                         date = value;
                       });
                     },
                   ),
                 ),
               ],),*/
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }

  void _showTimePicker(context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar hora", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.time,
                    onDateTimeChanged: (value){
                      setState(() {
                       // timeInitial = value;
                      });
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }

  void _selectedItemActivity(String item){
    Navigator.pop(context);
    setState(() {
      //optionTypeActivity =  item;
    });
  }

  Widget _addExternEmailWidget(){
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Column(
        children: <Widget>[
          Text("Añadir correo electronico externo"),
          SizedBox(height: 5,),
          Row(
            children: <Widget>[
              Flexible(
                child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey,
                            width: 5.0
                        )
                    ),
                    hintText: 'Preparar presentación',
                    // labelText: 'Actividad'
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.add_box, color: _themeApp.colorPink, size: 30,),
                onPressed: (){

                },
              )
            ],
          )
        ],
      ),
    );
  }
}
