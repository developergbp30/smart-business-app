import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_bloc.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_event.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_state.dart';
import 'package:smart_business/src/blocs/workplan/workplan_bloc.dart';
import 'package:smart_business/src/blocs/workplan/workplan_event.dart';
import 'package:smart_business/src/blocs/workplan/workplan_state.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';
import 'package:smart_business/src/presentation/screens/workplan/create_activity.dart';
import 'package:smart_business/src/presentation/screens/workplan/detail_activity.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/helper_color_gts.dart';
import 'package:smart_business/src/presentation/widgets/change_user_widget.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_left.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_task_month.dart';
import 'package:smart_business/src/presentation/widgets/modals/conventions.dart';
import 'package:smart_business/src/presentation/widgets/modals/modal_users.dart';
import 'package:smart_business/src/presentation/widgets/widget_circle_color.dart';
import 'package:smart_business/src/repositories/workplan/workplan_repository.dart';
import 'package:table_calendar/table_calendar.dart';
class WorkPlanPage extends StatefulWidget {
  static const String routeName = 'workplan_page';
  @override
  _WorkPlanPageState createState() => _WorkPlanPageState();
}

class _WorkPlanPageState extends State<WorkPlanPage> {
  ThemeApp _themeApp = ThemeApp();
  CalendarController _calendarController;
  Map<DateTime, List> _events;
  List _selectedEvents;
  DateTime _headerDate;
  WorkPlanBloc _workPlanBloc;
  DateTime _selectedDay;
  EventsCalendarBloc _eventsCalendarBloc;
  int userID = 0;

  @override
  void initState() {
    _calendarController = CalendarController();
     var yMonth =  DateFormat("yyyy-MM").format(DateTime.now());
     _eventsCalendarBloc = EventsCalendarBloc();
     _workPlanBloc = WorkPlanBloc(workPlanRepository: WorkPlanRepository());
    _workPlanBloc.dispatch(LoadWorkPlanEvent(idUser: userID, yearMonth: yMonth));

    super.initState();
    _headerDate = DateTime.now();
    _selectedDay = DateTime.parse(DateFormat("yyyy-MM-dd").format(DateTime.now()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(1.0),
      drawer: Drawer(
        child: DrawerLeft(),
      ),
      endDrawer: Drawer(
        child: DrawerTask(type: 1),
      ),
      appBar: AppBar(
        backgroundColor: _themeApp.colorPink,
        leading: Builder(builder: (context) => IconButton(icon: Icon(Icons.menu), onPressed: () => Scaffold.of(context).openDrawer()),),
        title: Text("Plan de trabajo"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
             bottom: Radius.circular(12),
        )
        ),
        actions: <Widget>[
          InkWell(
            child: AspectRatio(
                aspectRatio: 1.4,
                child: ChangeUserWidget()),
            onTap: (){
              _showUserModal(context);
            },
          )
        ],
      ),
      body: BlocListener(
        bloc: _workPlanBloc,
        listener: (context, state){
          if(state is ErrorWorkPlaneState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("${state.message}"),backgroundColor: Colors.red,));
          }
        },
        child: Column(
          children: <Widget>[
            customCalendarHeader(context),
            _buildCalendar(),
            SizedBox(height: 20,),
            Container(
              color: Colors.transparent,
              width: MediaQuery.of(context).size.width/2,
              padding: const EdgeInsets.all(0),
              child: FlatButton(
                padding: const EdgeInsets.all(0),
                color: Colors.transparent,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(50.0),
                      side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.info_outline,color: Colors.black54),
                      Text(' Convenciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                    ],
                  ),
                  // color: Color(0xff5D5D5D), //Colors.pink,
                  onPressed: () async {
                    modalConventions(context);
                  }),
            ),
            SizedBox(height: 10,),
            Expanded(child: _buildEventList(),)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: _themeApp.colorPink,
        onPressed:() async {
           var res =  await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateActivity()));

           if(res !=  null  && res){
             var yMonth =  DateFormat("yyyy-MM").format(DateTime.now());
             _workPlanBloc.dispatch(LoadWorkPlanEvent(idUser: userID, yearMonth: yMonth));
           }
        },
        tooltip: 'Crear',
        child: Icon(Icons.add),
      ),
    );
  }

  selectUser(UserModel val){
    print("USUARIO SELECCIONADO");
    print(val.toJson());
    userID = val.id;
    var yMonth =  DateFormat("yyyy-MM").format(DateTime.now());
    _workPlanBloc.dispatch(LoadWorkPlanEvent(idUser: userID, yearMonth: yMonth));
    Navigator.of(context).pop();
  }

  Widget customCalendarHeader(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
        Expanded(
          child: InkWell(
              child: Text("${DateFormat.y().format(_headerDate)} ${DateFormat.MMMM('es').format(_headerDate)}", textAlign: TextAlign.center, style: TextStyle(fontSize: 20),),
              onTap: (){
                _settingModalBottomSheetMonth(context);
              },
          )
        ),

          Builder(
            builder: (context) =>
                Container(
                  width: 50,
                  height: 50,
                  child: Center(
                    child: FlatButton(
                      color: _themeApp.colorPink,
                      child: Icon(FontAwesomeIcons.calendarCheck, color: Colors.white,),
                        onPressed: () => Scaffold.of(context).openEndDrawer()),
                  ),
                )
          ),

      ],
      )
    );

}

  Widget _buildCalendar(){
    return BlocBuilder<WorkPlanBloc, WorkPlanState>(
      bloc: _workPlanBloc,
      builder: (context, state){
        if(state is LoadedWorkPlaneState){

          _eventsCalendarBloc.dispatch(ChangeDayEventsCalendarEvent(state.events[_selectedDay]));

          return TableCalendar(
            locale: 'es_CO',
            events: state.events,
            headerVisible: false,
            calendarController: _calendarController,
            calendarStyle: CalendarStyle(
                todayColor: Colors.grey.withOpacity(0.5),
                selectedColor:_themeApp.colorPink
            ),
            headerStyle: HeaderStyle(
                centerHeaderTitle: true,
                formatButtonVisible: false
            ),
            builders: CalendarBuilders(
              markersBuilder: (context, date, events, holidays) {
                final children = <Widget>[];

                if (events.isNotEmpty) {
                  children.add(
                    Positioned(
                      right: 1,
                      bottom: 1,
                      child: _buildEventsMarker(date, events),
                    ),
                  );
                }

                if (holidays.isNotEmpty) {
                  children.add(
                    Positioned(
                      right: -2,
                      top: -2,
                      child: _buildHolidaysMarker(),
                    ),
                  );
                }

                return children;
              },
            ),
            onDaySelected: (date, events) {
              _onDaySelected(date, events);
            },
            onVisibleDaysChanged: (_, __, ___) {
              setState(() {
                _headerDate = _calendarController.focusedDay;
              });

              var yMonth =  DateFormat("yyyy-MM").format(_headerDate);
              _workPlanBloc.dispatch(LoadWorkPlanEvent(idUser: 0, yearMonth: yMonth));

            },
          );
        }
        return TableCalendar(
          locale: 'es_CO',
          events: _events,
          headerVisible: false,
          calendarController: _calendarController,
          calendarStyle: CalendarStyle(
              todayColor: Colors.grey.withOpacity(0.5),
              selectedColor:_themeApp.colorPink
          ),
          headerStyle: HeaderStyle(
              centerHeaderTitle: true,
              formatButtonVisible: false
          ),
          builders: CalendarBuilders(
            markersBuilder: (context, date, events, holidays) {
              final children = <Widget>[];

              if (events.isNotEmpty) {
                children.add(
                  Positioned(
                    right: 1,
                    bottom: 1,
                    child: _buildEventsMarker(date, events),
                  ),
                );
              }

              if (holidays.isNotEmpty) {
                children.add(
                  Positioned(
                    right: -2,
                    top: -2,
                    child: _buildHolidaysMarker(),
                  ),
                );
              }

              return children;
            },
          ),
          onDaySelected: (date, events) {
            _onDaySelected(date, events);
          },
          onVisibleDaysChanged: (_, __, ___) {
            setState(() {
              _headerDate = _calendarController.focusedDay;
            });
          },
        );
      },
    );
  }

  void _onDaySelected(DateTime day, List events) {
    _eventsCalendarBloc.dispatch(ChangeDayEventsCalendarEvent(events));
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: _calendarController.isSelected(date)
            ? Colors.grey.withOpacity(0.6)
            : _calendarController.isToday(date) ? Colors.grey.withOpacity(0.6) : Colors.grey.withOpacity(0.6),
      ),
      width: 16.0,
      height: 16.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.black54,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildEventList() {
   return BlocBuilder(
      bloc: _eventsCalendarBloc,
      builder: (context, state){
        if(state is ChangeDayEventsCalendarState){
          if(state.events == null) return Container();
          return ListView.builder(
              itemCount: state.events.length,
              itemBuilder: (context, i) =>  _cardItem(context, state.events[i]));
        }
        return Container();
      },
    );
  }

  Widget _cardItem(BuildContext context, WorkPlaneModel event) {
    return Container(
      margin: const EdgeInsets.all(5),
      child: InkWell(
        onTap: () async {
         var res = await  Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => DetailActivityPage(args: event,)));
           if(res !=  null  && res){
             var yMonth =  DateFormat("yyyy-MM").format(DateTime.now());
             _workPlanBloc.dispatch(LoadWorkPlanEvent(idUser: userID, yearMonth: yMonth));
           }
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        WidgetCircleColor(color: helperColorGts(GtsTodos(gtsTipos: event.gtsTipos)),),
                        Container(
                            margin: const EdgeInsets.only(left: 5),
                            width: MediaQuery.of(context).size.width/2,
                            child: Text("${event.titulo}", textAlign: TextAlign.left, style: TextStyle(color: helperColorGts(GtsTodos(gtsTipos: event.gtsTipos)), fontWeight: FontWeight.bold),)),

                      ],
                    ),

                    Badge(
                      badgeContent: Text('${event.gtsComantaryCount}', style: TextStyle(color: Colors.white),),
                      child: Icon(Icons.message, color: Colors.black54),
                    )
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(top: 5, left: 30),
                    width: MediaQuery.of(context).size.width,
                    child: Text(event.descripcion, style: TextStyle(color: Colors.grey.withOpacity(1.0)),)),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(top: 5, left: 30),
                  child: Text("${event.gtsTipos != null ? event.gtsTipos.nombre : ""}", textAlign: TextAlign.left, style: TextStyle(color: Colors.grey.withOpacity(1.0))),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                    Container(
                        margin: const EdgeInsets.only(top: 5, left: 30),
                        child: Text("${event.horaInicio} - ${event.horaFin}",style: TextStyle(color: Colors.grey.withOpacity(1.0)))),
                    _activityToTimeIcon(event.ptAtiempo, event.bFecha)
                  ],

                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /*Widget _buildBurbleNotification(int number){
    return Positioned(
      left: 13,
      bottom: 33,
      child: Container(
        decoration: new BoxDecoration(
            color: Colors.red,
            borderRadius: new BorderRadius.circular(10.0)
        ),
        height: 17,
        width: 17,
        padding: const EdgeInsets.all(1),
        child: Center(child: Text("$number", style: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.w500),)),
        // color: Colors.blueAccent,
      ),
    );
  }*/



  void _settingModalBottomSheetMonth(context){
    showModalBottomSheet(
      backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            padding: const EdgeInsets.only(bottom: 10),
            child: new Wrap(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar el mes", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
              ],
            ),
          );
        }
    );
  }

  Widget  _circleTitle(Color color, String text){
    return Row(
      children: <Widget>[
        WidgetCircleColor(color: color),
        Text(" $text")
      ],
    );
  }

  _showUserModal(BuildContext context){
    showModalBottomSheet(
       isScrollControlled: true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            height: MediaQuery.of(context).size.height/1.5,
             padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: ModalUsersWidget(function: selectUser,));
        }
    );
  }

  Widget _activityToTimeIcon(int toTime, String dateFinalized){
    print("P A tiempo ${toTime} $dateFinalized");
    if(dateFinalized == null){
      if(toTime == 1){
        return  Icon(FontAwesomeIcons.solidClock, color: Colors.green);
      }else if(toTime == 0){
        return Icon(FontAwesomeIcons.clock, color: Colors.orangeAccent,);
      }
      return SizedBox();
    }

    return Icon(Icons.check_circle, color: Colors.green,);
  }
}


