
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_business/src/presentation/screens/bitacora/bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/bitacora/detail_bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/clients/clients_page.dart';
import 'package:smart_business/src/presentation/screens/home/home_page.dart';
import 'package:smart_business/src/presentation/screens/invitations/invitations_page.dart';
import 'package:smart_business/src/presentation/screens/login/code_company_page.dart';
import 'package:smart_business/src/presentation/screens/login/login_page.dart';
import 'package:smart_business/src/presentation/screens/pdf_page/pdf_page.dart';
import 'package:smart_business/src/presentation/screens/programing/programing_page.dart';
import 'package:smart_business/src/presentation/screens/workplan/workplan_page.dart';

navigateToLogin(BuildContext context) =>
    Navigator.of(context).pushNamed(LoginPage.routeName);

navigateFromLoginToHome(BuildContext context) =>
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePage()),
        (Route<dynamic> route) => false);

navigateToWorkPlan(BuildContext context) =>
    Navigator.of(context).pushNamed(WorkPlanPage.routeName);

navigateToInvitations(BuildContext context) =>
    Navigator.of(context).pushNamed(InvitationsPage.routeName);

navigateToClients(BuildContext context) =>
    Navigator.of(context).pushNamed(ClientsPage.routeName);

navigateToBitacora(BuildContext context) =>
    Navigator.of(context).pushNamed(BitacoraPage.routeName);

navigateToDetailBitacora(BuildContext context) =>
    Navigator.of(context).pushNamed(DetailBitacoraPage.routeName);

navigateToForPrograming(BuildContext context) =>
    Navigator.of(context).pushNamed(ProgramingPage.routeName);

navigateToRootApp(BuildContext context) =>
    Navigator.of(context).pushNamedAndRemoveUntil(CodeCompanyPage.routeName,(Route<dynamic> route) => false);

navigateToPDFPage(BuildContext context) =>
    Navigator.of(context).pushNamed(PdfPage.routeName);