import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/request_balance_expenses.dart';

class ExpenseHttp extends ApiProvider {


  Future<List<ExpenseTypeModel>> typeExpense() async {
    try{

      Response response = await dio.get("/presupuestos_generales/gastos-tipos",
          options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ExpenseTypeModel.fromJson(json))
          .toList();

    }catch (error){
      return  Future.error(error);
    }
  }

  Future<ReqBalanceExpensesModel> balanceExpenses(int id) async {
    try{
      Response response = await dio.get("/presupuestos_generales/$id/valores-aprobados",
          options: Options(headers: {"requirestoken": true}));
      print("Rs: ${response.data}");
      return ReqBalanceExpensesModel.fromJson(response.data);
    }catch (error){
      print("Error aqui $error");
      return  Future.error(error);
    }
  }
}