class ResponseProjectModel {
  int value;
  String label;
  Clientes clientes;
  int idCliente;

  ResponseProjectModel({this.value, this.label, this.clientes, this.idCliente});

  ResponseProjectModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    label = json['label'];
    clientes = json['clientes'] != null
        ? new Clientes.fromJson(json['clientes'])
        : null;
    idCliente = json['id_cliente'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['label'] = this.label;
    if (this.clientes != null) {
      data['clientes'] = this.clientes.toJson();
    }
    data['id_cliente'] = this.idCliente;
    return data;
  }
}

class Clientes {
  int id;
  int kpiValor;
  String kpiUpdated;
  String nombreLegal;
  String nombreCorto;
  String logo;
  String logoMini;
  String banner1;
  dynamic banner2;
  int tamano;
  String fechaInauguracion;
  int fechaExacta;
  int utilidadEstado;
  int creditoDias;
  int creditoCupo;
  dynamic idmCiudad;
  int idSedePrincipal;
  int idFormulario;
  dynamic idOrigen;
  dynamic idEvento;
  int idUserIdentifico;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  dynamic deletedAt;

  Clientes(
      {this.id,
        this.kpiValor,
        this.kpiUpdated,
        this.nombreLegal,
        this.nombreCorto,
        this.logo,
        this.logoMini,
        this.banner1,
        this.banner2,
        this.tamano,
        this.fechaInauguracion,
        this.fechaExacta,
        this.utilidadEstado,
        this.creditoDias,
        this.creditoCupo,
        this.idmCiudad,
        this.idSedePrincipal,
        this.idFormulario,
        this.idOrigen,
        this.idEvento,
        this.idUserIdentifico,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  Clientes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    kpiValor = json['kpi_valor'];
    kpiUpdated = json['kpi_updated'];
    nombreLegal = json['nombre_legal'];
    nombreCorto = json['nombre_corto'];
    logo = json['logo'];
    logoMini = json['logo_mini'];
    banner1 = json['banner_1'];
    banner2 = json['banner_2'];
    tamano = json['tamano'];
    fechaInauguracion = json['fecha_inauguracion'];
    fechaExacta = json['fecha_exacta'];
    utilidadEstado = json['utilidad_estado'];
    creditoDias = json['credito_dias'];
    creditoCupo = json['credito_cupo'];
    idmCiudad = json['idm_ciudad'];
    idSedePrincipal = json['id_sede_principal'];
    idFormulario = json['id_formulario'];
    idOrigen = json['id_origen'];
    idEvento = json['id_evento'];
    idUserIdentifico = json['id_user_identifico'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['kpi_valor'] = this.kpiValor;
    data['kpi_updated'] = this.kpiUpdated;
    data['nombre_legal'] = this.nombreLegal;
    data['nombre_corto'] = this.nombreCorto;
    data['logo'] = this.logo;
    data['logo_mini'] = this.logoMini;
    data['banner_1'] = this.banner1;
    data['banner_2'] = this.banner2;
    data['tamano'] = this.tamano;
    data['fecha_inauguracion'] = this.fechaInauguracion;
    data['fecha_exacta'] = this.fechaExacta;
    data['utilidad_estado'] = this.utilidadEstado;
    data['credito_dias'] = this.creditoDias;
    data['credito_cupo'] = this.creditoCupo;
    data['idm_ciudad'] = this.idmCiudad;
    data['id_sede_principal'] = this.idSedePrincipal;
    data['id_formulario'] = this.idFormulario;
    data['id_origen'] = this.idOrigen;
    data['id_evento'] = this.idEvento;
    data['id_user_identifico'] = this.idUserIdentifico;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}


class TaskProjectModel {
  int id;
  String nombre;
  int idCliente;
  int idTipo;
  Clientes cliente;
  Tipo tipo;

  TaskProjectModel(
      {this.id,
        this.nombre,
        this.idCliente,
        this.idTipo,
        this.cliente,
        this.tipo});

  TaskProjectModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    idCliente = json['id_cliente'];
    idTipo = json['id_tipo'];
    cliente =
    json['cliente'] != null ? new Clientes.fromJson(json['cliente']) : null;
    tipo = json['tipo'] != null ? new Tipo.fromJson(json['tipo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['id_cliente'] = this.idCliente;
    data['id_tipo'] = this.idTipo;
    if (this.cliente != null) {
      data['cliente'] = this.cliente.toJson();
    }
    if (this.tipo != null) {
      data['tipo'] = this.tipo.toJson();
    }
    return data;
  }
}



class Tipo {
  int id;
  String nombre;
  String color;

  Tipo({this.id, this.nombre, this.color});

  Tipo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    color = json['color'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['color'] = this.color;
    return data;
  }
}

