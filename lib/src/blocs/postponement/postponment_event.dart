import 'package:equatable/equatable.dart';

abstract class PostponementEvent extends Equatable {
  PostponementEvent([List props = const []]) : super(props);
}

class LoadHistoryPostponementEvent extends PostponementEvent{
  final int idGts;
  final int page;
  LoadHistoryPostponementEvent({this.page, this.idGts}) : super([page, idGts]);

  @override
  String toString()  => '$runtimeType { idGts : $idGts page: $page }';
}