import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_event.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_state.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_invitation_bloc.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/for_views/data_screen_bitacora.dart';
import 'package:smart_business/src/presentation/screens/comments/comments_page.dart';
import 'package:smart_business/src/presentation/screens/history_postponement/history_postponement_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/calendar_day/calendar_day.dart';

import '../create_bitacora_page.dart';

class TypeFileWidget{
  IconData icon;
  Color color;
  TypeFileWidget({this.icon, this.color});
}

class SegmentDetail extends StatelessWidget {
  ResponseDetailBitacoraModel args;
  String token;
  SegmentDetail({this.args, this.token});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: MultiBlocListener(
        listeners: [
          BlocListener<BitacoraBloc, BitacoraState>(
            listener: (context, state){
              if(state is SuccessDeleteBinnacleState){
                Scaffold.of(context).showSnackBar(SnackBar(content: Text("Eliminado"), backgroundColor: Colors.green,));
                Future.delayed(Duration(seconds: 2)).then((time){
                  Navigator.of(context).pop();
                });
              }
            },
          )
        ],
        child: ListView(
          children: <Widget>[
            Text("${args.id}"),
            _cardInfo(context),
            _dateFinalized(args.bFecha),
            ListTile(
              title: RichText(text: TextSpan(
                  text: "Tipo: ",
                  style: TextStyle(color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: "${args.tipoActividad}",
                      style: TextStyle(color: Colors.grey[600]),
                    )
                  ]
              ),),
              subtitle: Padding(padding: const EdgeInsets.only(left: 30),
                child: args.nombreActividad != null  ?
                Text("${args.nombreActividad.codigo != null ? args.nombreActividad.codigo+" - " : ""} "
                    " ${args.nombreActividad.nombre}", textAlign: TextAlign.left,)
                    : SizedBox()
                ,),
            ),
            ListTile(
              title: RichText(text: TextSpan(
                  text: "Subtipo: ",
                  style: TextStyle(color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: "${args.gtsTipos != null ? args.gtsTipos.nombre : ""}",
                      style: TextStyle(color: Colors.grey[600]),
                    )
                  ]
              ),),
            ),
            _client(),
            ListTile(
              title: Text("Descripción", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
              subtitle: Text(args.descripcion, style: TextStyle(fontStyle: FontStyle.italic),),
            ),
            SizedBox(height: 20,),
            Divider(),
            ListTile(
              title: Text("Resultados", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
              subtitle: Text("${args.bResultados}", style: TextStyle(fontStyle: FontStyle.italic),),
            ),
            Divider(),
            Wrap(
                children:  _renderDocuments()
            ),

            SizedBox(height: 20,),
            _activityApproved(),
            // Row Contacts
            Center(
              child: Badge(
                  elevation: 0,
                  badgeColor: Colors.grey[200],
                  shape: BadgeShape.square,
                  borderRadius: 20,
                  toAnimate: false,
                  badgeContent:
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.person,color: Colors.black54),
                      Text(' Contactos', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                    ],)
              ),
            ),
            // <------- list of contacts --->
            _contacts(context),
            // <-- finish list of contacts -->
            // Row invitations
            Center(
              child: Badge(
                  elevation: 0,
                  badgeColor: Colors.grey[200],
                  shape: BadgeShape.square,
                  borderRadius: 20,
                  toAnimate: false,
                  badgeContent:
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.email,color: Colors.black54),
                      Text(' Invitaciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                    ],)
              ),
            ),
            // <------- list of invitations --->
            _guestUsers(context),
            // <-- finish list of invitations -->
            // Row Notification activity
            Center(
              child: Badge(
                  elevation: 0,
                  badgeColor: Colors.grey[200],
                  shape: BadgeShape.square,
                  borderRadius: 20,
                  toAnimate: false,
                  badgeContent:
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.email,color: Colors.black54),
                      Text('! Notificación de actividad', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                    ],)
              ),
            ),
            // <------- list of Notification activity --->
            _notificationEmail(context),
            // <-- finish list of Notification activity -->
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 1),
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: (){
                    _modalDelete(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.trash),
                    Text("Eliminar")
                  ],
                ),
              ),
              SizedBox(width: 10,),
              InkWell(
                onTap: () async {
                  SetFormBitacora set =  new SetFormBitacora(
                    id: args.id,
                    idClient: args.idCliente,
                    idActa: args.idActa,
                    idOpportunity: args.idOportunidad,
                    title: args.tipoActividad,
                    typeActivity: args.nombreActividad !=  null ? args.nombreActividad.nombre : '',
                    idTypeActivity: args.nombreActividad !=  null ? args.nombreActividad.id : null,
                    subTypeActivity: args.gtsTipos!= null ? args.gtsTipos.nombre : '',
                    nameTask: args.titulo,
                    description: args.descripcion,
                    results: args.bResultados,
                    date: args.bFecha,
                    startTime: args.bHoraInicio,
                    endTime: args.bHoraFin,
                    disabledModalDynamic: true,
                    disabledModalAct: true,

                  );
                  await Navigator.push(context, new MaterialPageRoute(builder: (context) => CreateBitacoraPage(setFormBitacora: set,)));
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.pencilAlt),
                    Text("Editar")
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _cardInfo(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(10.0),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomLeft,
            stops: [0.3, 1],
            colors: [Color(0xCC079CE5),  Color(0xff079CE5)],


          )
      ),
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.only(top: 8, left: 10, right: 10, bottom: 8),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Flexible(child: Text("${args.titulo}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),))
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text("${DateFormat("dd MMM yyyy").format(DateTime.parse(args.bFecha).toLocal()) }, ${args.bHoraInicio} - ${args.bHoraFin}", style: TextStyle(color: Colors.white))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              InkWell(
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryPostponement(idGts: args.id,)));
                },
                child: Badge(
                  badgeContent: Text('${args.gtsAplazamientosCount}', style: TextStyle(color: Colors.white)),
                  child: Icon(FontAwesomeIcons.clock, color: Colors.white),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              InkWell(
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context) => CommentsPage(idGts: args.id,)));
                },
                child: Badge(
                  badgeContent: Text('${args.gtsComentariosCount}', style: TextStyle(color: Colors.white),),
                  child: Icon(Icons.message, color: Colors.white),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  _client(){
    if(args.cliente !=  null){
      return  ListTile(
        leading:  new CircleAvatar(
          backgroundImage: loadImageApi(args.cliente.logo, token)
        ),
        title: Text(args.cliente.nombreCorto, style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
        subtitle: Text("Cliente", style: TextStyle(fontStyle: FontStyle.italic),),
      );
    }
    return SizedBox();
  }

  List<Widget> _renderDocuments(){
    List<Widget> items = [];
    if(args.gtsAdjuntos !=  null && args.gtsAdjuntos.length > 0){
      args.gtsAdjuntos.forEach((item){
        TypeFileWidget typeFileWidget =  _typeFileIcon(item.archivo);
        items.add(ListTile(
          leading: Icon(typeFileWidget.icon, color: typeFileWidget.color,),
          title: Text(item.nombre),
        ));
      });
    }
    return items;
  }

  TypeFileWidget _typeFileIcon(String name){
    if(name.contains(".jpg") || name.contains(".png")){
      return new TypeFileWidget(icon: Icons.image, color: Colors.green);
    }
    if(name.contains(".pdf")){

      return new TypeFileWidget(icon: FontAwesomeIcons.solidFilePdf, color: Colors.red);
    }

    return new TypeFileWidget(icon: FontAwesomeIcons.file, color: Colors.grey);
  }

  Widget _dateFinalized(String date){

    if(date == null) return Container();
    var month = DateFormat.MMM().format(DateTime.parse(date));
    var day = DateFormat.d().format(DateTime.parse(date));
    var year = DateFormat.y().format(DateTime.parse(date));

    print("${args.bHoraInicio}   ${args.bHoraFin}");

    return  ListTile(
      leading: CalendarDay(month: month, day: day),
      title: Row(children: <Widget>[
        Icon(Icons.check_circle, color: Colors.green,),
        SizedBox(width: 10,),
        Text("Realizada", style: TextStyle(color: Colors.green, fontStyle: FontStyle.italic),)
      ],),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("${args.bHoraInicio}  -  ${args.bHoraFin}", style: TextStyle(fontSize: 14, color: Colors.black87)),
          Text("$year", style: TextStyle(fontSize: 12, color: Colors.grey.withOpacity(0.6)),)
        ],
      ),
    );
  }

  Widget _contacts(BuildContext context){
    return BlocBuilder<GtsInvitationBloc, GtsInvitationState>(
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          if(state.contacts == null || state.contacts.length == 0) return Container(width:MediaQuery.of(context).size.width, height: 20);
         // listContacts =  state.contacts;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.contacts.length,
              itemBuilder: (context, i) => ListTile(
                leading:  new CircleAvatar(
                    backgroundImage: loadImageApi(state.contacts[i].foto, state.token)
                ),
                title: Text("${state.contacts[i].nombre}"),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _guestUsers(BuildContext context){
    return BlocBuilder<GtsInvitationBloc, GtsInvitationState>(
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          if(state.users == null || state.users.length == 0) return Container(width:MediaQuery.of(context).size.width, height: 20);
         // listGuest =  state.users;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.users.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                  position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.white,
                  badgeContent: _iconStatusGuestUser(state.users[i].respuesta),
                  child: new CircleAvatar(
                      backgroundImage: loadImageApi(state.users[i].datosInvitados.foto, state.token)
                  ),
                ),
                title: Text("${state.users[i].datosInvitados.nombre}"),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _iconStatusGuestUser(var value){
    if(value == null){
      return Container();
    }
    else if(value == 1) return Icon(Icons.check_circle, color: ThemeApp().colorPink,);
    if(value == 2 || value == 3 ) return Icon(Icons.cancel, color: Colors.grey.withOpacity(0.6),);

    return Container();
  }

  Widget _notificationEmail(BuildContext context){
    return BlocBuilder<GtsInvitationBloc, GtsInvitationState>(
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
        //  listEmails = state.email;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.email.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                  position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.transparent,
                  badgeContent: _iconStatusGuestUser(state.email[i].respuesta),
                  child:  state.email[i].datosInvitados == null ?
                  new CircleAvatar(
                      child: Icon(FontAwesomeIcons.envelope, color: Colors.grey,),
                      backgroundColor: Colors.grey.withOpacity(0.2))
                      : new CircleAvatar(
                      backgroundImage: loadImageApi(state.email[i].datosInvitados.foto, state.token)
                  ) ,
                ),
                title: Text("${state.email[i].datosInvitados != null ? state.email[i].datosInvitados.nombre : state.email[i].correoExterno}"),
                subtitle: state.email[i].datosInvitados == null ? Text("correo electronico externo") : Text(state.email[i].datosInvitados.cargo),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _activityApproved(){
    return BlocBuilder<GtsInvitationBloc, GtsInvitationState>(
        builder: (context, state){
          if(state is UnloadGtsGuestUsers){
            BlocProvider.of<GtsInvitationBloc>(context).dispatch(LoadGtsGuestUsersEvent(args.id));
          }
          if(state is LoadedGtsGuestUsers){
            if(state.userApproved.datosUsuario != null){
              return Container(
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.all(10),
                decoration: new BoxDecoration(
                    color: Colors.grey[250],
                    border: Border.all(color: Colors.grey[350], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text("Actividad aprobada", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Chip(
                          avatar: CircleAvatar(
                            backgroundImage: loadImageApi(state.userApproved.datosUsuario.foto, state.token),
                            // child: ,
                          ),
                          label:Text(state.userApproved.datosUsuario.nombre),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.calendar_today, color: Colors.green,),
                            Text("${DateFormat("dd MMMM yyyy").format(DateTime.parse(state.userApproved.ptAprobadoFecha).toLocal())}", style: TextStyle(color: Colors.grey[600]),)
                          ],
                        )
                      ],
                    )
                  ],
                ),
              );
            }
            return Container(
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.all(10),
              decoration: new BoxDecoration(
                  color: Colors.grey[250],
                  border: Border.all(color: Colors.grey[350], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Column(
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text("Actividad pendiente por aprobación", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[],
                  )
                ],
              ),
            );
          }
          return Container();
        });
  }

  void _modalDelete(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Eliminar bitácora", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20),
                  Text("¿Esta seguro que desea eliminar esta bitácora?"),
                  SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: new ThemeApp().colorPink),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            BlocProvider.of<BitacoraBloc>(context).dispatch(DeleteBinnacleEvent(id: args.id));
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          );
        }
    );
  }
}
