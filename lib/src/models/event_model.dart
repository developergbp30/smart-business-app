class ResponseEventModel {
  int value;
  String label;

  ResponseEventModel({this.value, this.label});

  ResponseEventModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['label'] = this.label;
    return data;
  }
}


class TaskEventModel {
  int id;
  String nombre;
  String fechaInicio;
  int idPadre;
  int idTipoEvento;
  String version;
  String titulo;
  Padre padre;
  Padre elTipo;

  TaskEventModel(
      {this.id,
        this.nombre,
        this.fechaInicio,
        this.idPadre,
        this.idTipoEvento,
        this.version,
        this.titulo,
        this.padre,
        this.elTipo});

  TaskEventModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    fechaInicio = json['fecha_inicio'];
    idPadre = json['id_padre'];
    idTipoEvento = json['id_tipo_evento'];
    version = json['version'];
    titulo = json['titulo'];
    padre = json['padre'] != null ? new Padre.fromJson(json['padre']) : null;
    elTipo =
    json['el_tipo'] != null ? new Padre.fromJson(json['el_tipo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['fecha_inicio'] = this.fechaInicio;
    data['id_padre'] = this.idPadre;
    data['id_tipo_evento'] = this.idTipoEvento;
    data['version'] = this.version;
    data['titulo'] = this.titulo;
    if (this.padre != null) {
      data['padre'] = this.padre.toJson();
    }
    if (this.elTipo != null) {
      data['el_tipo'] = this.elTipo.toJson();
    }
    return data;
  }
}

class Padre {
  int id;
  String nombre;

  Padre({this.id, this.nombre});

  Padre.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    return data;
  }
}
