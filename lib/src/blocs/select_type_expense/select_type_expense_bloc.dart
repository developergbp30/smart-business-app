import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/select_type_expense/select_type_expense_event.dart';
import 'package:smart_business/src/blocs/select_type_expense/select_type_expense_state.dart';
import 'package:smart_business/src/repositories/expenses/expense_repository.dart';
import 'package:meta/meta.dart';
class SelectExpenseBloc extends Bloc<SelectExpenseEvent, SelectExpenseState>{
  ExpenseRepository  _expenseRepository;
  SelectExpenseBloc({@required ExpenseRepository expenseRepository}) : assert(expenseRepository !=  null),
        _expenseRepository = expenseRepository;


  @override
  SelectExpenseState get initialState => UnloadDataSelectExpenseState();

  @override
  Stream<SelectExpenseState> mapEventToState(SelectExpenseEvent event) async* {

    if(event is GetTypeExpenseEvent){
        yield* _mapGetTypeSelectToState();
    }
    else  if(event is SelectExpenseEvent){

    }
  }

  Stream<SelectExpenseState> _mapGetTypeSelectToState() async* {
    try{
        var res  =  await _expenseRepository.typeExpense();
        yield ListTypeExpenseState(types: res);
    }catch (error){

    }
  }

}