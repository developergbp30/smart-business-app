import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/campus_segment/detail_campus.dart';

class ExampleModel{
  String urlImage;
  String title;
  String direction;
  ExampleModel({this.urlImage, this.title, this.direction});
}

class HeadQuartersSegment extends StatelessWidget {
  List<ExampleModel> list = [
    new ExampleModel(urlImage: "https://cdn.pixabay.com/photo/2014/05/03/00/37/one-world-trade-center-336594_960_720.jpg", title: "Estados Unidos, New York", direction: "111 8th Ave, New York, NY 10011, Estados Unidos"),
    new ExampleModel(urlImage: "https://cdn.pixabay.com/photo/2016/01/19/19/26/amsterdam-1150319_960_720.jpg", title: "Paises Bajos, Amsterdam", direction: "Claude Debussylaan 34, 1082 MD"),
    new ExampleModel(urlImage: "https://cdn.pixabay.com/photo/2017/07/26/18/50/lighthouse-2542726_960_720.jpg", title: "Irlanda, Dublin", direction: "Google Building Gord House, 4 Barrow St, D04"),
    new ExampleModel(urlImage: "https://cdn.pixabay.com/photo/2019/08/06/11/58/amsterdam-4388160_960_720.jpg", title: "Paises Bajos, Amsterdam", direction: "Claude Debussylaan 34, 1082 MD"),
  ];
  @override
  Widget build(BuildContext context) {

    return  Container(
      height: MediaQuery.of(context).size.height/1.5,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[Text("Sedes", style: TextStyle(color: Color(0xFF11BA19), fontWeight: FontWeight.w600),)],),
          ),
          //--->  Button Maps
          Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 20, right: 20),
            color: Colors.transparent,
            padding: const EdgeInsets.all(8),
            child: FlatButton(
                padding: const EdgeInsets.all(0),
                color: Colors.transparent,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(50.0),
                    side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(FontAwesomeIcons.map,color: Colors.green),
                    SizedBox(width: 6,),
                    Text(" Visualizar en mapa", style: TextStyle(color: Colors.green),),
                  ],
                ),
                // color: Color(0xff5D5D5D), //Colors.pink,
                onPressed: () async {}),
          ),
          //-->  Finish Button Maps
          Expanded(child: ListView.builder(
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (context, i) => itemWidget(context, list[i])))
        ],
      ),
    );
  }

  Widget itemWidget(BuildContext context, ExampleModel example){

    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      child: InkWell(
        onTap: () async {
         await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => DetailCampus()));
        },
        child: Card(
          elevation: 2,
          child: Column(children: <Widget>[
            Row(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    image: DecorationImage(
                    image:NetworkImage(example.urlImage) ,
                    fit: BoxFit.cover,
                    )
                  ),
                  //  color: Colors.blueGrey,
                  height: 100,
                  width: 110,
                 // child: Image.network(example.urlImage),
                ),
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Icon(FontAwesomeIcons.flag, color: Color(0xFF07703C), size: 20,),
                      SizedBox(width: 2,),
                      Flexible(child: Text(example.title, style: TextStyle(color: Color(0xFF5D5D5D)),))
                    ],),
                    Row(children: <Widget>[
                      Icon(FontAwesomeIcons.mapMarker, color: Color(0xFF07703C), size: 20,),
                      Flexible(child: Text(example.direction,style: TextStyle(color: Color(0xFF5D5D5D), )))
                    ],),
                  ],
                ),
              )
            ],)
          ],),),
      ),
    );
  }
}
