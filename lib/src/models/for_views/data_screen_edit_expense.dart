class SetFormExpense{
  int id;
  int idBinnacle;
  String title;
  String description;
  int value;
  int type;

  SetFormExpense({
    this.id,
    this.idBinnacle,
    this.title,
    this.description,
    this.value,
    this.type});
}