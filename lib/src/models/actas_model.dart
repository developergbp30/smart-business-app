import 'package:smart_business/src/models/workplane_model.dart';

import 'opportunity_model.dart';

class TaskActaModel {
  int id;
  Null tipo;
  String fecha;
  Null horaInicio;
  Null horaFin;
  String titulo;
  String descripcion;
  String cliente;
  Clientes informacionCliente;

  TaskActaModel(
      {this.id,
        this.tipo,
        this.fecha,
        this.horaInicio,
        this.horaFin,
        this.titulo,
        this.descripcion,
        this.cliente});

  TaskActaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    fecha = json['fecha'];
    horaInicio = json['hora_inicio'];
    horaFin = json['hora_fin'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    cliente = json['cliente'];
    informacionCliente = json['informacion_cliente'] != null ? new Clientes.fromJson(json['informacion_cliente']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['fecha'] = this.fecha;
    data['hora_inicio'] = this.horaInicio;
    data['hora_fin'] = this.horaFin;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['cliente'] = this.cliente;
    return data;
  }
}


class ActaParcialModel {
  int id;
  dynamic tipo;
  String fecha;
  String horaInicio;
  String horaFin;
  String titulo;
  String descripcion;
  String cliente;
  int idCliente;
  int idDinamico;
  int idPqr;
  int idOportunidad;
  int idActa;
  int idEventos;
  int idCompetencias;
  String miTipo;

  ActaParcialModel(
      {this.id,
        this.tipo,
        this.fecha,
        this.horaInicio,
        this.horaFin,
        this.titulo,
        this.descripcion,
        this.cliente,
        this.idCliente,
        this.idDinamico,
        this.idPqr,
        this.idOportunidad,
        this.idActa,
        this.idEventos,
        this.idCompetencias,
        this.miTipo});

  ActaParcialModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    fecha = json['fecha'];
    horaInicio = json['hora_inicio'];
    horaFin = json['hora_fin'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    cliente = json['cliente'];
    idCliente = json['id_cliente'];
    idDinamico = json['id_dinamico'];
    idPqr = json['id_pqr'];
    idOportunidad = json['id_oportunidad'];
    idActa = json['id_acta'];
    idEventos = json['id_eventos'];
    idCompetencias = json['id_competencias'];
    miTipo = json['mi_tipo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['fecha'] = this.fecha;
    data['hora_inicio'] = this.horaInicio;
    data['hora_fin'] = this.horaFin;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['cliente'] = this.cliente;
    data['id_cliente'] = this.idCliente;
    data['id_dinamico'] = this.idDinamico;
    data['id_pqr'] = this.idPqr;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_acta'] = this.idActa;
    data['id_eventos'] = this.idEventos;
    data['id_competencias'] = this.idCompetencias;
    data['mi_tipo'] = this.miTipo;
    return data;
  }
}

