import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/clients/clients_event.dart';
import 'package:smart_business/src/blocs/clients/clients_state.dart';
import 'package:smart_business/src/repositories/clients/clients_repository.dart';
import 'package:meta/meta.dart';

class ClientBloc extends Bloc<ClientsEvent, ClientsState> {
  final ClientRepository _clientRepository;

  ClientBloc({@required ClientRepository clientRepository})
      : assert(clientRepository != null),
        _clientRepository = clientRepository;

  @override
  ClientsState get initialState => UnloadClientsState();

  @override
  Stream<ClientsState> mapEventToState(ClientsEvent event) async* {
    if (event is LoadClientsEvent) {
      yield* _mapClientListToState();
    } else if (event is ChangeClientEvent) {
      yield* _mapChangeClientTState(event);
    }
    if(event is FilterClientEvent){
      yield* _mapFilterToState(event);
    }
  }

  Stream<ClientsState> _mapClientListToState() async* {
    try {
      yield SendingRequestClientsState();
      final res = await _clientRepository.getClients();
      yield LoadedClientsState(clients: res, preview: null);
    } catch (error) {
      print(error);
      yield ErrorRequestCliState(message: error.toString());
    }
  }

  Stream<ClientsState> _mapChangeClientTState(ChangeClientEvent event) async* {
    try {
      yield SendingRequestClientsState();
      final res = await _clientRepository.changeClient(event.clientID);
      yield ClientChangedState(res.toString());
      await Future.delayed(Duration(seconds: 2));
      dispatch(LoadClientsEvent());
    } catch (error) {
      print(error);
      yield ErrorRequestCliState(message: error.toString());
    }
  }

  Stream<ClientsState> _mapFilterToState(FilterClientEvent event) async*{
    try{
      var filter =  event.clients.where((item) => item.nombreCorto.contains(event.query)).toList();
      yield LoadedClientsState(clients: filter, preview: event.clients);
    }catch (error){
      yield ErrorRequestCliState(message: error.toString());
    }
  }
}
