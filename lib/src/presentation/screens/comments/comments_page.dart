import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_business/src/blocs/comments/comments_bloc.dart';
import 'package:smart_business/src/blocs/comments/comments_event.dart';
import 'package:smart_business/src/blocs/comments/comments_state.dart';
import 'package:smart_business/src/models/comments_model.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/repositories/comments/comments_repository.dart';
class CommentsPage extends StatefulWidget {
  final int idGts;
  CommentsPage({@required this.idGts});

  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  ThemeApp _themeApp = ThemeApp();
  CommentBloc _commentBloc;
  final _formKey =  GlobalKey<FormState>();
  CommentCreateModel createModel;
  TextEditingController commentaryController = new TextEditingController();
  ScrollController _controller = ScrollController();

  String tokenUser;

  @override
  void initState() {
    getToken();
    _commentBloc = CommentBloc(commentsRepository: CommentsRepository());
    _commentBloc.dispatch(LoadCommentsEvent(widget.idGts));
    createModel = new CommentCreateModel(idGts: widget.idGts);
    super.initState();
  }

  getToken() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      tokenUser =  prefs.get("token");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: _themeApp.colorPink, //change your color here
        ),
        leading: IconButton(icon:Icon( Icons.arrow_back_ios),
          onPressed: (){
            Navigator.pop(context);
          },),
        backgroundColor: Colors.white,
        title: Text("Comentarios", style: TextStyle(color: _themeApp.colorPink),),),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(child: BlocBuilder(
                  bloc: _commentBloc,
                  builder: (context, state){
                if(state is LoadedCommentsState){
                  Timer(Duration(milliseconds: 1000), () => _controller.jumpTo(_controller.position.maxScrollExtent));
                  return ListView.builder(
                    controller: _controller,
                      itemCount: state.comments.length,
                      itemBuilder: (context, i) => showComment(state.comments[i]));
                }
                return Container();
              })),
              _inputBootom()
            ],
          )
        ],
      ),
    );
  }

   Widget showComment(CommentModel comment) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(bottom: 5, top: 5, right: 10, left: 5),
      child:  Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new CircleAvatar(
            backgroundImage: loadImageApi(comment.datosUsario.foto, tokenUser),
          ),
          Flexible(
            child:  Container(
              margin: const EdgeInsets.only(left: 5),
              padding: const EdgeInsets.all(8.0),
              decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.circular(10.0),
                  color: Colors.grey.withOpacity(0.2)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(comment.datosUsario.nombre, style: TextStyle(color: _themeApp.colorPink),),
                  Text(comment.comentario),
                  SizedBox(
                    height: 10,
                  ),
                  Text("${DateFormat("dd MMMM yyyy").format(DateTime.parse(comment.createdAt))}")
                ],
              ),
            ),
          )
        ],
      ),
    );
   }

  _inputBootom() {
    return Container(
      padding: const EdgeInsets.all(2.0),
      //height: 50,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Form(
            key: _formKey,
            child: Flexible(
              child:  TextFormField(
                controller: commentaryController,
                  minLines: 1,
                  maxLines: 5,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(2),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.blueGrey,
                            width: 5.0
                        )
                    ),
                    hintText: 'Escribe un comentario',
                    //suffixIcon: Icon(Icons.send, color: _themeApp.colorPink,),
                    // labelText: 'Usuario'
                  ),
                  /*validator: (value){
                    if(value.isEmpty){
                      return  "";
                    }
                    return null;
                  },*/
                  onSaved: (value){
                    setState(() {
                      createModel.message = value;
                    });
                  }
              ),
            ),
          ),
          IconButton(
            icon: Icon(Icons.send, color: _themeApp.colorPink,),
            onPressed: (){
              if(commentaryController.text != ""){
                _formKey.currentState.save();
                _commentBloc.dispatch(CreateCommentsEvent(createModel));
                _formKey.currentState.reset();
              }

            },
          )
        ],
      ),
    );
  }
}
