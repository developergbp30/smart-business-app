import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_event.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_state.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_invitation_bloc.dart';
import 'package:smart_business/src/blocs/workplan/workplan_bloc.dart';
import 'package:smart_business/src/blocs/workplan/workplan_event.dart';
import 'package:smart_business/src/blocs/workplan/workplan_state.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/guest_user_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';
import 'package:smart_business/src/presentation/screens/comments/comments_page.dart';
import 'package:smart_business/src/presentation/screens/history_postponement/history_postponement_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/calendar_day/calendar_day.dart';
import 'package:smart_business/src/presentation/widgets/steper_datepicker/steper_datepicker.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
import 'package:smart_business/src/repositories/workplan/workplan_repository.dart';

import 'edit_activity.dart';

class DetailActivityPage extends StatefulWidget {
  WorkPlaneModel args;

  DetailActivityPage({this.args});

  @override
  _DetailActivityPageState createState() => _DetailActivityPageState();
}

class _DetailActivityPageState extends State<DetailActivityPage> {

  ThemeApp _themeApp = ThemeApp();
  String tokenUser;
  GtsInvitationBloc _gtsInvitationBloc;
  TextEditingController descriptionCtrl;
  WorkPlanBloc _workPlanBloc;

  DateTime changeDate;
  DateTime changeStartTime;
  DateTime changeEndTime;

  List<RequestContactModel> listContacts;

  List<GuestUserModel> listGuest;

  List<GuestUserModel> listEmails;

  @override
  void initState() {
    getToken();
    _workPlanBloc = WorkPlanBloc(workPlanRepository: WorkPlanRepository());

    descriptionCtrl  = new TextEditingController();
    _gtsInvitationBloc = GtsInvitationBloc(invitationsRepository: InvitationsRepository());
    _gtsInvitationBloc.dispatch(LoadGtsGuestUsersEvent(widget.args.id));
    super.initState();
  }

  getToken() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      tokenUser =  prefs.get("token");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(1.0),
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: _themeApp.colorPink, //change your color here
        ),
        leading: IconButton(icon:Icon( Icons.arrow_back_ios),
          onPressed: (){
          Navigator.pop(context);
        },),
        backgroundColor: Colors.white.withOpacity(1.0),
        title: Text("Detalle de la actividad", style: TextStyle(color: _themeApp.colorPink),),),
      body: BlocListener(
        bloc: _workPlanBloc,
        listener: (context, state){
          if(state is SuccessRejectWorkPlaneState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Plan de trabajo rechazado"), backgroundColor: Colors.green,));
            Future.delayed(Duration(seconds: 2)).then((time){
              Navigator.of(context).pop(true);
            });
          }

          if(state is SuccessCancelWorkPlaneState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Plan de trabajo cancelado"), backgroundColor: Colors.green,));
            Future.delayed(Duration(seconds: 2)).then((time){
              Navigator.of(context).pop(true);
            });
          }

          if(state is SuccessApprovedWorkPlaneState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Aprobado"), backgroundColor: Colors.green,));
          }

          if(state is SuccessPostponementPlaneState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text("${state.message}"), backgroundColor: Colors.green,));
          }
        },
        child: Container(
          child: ListView(
            children: <Widget>[
              _cardInfo(),
              _activityToTime(widget.args.ptAtiempo),

              _dateFinalized(widget.args.bFecha),

              ListTile(
                title: RichText(text: TextSpan(
                  text: "Tipo: ",
                  style: TextStyle(color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: "${widget.args.tipoActividad}",
                      style: TextStyle(color: Colors.grey[600]),
                    )
                  ]
                ),),
                subtitle: Padding(padding: const EdgeInsets.only(left: 30),
                child: widget.args.nombreCodigo != null  ?
                Text("${widget.args.nombreCodigo.codigo != null ? widget.args.nombreCodigo.codigo+" - " : ""} "
                    " ${widget.args.nombreCodigo.nombre}", textAlign: TextAlign.left,)
                  : SizedBox()
                  ,),
              ),
              ListTile(
                title: RichText(text: TextSpan(
                  text: "Subtipo: ",
                  style: TextStyle(color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: "${widget.args.gtsTipos != null ? widget.args.gtsTipos.nombre : ""}",
                      style: TextStyle(color: Colors.grey[600]),
                    )
                  ]
                ),),
              ),
              widget.args.cliente != null ?
              ListTile(
                leading:  new CircleAvatar(
                  backgroundImage: loadImageApi(widget.args.cliente.logo, tokenUser),
                ),
                title: Text(widget.args.cliente.nombreCorto, style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
                subtitle: Text("Cliente", style: TextStyle(fontStyle: FontStyle.italic),),
              )
              : Container(),
              ListTile(
                title: Text("Descripción", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),),
                subtitle: Text(widget.args.descripcion, style: TextStyle(fontStyle: FontStyle.italic),),
              ),
              Divider(),
              // Row Contacts
              Center(
                child: Badge(
                    elevation: 0,
                    badgeColor: Colors.grey[200],
                    shape: BadgeShape.square,
                    borderRadius: 20,
                    toAnimate: false,
                    badgeContent:
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.person,color: Colors.black54),
                        Text(' Contactos', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                      ],)
                ),
              ),
              // <------- list of contacts --->
               _contacts(context),
              // <-- finish list of contacts -->

              // Row invitations
              Center(
                child: Badge(
                    elevation: 0,
                    badgeColor: Colors.grey[200],
                    shape: BadgeShape.square,
                    borderRadius: 20,
                    toAnimate: false,
                    badgeContent:
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.email,color: Colors.black54),
                        Text(' Invitaciones', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                      ],)
                ),
              ),
              // <------- list of invitations --->
                 _guestUsers(context),
              // <-- finish list of invitations -->
              // Row Notification activity
              Center(
                child: Badge(
                    elevation: 0,
                    badgeColor: Colors.grey[200],
                    shape: BadgeShape.square,
                    borderRadius: 20,
                    toAnimate: false,
                    badgeContent:
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.email,color: Colors.black54),
                        Text('! Notificación de actividad', style: TextStyle(color: Colors.grey.withOpacity(1.0), fontWeight: FontWeight.bold)),
                      ],)
                ),
              ),
              // <------- list of Notification activity --->
                  _notificationEmail(context),
              // <-- finish list of Notification activity -->
              _activityApproved()
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 1),
          height: 50,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: (){
                  _modalReject(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.ban),
                    Text("Rechazar")
                  ],
                ),
              ),
              InkWell(
                onTap: (){
                  _modalApproved(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.checkCircle),
                    Text("Aprobar")
                  ],
                ),
              ),
              SizedBox(width: 20,),
              InkWell(
                onTap: () async {
                  await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => EditActivityPage(
                    args: widget.args,
                    listContacts: listContacts,
                    listEmails: listEmails,
                    listGuest: listGuest,
                  )));
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.pencilAlt),
                    Text("Editar")
                  ],
                ),
              ),
              InkWell(
                  onTap: (){
                    _modalPostpone(context);
                  },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.history),
                    Text("Aplazar")
                  ],
                ),
              ),
              InkWell(
                onTap: (){
                  _modalCancel(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.timesCircle),
                    Text("Cancelar")
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  _cardInfo() {
    return Container(
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(10.0),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomLeft,
          stops: [0.3, 1],
         colors: [Color(0xCC079CE5),  Color(0xff079CE5)],


        )
      ),
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.only(top: 8, left: 10, right: 15, bottom: 8),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Flexible(child: Text("${widget.args.titulo}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),))
            ],
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text("${DateFormat("dd MMM yyyy").format(DateTime.parse(widget.args.fecha).toLocal()) }, ${widget.args.horaInicio} - ${widget.args.horaFin}", style: TextStyle(color: Colors.white))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              InkWell(
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryPostponement(idGts: widget.args.id,)));
                },
                child: Badge(
                  badgeContent: Text('${widget.args.gtsAaplazamientosCount}', style: TextStyle(color: Colors.white),),
                  child: Icon(FontAwesomeIcons.clock, color: Colors.white),
                ),
              ),
              SizedBox(width: 10,),
              InkWell(
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context) => CommentsPage(idGts: widget.args.id,)));
                },
                child: Badge(
                  badgeContent: Text('${widget.args.gtsComantaryCount}', style: TextStyle(color: Colors.white),),
                  child: Icon(Icons.message, color: Colors.white),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _activityToTime(int toTime){
    print("P A tiempo ${toTime}");

    if(toTime == 1){
      return  ListTile(
        leading: Icon(FontAwesomeIcons.solidClock, color: Colors.green,),
        title: Text("Actividad a tiempo", style: TextStyle(color: Colors.green, fontStyle: FontStyle.italic),),
      );
    }else if(toTime == 0){
     return ListTile(
        leading: Icon(FontAwesomeIcons.clock, color: Colors.orangeAccent,),
        title: Text("Actividad a destiempo", style: TextStyle(color: Colors.orangeAccent, fontStyle: FontStyle.italic),),
      );
    }
    return Container();
  }

  Widget _dateFinalized(String date){

    if(date == null) return Container();
    var month = DateFormat.MMM().format(DateTime.parse(date));
    var day = DateFormat.d().format(DateTime.parse(date));
    var year = DateFormat.y().format(DateTime.parse(date));

    print("${widget.args.bHoraInicio}   ${widget.args.bHoraFin}");

    return  ListTile(
      leading: CalendarDay(month: month, day: day),
      title: Row(children: <Widget>[
        Icon(Icons.check_circle, color: Colors.green,),
        SizedBox(width: 10,),
        Text("Realizada", style: TextStyle(color: Colors.green, fontStyle: FontStyle.italic),)
      ],),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("${widget.args.bHoraInicio}  -  ${widget.args.bHoraFin}", style: TextStyle(fontSize: 14, color: Colors.black87)),
          Text("$year", style: TextStyle(fontSize: 12, color: Colors.grey.withOpacity(0.6)),)
        ],
      ),
    );
  }

  Widget _contacts(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          if(state.contacts == null || state.contacts.length == 0) return Container(width:MediaQuery.of(context).size.width, height: 20);
          listContacts =  state.contacts;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.contacts.length,
              itemBuilder: (context, i) => ListTile(
                leading:  new CircleAvatar(
                    backgroundImage: loadImageApi(state.contacts[i].foto, tokenUser)
                ),
                title: Text("${state.contacts[i].nombre}"),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _guestUsers(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          if(state.users == null || state.users.length == 0) return Container(width:MediaQuery.of(context).size.width, height: 20);
          listGuest =  state.users;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.users.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                  position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.white,
                  badgeContent: _iconStatusGuestUser(state.users[i].respuesta),
                  child: new CircleAvatar(
                      backgroundImage: loadImageApi(state.users[i].datosInvitados.foto, tokenUser)
                  ),
                ),
                title: Text("${state.users[i].datosInvitados.nombre}"),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _notificationEmail(BuildContext context){
    return BlocBuilder(
      bloc: _gtsInvitationBloc,
      builder: (context, state){
        if(state is LoadedGtsGuestUsers){
          listEmails = state.email;
          return ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: state.email.length,
              itemBuilder: (context, i) => ListTile(
                leading:  Badge(
                  padding: EdgeInsets.all(0),
                  position: BadgePosition(bottom: 1, left: 30),
                  badgeColor: Colors.transparent,
                  badgeContent: _iconStatusGuestUser(state.email[i].respuesta),
                  child:  state.email[i].datosInvitados == null ?
                  new CircleAvatar(
                      child: Icon(FontAwesomeIcons.envelope, color: Colors.grey,),
                      backgroundColor: Colors.grey.withOpacity(0.2))
                      : new CircleAvatar(
                      backgroundImage: loadImageApi(state.email[i].datosInvitados.foto, tokenUser)
                  ) ,
                ),
                title: Text("${state.email[i].datosInvitados != null ? state.email[i].datosInvitados.nombre : state.email[i].correoExterno}"),
                subtitle: state.email[i].datosInvitados == null ? Text("correo electronico externo") : Text(state.email[i].datosInvitados.cargo),
              ));
        }
        return Container(width:MediaQuery.of(context).size.width, height: 20);
      },
    );
  }

  Widget _iconStatusGuestUser(var value){
    if(value == null){
      return Container();
    }
    else if(value == 1) return Icon(Icons.check_circle, color: _themeApp.colorPink,);
    if(value == 2 || value == 3 ) return Icon(Icons.cancel, color: Colors.grey.withOpacity(0.6),);

    return Container();
  }

  Widget _activityApproved(){
    return BlocBuilder(
        bloc: _gtsInvitationBloc,
        builder: (context, state){
          if(state is LoadedGtsGuestUsers){
            if(state.userApproved.datosUsuario != null){
              return Container(
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.all(10),
                decoration: new BoxDecoration(
                    color: Colors.grey[250],
                    border: Border.all(color: Colors.grey[350], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text("Actividad aprobada", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Chip(
                          avatar: CircleAvatar(
                            backgroundImage: loadImageApi(state.userApproved.datosUsuario.foto, tokenUser),
                            // child: ,
                          ),
                          label:Text(state.userApproved.datosUsuario.nombre),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.calendar_today, color: Colors.green,),
                            Text("${DateFormat("dd MMMM yyyy").format(DateTime.parse(state.userApproved.ptAprobadoFecha).toLocal())}", style: TextStyle(color: Colors.grey[600]),)
                          ],
                        )
                      ],
                    )
                  ],
                ),
              );
            }
            return Container(
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.all(10),
              decoration: new BoxDecoration(
                  color: Colors.grey[250],
                  border: Border.all(color: Colors.grey[350], width: 1),
                  borderRadius: new BorderRadius.circular(5.0)
              ),
              child: Column(
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text("Actividad pendiente por aprobación", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold), textAlign: TextAlign.left,)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[],
                  )
                ],
              ),
            );
          }
          return Container();
        });
  }

  //Modals
  void _modalReject(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Rechazar actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  Text("Justifique porque desea cancelar la actividad"),
                  SizedBox(height: 20,),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Justificación", style: TextStyle(color: Colors.grey[700]),),
                        SizedBox(height: 6),
                        TextFormField(
                          controller: descriptionCtrl,
                          keyboardType: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 5,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey,
                                    width: 5.0
                                )
                            ),
                            hintText: 'Ingresa la justificación',
                            // labelText: 'Descripcion'
                          ),
                          validator: (value){
                            if(value.isEmpty){
                              return 'Ingrese la descripción';
                            }
                            return null;
                          },
                          onSaved: (value){

                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            var data =  ApprovedRejectModel(
                              id: widget.args.id,
                              justification: descriptionCtrl.text,
                              ptApproved: 2
                            );
                            _workPlanBloc.dispatch(RejectWorkPlaneEvent(data: data));
                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          })
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }

  void _modalCancel(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Cancelar actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  Text("Justifique porque desea cancelar la actividad"),
                  SizedBox(height: 20,),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Justificación", style: TextStyle(color: Colors.grey[700]),),
                        SizedBox(height: 6),
                        TextFormField(
                          controller: descriptionCtrl,
                          keyboardType: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 5,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey,
                                    width: 5.0
                                )
                            ),
                            hintText: 'Ingresa la justificación',
                            // labelText: 'Descripcion'
                          ),
                          validator: (value){
                            if(value.isEmpty){
                              return 'Ingrese la descripción';
                            }
                            return null;
                          },
                          onSaved: (value){

                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            var data =  CancelWorkPlaneModel(
                              id: widget.args.id,
                              description: descriptionCtrl.text,
                              cancel: 1
                            );
                            _workPlanBloc.dispatch(CancelWorkPlaneEvent(data: data));
                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          })
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }

  void _modalApproved(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Aprobar actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20),
                  Text("¿Esta seguro que desea aprobar la actividad?"),
                  SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            descriptionCtrl.clear();
                            setState(() {});
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            var data =  ApprovedRejectModel(
                                id: widget.args.id,
                                justification: descriptionCtrl.text,
                                ptApproved: 1
                            );
                            _workPlanBloc.dispatch(ApprovedWorkPlaneEvent(data: data));
                            descriptionCtrl.clear();
                            setState(() {});
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          );
        }
    );
  }

  void _modalPostpone(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Aplazar actividad", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
               //-->   Text("Implement date"),
                  StepperDatePiker(fChanged: (date, start, end){
                    print("$date, $start, $end");
                    changeDate =  date;
                    changeStartTime = start;
                    changeEndTime = end;
                  },),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Justificación", style: TextStyle(color: Colors.grey[700]),),
                        SizedBox(height: 6),
                        TextFormField(
                          controller: descriptionCtrl,
                          keyboardType: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 5,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey,
                                    width: 5.0
                                )
                            ),
                            hintText: 'Ingresa la justificación',
                            // labelText: 'Descripcion'
                          ),
                          validator: (value){
                            if(value.isEmpty){
                              return 'Ingrese la descripción';
                            }
                            return null;
                          },
                          onSaved: (value){

                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            setState(() {});
                            descriptionCtrl.clear();
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {

                            var data  =  RequestPostponementModel(
                              id: widget.args.id,
                              beforeDate: widget.args.fecha,
                              newDate: changeDate.toString(),
                              startTime: DateFormat.Hms().format(changeStartTime),
                              endTime: DateFormat.Hms().format(changeEndTime),
                              justification: descriptionCtrl.text
                            );

                            _workPlanBloc.dispatch(PostponementWorkPlaneEvent(data: data));
                            descriptionCtrl.clear();
                            setState(() {});
                            Navigator.of(context).pop();
                          })
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }
}
