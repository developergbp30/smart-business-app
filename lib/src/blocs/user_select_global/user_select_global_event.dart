import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/user_model.dart';

abstract class UserSelectGbEvent extends Equatable {
  UserSelectGbEvent([List props = const []]) : super(props);
}

class FindUserGlobalEvent extends UserSelectGbEvent{}

class ChangeUserGlobalEvent  extends UserSelectGbEvent{
  final UserModel user;
  ChangeUserGlobalEvent(this.user) : super([user]);
  @override
  String toString() => '$runtimeType { user : ${user.id} }';
}