class DocumentsModel {
  int id;
  String nombre;
  String archivo;
  int tipo;
  int idGts;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  DocumentsModel(
      {this.id,
        this.nombre,
        this.archivo,
        this.tipo,
        this.idGts,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  DocumentsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    archivo = json['archivo'];
    tipo = json['tipo'];
    idGts = json['id_gts'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['archivo'] = this.archivo;
    data['tipo'] = this.tipo;
    data['id_gts'] = this.idGts;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}
