import 'package:equatable/equatable.dart';

abstract class TaskEvent extends Equatable {
  TaskEvent([List props = const []]) : super(props);
}

class LoadTaskDefeatedEvent extends TaskEvent{
  final typeTask;
  LoadTaskDefeatedEvent({this.typeTask});
  @override
  String toString() => '$runtimeType { typeTask: $typeTask}';
}

class LoadTaskValidEvent extends TaskEvent{
  final typeTask;
  LoadTaskValidEvent({this.typeTask});
  @override
  String toString() => '$runtimeType { typeTask: $typeTask}';
}

class LoadTaskPqrEvent extends TaskEvent{
  final typeTask;
  final int userId;
  LoadTaskPqrEvent({this.typeTask, this.userId}) : super([typeTask, userId]);
  @override
  String toString() => '$runtimeType { typeTask: $typeTask userId: $userId}';
}

class LoadTaskOpsEvent extends TaskEvent{
  final typeTask;
  LoadTaskOpsEvent({this.typeTask}) : super([typeTask]);
  @override
  String toString() => '$runtimeType { typeTask: $typeTask}';
}

class LoadTaskEventsEvent extends TaskEvent{
  final typeTask;
  LoadTaskEventsEvent({this.typeTask}) : super([typeTask]);
  @override
  String toString() => '$runtimeType { typeTask: $typeTask}';
}

class LoadTaskActasEvent extends TaskEvent{
  final typeTask;
  LoadTaskActasEvent({this.typeTask}) : super([typeTask]);
  @override
  String toString() => '$runtimeType { typeTask: $typeTask}';
}

class LoadTaskProjectEvent extends TaskEvent{
  final typeTask;
  LoadTaskProjectEvent({this.typeTask}) : super([typeTask]);
  @override
  String toString() => '$runtimeType { typeTask: $typeTask}';
}

class FindDetailTaskEvent extends TaskEvent{
  final int id;
  FindDetailTaskEvent({this.id}) : super([id]);
  @override
  String toString() => '$runtimeType {id: $id} ';
}

class FindDetailActaEvent extends TaskEvent{
  final int id;
  FindDetailActaEvent({this.id}) : super([id]);
  @override
  String toString() => '$runtimeType {id: $id} ';
}