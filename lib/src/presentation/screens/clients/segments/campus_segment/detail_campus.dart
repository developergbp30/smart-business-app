import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'create_edit_campus.dart';
class DetailCampus extends StatefulWidget {
  @override
  _DetailCampusState createState() => _DetailCampusState();
}

class _DetailCampusState extends State<DetailCampus> {
  final Color greenColor =  Color(0xFF11BA19);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Detalle sede", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                 // borderRadius: BorderRadius.all(Radius.circular(10)),
                  image: DecorationImage(
                    image:NetworkImage("https://cdn.pixabay.com/photo/2020/02/20/13/25/bangkok-4864747_960_720.jpg") ,
                    fit: BoxFit.cover,
                  )
              ),
              //  color: Colors.blueGrey,
              height: 300,
              width: MediaQuery.of(context).size.width,
              // child: Image.network(example.urlImage),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 5),
                    child: Column(
                      children: <Widget>[
                        //City
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                          RichText(text: TextSpan(text: "Pais: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                              children: <TextSpan>[
                                TextSpan(text: "Estados Unidos",style: TextStyle(color: Colors.grey))
                              ])),
                          Switch(
                              activeColor: greenColor,
                              value: false, onChanged: (value){

                          })
                        ], ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          RichText(text: TextSpan(text: "Estado: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                              children: <TextSpan>[
                                TextSpan(text: "New York",style: TextStyle(color: Colors.grey))
                              ])),
                        ], ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          RichText(text: TextSpan(text: "Ciudad: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                              children: <TextSpan>[
                                TextSpan(text: "New York",style: TextStyle(color: Colors.grey))
                              ])),
                        ], ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          RichText(text: TextSpan(text: "Dirección: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                              children: <TextSpan>[
                                TextSpan(text: "1600 Amphitheatre",style: TextStyle(color: Colors.grey))
                              ])),
                        ], ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comentarios", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),),
                          ],
                        ),
                        SizedBox(height: 5,),
                        Text("industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", textAlign: TextAlign.justify,),
                        SizedBox(height: 20,),
                        /**----------------------------
                         *  Additional  information
                         *  -------------------------**/
                        Row(children: <Widget>[Text("Información adicional", style: TextStyle(color: greenColor, fontWeight: FontWeight.w600, fontSize: 16),)],),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            RichText(text: TextSpan(text: "Tipo de sede: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                                children: <TextSpan>[
                                  TextSpan(text: "Producción",style: TextStyle(color: Colors.grey))
                                ])),
                          ], ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            RichText(text: TextSpan(text: "Compras mensuales: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                                children: <TextSpan>[
                                  TextSpan(text: "Producción",style: TextStyle(color: Colors.grey))
                                ])),
                          ], ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            RichText(text: TextSpan(text: "Personas a cargo: ",style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
                                children: <TextSpan>[
                                  TextSpan(text: "Maria Antonieta Perez",style: TextStyle(color: Colors.grey))
                                ])),
                          ], ),
                        SizedBox(height: 50,),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8.0,
        child: Container(
          padding: const EdgeInsets.only(top: 5),
          height: 60,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: () async {
                    _modalDelete(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.trash),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text("Eliminar"),
                    )
                  ],
                ),
              ),
              InkWell(
                onTap: () async {
                  await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateCampusPage()));
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.pencilAlt),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text("Editar"),
                    )
                  ],
                ),
              ),
              InkWell(
                onTap: () async {
                  _modalPrincipal(context);
                },
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.star),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text("Principal"),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _modalDelete(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Eliminar sede", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(mainAxisAlignment:MainAxisAlignment.center, children: <Widget>[Text("¿Está seguro que desea eliminar esta sede?")],)
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: greenColor),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            Navigator.of(context).pop();
                          })
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }

  void _modalPrincipal(context){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: new Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(10),
                    child: Center(child: Text("Sede principal", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                  ),
                  SizedBox(height: 20,),

                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(mainAxisAlignment:MainAxisAlignment.center, children: <Widget>[Flexible(child: Text("¿Está seguro que desea nombrar esta sede como sede principal?"))],)
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            Navigator.of(context).pop();
                          }),
                      SizedBox(width: 20),
                      FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Aceptar", style: TextStyle(color: greenColor),),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            Navigator.of(context).pop();
                          })
                    ],
                  )
                ],
              ),
            ),
          );
        }
    );
  }
}
