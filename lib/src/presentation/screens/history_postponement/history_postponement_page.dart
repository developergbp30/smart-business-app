import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/postponement/postponment_bloc.dart';
import 'package:smart_business/src/blocs/postponement/postponment_event.dart';
import 'package:smart_business/src/blocs/postponement/postponment_state.dart';
import 'package:smart_business/src/models/postponement_model.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/calendar_day/calendar_day.dart';
import 'package:smart_business/src/repositories/postponement/postponement_repository.dart';

class HistoryPostponement extends StatefulWidget {
  final int idGts;
  HistoryPostponement({@required this.idGts});
  @override
  _HistoryPostponementState createState() => _HistoryPostponementState();
}

class _HistoryPostponementState extends State<HistoryPostponement> {
  ThemeApp _themeApp = ThemeApp();
  PostponementBloc _postponementBloc;

  @override
  void initState() {
    _postponementBloc = PostponementBloc(postponementRepository: PostponementRepository());
    _postponementBloc.dispatch(LoadHistoryPostponementEvent(idGts: widget.idGts, page: 1));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: _themeApp.colorPink, //change your color here
        ),
        leading: IconButton(icon:Icon( Icons.arrow_back_ios),
          onPressed: (){
            Navigator.pop(context);
          },),
        backgroundColor: Colors.white,
        title: Text("Historial de aplazamientos", style: TextStyle(color: _themeApp.colorPink),),),
      body: Container(
        child: BlocBuilder<PostponementBloc, PostponementState>(
          bloc: _postponementBloc,
          builder: (context, state){
            if(state is LoadedHistoryPostponementState){
              return ListView.separated(
                separatorBuilder: (context, i) => SizedBox(height: 10,),
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: state.history.length,
                  itemBuilder: (context, i) => _item(state.history[i], state.token));
            }
            return Container();
          },
        ),
      ),
    );
  }

  Widget _item(PostponementModel item, String token){
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5, top: 5),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                child: CircleAvatar(
                    backgroundImage: loadImageApi(item.datosUsario.foto, token)
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(item.datosUsario.nombre),
                  Text(item.datosUsario.cargo)
                ],
              ),
              SizedBox(width: 50,),
              _containerDate(item.fechaAnterior, item.fechaNueva)
            ],
          ),
        ),
      ),
    );
  }
  Widget _containerDate(String startDate, String endDate){
    var monthStart =  DateFormat.MMM().format(DateTime.parse(startDate));
    var dayStart =  DateFormat.d().format(DateTime.parse(startDate));

    var monthEnd =  DateFormat.MMM().format(DateTime.parse(endDate));
    var dayEd =  DateFormat.d().format(DateTime.parse(endDate));

    return Container(
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  CalendarDay(month: monthStart, day: dayStart,),
                  SizedBox(height: 10,),
                ],
              ),
              Icon(Icons.arrow_forward, color: Colors.black54,size: 26,),
              Column(
                children: <Widget>[
                  CalendarDay(month:monthEnd, day: dayEd,headerColor: Colors.green,),
                  SizedBox(height: 10,),
                ],
              ),
            ],
          ),
          Row(
           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("${DateFormat.y().format(DateTime.parse(startDate))}", style: TextStyle(fontSize: 12),),
              SizedBox(width: 20,),
              Text("${DateFormat.y().format(DateTime.parse(endDate))}", style: TextStyle(fontSize: 12),)
            ],
          )
        ],
      ),
    );
  }
}
