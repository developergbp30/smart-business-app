import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/login_request_model.dart';

abstract class AuthEvent extends Equatable {
  AuthEvent([List props = const []]) : super(props);
}

class StatusAuthEvent extends AuthEvent{}

class VerificationCodeCompanyEvent  extends AuthEvent{
  final String codeCompany;
  VerificationCodeCompanyEvent(this.codeCompany) : super([codeCompany]);
}

class LogAuthEvent extends AuthEvent {}

class LoginEvent extends AuthEvent{
  final LoginRequestModel login;
  LoginEvent(this.login) : super([login]);
}

class CloseSessionEvent extends AuthEvent{}
