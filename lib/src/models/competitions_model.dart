class ResponseCompetitionModel {
  int value;
  String label;

  ResponseCompetitionModel({this.value, this.label});

  ResponseCompetitionModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['value'] = this.value;
    data['label'] = this.label;
    return data;
  }
}
