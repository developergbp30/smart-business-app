import 'package:smart_business/src/api/services/postponement_http_service.dart';
import 'package:smart_business/src/models/postponement_model.dart';

class PostponementRepository {

  PostponementHttp _postponementHttp = PostponementHttp();

  Future<List<PostponementModel>> getPostponement(int idGts, int page) async{
    try{
      return await _postponementHttp.list(idGts, page);
    }catch (error){
      return Future.error(error);
    }
  }
}