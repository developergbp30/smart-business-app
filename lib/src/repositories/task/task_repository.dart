import 'package:smart_business/src/api/services/task_http_service.dart';
import 'package:smart_business/src/models/actas_model.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/models/overdue_task_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

class TaskRepository {
    TaskHttp _taskHttp = TaskHttp();

  Future<List<OverdueTaskModel>> defeated(int idUser) async{
    try{
      return _taskHttp.overdue(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOverdueBinnacleModel>> defeatedBinnacle(int idUser) async{
    try{
      return _taskHttp.overdueBinnacle(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOverdueBinnacleModel>> validBinnacle(int idUser) async{
    try{
      return _taskHttp.validBinnacle(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskPqrModel>> pqr(int idUser) async{
    try{
      return _taskHttp.pqr(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskPqrBinnacleModel>> pqrBinnacle(int idUser) async{
    try{
      return _taskHttp.pqrBinnacle(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOpportunityModel>> ops(int idUser) async{
    try{
      return _taskHttp.ops(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskOpportunityModel>> opsBinnacle(int idUser) async{
    try{
      return _taskHttp.opsBinnacle(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskEventModel>> event() async{
    try{
      return _taskHttp.event();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<TaskEventModel>> eventBinnacle() async{
    try{
      return _taskHttp.eventBinnacle();
    }catch (error){
      return Future.error(error);
    }
  }

    Future<List<TaskActaModel>> actas(int idUser) async{
    try{
      return _taskHttp.actas(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

    Future<List<TaskActaModel>> actasBinnacle(int idUser) async{
    try{
      return _taskHttp.actas(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

    Future<List<TaskProjectModel>> project() async{
    try{
      return _taskHttp.project();
    }catch (error){
      return Future.error(error);
    }
  }

    Future<List<TaskProjectModel>> projectBinnacle() async{
    try{
      return _taskHttp.projectBinnacle();
    }catch (error){
      return Future.error(error);
    }
  }

    Future<ActaParcialModel> detailActa(int idActa) async{
    try{
      return _taskHttp.detailActa(idActa);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<WorkPlaneModel> detailTask(int idGts) async {
    try{
      return _taskHttp.detailTask(idGts);
    }catch  (error){
      return Future.error(error);
    }
  }
}