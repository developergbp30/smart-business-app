import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class StepperDatePiker extends StatefulWidget {
  Function fChanged;
  StepperDatePiker({@required this.fChanged});
  @override
  _StepperDatePikerState createState() => _StepperDatePikerState();
}

class _StepperDatePikerState extends State<StepperDatePiker> {
  int stepper = 0;

  DateTime date;
  DateTime startTime;
  DateTime endTime;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _stepperTitle(),
          ),
          _render()
        ],
      ),
    );
  }
  Widget _stepperTitle(){
    var style = TextStyle(fontSize: 16, color: Colors.black54);
    return stepper == 0 ? Text("Fecha", style: style)
        : stepper == 1 ? Text("Hora inicio", style: style) : Text("Hora fin", style: style);
  }
 Widget _render() {
    if(stepper == 0){
      return  Row(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width/1.2,
            height: 200,
            child: CupertinoDatePicker(
              initialDateTime: date,
              mode: CupertinoDatePickerMode.date,
              //maximumDate: maximumDate,
              onDateTimeChanged: (value){
                date = value;
                widget.fChanged(date, startTime, endTime);

              },
            ),
          ),
          IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            onPressed: (){
              setState(() {
                stepper = 1;
              });

              print(stepper);
            },
          )
        ],
      );
    }

    if(stepper == 1){
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: (){
              setState(() {
                stepper = 0;
              });
            },
          ),
          Container(
            color: Colors.red,
            width: MediaQuery.of(context).size.width/1.4,
            height: 200,
            child: CupertinoDatePicker(
              initialDateTime: startTime,
              mode: CupertinoDatePickerMode.time,
              onDateTimeChanged: (value){
                startTime = value;
                widget.fChanged(date, startTime, endTime);
              },
            ),
          ),
          IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            onPressed: (){
              setState(() {
                stepper = 2;
              });
            },
          )
        ],
      );
    }
    if(stepper == 2){
      return Row(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: (){
              setState(() {
                stepper = 1;
              });
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width/1.2,
            height: 200,
            child: CupertinoDatePicker(
              initialDateTime: endTime,
              mode: CupertinoDatePickerMode.time,
              onDateTimeChanged: (value){
                endTime = value;
                widget.fChanged(date, startTime, endTime);
              },
            ),
          ),

        ],
      );
    }
    return SizedBox();
  }
}
