import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/user_model.dart';

class UsersHttp extends ApiProvider{

  Future<List<UserModel>> getUsers() async {
    try{
      Response response = await dio.get("/gestion-tiempo/listar-usuarios", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new UserModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}