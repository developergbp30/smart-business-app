import 'package:smart_business/src/api/services/expense_http_service.dart';
import 'package:smart_business/src/models/expenses_model.dart';
import 'package:smart_business/src/models/request_balance_expenses.dart';

class ExpenseRepository{
  ExpenseHttp _expenseHttp = ExpenseHttp();

  Future<List<ExpenseTypeModel>> typeExpense() async {
    try{
        return _expenseHttp.typeExpense();
    }catch (error){
      return  Future.error(error);
    }
  }

  Future<ReqBalanceExpensesModel> balance(int id) async{
    try{
      return _expenseHttp.balanceExpenses(id);
    }catch (error){
      return  Future.error(error);
    }
  }
}