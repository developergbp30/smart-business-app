import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/client_model.dart';

abstract class ClientsState extends Equatable {
  ClientsState([List props = const []]) : super(props);
}

class UnloadClientsState extends ClientsState{}

class SendingRequestClientsState extends ClientsState{}

class LoadedClientsState extends ClientsState{
  final List<ClientModel> clients;
  final List<ClientModel> preview;
  LoadedClientsState({this.clients, this.preview}) : super([clients, preview]);

  @override
  String toString() => '$runtimeType {$clients} }';
}

class ErrorRequestCliState extends ClientsState{
  final String message;
  ErrorRequestCliState({this.message}) : super([message]);
}

class ClientChangedState extends ClientsState{
  final String statusCode;
  ClientChangedState(this.statusCode) : super([statusCode]);

  @override
  String toString() => '$runtimeType {$statusCode} }';
}