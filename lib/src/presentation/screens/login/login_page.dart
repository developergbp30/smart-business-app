import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_event.dart';
import 'package:smart_business/src/blocs/auth/auth_state.dart';
import 'package:smart_business/src/models/login_request_model.dart';
import 'package:smart_business/src/presentation/utils/navigation.dart';
class LoginPage extends StatefulWidget {
  static const String routeName = 'login_page';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final LoginRequestModel loginRequestModel = LoginRequestModel();

  String imageBackground;

  String imageLogo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child:  BlocListener<AuthBloc, AuthState>(
          listener: (BuildContext context, AuthState state){
            if(state is Authenticated){
              navigateFromLoginToHome(context);
            }//SendingLoginState
            if(state is SendingLoginState){
              Scaffold.of(context).showSnackBar(SnackBar(content: LinearProgressIndicator(),));
            }
            if(state is ErrorLoginState){
              Scaffold.of(context).showSnackBar(SnackBar(content: Text("${state.message}",), backgroundColor: Colors.red,));
            }
          },
          child: BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state){
              if(state is SuccessVerificationCodeCompany){
                imageBackground =  state.companyModel.imageBackground;
                imageLogo = state.companyModel.imageLogo;

              }
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageBackground != "" ? new NetworkImage(imageBackground) : AssetImage("assets/images/fondo_login.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      Center(
                        child: Container(
                          padding: const EdgeInsets.all(12),
                          decoration: new BoxDecoration(
                              color: Colors.white.withOpacity(0.8),
                              borderRadius: new BorderRadius.circular(10.0)
                          ),
                          margin: const EdgeInsets.only(left: 20, right: 20, top: 100, bottom: 50),
                          // color: Colors.red,
                          height: MediaQuery.of(context).size.height/1.5,
                          alignment: Alignment(0.0, 0.0),
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 70.0,
                                child: imageLogo != null ? Image.network(imageLogo) : AssetImage("assets/images/logo_cliente.png"),
                              ),
                              SizedBox(height: 20,),
                              TextFormField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.blueGrey,
                                              width: 5.0
                                          )
                                      ),
                                      hintText: 'Usuario',
                                      prefixIcon: Icon(Icons.perm_identity),
                                     // labelText: 'Usuario'
                                  ),
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Ingrese el usuario";
                                    }
                                    return null;
                                  },
                                  onSaved: (value){
                                    setState(() {
                                      loginRequestModel.username = value;
                                    });
                                  }
                              ),
                              SizedBox(height: 15,),
                              TextFormField(
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.blueGrey,
                                              width: 5.0
                                          )
                                      ),
                                      hintText: 'Contraseña',
                                      prefixIcon: Icon(Icons.vpn_key),
                                     // labelText: 'Contraseña'
                                  ),
                                validator: (value){
                                  if(value.isEmpty){
                                    return "Ingrese la contraseña";
                                  }
                                  return null;
                                },
                                onSaved: (value){
                                  setState(() {
                                    loginRequestModel.password = value;
                                  });
                                },
                              ),
                              SizedBox(height: 10,),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 39.68,
                                child: RaisedButton(
                                    shape: new RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(5.0),
                                        side: BorderSide(color: Color(0xff5D5D5D))),
                                    child: Text("Ingresar", style: TextStyle(color: Colors.white),),
                                    color: Color(0xff5D5D5D),//Colors.pink,
                                    onPressed: () {
                                      if(_formKey.currentState.validate()){
                                        _formKey.currentState.save();
                                        BlocProvider.of<AuthBloc>(context).dispatch(LoginEvent(loginRequestModel));
                                      }

                                    }),
                              ),
                              SizedBox(height: 10,),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 39.68,
                                child: FlatButton(
                                    child: Text("Cerrar", style: TextStyle(color: Colors.grey),),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                              ),
                              SizedBox(
                                height: 20.0,
                                child: Image.asset("assets/images/landscape_gbp_logo.png", fit: BoxFit.contain,),
                              ),
                            ],
                          ),
                        ),

                      ),
                      SizedBox(
                        height: 42.0,
                        child: Image.asset("assets/images/logo_gbp.png", fit: BoxFit.contain,),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
