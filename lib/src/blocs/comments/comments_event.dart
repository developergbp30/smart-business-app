
import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/comments_model.dart';

abstract class CommentsEvent extends Equatable {
  CommentsEvent([List props = const []]) : super(props);
}

class LoadCommentsEvent extends CommentsEvent{
  final int idGts;
  LoadCommentsEvent(this.idGts);
}

class CreateCommentsEvent extends CommentsEvent{
  final CommentCreateModel create;
  CreateCommentsEvent(this.create) : super([create]);
}