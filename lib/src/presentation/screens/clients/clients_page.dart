import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/clients/clients_bloc.dart';
import 'package:smart_business/src/blocs/clients/clients_event.dart';
import 'package:smart_business/src/blocs/clients/clients_state.dart';
import 'package:smart_business/src/models/client_model.dart';
import 'package:smart_business/src/presentation/screens/clients/create_edit_client_page.dart';
import 'package:smart_business/src/presentation/screens/clients/detail_client_page.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_left.dart';
import 'package:smart_business/src/presentation/widgets/widget_circle_color.dart';
import 'package:smart_business/src/repositories/clients/clients_repository.dart';
class ClientsPage extends StatefulWidget {
  static const String routeName = 'clients_page';
  @override
  _ClientsPageState createState() => _ClientsPageState();
}

class _ClientsPageState extends State<ClientsPage> {
  ClientBloc _clientBloc;
  bool activeSearch =  false;
  @override
  void initState() {
   _clientBloc = ClientBloc(clientRepository: ClientRepository());
   _clientBloc.dispatch(LoadClientsEvent());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: DrawerLeft(),
      ),
      appBar: AppBar(
          leading: !activeSearch ? Builder(builder: (context) => IconButton(icon: Icon(Icons.menu), onPressed: () => Scaffold.of(context).openDrawer()),)
          : Builder(builder: (context) => IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.green,), onPressed: () => setState(() { activeSearch = !activeSearch;})),),
        backgroundColor: !activeSearch ? Colors.green : Colors.white,
        title: !activeSearch ? Text("Clientes", style: TextStyle(color: Colors.white),) : _seachBox(),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search, color: Colors.white,), onPressed: (){
              setState(() {
                activeSearch = !activeSearch;
              });
            })
          ],
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(12),
              )
          )
      ),
      body: BlocListener<ClientBloc, ClientsState>(
        bloc: _clientBloc,
        listener: (context, state){
          if(state is ClientChangedState){
            Scaffold.of(context)
                .showSnackBar(
                SnackBar(content:
                  Text("Cambio de cliente exitoso", style: TextStyle(color: Colors.black),),backgroundColor: Colors.green,));
          }
        },
        child: RefreshIndicator(
          child: Container(
            child: BlocBuilder<ClientBloc, ClientsState>(
              bloc: _clientBloc,
              builder: (context, state){
                if(state is LoadedClientsState){
                  return ListView.builder(
                    itemBuilder: (context, i) => itemWidget(context, state.clients[i]),
                  itemCount: state.clients.length,);
                }

                if(state is SendingRequestClientsState){
                  return Center(child: CircularProgressIndicator(),);
                }
                return Center(child: Text("Sin resultados"),);
              },
            ),
          ),
          onRefresh: ()async {
           await  Future.delayed(Duration(seconds: 2));
           _clientBloc.dispatch(LoadClientsEvent());
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed:() async {
          var res =  await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateClientPage()));

        },
        tooltip: 'Crear',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _items(BuildContext context, ClientModel clientModel){
    return ListTile(
      title: Text(clientModel.nombreLegal),
      onTap: (){
        _clientBloc.dispatch(ChangeClientEvent(clientModel.id));
      },
    );
  }

  Widget itemWidget(BuildContext, ClientModel clientModel){
    print(clientModel.toJson());
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      child: InkWell(
        onTap: () async {
          await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => DetailClientPage()));
        },
        child: Card(
          elevation: 2,
          child: Column(children: <Widget>[
           Row(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
              //  color: Colors.blueGrey,
                height: 60,
                width: 60,
                child: Image.network(
                  'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/1200px-Apple_logo_black.svg.png',
                )
                ,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 5, right: 5, top: 15, bottom: 15),
              color: Colors.green,
              height: 100,
              width: 2,
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(clientModel.nombreCorto),
                  Text(clientModel.ciudad !=  null ?  clientModel.ciudad : ''),
                  SizedBox(height: 5,),
                   Text("Oportunidades Vigentes",style: TextStyle(color: Colors.lightBlue)),
                  SizedBox(height: 5,),
                  Container(
                    width: MediaQuery.of(context).size.width-150,
                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        WidgetCircleColor(height: 10,width: 10,),
                        SizedBox(width: 5,),
                        Text("2"),
                        SizedBox(width: 30,),
                        Flexible(
                          child: RichText(
                            text: TextSpan(text: "Valor:",
                                style: TextStyle(color: Colors.lightBlue),
                                children: <TextSpan>[
                              TextSpan(text: "\t\$1.264.000", style: TextStyle(color: Colors.black54)),
                            ]),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],)
        ],),),
      ),
    );
  }

  Widget _seachBox(){
    return TextField(decoration: InputDecoration(
      hintText: 'Busqueda',
      hintStyle: TextStyle(color: Colors.black54)
    ), onChanged: (value){
      if(_clientBloc.currentState is LoadedClientsState){
        print(_clientBloc.currentState.props);
        var list  = _clientBloc.currentState.props[1] == null ? _clientBloc.currentState.props[0] : _clientBloc.currentState.props[1];
        _clientBloc.dispatch(FilterClientEvent(query: value, clients: list));
      }
    },);
  }
}
