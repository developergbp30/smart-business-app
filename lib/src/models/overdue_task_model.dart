import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/invitations.dart';

class OverdueTaskModel {
  int id;
  String titulo;
  String fecha;
  String horaInicio;
  String horaFin;
  String actividadPrincipal;
  ActividadSecundaria actividadSecundaria;
  String nombreActividad;

  OverdueTaskModel(
      {this.id,
        this.titulo,
        this.fecha,
        this.horaInicio,
        this.horaFin,
        this.actividadPrincipal,
        this.actividadSecundaria,
        this.nombreActividad});

  OverdueTaskModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titulo = json['titulo'];
    fecha = json['fecha'];
    horaInicio = json['hora_inicio'];
    horaFin = json['hora_fin'];
    actividadPrincipal = json['actividad_principal'];
    actividadSecundaria = json['actividad_secundaria'] != null
        ? new ActividadSecundaria.fromJson(json['actividad_secundaria'])
        : null;
    nombreActividad = json['nombre_actividad'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['titulo'] = this.titulo;
    data['fecha'] = this.fecha;
    data['hora_inicio'] = this.horaInicio;
    data['hora_fin'] = this.horaFin;
    data['actividad_principal'] = this.actividadPrincipal;
    if (this.actividadSecundaria != null) {
      data['actividad_secundaria'] = this.actividadSecundaria.toJson();
    }
    data['nombre_actividad'] = this.nombreActividad;
    return data;
  }
}

class ActividadSecundaria {
  int id;
  String nombre;
  int idPadre;
  int tipoPadre;
  InfoPadre infoPadre;

  ActividadSecundaria(
      {this.id, this.nombre, this.idPadre, this.tipoPadre, this.infoPadre});

  ActividadSecundaria.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    idPadre = json['id_padre'];
    tipoPadre = json['tipo_padre'];
    infoPadre = json['info_padre'] != null
        ? new InfoPadre.fromJson(json['info_padre'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['id_padre'] = this.idPadre;
    data['tipo_padre'] = this.tipoPadre;
    if (this.infoPadre != null) {
      data['info_padre'] = this.infoPadre.toJson();
    }
    return data;
  }
}

class InfoPadre {
  int id;
  String nombre;
  int idPadre;
  dynamic tipoPadre;
  dynamic posicion;
  int createdBy;
  int updatedBy;
  String deletedAt;

  InfoPadre(
      {this.id,
        this.nombre,
        this.idPadre,
        this.tipoPadre,
        this.posicion,
        this.createdBy,
        this.updatedBy,
        this.deletedAt});

  InfoPadre.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    idPadre = json['id_padre'];
    tipoPadre = json['tipo_padre'];
    posicion = json['posicion'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['id_padre'] = this.idPadre;
    data['tipo_padre'] = this.tipoPadre;
    data['posicion'] = this.posicion;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}


// Task Binnacle
class TaskOverdueBinnacleModel {
  int id;
  int idUserPrincipal;
  dynamic ptTipo;
  int idPqr;
  int idActa;
  int idActaTema;
  int idBitacora;
  String ptFecha;
  String ptHoraInicio;
  String ptHoraFin;
  dynamic ptAtiempo;
  dynamic ptAprobado;
  int idUserAprobado;
  dynamic ptRechazoJustificacion;
  dynamic ptCancelada;
  dynamic ptCancelaJustificacion;
  dynamic ptAprobadoFecha;
  String titulo;
  String descripcion;
  int idSubtipoActividad;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idPqrRelacionada;
  int idProyecto;
  String bResultados;
  String bFecha;
  String bHoraInicio;
  String bHoraFin;
  dynamic bAtiempo;
  String bManoObra;
  int gtsInformeInfo;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  String color;
  ActividadGts actividadGts;
  GtsTipos gtsTipos;

  TaskOverdueBinnacleModel(
      {this.id,
        this.idUserPrincipal,
        this.ptTipo,
        this.idPqr,
        this.idActa,
        this.idActaTema,
        this.idBitacora,
        this.ptFecha,
        this.ptHoraInicio,
        this.ptHoraFin,
        this.ptAtiempo,
        this.ptAprobado,
        this.idUserAprobado,
        this.ptRechazoJustificacion,
        this.ptCancelada,
        this.ptCancelaJustificacion,
        this.ptAprobadoFecha,
        this.titulo,
        this.descripcion,
        this.idSubtipoActividad,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idPqrRelacionada,
        this.idProyecto,
        this.bResultados,
        this.bFecha,
        this.bHoraInicio,
        this.bHoraFin,
        this.bAtiempo,
        this.bManoObra,
        this.gtsInformeInfo,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.color,
        this.actividadGts,
        this.gtsTipos});

  TaskOverdueBinnacleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    ptTipo = json['pt_tipo'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idActaTema = json['id_acta_tema'];
    idBitacora = json['id_bitacora'];
    ptFecha = json['pt_fecha'];
    ptHoraInicio = json['pt_hora_inicio'];
    ptHoraFin = json['pt_hora_fin'];
    ptAtiempo = json['pt_atiempo'];
    ptAprobado = json['pt_aprobado'];
    idUserAprobado = json['id_user_aprobado'];
    ptRechazoJustificacion = json['pt_rechazo_justificacion'];
    ptCancelada = json['pt_cancelada'];
    ptCancelaJustificacion = json['pt_cancela_justificacion'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idPqrRelacionada = json['id_pqr_relacionada'];
    idProyecto = json['id_proyecto'];
    bResultados = json['b_resultados'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    bAtiempo = json['b_atiempo'];
    bManoObra = json['b_mano_obra'];
    gtsInformeInfo = json['gts_informe_info'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    color = json['color'];
    actividadGts = json['actividad_gts'] != null ? ActividadGts.fromJson(json['actividad_gts']) : null;
    gtsTipos = json['gts_tipos'] != null ?  new GtsTipos.fromJson(json['gts_tipos']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['pt_tipo'] = this.ptTipo;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_acta_tema'] = this.idActaTema;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['pt_hora_inicio'] = this.ptHoraInicio;
    data['pt_hora_fin'] = this.ptHoraFin;
    data['pt_atiempo'] = this.ptAtiempo;
    data['pt_aprobado'] = this.ptAprobado;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_rechazo_justificacion'] = this.ptRechazoJustificacion;
    data['pt_cancelada'] = this.ptCancelada;
    data['pt_cancela_justificacion'] = this.ptCancelaJustificacion;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_pqr_relacionada'] = this.idPqrRelacionada;
    data['id_proyecto'] = this.idProyecto;
    data['b_resultados'] = this.bResultados;
    data['b_fecha'] = this.bFecha;
    data['b_hora_inicio'] = this.bHoraInicio;
    data['b_hora_fin'] = this.bHoraFin;
    data['b_atiempo'] = this.bAtiempo;
    data['b_mano_obra'] = this.bManoObra;
    data['gts_informe_info'] = this.gtsInformeInfo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['color'] = this.color;
    data['actividad_gts'] = this.actividadGts;
    data['gts_tipos'] = this.gtsTipos;
    return data;
  }
}





