import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/select_binnacle/select_binnacle_event.dart';
import 'package:smart_business/src/blocs/select_binnacle/select_binnacle_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:meta/meta.dart';

class SelectBinnacleBloc extends Bloc<SelectBinnacleEvent, SelectBinnacleState>{
  BitacoraRepository _bitacoraRepository;
  AuthRepository _authRepository = AuthRepository();

  SelectBinnacleBloc({@required BitacoraRepository bitacoraRepository}) : assert(bitacoraRepository !=  null), _bitacoraRepository = bitacoraRepository;

  @override
  SelectBinnacleState get initialState => EmptySelectBinnacleState();

  @override
  Stream<SelectBinnacleState> mapEventToState(SelectBinnacleEvent event) async* {
    if(event is GetDataSelectBinnacleEvent){
      yield* _mapGetDataSelectToState();
    }
    else if(event is SelectedBinnacleEvent){
      yield* _mapInfoItemSelectedToState(event);
    }
  }

  Stream<SelectBinnacleState> _mapGetDataSelectToState() async*{
    try{
        yield LoadingDataSelectBinnacleState();

        var us =  await _authRepository.userData();

        var res =  await _bitacoraRepository.getSelectBinnacle(us.id);

        yield ListSelectBinnacleState(items: res);

    }catch (error){
      yield ErrorSelectBinnacleState(message: error.toString());
    }
  }

  Stream<SelectBinnacleState> _mapInfoItemSelectedToState(SelectedBinnacleEvent event) async*{
    try{
      var res =  await _bitacoraRepository.getInfoWorkPlane(event.selected.value);
      yield ListSelectBinnacleState(items: event.items, selected: event.selected, infoItem: res);
    }catch (error){
      yield ErrorSelectBinnacleState(message: error.toString());
    }
  }

}