import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/pqr_model.dart';

class PqrHttp extends ApiProvider {

  Future<List<ResponsePqrModel>> listPqrUser(int idUser) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Pqrs/$idUser/Action-Load-Select-Dinamic", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponsePqrModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}