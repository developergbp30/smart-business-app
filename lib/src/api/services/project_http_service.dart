import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';

class ProjectHttp extends ApiProvider {

  Future<List<ResponseProjectModel>> listProjectUser(int idUser) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Proyectos/$idUser/Action-Load-Select-Dinamic", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseProjectModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}