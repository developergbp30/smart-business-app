import 'package:flutter/material.dart';
class TimeLineWidget extends StatelessWidget {
  Color color;
  double heightCylinder = 50;
  TimeLineWidget({this.color, this.heightCylinder});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _circle($height: 20.0, $width: 20.0),
        _cylinder($height: heightCylinder),
        _circle($height: 10.0, $width: 10.0),
      ],
    );
  }

  Widget _circle({$height, $width}) {
    return  Container(
      decoration: new BoxDecoration(
          color: color == null ? Colors.blueAccent : color,
          borderRadius: new BorderRadius.circular(10.0)
      ),
      height: $height == null ? 20 : $height,
      width: $width == null ? 20 : $width,
      // color: Colors.blueAccent,
    );
  }
  Widget _cylinder({$height, $width}){
    return Container(
      color: color == null ? Colors.blueAccent : color,
      width: $width == null ? 2 : $width,
      height: $height == null ? 50: $height,
    );
  }
}
