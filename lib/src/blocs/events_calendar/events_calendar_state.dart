import 'package:equatable/equatable.dart';

abstract class EventsCalendarState extends Equatable {
  EventsCalendarState([List props = const []]) : super(props);
}

class EmptyEventsCalendarState extends EventsCalendarState {}

class ChangeDayEventsCalendarState extends EventsCalendarState {
  final List<dynamic> events;
  ChangeDayEventsCalendarState(this.events) : super([events]);
}