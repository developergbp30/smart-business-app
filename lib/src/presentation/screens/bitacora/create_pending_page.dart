import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
class CreatePendingPage extends StatefulWidget {
  final int idBitacora;
  CreatePendingPage({this.idBitacora});
  @override
  _CreatePendingPageState createState() => _CreatePendingPageState();
}

class _CreatePendingPageState extends State<CreatePendingPage> {
  ThemeApp _themeApp = ThemeApp();
  final _formKey = GlobalKey<FormState>();
  BitacoraBloc bitacoraBloc;
  TextEditingController titleCtrl;
  TextEditingController descriptionCtrl;
  DateTime dateActivity;
  DateTime maximumDate;
  CreatePendingModel _createPendingModel;

  @override
  void initState() {

    _createPendingModel =  CreatePendingModel();
    titleCtrl = TextEditingController();
    descriptionCtrl = TextEditingController();
    bitacoraBloc =  BitacoraBloc(bitacoraRepository: BitacoraRepository());

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(
            icon: Icon(
              Icons.clear,
              color: _themeApp.colorPink,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(
          "Nuevo Pendiente",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: _themeApp.colorPink),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.check,
                color: _themeApp.colorPink,
              ),
              onPressed: () {
                register();
              })
        ],
      ),
      body: BlocListener(
        bloc: bitacoraBloc,
        listener: (context, state){
          if(state is SuccesRegisterBitacoraState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.response.message),backgroundColor: Colors.green,));
            Future.delayed(Duration(seconds: 2)).then((time){
              Navigator.of(context).pop();
            });
          }
          if(state is WarningBitacoraState){
            Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.message),backgroundColor: Colors.yellowAccent,));
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Container(
                    margin:
                    const EdgeInsets.only(top: 5, bottom: 5, left: 10),
                    child: Text(
                      "Nombre del pendiente",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                TextFormField(
                  controller: titleCtrl,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.grey, width: 5.0)),
                    hintText: 'Ingrese el nombre del pendiente',
                    // labelText: 'Actividad'
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Ingrese el nombre';
                    }
                    return null;
                  },
                ),
                Container(
                    margin:
                    const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                    child: Text(
                      "Fecha",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                InkWell(
                  child: Container(
                    height: 40,
                    decoration: new BoxDecoration(
                        color: Colors.grey[350],
                        border: Border.all(color: Colors.grey[500], width: 1),
                        borderRadius: new BorderRadius.circular(5.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              "${dateActivity != null ? DateFormat.yMd().format(dateActivity) : ""}"),
                          Icon(Icons.expand_more)
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    _showDatePicker(context, (value) {
                      setState(() {
                        dateActivity = value;
                      });
                    });
                  },
                ),
                Container(
                    margin:
                    const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                    child: Text(
                      "Descripción",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                TextFormField(
                  controller: descriptionCtrl,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.grey, width: 5.0)),
                    hintText: 'Ingresa la descipción',
                    // labelText: 'Descripcion'
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Ingresa la descipción';
                    }
                    return null;
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showDatePicker(context, fChangeTime) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                        "Seleccionar fecha",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      )),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    maximumDate: maximumDate,
                    onDateTimeChanged: (value) {
                      fChangeTime(value);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(
                                color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Cerrar",
                              style: TextStyle(
                                  color: Colors.grey.withOpacity(0.6)),
                            ),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(
                                color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Aceptar",
                              style: TextStyle(color: _themeApp.colorPink),
                            ),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        });
  }

  void register(){
    if(_formKey.currentState.validate()){
      _formKey.currentState.save();
      // fill model
      _createPendingModel.titulo = titleCtrl.text;
      _createPendingModel.idBitacora = widget.idBitacora;
      _createPendingModel.descripcion =  descriptionCtrl.text;
      _createPendingModel.ptFecha = dateActivity.toString();

      bitacoraBloc.dispatch(CreatePendingEvent(data: _createPendingModel));
    }
  }
}
