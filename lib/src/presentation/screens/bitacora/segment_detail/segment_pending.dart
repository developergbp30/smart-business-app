import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/blocs/task/task_event.dart';
import 'package:smart_business/src/blocs/task/task_state.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/presentation/screens/bitacora/segment_detail/pending/detail_pending.dart';
import 'package:smart_business/src/presentation/screens/workplan/detail_activity.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/widget_time_line_burble.dart';
import 'package:smart_business/src/repositories/task/task_repository.dart';

import '../create_pending_page.dart';



class SegmentPending extends StatefulWidget {
  int idBitacora;
  List<ResponsePendingModel> args;
  String token;
  SegmentPending({this.args, this.idBitacora, this.token});

  @override
  _SegmentPendingState createState() => _SegmentPendingState();
}

class _SegmentPendingState extends State<SegmentPending> {
  ThemeApp _themeApp = ThemeApp();
  //final _dialogKey = GlobalKey<Dialog>();
  TaskBloc _taskBloc;
  @override
  void initState() {
    _taskBloc = TaskBloc(taskRepository: TaskRepository());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.args);
    return Scaffold(
      body: MultiBlocListener(
        listeners: [
          BlocListener<TaskBloc, TaskState>(
            listener: (context, state){
              if(state is DetailTaskState){
                Navigator.of(context).pop();
                Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => DetailActivityPage(args: state.detail,)));
              }

              if(state is SendingRequestTaskState){
                _onLoading();
              }

            },
            bloc: _taskBloc,
          )
        ],
        child: ListView.builder(
            itemCount: widget.args.length,
            itemBuilder: (context, i) => _itemCard(context, widget.args[i])),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: _themeApp.colorPink,
        onPressed:() async {
          await Navigator.push(context, new MaterialPageRoute(builder: (context) => CreatePendingPage(idBitacora: widget.idBitacora,)));
        },
        tooltip: 'Crear',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _itemCard(BuildContext context, ResponsePendingModel example) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: InkWell(
        onTap: () async {
          // DetailPending
         // await Navigator.push(context, new MaterialPageRoute(builder: (context) => DetailPending(args: example,token: token,)));
         // await Navigator.push(context, new MaterialPageRoute(builder: (context) => DetailPending(args: example,token: widget.token,)));
          _taskBloc.dispatch(FindDetailTaskEvent(id: example.id));
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      //margin: const EdgeInsets.only(top: 10),
                        child: TimeLineWidget(color: new ThemeApp().colorPink,)),
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, top: 0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Chip(
                                  avatar: CircleAvatar(
                                    backgroundImage: loadImageApi(example.datosUsuario.foto, widget.token),
                                    // child: ,
                                  ),
                                  label:Text(example.datosUsuario.nombre),
                                ),
                                _renderDate(example),
                              ],

                            ),
                            _renderStatus(example)

                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Divider(),
                Row(children: <Widget>[
                  Flexible(
                    child: Container(
                        margin: const EdgeInsets.only(top: 5, bottom: 5),
                        child: Text("${example.descripcion}", style: TextStyle(color: Colors.grey[700]),)),
                  ),
                ],)
              ],
            ),
          ),),
      ),
    );
  }

  _renderStatus(ResponsePendingModel example){
    if(example.ptAprobado !=  null) return  Container(
      margin: const EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.green),
          SizedBox(width: 5,),
          Text("Realizado")
        ],
      ),
    );
    if(example.ptCancelada !=  null) return  Container(
      margin: const EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.red),
          SizedBox(width: 5,),
          Text("Cancelado")
        ],
      ),
    );

    return  Container(
      margin: const EdgeInsets.only(top: 5),
      child: Row(
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.grey),
          SizedBox(width: 5,),
          Text("Pendiente")
        ],
      ),
    );;
  }

  _renderDate(ResponsePendingModel example){
    if(example.ptAprobado !=  null) return  Text(DateFormat.yMMMd('es').format(DateTime.parse(example.ptAprobadoFecha)), style: TextStyle(color: Colors.grey[700]),);
    if(example.ptCancelada !=  null) return  Text(DateFormat.yMMMd('es').format(DateTime.parse(example.ptFecha)), style: TextStyle(color: Colors.grey[700]),);

    return  Text(DateFormat.yMMMd('es').format(DateTime.parse(example.ptFecha)), style: TextStyle(color: Colors.grey[700]),);
  }

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                new CircularProgressIndicator(),
                SizedBox(width: 20,),
                new Text("Por favor espere"),
              ],
            ),
          ),
        );
      },
    );
  }
}
