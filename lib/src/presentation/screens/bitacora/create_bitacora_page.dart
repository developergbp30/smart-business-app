import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_bloc.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_event.dart';
import 'package:smart_business/src/blocs/select_activity/select_activity_state.dart';
import 'package:smart_business/src/blocs/select_binnacle/select_binnacle_bloc.dart';
import 'package:smart_business/src/blocs/select_binnacle/select_binnacle_event.dart';
import 'package:smart_business/src/blocs/select_binnacle/select_binnacle_state.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/client_model.dart';
import 'package:smart_business/src/models/competitions_model.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/for_views/data_screen_bitacora.dart';
import 'package:smart_business/src/models/for_views/data_screen_create_activity.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';
import 'package:smart_business/src/models/select_binnacle_model.dart';
import 'package:smart_business/src/presentation/screens/contacts/contacts_page.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/widgets/progress_indicator/appbar_bottom_linear_progress.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:smart_business/src/repositories/workplan/workplan_repository.dart';

class CreateBitacoraPage extends StatefulWidget {
  SetFormBitacora setFormBitacora;
  SetFormActivity setFormActivity;
  int idSelectWorkPlane;
  CreateBitacoraPage({this.setFormBitacora, this.setFormActivity, this.idSelectWorkPlane});
  @override
  _CreateBitacoraPageState createState() => _CreateBitacoraPageState();
}

class _CreateBitacoraPageState extends State<CreateBitacoraPage> {
  ThemeApp _themeApp = ThemeApp();
  List<ContactModel> addContacts;
  final _formKey = GlobalKey<FormState>();

  //Blocs
  SelectActivityBloc _selectActivityBloc;
  SelectBinnacleBloc _selectBinnacleBloc;
  BitacoraBloc _bitacoraBloc;
  // Model Data
  CreateBitacoraModel _createBitacoraModel;

  var date;

  String optionTypeActivity;

  String timeInitial;

  bool disabledModalAct = false;

  ResponseActivityModel typeActivity;

  ResponseActivityModel subTypeActivity;

  String labelSelectDynamic;

  bool disabledModalDynamic = false;

  int idClient;

  int idOpp;

  int idActa;

  int idComp;

  int idProject;

  int idEvent;

  int idPqr;

  bool showButtonAddContact;

  int idClientContact;

  String endTime;

  DateTime dateActivity;

  DateTime maximumDate;

  TextEditingController titleCtrl;
  TextEditingController nameTaskCtrl;
  TextEditingController descriptionCtrl;
  TextEditingController resultCtrl;
  TextEditingController emailExternalCtrl;

  var bandSetSubtypes =  false;

  SelectBinnacleModel activityWorkPlane;

  bool disabledSelectActivityWp =  false;

  bool BAND_SET_SELECT_WORK_PLANE = false;

  @override
  void initState() {
    _createBitacoraModel = CreateBitacoraModel();
    //initialize inputs
     titleCtrl = TextEditingController();
    nameTaskCtrl = TextEditingController();
    descriptionCtrl = TextEditingController();
    resultCtrl = TextEditingController();
    emailExternalCtrl = TextEditingController();

    //initialize Blocs
    // --> Bloc for select name Activity of Binnacle
    _selectBinnacleBloc = SelectBinnacleBloc(bitacoraRepository: BitacoraRepository());
    _selectBinnacleBloc.dispatch(GetDataSelectBinnacleEvent());

    // --> Bloc for selects (Type Activity, SubType Activity, Dynamic Select{ Opportunities, Pqr, Events, Projects, etc}
    _selectActivityBloc =
        SelectActivityBloc(workPlanRepository: WorkPlanRepository());
    _selectActivityBloc.dispatch(LoadDataForSelectsEvent());
    //--> Bloc For Register Bitacora
    _bitacoraBloc = BitacoraBloc(bitacoraRepository: BitacoraRepository());

    //set Form for Update
    if(widget.setFormBitacora !=  null){
      _setForm();
    }
    super.initState();
  }

  @override
  void dispose() {
    _selectActivityBloc.dispose();
    super.dispose();
  }

  void _setForm(){
    var set  =  widget.setFormBitacora;
    if(set.nameTask !=  null) nameTaskCtrl.value = TextEditingValue(text: set.nameTask);
    descriptionCtrl.value = TextEditingValue(text: set.description);
    resultCtrl.value = TextEditingValue(text: set.results);
    dateActivity = DateTime.parse(set.date);
    timeInitial =set.startTime;
    endTime =  set.endTime;

    disabledModalAct = set.disabledModalDynamic;
    disabledModalDynamic = set.disabledModalDynamic;

    _createBitacoraModel.id =  set.id;
    idActa =  set.idActa;
    idClient = set.idClient;
    idOpp = set.idOpportunity;

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(
            icon: Icon(
              Icons.clear,
              color: _themeApp.colorPink,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(
          "Crear Bitácora",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: _themeApp.colorPink),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.check,
                color: _themeApp.colorPink,
              ),
              onPressed: () {
                register();
              })
        ],
        bottom: PreferredSize(
          preferredSize: Size(double.infinity, 1.0),
          child: BlocBuilder<BitacoraBloc, BitacoraState>(
          bloc: _bitacoraBloc,
          builder: (context, state){
            if(state is SendingRequestBinnacleState){
              return MyLinearProgressIndicator();
            }

            return SizedBox();
          },
        ),
        ),
      ),
      body: MultiBlocListener(
        listeners: [
          BlocListener(
            bloc: _bitacoraBloc,
            listener: (context, state){
              if(state is SuccesRegisterBitacoraState){
                Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.response.message),backgroundColor: Colors.green,));
                  Future.delayed(Duration(seconds: 2)).then((time){
                    Navigator.of(context).pop(true);
                  });
              }
              if(state is WarningBitacoraState){
                Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.message),backgroundColor: Colors.yellowAccent,));
              }
            },
          ),
          BlocListener(
            bloc: _selectActivityBloc,
            listener: (context, state){
              if(state is DataForSelectState){
              if(widget.setFormBitacora != null && bandSetSubtypes == false){
                bandSetSubtypes = true;
                var selected =  state.activities.where((item)=> item.nombre == widget.setFormBitacora.title).toList().first;
                typeActivity = ResponseActivityModel(nombre:  selected.nombre, id: selected.id);
                subTypeActivity = ResponseActivityModel(nombre: widget.setFormBitacora.subTypeActivity, id: widget.setFormBitacora.idSubTypeActivity);
                labelSelectDynamic = widget.setFormBitacora.typeActivity;

                setState(() {});
                _selectActivityBloc.dispatch(FindSubTypeActivity(activities: state.activities, selected: selected));
              }
              /**------------------------------------------------
               *
               * When creating from the right Drawer tasks
               *
               * -----------------------------------------------**/
              if(widget.setFormActivity !=  null && bandSetSubtypes ==  false){
                bandSetSubtypes = true;
                disabledSelectActivityWp =  true;
                var selected =  state.activities.where((item)=> item.nombre == widget.setFormActivity.typeActivity).toList().first;
                typeActivity = ResponseActivityModel(nombre:  selected.nombre, id: selected.id);

                labelSelectDynamic = widget.setFormActivity.title;
                disabledModalAct =  widget.setFormActivity.disabledModalAct;
                disabledModalDynamic =  widget.setFormActivity.disabledModalDynamic;
                idOpp = widget.setFormActivity.idOpportunity;
                idClient = widget.setFormActivity.idClient;

                switch(widget.setFormActivity.typeActivity){
                  case 'Pqr':
                    idPqr =widget.setFormActivity.idTypeActivity;
                    break;
                  case 'Proyectos':
                    idProject = widget.setFormActivity.idTypeActivity;
                    break;
                }
                setState(() {});
                _selectActivityBloc.dispatch(FindSubTypeActivity(activities: state.activities, selected: selected));
              }
              }
            },
          ),
          BlocListener(
            bloc: _selectBinnacleBloc,
            listener: (context, state){
              if(state is ListSelectBinnacleState){
                if(state.infoItem !=  null){
                  if(_selectActivityBloc.currentState is DataForSelectState){
                   List<ResponseActivityModel> activities =  _selectActivityBloc.currentState.props[0];
                    print(activities.first);
                   var selected =  activities.where((item)=> item.nombre == state.infoItem.tipoActividad).toList().first;
                   typeActivity = ResponseActivityModel(nombre:  selected.nombre, id: selected.id);
                   subTypeActivity = ResponseActivityModel(nombre:  state.infoItem.gtsTipos.nombre, id: state.infoItem.gtsTipos.id);
                   labelSelectDynamic = state.infoItem.nombreSelect;
                   idPqr = state.infoItem.idPqr;
                   idActa =  state.infoItem.idActa;
                   idOpp =  state.infoItem.idOportunidad;
                   idClient =  state.infoItem.idCliente;
                   idEvent = state.infoItem.idEvento;
                   idComp =  state.infoItem.idCompetencia;
                   idProject =  state.infoItem.idProyecto;

                   setState(() {});
                   _selectActivityBloc.dispatch(FindSubTypeActivity(activities: activities, selected: selected));
                  }

                }
                /**
                 * When creating from the right Drawer tasks valid or expired
                 * -----------------------**/
                if(widget.idSelectWorkPlane !=  null && BAND_SET_SELECT_WORK_PLANE == false){
                  BAND_SET_SELECT_WORK_PLANE = true;
                  var select =  state.items.where((item) => item.value == widget.idSelectWorkPlane).toList().first;
                  activityWorkPlane =  select;
                  _selectBinnacleBloc.dispatch(SelectedBinnacleEvent(selected: select, items: state.items));
                  setState(() {});
                }
              }

            },
          )
        ],
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    Container(
                        margin:
                            const EdgeInsets.only(top: 5, bottom: 5, left: 10),
                        child: Text(
                          "Actividad plan de trabajo",
                          style: TextStyle(color: Colors.grey[700]),
                        )),
                    InkWell(
                      child: Container(
                        height: 40,
                        decoration: new BoxDecoration(
                            color: Colors.grey[350],
                            border: Border.all(color: Colors.grey[500], width: 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                               Flexible(child: Text("${activityWorkPlane !=  null ?  activityWorkPlane.label : ""}")),
                              Icon(Icons.expand_more)
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        if(!disabledSelectActivityWp)_modalActivityWorkPlane(context);
                      },
                    ),
                    Container(
                        margin:
                            const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                        child: Text(
                          "Fecha",
                          style: TextStyle(color: Colors.grey[700]),
                        )),
                    InkWell(
                      child: Container(
                        height: 40,
                        decoration: new BoxDecoration(
                            color: Colors.grey[350],
                            border: Border.all(color: Colors.grey[500], width: 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                  "${dateActivity != null ? DateFormat.yMd().format(dateActivity) : ""}"),
                              Icon(Icons.expand_more)
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        _showDatePicker(context, (value) {
                          setState(() {
                            dateActivity = value;
                          });
                        });
                      },
                    ),
                    Container(
                        margin:
                            const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                        child: Text(
                          "Hora de inicio",
                          style: TextStyle(color: Colors.grey[700]),
                        )),
                    InkWell(
                      child: Container(
                        height: 40,
                        decoration: new BoxDecoration(
                            color: Colors.grey[350],
                            border: Border.all(color: Colors.grey[500], width: 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("${timeInitial != null ? timeInitial : ""}"),
                              Icon(Icons.expand_more)
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        _showTimePicker(context, (value) {
                          print(value);
                          timeInitial = DateFormat.Hm().format(value);
                          setState(() {});
                        });
                      },
                    ),
                    Container(
                        margin:
                            const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                        child: Text(
                          "Hora de finalización",
                          style: TextStyle(color: Colors.grey[700]),
                        )),
                    InkWell(
                      child: Container(
                        height: 40,
                        decoration: new BoxDecoration(
                            color: Colors.grey[350],
                            border: Border.all(color: Colors.grey[500], width: 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("${endTime != null ? endTime : ""}"),
                              Icon(Icons.expand_more)
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        _showTimePicker(context, (value) {
                          print(value);
                          endTime = DateFormat.Hm().format(value);
                          setState(() {});
                        });
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    // GROUP IN CONTAINER
                    Container(
                      color: Colors.grey.withOpacity(0.2),
                      padding: const EdgeInsets.all(8),
                      child: Wrap(
                        children: <Widget>[
                          Container(
                              margin: const EdgeInsets.only(
                                  top: 5, bottom: 5, left: 10),
                              child: Text(
                                "Nombre de la tarea",
                                style: TextStyle(color: Colors.grey[700]),
                              )),
                          TextFormField(
                            controller: nameTaskCtrl,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey, width: 5.0)),
                              hintText: 'Ingrese el nombre de la tarea',
                              // labelText: 'Actividad'
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese el nombre de la tarea';
                              }
                              return null;
                            },
                          ),
                          Container(
                              margin: const EdgeInsets.only(
                                  top: 20, bottom: 5, left: 10),
                              child: Text(
                                "Tipo de Actividad",
                                style: TextStyle(color: Colors.grey[700]),
                              )),
                          InkWell(
                            child: Container(
                              height: 40,
                              decoration: new BoxDecoration(
                                  color: Colors.grey[350],
                                  border: Border.all(
                                      color: Colors.grey[500], width: 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                        "${typeActivity != null ? typeActivity.nombre : ""}"),
                                    Icon(Icons.expand_more)
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              if (!disabledModalAct)
                                _modalTypeActivities(context);
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                              margin: const EdgeInsets.only(
                                  top: 20, bottom: 5, left: 10),
                              child: Text(
                                "Subtipo de Actividad",
                                style: TextStyle(color: Colors.grey[700]),
                              )),
                          InkWell(
                            child: Container(
                              height: 40,
                              decoration: new BoxDecoration(
                                  color: Colors.grey[350],
                                  border: Border.all(
                                      color: Colors.grey[500], width: 1),
                                  borderRadius: new BorderRadius.circular(5.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                        "${subTypeActivity != null ? subTypeActivity.nombre : ""}"),
                                    Icon(Icons.expand_more)
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              _modalSubTypeActivities(context);
                            },
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          _renderSelectDynamic(),
                        ],
                      ),
                    ),
                    // FINISH GROUP IN CONTAINER
                    Container(
                        margin:
                            const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                        child: Text(
                          "Descripción",
                          style: TextStyle(color: Colors.grey[700]),
                        )),
                    TextFormField(
                      controller: descriptionCtrl,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 5.0)),
                        hintText: 'Ingresa la descipción',
                        // labelText: 'Descripcion'
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingresa la descipción';
                        }
                        return null;
                      },
                    ),
                    Container(
                        margin:
                            const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                        child: Text(
                          "Resultados",
                          style: TextStyle(color: Colors.grey[700]),
                        )),
                    TextFormField(
                      controller: resultCtrl,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 5.0)),
                        hintText: 'Ingresa la descipción',
                        // labelText: 'Descripcion'
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingrese la descripcion';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(left: 20, right: 20),
                      color: Colors.transparent,
                      padding: const EdgeInsets.all(8),
                      child: FlatButton(
                          padding: const EdgeInsets.all(0),
                          color: Colors.transparent,
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0),
                              side: BorderSide(
                                  color: Colors.grey.withOpacity(0.7))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(FontAwesomeIcons.fileUpload,
                                  color: _themeApp.colorPink),
                              Text(
                                " Subir archivos",
                                style: TextStyle(color: _themeApp.colorPink),
                              ),
                            ],
                          ),
                          // color: Color(0xff5D5D5D), //Colors.pink,
                          onPressed: () async {
                            var res = await Navigator.push(
                                context,
                                new MaterialPageRoute<BackToPageContactsModel>(
                                    builder: (context) => ContactsPage()));
                            if (res != null) {
                              print(res.contacts.first.name);
                              setState(() {
                                addContacts = res.contacts;
                              });
                            }
                          }),
                    ),
                    Wrap(children: _buildContactsSelected()),
                    // add invitations
                  ],
                )),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildContactsSelected() {
    List<Widget> widget = [];
    if (addContacts != null && addContacts.length > 0) {
      for (var i = 0; i < addContacts.length; i++) {
        widget.add(Chip(
          avatar: CircleAvatar(
            backgroundImage: new NetworkImage(addContacts[i].avatarUrl),
            // child: ,
          ),
          label: Text("${addContacts[i].name}"),
        ));
      }
    }

    return widget;
  }

  void _modalActivityWorkPlane(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                        "Actividad",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      )),
                ),
                Expanded(
                  child: BlocBuilder<SelectBinnacleBloc, SelectBinnacleState>(
                    bloc: _selectBinnacleBloc,
                    builder: (context, state) {
                      if (state is ListSelectBinnacleState) {
                        return ListView.builder(
                            itemCount: state.items.length,
                            itemBuilder: (context, i) => ListTile(
                              title: Text(state.items[i].label, style: TextStyle(color: _itemSelected(state.items[i].value, state.selected !=  null ? state.selected.value : null)),),
                              onTap: () {
                                activityWorkPlane =  state.items[i];
                                _selectBinnacleBloc.dispatch(SelectedBinnacleEvent(selected: state.items[i], items: state.items));
                                setState(() {});

                                Navigator.of(context).pop();
                              },
                            ));
                      }
                      if(state is LoadingDataSelectBinnacleState){
                        return Center(child: CircularProgressIndicator(),);
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

 Color _itemSelected(firstItem, selectItem){
    if(selectItem == null) return Colors.black;
    else {
      if(firstItem == selectItem) return ThemeApp().colorPink;
      else return Colors.black;
    }
 }

  void _modalTypeActivities(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                    "Tipo de actividad",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
                ),
                Expanded(
                  child: BlocBuilder<SelectActivityBloc, SelectActivityState>(
                    bloc: _selectActivityBloc,
                    builder: (context, state) {
                      if (state is DataForSelectState) {
                        return ListView.builder(
                            itemCount: state.activities.length,
                            itemBuilder: (context, i) => ListTile(
                                  title: Text(state.activities[i].nombre),
                                  onTap: () {
                                    typeActivity = state.activities[i];
                                    subTypeActivity = null;
                                    labelSelectDynamic = null;
                                    setState(() {});
                                    _selectActivityBloc.dispatch(
                                        FindSubTypeActivity(
                                            activities: state.activities,
                                            selected: state.activities[i]));
                                    Navigator.of(context).pop();
                                  },
                                ));
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  void _modalSubTypeActivities(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                    "Subtipo de actividad",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
                ),
                Expanded(
                  child: BlocBuilder<SelectActivityBloc, SelectActivityState>(
                    bloc: _selectActivityBloc,
                    builder: (context, state) {
                      if (state is DataForSelectState) {
                        if (state.subTypeActivities == null) return SizedBox();
                        return ListView.builder(
                            itemCount: state.subTypeActivities.length,
                            itemBuilder: (context, i) => ListTile(
                                  title:
                                      Text(state.subTypeActivities[i].nombre),
                                  onTap: () {
                                    subTypeActivity =
                                        state.subTypeActivities[i];
                                    setState(() {});
                                    Navigator.of(context).pop();
                                  },
                                ));
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _renderSelectDynamic() {
    return BlocBuilder<SelectActivityBloc, SelectActivityState>(
      bloc: _selectActivityBloc,
      builder: (context, state) {
        if (state is DataForSelectState) {
          return state.showThirdSelect != null &&
                  state.showThirdSelect &&
                  state.thirdSelect != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "${state.thirdSelect.label}",
                      style: TextStyle(color: Colors.grey[700]),
                    ),
                    InkWell(
                      child: Container(
                        height: 40,
                        decoration: new BoxDecoration(
                            color: Colors.grey[350],
                            border:
                                Border.all(color: Colors.grey[500], width: 1),
                            borderRadius: new BorderRadius.circular(5.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(
                                child: Text(
                                    "${labelSelectDynamic != null ? labelSelectDynamic : ""}"),
                              ),
                              Icon(Icons.expand_more)
                            ],
                          ),
                        ),
                      ),
                      onTap: () {
                        if (!disabledModalDynamic) _modalSelectDynamic(context);
                        /*showCupertinoModalPopup(context: context, builder: (context){
                    return actionSheetActivity(context);
                  });*/
                      },
                    ),
                  ],
                )
              : Container(
                  height: 20,
                );
        }

        return Container(
          height: 20,
        );
      },
    );
  }

  void _modalSelectDynamic(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                    "Subtipo de actividad",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
                ),
                Expanded(
                  child: BlocBuilder<SelectActivityBloc, SelectActivityState>(
                    bloc: _selectActivityBloc,
                    builder: (context, state) {
                      if (state is DataForSelectState) {
                        if (state.showThirdSelect &&
                            state.thirdSelect != null) {
                          print(state.thirdSelect.type);
                          if (state.thirdSelect.type == 1 ||
                              state.thirdSelect.type == 2 ||
                              state.thirdSelect.type == 3) {
                            print(state.thirdSelect.data);
                            return _subRenderListOpportunities(
                                state.thirdSelect.data);
                          } else if (state.thirdSelect.type == 4) {
                            return _subRenderListPqr(state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 5) {
                            return _subRenderListEvents(state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 6) {
                            return _subRenderListClient(state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 7) {
                            return _subRenderListCompetition(
                                state.thirdSelect.data);
                          }
                          if (state.thirdSelect.type == 8) {
                            return _subRenderListProject(
                                state.thirdSelect.data);
                          }
                          return SizedBox();
                        }
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  _subRenderListOpportunities(List<ResponseOpportunityModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(item[i].label),
              onTap: () {
                labelSelectDynamic = item[i].label;
                _setIds(item[i].value, 'ops');
                setState(() {});
                Navigator.of(context).pop();
              },
            ));
  }

  _subRenderListPqr(List<ResponsePqrModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(item[i].label),
              onTap: () {
                labelSelectDynamic = item[i].label;
                _setIds(item[i].value, 'pqr');
                if (item[i].clientes != null) {
                  showButtonAddContact = true;
                  idClientContact = item[i].clientes.id;
                  //_setIds(item[i].clientes.id, 'client');
                  _setIds(item[i].value, 'pqr');
                } else {
                  showButtonAddContact = false;
                  idClientContact = null;
                }
                setState(() {});
                Navigator.of(context).pop();
              },
            ));
  }

  _subRenderListEvents(List<ResponseEventModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(item[i].label),
              onTap: () {
                labelSelectDynamic = item[i].label;
                _setIds(item[i].value, 'event');
                setState(() {});
                Navigator.of(context).pop();
              },
            ));
  }

  _subRenderListClient(List<ResponseClientModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(item[i].label),
              onTap: () {
                labelSelectDynamic = item[i].label;
                _setIds(item[i].value, 'client');
                setState(() {});
                Navigator.of(context).pop();
              },
            ));
  }

  _subRenderListCompetition(List<ResponseCompetitionModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(item[i].label),
              onTap: () {
                labelSelectDynamic = item[i].label;
                _setIds(item[i].value, 'comp');
                setState(() {});
                Navigator.of(context).pop();
              },
            ));
  }

  _subRenderListProject(List<ResponseProjectModel> item) {
    return ListView.builder(
        itemCount: item.length,
        itemBuilder: (context, i) => ListTile(
              title: Text(item[i].label),
              onTap: () {
                labelSelectDynamic = item[i].label;
                _setIds(item[i].value, 'project');
                setState(() {});
                Navigator.of(context).pop();
              },
            ));
  }

  void _setIds(int value, type) {
    for (var i = 0; i < 5; i++) {
      if (type == "client") {
        idClient = value;
      } else {
        idClient = null;
      }
      if (type == "ops") {
        idOpp = value;
      } else {
        idOpp = null;
      }
      if (type == "pqr") {
        idPqr = value;
      } else {
        idPqr = null;
      }
      if (type == "event") {
        idEvent = value;
      } else {
        idEvent = null;
      }
      if (type == "project") {
        idProject = value;
      } else {
        idProject = null;
      }
      if (type == "comp") {
        idComp = value;
      } else {
        idComp = null;
      }
      if (type == "acta") {
        idActa = value;
      } else {
        idActa = null;
      }
    }
  }

  void _showDatePicker(context, fChangeTime) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                    "Seleccionar fecha",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    maximumDate: maximumDate,
                    onDateTimeChanged: (value) {
                      fChangeTime(value);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(
                                color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Cerrar",
                              style: TextStyle(
                                  color: Colors.grey.withOpacity(0.6)),
                            ),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(
                                color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Aceptar",
                              style: TextStyle(color: _themeApp.colorPink),
                            ),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        });
  }

  void _showTimePicker(context, fChangeTime) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                    "Seleccionar hora",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.time,
                    onDateTimeChanged: (value) {
                      fChangeTime(value);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(
                                color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Cerrar",
                              style: TextStyle(
                                  color: Colors.grey.withOpacity(0.6)),
                            ),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          setState(() {});
                          Navigator.of(context).pop();
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(
                                color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Aceptar",
                              style: TextStyle(color: _themeApp.colorPink),
                            ),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          setState(() {});
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        });
  }

  void register(){
    if(_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _createBitacoraModel.fecha = dateActivity.toString();//DateFormat('yyyy-MMMM-dd').format(dateActivity);
      _createBitacoraModel.hora = [timeInitial, endTime];
      _createBitacoraModel.tarea =  nameTaskCtrl.text;
      _createBitacoraModel.tipoActividadId = typeActivity.nombre;
      _createBitacoraModel.subActividadId = subTypeActivity.id;
      _createBitacoraModel.descripcion = descriptionCtrl.text;
      _createBitacoraModel.resultados = resultCtrl.text;
      _createBitacoraModel.idCliente =  idClient;
      _createBitacoraModel.idPqr =  idPqr;
      _createBitacoraModel.idOportunidad = idOpp;
      _createBitacoraModel.idActa = idActa;
      _createBitacoraModel.idEventos = idEvent;
      _createBitacoraModel.idCompetencias = idComp;
      _createBitacoraModel.idProyecto =  idProject;
      _createBitacoraModel.listadoAjuntos = []; //Todo: implement upload files

      print(_createBitacoraModel.toJson());
      _bitacoraBloc.dispatch(RegisterBitacoraEvent(data: _createBitacoraModel));
    }
  }
}
