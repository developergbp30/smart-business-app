import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/bitacora_detail/bitacora_detail_event.dart';
import 'package:smart_business/src/blocs/bitacora_detail/bitacora_detail_state.dart';
import 'package:smart_business/src/models/request_balance_expenses.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:meta/meta.dart';

class BitacoraDetailBloc extends Bloc<BitacoraDetailEvent, BitacoraDetailState>{
  BitacoraRepository _bitacoraRepository;
  AuthRepository _authRepository =  AuthRepository();
  BitacoraDetailBloc({@required BitacoraRepository bitacoraRepository}) :
        assert(bitacoraRepository != null), _bitacoraRepository = bitacoraRepository;

  @override
  BitacoraDetailState get initialState => UnloadBitacoraDetailState();

  @override
  Stream<BitacoraDetailState> mapEventToState(BitacoraDetailEvent event) async* {
    if(event is FindDetailBitacoraEvent){
      yield* _mapDetailToState(event);
    }
  }

  Stream<BitacoraDetailState> _mapDetailToState(FindDetailBitacoraEvent event) async*{
    try{
      var token =  await _authRepository.getToken();
      var res =  await _bitacoraRepository.detail(event.id);
      var pending = await _bitacoraRepository.listPending(event.id);
      var expenses =  await _bitacoraRepository.listExpenses(event.id);
      var balanceExpense = await _bitacoraRepository.balanceExpense(event.id);

      yield DetailBitacoraState(detail: res, pending: pending,expenses:  expenses, balanceExpenses: balanceExpense, token: token);

    }catch (error){
        print("\t Error en el Bloc\n $error");
    }
  }


}