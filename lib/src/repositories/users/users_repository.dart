import 'package:smart_business/src/api/services/users_http_service.dart';
import 'package:smart_business/src/models/user_model.dart';

class UsersRepository {
  UsersHttp _usersHttp = UsersHttp();

  Future<List<UserModel>> getUsers() async {
    try{
        return await _usersHttp.getUsers();
    }catch (error){
      return Future.error(error);
    }
  }
}