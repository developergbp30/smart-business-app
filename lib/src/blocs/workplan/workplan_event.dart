
import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

abstract class WorkPlanEvent extends Equatable {
  WorkPlanEvent([List props = const []]) : super(props);
}


class LoadWorkPlanEvent extends WorkPlanEvent{
  final int idUser;
  final String yearMonth;

  LoadWorkPlanEvent({this.idUser, this.yearMonth}) : super([idUser, yearMonth]);

}

class DataForSelectsEvent extends WorkPlanEvent{}

/*class FindSubTypeActivity extends WorkPlanEvent{
  final List<ResponseActivityModel> activities;
  final ResponseActivityModel selected;

  FindSubTypeActivity({this.activities, this.selected}) : super([activities]);
}*/

class FindThirdSelectEvent extends WorkPlanEvent{
  final int idSubActivity;
  final List<ResponseActivityModel> activities;
  final List<ResponseActivityModel> subTypeActivities;

  FindThirdSelectEvent({this.idSubActivity, this.activities, this.subTypeActivities})
      : super([idSubActivity, activities, subTypeActivities]);

  @override
  String toString() => '$runtimeType { idSubActivity $idSubActivity }';
}

class SendRegisterActivityEvent extends WorkPlanEvent{
  final CreateActivityModel data;
  SendRegisterActivityEvent({this.data}) : super([data]);
}

class ApprovedWorkPlaneEvent extends WorkPlanEvent{
  final ApprovedRejectModel data;
  ApprovedWorkPlaneEvent({this.data}) : super([data]);
}

class RejectWorkPlaneEvent extends WorkPlanEvent{
  final ApprovedRejectModel data;
  RejectWorkPlaneEvent({this.data}) : super([data]);

  @override
  String toString() => '$runtimeType {${data.toJson()}';
}

class CancelWorkPlaneEvent extends WorkPlanEvent{
  final CancelWorkPlaneModel data;
  CancelWorkPlaneEvent({this.data}) : super([data]);

  @override
  String toString() => '$runtimeType {${data.toJson()}';
}

class PostponementWorkPlaneEvent extends WorkPlanEvent{
  final RequestPostponementModel data;
  PostponementWorkPlaneEvent({this.data}) : super([data]);

  @override
  String toString() => '$runtimeType {${data.toJson()}';
}

