import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';

class ContactModel {
  String name;
  String company;
  String avatarUrl;
  bool selected = false;

  ContactModel({this.name, this.company, this.avatarUrl});
}

class BackToPageContactsModel {
    List<dynamic> contacts;
    List<ClientesContacto> clientContacts;
    List<UserModel> users;
    BackToPageContactsModel({this.contacts, this.clientContacts, this.users});
}

class RequestContactModel {
  int id;
  String nombreCorto;
  String foto;
  String nombre;
  int idUser;

  RequestContactModel(
      {this.id, this.nombreCorto, this.foto, this.nombre, this.idUser});

  RequestContactModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombreCorto = json['nombre_corto'];
    foto = json['foto'];
    nombre = json['nombre'];
    idUser = json['id_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre_corto'] = this.nombreCorto;
    data['foto'] = this.foto;
    data['nombre'] = this.nombre;
    data['id_user'] = this.idUser;
    return data;
  }
}
