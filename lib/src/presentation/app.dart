import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_state.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/blocs/user_in_session/userInSession_bloc.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_bloc.dart';
import 'package:smart_business/src/presentation/screens/bitacora/bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/bitacora/detail_bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/clients/clients_page.dart';
import 'package:smart_business/src/presentation/screens/home/home_page.dart';
import 'package:smart_business/src/presentation/screens/invitations/invitations_page.dart';
import 'package:smart_business/src/presentation/screens/login/code_company_page.dart';
import 'package:smart_business/src/presentation/screens/login/login_page.dart';
import 'package:smart_business/src/presentation/screens/pdf_page/pdf_page.dart';
import 'package:smart_business/src/presentation/screens/programing/programing_page.dart';
import 'package:smart_business/src/presentation/screens/workplan/workplan_page.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/task/task_repository.dart';
class App extends StatelessWidget {
  final AuthRepository authRepository;
  final AuthBloc authBloc;

  const App({
    Key key,
    @required this.authRepository,
    @required this.authBloc}
    ) : assert(authBloc != null),
        assert(authBloc != null),
  super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<AuthRepository>.value(value: authRepository)
      ],
      child: MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<UserInSessionBloc>(builder: (context) => UserInSessionBloc(authRepository: authRepository),),
            BlocProvider<UserSelectGlobal>(builder: (context) => UserSelectGlobal()),
            BlocProvider<TaskBloc>(builder: (context) => TaskBloc(taskRepository: TaskRepository()),)
          ],
          child: MultiBlocListener(
            listeners: [

            ],
            child: BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state){
                return MaterialApp(
                  theme: ThemeData(fontFamily: 'Monserrat'),
                  debugShowCheckedModeBanner: false,
                  initialRoute: state is UnAuthenticate ? CodeCompanyPage.routeName : HomePage.routeName,
                  routes: {
                    CodeCompanyPage.routeName: (context) => CodeCompanyPage(),
                    LoginPage.routeName: (context) => LoginPage(),
                    HomePage.routeName: (context) => HomePage(),
                    WorkPlanPage.routeName: (context) => WorkPlanPage(),
                    InvitationsPage.routeName: (context) =>InvitationsPage(),
                    ClientsPage.routeName: (context) => ClientsPage(),
                    BitacoraPage.routeName:(context)=> BitacoraPage(),
                    DetailBitacoraPage.routeName:(context) => DetailBitacoraPage(),
                    ProgramingPage.routeName: (context) => ProgramingPage(),
                    PdfPage.routeName : (context) => PdfPage(),
                  },
                );
              },
            ),
          )),
    );
  }
}
