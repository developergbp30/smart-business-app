import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/opportunity_model.dart';

abstract class OpportunityState extends Equatable {
  OpportunityState([List props = const []]) : super(props);
}

class UnLoadOpportunities extends OpportunityState{}

class LoadedOpportunitiesState extends OpportunityState{
    final List<OpportunityModel> opportunities;
    LoadedOpportunitiesState(this.opportunities) : super([opportunities]);
  @override
  String toString() => '$runtimeType { ${opportunities.length} }';

}

class SearchOpportunityState extends OpportunityState{
  final String query;
  final List<OpportunityModel> opportunity;
  final List<OpportunityModel> filterOpportunities;

  SearchOpportunityState({this.query, this.opportunity, this.filterOpportunities})
      : super([query, opportunity, filterOpportunities]);
}