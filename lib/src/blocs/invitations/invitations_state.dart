import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/invitations.dart';
abstract class InvitationsState extends Equatable {
  InvitationsState([List props = const []]) : super(props);
}

class UnloadInvitationsState extends InvitationsState{}

class LoadedInvitationsState extends InvitationsState{
 final  List<InvitationReceivedModel> invitations;
 final int type; //if type is 1 received or type is 2 send
 LoadedInvitationsState({this.invitations, this.type}) : super([invitations, type]);

 @override
  String toString() => '$runtimeType $invitations';
}

class ErrorInvitationsState extends InvitationsState{
  final String message;
  final String statusCode;
  ErrorInvitationsState({this.message, this.statusCode});

}

class LoadingInvitationsState extends InvitationsState{}

class SuccessApprovedInvitationsState extends InvitationsState{
  final String message;
  SuccessApprovedInvitationsState(this.message) : super([message]);

  @override
  String toString() => 'message $message';
}

class SuccessRejectInvitationsState extends InvitationsState{
  final String message;
  SuccessRejectInvitationsState(this.message) : super([message]);

  @override
  String toString() => 'message $message';
}

class SuccessCancelInvitationsState extends InvitationsState{
  final String message;
  SuccessCancelInvitationsState(this.message) : super([message]);

  @override
  String toString() => 'message $message';
}