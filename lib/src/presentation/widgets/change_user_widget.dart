import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_bloc.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_event.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_state.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
class ChangeUserWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return BlocBuilder<UserSelectGlobal, UserSelectGbState>(
      builder: (context, state){
        if(state is ChangedUserGlobalState){
          return Container(
            //color: Colors.green,
            height: 50,
            padding: const EdgeInsets.all(2),
            child:Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width: 10,),
                    new CircleAvatar(
                      backgroundImage: loadImageApi(state.user.foto, state.token),
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
                Text("${state.user.nombre}", textAlign: TextAlign.center, style: TextStyle(fontSize: 10),)
              ],
            ),);
        }

        else if(state is EmptyUserGlobalState){
          BlocProvider.of<UserSelectGlobal>(context).dispatch(FindUserGlobalEvent());
        }
        return Container();
      },
    );
  }
}
