import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/user_model.dart';

abstract class UsersState extends Equatable {
  UsersState([List props = const []]) : super(props);
}

class UnloadUsersState extends UsersState{}

class LoadingUsersState extends UsersState{}

class LoadedUsersState extends UsersState{
  final List<UserModel> users;
  final String token;
  LoadedUsersState(this.users, {this.token}) : super([users]);
}

class FilteredUserState extends UsersState{
  final List<UserModel> usersList;
  final List<UserModel> filtered;
  final String token;

  FilteredUserState({this.usersList, this.filtered, this.token}) : super([usersList, filtered]);
}

class ErrorUsersState extends UsersState{
  final String message;
  ErrorUsersState({this.message}) : super([message]);
}