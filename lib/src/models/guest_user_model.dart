class GuestUserModel {
  int idUserRecibio;
  String invitadoFecha;
  int id;
  int respuesta;
  String respuestaFecha;
  String correoExterno;
  DatosInvitados datosInvitados;

  GuestUserModel(
      {this.idUserRecibio,
        this.invitadoFecha,
        this.id,
        this.respuesta,
        this.respuestaFecha,
        this.correoExterno,
        this.datosInvitados});

  GuestUserModel.fromJson(Map<String, dynamic> json) {
    idUserRecibio = json['id_user_recibio'];
    invitadoFecha = json['invitado_fecha'];
    id = json['id'];
    respuesta = json['respuesta'];
    respuestaFecha = json['respuesta_fecha'];
    correoExterno = json['correo_externo'];
    datosInvitados = json['datos_invitados'] != null
        ? new DatosInvitados.fromJson(json['datos_invitados'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_user_recibio'] = this.idUserRecibio;
    data['invitado_fecha'] = this.invitadoFecha;
    data['id'] = this.id;
    data['respuesta'] = this.respuesta;
    data['respuesta_fecha'] = this.respuestaFecha;
    data['correo_externo'] = this.correoExterno;
    if (this.datosInvitados != null) {
      data['datos_invitados'] = this.datosInvitados.toJson();
    }
    return data;
  }
}

class DatosInvitados {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  DatosInvitados(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  DatosInvitados.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}
