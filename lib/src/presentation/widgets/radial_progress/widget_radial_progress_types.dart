import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:super_tooltip/super_tooltip.dart';
import 'package:vector_math/vector_math_64.dart' as math;
class TypeProgressData {
  Color color;
  double percentage;
  TypeProgressData({this.color, this.percentage});
}
class RadialProgressTypes extends StatefulWidget {
  double goalCompleted = 0.7;
  double $widthContainer = 200;
  double $heightContainer = 200;
  double $sizeLabel = 20;
  Color  $colorLabel = Colors.blue;
  double $strokeWidthBase = 20.0;
  double $strokeWidthProgress = 20.0;
  Color  $progressColor = Colors.blue;

  RadialProgressTypes({this.goalCompleted, this.$widthContainer, this.$heightContainer, this.$sizeLabel, this.$colorLabel, this.$strokeWidthBase, this.$strokeWidthProgress, this.$progressColor});
  @override
  _RadialProgressTypesState createState() => _RadialProgressTypesState();
}

class _RadialProgressTypesState extends State<RadialProgressTypes> with SingleTickerProviderStateMixin {
  AnimationController _radialProgressAnimationController;
  Animation<double> _progressAnimation;
  double progressDegrees = 0;
  RadialPainter radialPainter;
  var count = 0;
  List<TypeProgressData> series = [
    new TypeProgressData(color: Colors.deepPurple, percentage: 70),
    new TypeProgressData(color: Colors.red, percentage: 50),
    new TypeProgressData(color: Colors.yellow, percentage: 30),
  ];

  BuildContext _context;

  SuperTooltip tooltipMessage;

  @override
  void initState() {
    super.initState();

    radialPainter = RadialPainter(progressInDegrees: progressDegrees, $strokeWidthBase: widget.$strokeWidthBase,
        $strokeWidthProgress: widget.$strokeWidthProgress, $progressColor: widget.$progressColor != null ? widget.$progressColor : Colors.blue,
        series: series);

    _radialProgressAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 3));
      _progressAnimation =  Tween(begin: 0.0, end: 360.0)
          .animate(CurvedAnimation(parent:  _radialProgressAnimationController, curve: Curves.decelerate))
            ..addListener((){
              setState(() {
                progressDegrees = widget.goalCompleted * _progressAnimation.value;
              });
              print(progressDegrees);
            });

    _radialProgressAnimationController.forward();


  }

  @override
  Widget build(BuildContext context) {
    _context =  context;
    return GestureDetector(
      onTapUp: _onTapPaint,
      onPanUpdate: _onPaunUpdate,
      child: CustomPaint(
          child: Container(
            width: widget.$widthContainer,
            height: widget.$heightContainer,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("${widget.goalCompleted}%", style: TextStyle(fontSize: widget.$sizeLabel, color: widget.$colorLabel),)
              ],
            ),
          ),
      painter: radialPainter,),
    );
  }

  void _onTapPaint(TapUpDetails tap){
    print("\t Elemento presionado");
    print(tap.globalPosition);
    RenderBox renderBox = context.findRenderObject();
    var position =  renderBox.globalToLocal(tap.globalPosition);
    Offset center  = radialPainter.center;
    print("Radianes de  30 ${math.radians(30)} | R 50 ${math.radians(50)} | R 70 ${math.radians(70)}" );
    print(30 * pi / 180);

    print("posicion ${position}");

    print("coord ${radialPainter.center} | ${position.dy}");

    var angle = coordinatesToRadians(center, Offset(-95, position.dy));
    var angl2 = coordinatesToRadians(center, Offset(-95, 30));
    print("angulo ${angle} | 30 =  ${angl2} | 50 = ${coordinatesToRadians(center, Offset(-95, 50))} | 70 = ${coordinatesToRadians(center, Offset(-95, 70))}");

    var percentage  =  radiansToPercentage(angle);
    if(angle > -95 && angl2 <= coordinatesToRadians(center, Offset(-95, 30))){
      _toolTip("El de 30 ");
    }
    if(angle > coordinatesToRadians(center, Offset(-95, 30)) && angl2 <= coordinatesToRadians(center, Offset(-95, 50))){
      _toolTip("El de 50");
    }
    if(angle > coordinatesToRadians(center, Offset(-95, 50)) && angl2 <= coordinatesToRadians(center, Offset(-95, 70))){
      _toolTip("el de 70");
    }
    print("Porcentaje ${percentage}");

  }

  double coordinatesToRadians(Offset center, Offset coords){
    var a  =  coords.dx - center.dx;
    var b = center.dy - coords.dy;

    print(a);
    print(b);

    var $atan =  atan2(b, a);
    print("Atan ${atan($atan)}");
    return $atan;
  }

  double percentageToRadians(double percentage) =>((2 * pi * percentage) / 100);

  double radiansToPercentage(double radians) {
    var normalized = radians < 0 ? -radians : 2 * pi - radians;
    var percentage = ((100 * normalized) / (2 * pi));
    return (percentage + 25) % 100;
  }
  _onPaunUpdate(DragUpdateDetails details) {
    print("_onPanUpdate");
    print(details.globalPosition);

  }

  _toolTip(String message){
    if(tooltipMessage != null){
      try{
        tooltipMessage.close();
      }catch(error){
        print(error);
      }

    }
    tooltipMessage = new SuperTooltip(
      popupDirection: TooltipDirection.down,
      content: new Material(
          child: Text(
            "$message",
            softWrap: true,
          )),
    );

    tooltipMessage.show(_context);


  }
}

class RadialPainter extends CustomPainter {
  double progressInDegrees = 100.0;
  double $strokeWidthBase = 20.0;
  double $strokeWidthProgress = 20.0;
  Color  $progressColor = Colors.blue;
  List<TypeProgressData> series;
  Offset center;
  RadialPainter({this.progressInDegrees, this.$strokeWidthBase, this.$strokeWidthProgress, this.$progressColor, this.series});

  @override
  void paint(Canvas canvas, Size size) {

    Paint paint = Paint()
        ..color =  Colors.black12
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.stroke
        ..strokeWidth = $strokeWidthBase;
     center = Offset(size.width / 2, size.height / 2);
    canvas.drawCircle(center, size.width/2, paint);

    Paint progressPaint = Paint()
    ..color = $progressColor
    ..strokeCap = StrokeCap.round
    ..style =  PaintingStyle.stroke
    ..strokeWidth = $strokeWidthProgress;

   /* canvas.drawArc(Rect.fromCircle(center: center, radius: size.width/2),
        math.radians(-90), math.radians(progressInDegrees), false, progressPaint);*/

    // Second
    series.forEach((item){
      Paint progressPaint2 = Paint()
        ..color = item.color
        ..strokeCap = StrokeCap.round
        ..style =  PaintingStyle.stroke
        ..strokeWidth = $strokeWidthProgress;

      canvas.drawArc(Rect.fromCircle(center: center, radius: size.width/2),
          math.radians(-90), math.radians(item.percentage), false, progressPaint2);
    });

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

}
