import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/users/users_event.dart';
import 'package:smart_business/src/blocs/users/users_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/users/users_repository.dart';
import 'package:meta/meta.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState>{
  UsersRepository _usersRepository;
  AuthRepository _authRepository = AuthRepository();

  UsersBloc({@required UsersRepository usersRepository}) : assert(usersRepository!=null), _usersRepository =  usersRepository;

  @override
  UsersState get initialState => UnloadUsersState();

  @override
  Stream<UsersState> mapEventToState(UsersEvent event) async*{
    if(event is LoadUsersEvent){
      yield* _mapLoadUserState();
    }
    else if(event is FilterUsersEvent){
      yield* _mapFilterState(event);
    }

  }
  Stream<UsersState> _mapLoadUserState() async*{
    try{
        yield LoadingUsersState();
        var res =  await _usersRepository.getUsers();
        var token = await _authRepository.getToken();

        yield LoadedUsersState(res, token: token);
    }catch (error){
      print(error);
      yield ErrorUsersState(message: error.toString());
    }
  }

  Stream<UsersState>  _mapFilterState(FilterUsersEvent event) async*{
    try{
      var token = await _authRepository.getToken();
      var filter =  event.usersList.where((item) => item.nombre.contains(event.query)).toList();

     yield FilteredUserState(usersList: event.usersList, filtered: filter, token:token);
    }catch (error){

    }
  }
}