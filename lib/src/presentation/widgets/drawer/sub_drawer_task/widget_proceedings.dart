import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/blocs/task/task_event.dart';
import 'package:smart_business/src/blocs/task/task_state.dart';
import 'package:smart_business/src/models/actas_model.dart';
import 'package:smart_business/src/models/for_views/data_screen_create_activity.dart';
import 'package:smart_business/src/presentation/screens/workplan/create_activity.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';

import '../../widget_time_line_burble.dart';

class Proceedings extends StatelessWidget {

  int _typeActivityId(ActaParcialModel data){
    if(data.idActa !=  null) return data.idActa;
    if(data.idPqr !=  null) return data.idPqr;
    if(data.idOportunidad !=  null) return data.idOportunidad;
    if(data.idEventos !=  null) return data.idEventos;
    if(data.idCompetencias !=  null) return data.idCompetencias;
  }

  @override
  Widget build(BuildContext context) {
    return  BlocListener<TaskBloc, TaskState>(
      listener: (context, state){
        if(state is DetailActaState){
          var detail   =  state.detail;
          var values  =  new SetFormActivity(
              id: detail.id,
              idActa: detail.id,
              idClient: detail.idCliente,
              title: detail.titulo,
              description: detail.descripcion,
              typeActivity: detail.miTipo,
              idTypeActivity: _typeActivityId(detail),
              disabledModalAct: true,
              disabledModalDynamic: true
          );
          Navigator.of(context).pop();
          Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => CreateActivity(setFormActivity: values,)));
        }
      },
      child: Column(
        children: <Widget>[
          Text("Actas", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),),
          BlocBuilder<TaskBloc, TaskState>(builder: (context, state){
            if(state is LoadedTaskActasState){
              return   Expanded(
                child: ListView.builder(
                    itemCount: state.task.length,
                    itemBuilder: (context, i) => _itemCard(context, state.task[i])),
              );
            }
            if(state is LoadedTaskActasBinnacleState){
              return   Expanded(
                child: ListView.builder(
                    itemCount: state.task.length,
                    itemBuilder: (context, i) => _itemCard(context, state.task[i])),
              );
            }
            if(state is SendingRequestTaskState){
              return Container(
                //color: Colors.red,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/2,
                  child: Center(child: CircularProgressIndicator(),));
            }
            return Container(height: 50,width: 50,);
          })
        ],
      ),
    );
  }

  Widget _itemCard(BuildContext context, TaskActaModel example) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: InkWell(
        onTap: () async {

          BlocProvider.of<TaskBloc>(context).dispatch(FindDetailActaEvent(id: example.id));

        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      //margin: const EdgeInsets.only(top: 10),
                        child: TimeLineWidget(color: new ThemeApp().colorPink,)),
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, top: 0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(example.titulo, style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.grey[700]),
                                  SizedBox(width: 5,),
                                  Text(example.fecha, style: TextStyle(color: Colors.grey[700]),),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.clock, size: 15,color: Colors.grey[700]),
                                  SizedBox(width: 5,),
                                  Text("${example.horaInicio } - ${example.horaFin}", style: TextStyle(color: Colors.grey[700]),),
                                ],
                              ),
                            ),

                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Divider(),
                Row(children: <Widget>[
                  Flexible(
                    child: Container(
                        margin: const EdgeInsets.only(top: 5, bottom: 5),
                        child: Text(example.descripcion, style: TextStyle(color: Colors.grey[700]),)),
                  ),
                ],)
              ],
            ),
          ),),
      ),
    );
  }
}
