import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/user_session_model.dart';

abstract class UserInSessionState extends Equatable {
  UserInSessionState([List props = const []]) : super(props);
}
class UnloadedDataUserInSession extends UserInSessionState {}

class LoadedDataUserInSession extends UserInSessionState {
 final UserSessionModel userSessionModel;
 final String token;
  LoadedDataUserInSession(this.userSessionModel, {this.token}) :  super([userSessionModel]);

 @override
 String toString() => '$runtimeType {${userSessionModel.toJson()} }';
}