import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';

abstract class ContactState extends Equatable {
  ContactState([List props = const []]) : super(props);
}

class UnLoadedContactsState extends ContactState{}

class LoadedContactsState extends ContactState{
  final List<ContactModel> contacts;
  LoadedContactsState(this.contacts): super([contacts]);

  @override
  String toString() => '$runtimeType {${contacts.length} }';
}

class LoadedClientContactState  extends ContactState{
  final List<ClientesContacto> clientContact;
  final List<ClientesContacto> previous;
  final String tokenUser;
  LoadedClientContactState({this.clientContact, this.tokenUser, this.previous}) : super([clientContact, tokenUser, previous]);
}

class SearchContactsState extends ContactState{
  final String query;
  final List<ContactModel> contacts;
  final List<ContactModel> contactFilters;
  SearchContactsState({this.query, this.contacts, this.contactFilters}) : super([query, contacts]);

  @override
  String toString() => '$runtimeType {$query ${contacts.length} }';
}

class LoadedUsersState extends ContactState{
  final List<UserModel> users;
  final List<UserModel> previous;
  final String tokenUser;
  LoadedUsersState({this.users, this.tokenUser, this.previous}) : super([users, tokenUser, previous]);
}