 import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';

  loadImageApi(String route, String tokenValue) {
    ApiProvider apiProvider = ApiProvider();

    return  new NetworkImage("${apiProvider.urlApi}/storage/$route", headers: {"Cookie":"token=$tokenValue"});

 }