import 'package:flutter/material.dart';

import '../widget_circle_color.dart';

  modalConventions(BuildContext context){
  showModalBottomSheet(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      context: context,
      builder: (BuildContext bc){
        return Container(
          padding: const EdgeInsets.only(bottom: 10),
          child: new Wrap(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.all(10),
                padding: const EdgeInsets.all(10),
                child: Center(child: Text("Convenciones", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _circleTitle(Colors.blue, "Oportunidades vigentes"),
                        SizedBox(height: 10,),
                        _circleTitle(Colors.green, "Oportunidades ganadas"),
                        SizedBox(height: 10,),
                        _circleTitle(Colors.grey, "Oportunidades perdidas"),
                        SizedBox(height: 10,),
                        _circleTitle(Colors.red, "Perzonalizado"),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _circleTitle(Colors.indigo, "Eventos"),
                        SizedBox(height: 10,),
                        _circleTitle(Colors.orangeAccent, "PQR"),
                        SizedBox(height: 10,),
                        _circleTitle(Colors.deepPurple, "Cliente")
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      }
  );
}

Widget  _circleTitle(Color color, String text){
  return Row(
    children: <Widget>[
      WidgetCircleColor(color: color),
      Text(" $text")
    ],
  );
}