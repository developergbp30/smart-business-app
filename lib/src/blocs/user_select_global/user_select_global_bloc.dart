import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_event.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_state.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';

class UserSelectGlobal extends Bloc<UserSelectGbEvent, UserSelectGbState> {

  AuthRepository _authRepository = AuthRepository();

  @override
  UserSelectGbState get initialState => EmptyUserGlobalState();

  @override
  Stream<UserSelectGbState> mapEventToState(UserSelectGbEvent event) async*{
    if(event is FindUserGlobalEvent){
      yield* _mapLoadUserGlobal();
    }
    else if(event is ChangeUserGlobalEvent){
      var token  =  await _authRepository.getToken();
     yield ChangedUserGlobalState(event.user, token: token);
    }
  }

  Stream<UserSelectGbState> _mapLoadUserGlobal() async* {
   try {
     var userSession = await _authRepository.userData();
     var token  =  await _authRepository.getToken();

     UserModel val = UserModel(id: userSession.id,
         nombre: userSession.datosUsuario.nombre,
         foto: userSession.datosUsuario.foto);
     yield ChangedUserGlobalState(val, token: token);
   } catch (error){

   }
  }
}