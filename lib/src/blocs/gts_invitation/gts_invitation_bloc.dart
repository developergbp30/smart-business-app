import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_event.dart';
import 'package:smart_business/src/blocs/gts_invitation/gts_initation_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/invitations/invitations_repository.dart';
import 'package:meta/meta.dart';
class GtsInvitationBloc extends Bloc<GtsInvitationEvent, GtsInvitationState>{
  InvitationsRepository _invitationsRepository;
  AuthRepository authRepository = AuthRepository();
  GtsInvitationBloc({@required InvitationsRepository invitationsRepository})
  : assert(invitationsRepository != null), _invitationsRepository = invitationsRepository;
  @override
  GtsInvitationState get initialState => UnloadGtsGuestUsers();

  @override
  Stream<GtsInvitationState> mapEventToState(GtsInvitationEvent event) async*{
    if(event  is LoadGtsGuestUsersEvent){
     yield* _mapLoadGuestUser(event);
    }
  }

  Stream<GtsInvitationState> _mapLoadGuestUser(LoadGtsGuestUsersEvent event) async*{
    try{
        var token =  await authRepository.getToken();
        var users = await _invitationsRepository.guestUsers(event.idGts);
        var email = await _invitationsRepository.notificationActividadEmail(event.idGts);
        var userApproved =  await _invitationsRepository.userApprovedActivity(event.idGts);
        var contacts =  await _invitationsRepository.contacts(event.idGts);
        yield LoadedGtsGuestUsers(users: users, email: email, userApproved: userApproved, contacts: contacts, token: token);
    }catch (error){
       yield ErrorGtsInvitation(message: error.toString());
    }
  }

}