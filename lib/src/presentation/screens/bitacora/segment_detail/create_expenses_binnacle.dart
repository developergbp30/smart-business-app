import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_bloc.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/blocs/select_type_expense/select_type_expense_bloc.dart';
import 'package:smart_business/src/blocs/select_type_expense/select_type_expense_event.dart';
import 'package:smart_business/src/blocs/select_type_expense/select_type_expense_state.dart';
import 'package:smart_business/src/models/create_expenses.dart';
import 'package:smart_business/src/models/for_views/data_screen_edit_expense.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:smart_business/src/repositories/expenses/expense_repository.dart';
class CreateBinnacleExpenses extends StatefulWidget {
  int idBitacora;
  SetFormExpense setFormExpense;

  CreateBinnacleExpenses({@required this.idBitacora, this.setFormExpense});

  @override
  _CreateBinnacleExpensesState createState() => _CreateBinnacleExpensesState();
}

class SelectedExpenseType {
  int id;
  String name;
  SelectedExpenseType({this.id, this.name});
}

class _CreateBinnacleExpensesState extends State<CreateBinnacleExpenses> {
  ThemeApp _themeApp = ThemeApp();
  final _formKey = GlobalKey<FormState>();
  TextEditingController titleCtrl = TextEditingController();
  TextEditingController reqValueCtrl = TextEditingController();
  TextEditingController descriptionCtrl = TextEditingController();
  DateTime dateActivity;
  DateTime maximumDate;
  BitacoraBloc bitacoraBloc;
  SelectExpenseBloc _selectExpenseBloc;

  String typeExpense = '';

  CreateExpensesModel _createExpensesModel;

  SelectedExpenseType selectedExpenseType;

  bool bandSetType =  false;

  @override
  void initState() {
    _createExpensesModel = CreateExpensesModel();
    _createExpensesModel.unique =  Unique(id: null);
    _createExpensesModel.values = Values();
    _selectExpenseBloc = SelectExpenseBloc(expenseRepository: ExpenseRepository());

    //Load type expenses
    _selectExpenseBloc.dispatch(GetTypeExpenseEvent());

    titleCtrl = TextEditingController();
    descriptionCtrl = TextEditingController();
    bitacoraBloc =  BitacoraBloc(bitacoraRepository: BitacoraRepository());
    if(widget.setFormExpense !=  null){
        fillForm();
    }
    super.initState();
  }

  void fillForm(){
    var set  =  widget.setFormExpense;
    _createExpensesModel.unique.id = set.id;
    titleCtrl.value = TextEditingValue(text: set.title);
    descriptionCtrl.value = TextEditingValue(text: set.description);
    reqValueCtrl.value = TextEditingValue(text: set.value.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(
            icon: Icon(
              Icons.clear,
              color: _themeApp.colorPink,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        title: Text(
          "Crear gasto",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: _themeApp.colorPink),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.check,
                color: _themeApp.colorPink,
              ),
              onPressed: () {
                register();
              })
        ],
      ), //
      body: MultiBlocListener(
        listeners: [
          BlocListener(bloc: bitacoraBloc,listener: (context, state){
            if(state is SuccessCreateExpenseState){
              Scaffold.of(context).showSnackBar(SnackBar(content: Text("Registro Exitoso"),backgroundColor: Colors.green,));
              Future.delayed(Duration(seconds: 2)).then((time){
                Navigator.of(context).pop();
              });
            }

            if(state is SuccessUpdateExpenseState){
              Scaffold.of(context).showSnackBar(SnackBar(content: Text("Actualizacion exitosa"),backgroundColor: Colors.green,));
              Future.delayed(Duration(seconds: 2)).then((time){
                Navigator.of(context).pop();
              });
            }
            if(state is ErrorBitacoraState){
              Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.message),backgroundColor: Colors.red,));
            }
          }),
          BlocListener(
            bloc: _selectExpenseBloc,
            listener: (context, state){
              if(state is ListTypeExpenseState){

                if(widget.setFormExpense != null && bandSetType == false){
                  bandSetType = true;
                  var value  =  state.types.where((item) => item.id == widget.setFormExpense.type).toList().first;
                  selectedExpenseType =  new SelectedExpenseType(id: value.id, name:  value.nombre);
                  setState(() {});
                }

              }
            },
          )
        ],
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                // Name of expense
                Container(
                    margin:
                    const EdgeInsets.only(top: 5, bottom: 5, left: 10),
                    child: Text(
                      "Nombre del gasto",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                TextFormField(
                  controller: titleCtrl,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.grey, width: 5.0)),
                    hintText: 'Ingrese el nombre del gasto',
                    // labelText: 'Actividad'
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Ingrese el nombre';
                    }
                    return null;
                  },
                ),
                //--> Type expense
                Container(
                    margin:
                    const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                    child: Text(
                      "Tipo de gasto",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                InkWell(
                  child: Container(
                    height: 40,
                    decoration: new BoxDecoration(
                        color: Colors.grey[350],
                        border: Border.all(color: Colors.grey[500], width: 1),
                        borderRadius: new BorderRadius.circular(5.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("${selectedExpenseType !=  null ?  selectedExpenseType.name : ""}"),
                          Icon(Icons.expand_more)
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                      _modalTypeExpenses(context);
                  },
                ),
                // --> Values Requested
                Container(
                    margin:
                    const EdgeInsets.only(top: 5, bottom: 5, left: 10),
                    child: Text(
                      "Valor solicitado",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                TextFormField(
                  controller: reqValueCtrl,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.grey, width: 5.0)),
                    hintText: 'Ingrese el valor solicitado',
                    // labelText: 'Actividad'
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Ingrese el valor';
                    }
                    return null;
                  },
                ),
                //--> Description
                Container(
                    margin:
                    const EdgeInsets.only(top: 20, bottom: 5, left: 10),
                    child: Text(
                      "Descripción",
                      style: TextStyle(color: Colors.grey[700]),
                    )),
                TextFormField(
                  controller: descriptionCtrl,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.grey, width: 5.0)),
                    hintText: 'Ingresa la descipción',
                    // labelText: 'Descripcion'
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Ingresa la descipción';
                    }
                    return null;
                  },
                ),
                //--> Upload Files
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(left: 20, right: 20),
                  color: Colors.transparent,
                  padding: const EdgeInsets.all(8),
                  child: FlatButton(
                      padding: const EdgeInsets.all(0),
                      color: Colors.transparent,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(50.0),
                          side: BorderSide(
                              color: Colors.grey.withOpacity(0.7))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(FontAwesomeIcons.fileUpload,
                              color: _themeApp.colorPink),
                          Text(
                            " Subir archivos",
                            style: TextStyle(color: _themeApp.colorPink),
                          ),
                        ],
                      ),
                      // color: Color(0xff5D5D5D), //Colors.pink,
                      onPressed: () async {

                      }),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }

  register(){
    if(_formKey.currentState.validate()){
      _formKey.currentState.save();

      _createExpensesModel.values.titulo = titleCtrl.text;
      _createExpensesModel.values.descripcion = descriptionCtrl.text;
      _createExpensesModel.values.fecha =  DateTime.now().toString();
      _createExpensesModel.values.idTipo = selectedExpenseType.id;
      _createExpensesModel.values.valorSolicitado = int.parse(reqValueCtrl.text);
      _createExpensesModel.values.idGts = widget.idBitacora;

      if(widget.setFormExpense == null){
        bitacoraBloc.dispatch(CreateExpenseEvent(data: _createExpensesModel));
      }else{
        bitacoraBloc.dispatch(UpdateExpenseEvent(data: _createExpensesModel));
      }

    }
  }

  void _modalTypeExpenses(context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(
                      child: Text(
                        "Tipo de actividad",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                      )),
                ),
                Expanded(
                  child: BlocBuilder<SelectExpenseBloc, SelectExpenseState>(
                    bloc: _selectExpenseBloc,
                    builder: (context, state) {
                      if (state is ListTypeExpenseState) {
                        return ListView.builder(
                            itemCount: state.types.length,
                            itemBuilder: (context, i) => ListTile(
                              title: Text(state.types[i].nombre),
                              onTap: () {
                                selectedExpenseType =  new SelectedExpenseType(id: state.types[i].id, name: state.types[i].nombre);
                                setState(() {});
                                Navigator.of(context).pop();
                              },
                            ));
                      }
                      return Container(
                        child: Text("Sin resultados"),
                      );
                    },
                  ),
                )
              ],
            ),
          );
        });
  }
}
