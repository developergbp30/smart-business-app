import 'package:flutter/material.dart';
class WidgetCircleColor extends StatelessWidget {
  Color color;
  double height;
  double width;
  WidgetCircleColor({this.color, this.width = 20, this.height = 20});
  @override
  Widget build(BuildContext context) {
    return  Container(
      decoration: new BoxDecoration(
          color: color == null ? Colors.blueAccent : color,
          borderRadius: new BorderRadius.circular(10.0)
      ),
      height: height,
      width: width,
      // color: Colors.blueAccent,
    );
  }
}
