import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:smart_business/src/blocs/opportunities/opportunity_event.dart';
import 'package:smart_business/src/blocs/opportunities/opportunity_state.dart';
import 'package:smart_business/src/repositories/opportunity/opportunity_repository.dart';
import 'package:meta/meta.dart';

class OpportunityBloc extends Bloc<OpportunityEvent, OpportunityState> {
    final OpportunityRepository _opportunityRepository;
    OpportunityBloc({@required OpportunityRepository opportunityRepository}):
        assert(opportunityRepository != null), _opportunityRepository =  opportunityRepository;

  @override
  OpportunityState get initialState => UnLoadOpportunities();

  @override
  Stream<OpportunityState> mapEventToState(OpportunityEvent event) async* {

    if(event is LoadOpportunitiesEvent){
      yield* _mapLoadOpportunities();
    }
    else if(event is SearchOpportunityEvent){
          yield* _mapSearchToState(event);
    }
  }

  Stream<OpportunityState> _mapLoadOpportunities() async* {
    var res =  await _opportunityRepository.getListExample();
    LoadedOpportunitiesState(res);
  }

  Stream<OpportunityState> _mapSearchToState(SearchOpportunityEvent event) async*{
    var list  =  event.opportunity;
    var filter =  list.where((item)=> item.name.contains(event.query));
    yield SearchOpportunityState(query: event.query, opportunity: event.opportunity,
    filterOpportunities: filter.toList());
  }
}