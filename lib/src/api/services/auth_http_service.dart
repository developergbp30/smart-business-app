import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/login_request_model.dart';
import 'package:smart_business/src/models/user_session_model.dart';

class AuthHttp extends ApiProvider{

  Future<SessionCompanyModel> checkCodeCompany(String code) async {
    try{
      // http://empresa.fullsmartyes.com/api/mobile/123456
      Response response = await dio.get("/mobile/$code");
      return new SessionCompanyModel.fromJson(response.data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<LoginUserModel> loginApi(LoginRequestModel data) async {
    try{
      Response response = await dio.post("/login", data: data.toJson());
      return new LoginUserModel.fromJson(response.data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<UserSessionModel> userData() async {
    try{
      Response response = await dio.get("/user/mobile", options: Options(headers: {"requirestoken": true}));
      return new UserSessionModel.fromJson(response.data);

    }catch (error){
      print(error);
      return Future.error(error);
    }
  }
}