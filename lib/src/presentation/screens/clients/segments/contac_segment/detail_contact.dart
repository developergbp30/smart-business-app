import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:smart_business/src/presentation/screens/comments/comments_page.dart';
class DetailContact extends StatefulWidget {
  @override
  _DetailContactState createState() => _DetailContactState();
}

class _DetailContactState extends State<DetailContact> {
  final Color greenColor =  Color(0xFF11BA19);
  String exampleDate = "2029-03-10";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Contacto", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
      ),
      body: Container(child: ListView(
        children: <Widget>[
          _cardInfo(),
          SizedBox(height: 10,),
          Divider(),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10,),_containerDesition()
            ],
          ),
          SizedBox(height: 30,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10,),
              Icon(Icons.email, color: greenColor,),
              SizedBox(width: 5,),
              Text("pedrojose@google.com")
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10,),
              Icon(Icons.phone, color: greenColor,),
              SizedBox(width: 5,),
              Text("+15840 684 546")
            ],
          ),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10,),
              Icon(FontAwesomeIcons.mapMarkerAlt, color: greenColor,),
              SizedBox(width: 5,),
              Text("New York - 111 8th Ave, New York, NY 10011")
            ],
          ),
          SizedBox(height: 10,),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 15,),
              Text("Descripción")
            ],
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Flexible(child: Container(
                  margin: EdgeInsets.only(left: 15, right: 10),
                  child: Text("The final thing is to add the Checkbox widget in a similar approach to how we added the Switch widget. In fact, I’d expect that you already tried adding this out of curiosity after being able to implement the Switch widget. We’ll use a boolean variable to hold it’s selection state too, but in this case we’ll have it deselected by", textAlign: TextAlign.justify, style: TextStyle(color: Color(0xFF5D5D5D)),)))
            ],
          ),
          SizedBox(height: 10,),
          Divider(),
          SizedBox(height: 10,),
          _boss(),
          SizedBox(height: 10,),
          Divider(),
          SizedBox(height: 10,),
          Row(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Subordinados", style: TextStyle(color: greenColor, fontWeight: FontWeight.w600, fontSize: 16),),
            )
          ],),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Chip(
                avatar: CircleAvatar(
                  backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814__340.jpg"),
                  // child: ,
                ),
                backgroundColor: Colors.grey.withOpacity(0.3),
                shadowColor:Color(0xFFDBDBDB),
                label:Text("Juan Camilo"),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Chip(
                avatar: CircleAvatar(
                  backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814__340.jpg"),
                  // child: ,
                ),
                backgroundColor: Colors.grey.withOpacity(0.3),
                shadowColor:Color(0xFFDBDBDB),
                label:Text("Juan Camilo"),
              ),
            ],
          ),
        ],
      ),),
    );
  }

  _cardInfo() {
    return Container(
      decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(10.0),
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topLeft,
            stops: [0.3, 1],
            colors: [greenColor, Colors.green.withOpacity(0.6)],


          )
      ),
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.only(top: 8, left: 10, right: 15, bottom: 8),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Chip(
                  avatar: CircleAvatar(
                    backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814__340.jpg"),
                    // child: ,
                  ),
                  backgroundColor: Colors.white,//Color(0xFFFFFFFF),
                  shadowColor:Color(0xFFDBDBDB),
                  label:Text("Juan Camilo"),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.calendar_today, color: Colors.white,),
                    Padding(
                      padding: const EdgeInsets.only(left:3.0),
                      child: Text("${DateFormat.yMMMd('es').format(DateTime.parse(exampleDate).toLocal())}",
                        style: TextStyle(color: Colors.white, fontSize: 12),),
                    )
                  ],
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
             Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
               Text("Jefe de ventas", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),),
               SizedBox(height: 5,),
               Text("Comercial, comercial", style: TextStyle(color: Colors.white),)
             ],),
              InkWell(
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(builder: (context) => CommentsPage(idGts:1,)));
                },
                child: Badge(
                  badgeContent: Text('99', style: TextStyle(color: Colors.white),),
                  child: Icon(Icons.message, color: Colors.white),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  _containerDesition(){
    return Column(children: <Widget>[
      Container(
        width: 170,
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            color: greenColor,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5))
        ),
        child: Text("Toma de decisión", style: TextStyle(color: Colors.white),),
      ),
      Container(
        width: 170,
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            color: Colors.green.withOpacity(0.2),
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5))
        ),
        child: Text("No toma de decisión"),
      )
    ],);
  }

  Widget _boss(){
    return Container(
      child: Column(
        children: <Widget>[
          Row(children: <Widget>[
            Container(width: 80,height: 80,
            margin: EdgeInsets.only(left:10, right: 10),
            child: CircleAvatar(backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2019/11/25/13/54/girl-4652114_640.jpg"),),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
              Text("Jefe", style: TextStyle(color: Color(0xFF5D5D5D)),),
              Text("Lisa María Jara",style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600)),
              Text("Gerente", style: TextStyle(color: Color(0xFF5D5D5D)),),
            ],)
          ],),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10,),
              RichText(text: TextSpan(text: "Personas a cargo: ", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600), children: <TextSpan>[
                TextSpan(text: "2", style: TextStyle(color: Colors.grey.withOpacity(0.6)))
              ])),
            ],
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 10,),
              RichText(text: TextSpan(text: "Tiempo en la empresa: ", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600), children: <TextSpan>[
                TextSpan(text: "2 Años y 3 meses", style: TextStyle(color: Colors.grey.withOpacity(0.8)))
              ])),
            ],
          )
        ],
      ),
    );
  }
}
