import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/approved_user_model.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/guest_user_model.dart';
import 'package:smart_business/src/models/invitations.dart';

class InvitationsHttp extends ApiProvider {

  Future<List<InvitationReceivedModel>> getReceived(idUser) async {
    try{
      Response response = await dio.get("/gestion-tiempo/$idUser/listar-gts-invitados-recibidas", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new InvitationReceivedModel.fromJson(json))
          .toList();

    }catch (error){
       return Future.error(error);
    }
  }

  Future<List<InvitationReceivedModel>> getSend(idUser) async {
    try{
      Response response = await dio.get("/gestion-tiempo/$idUser/listar-gts-invitados-enviadas", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new InvitationReceivedModel.fromJson(json))
          .toList();

    }catch (error){
      print(error);
      return  Future.error(error);
    }
  }

  Future approved(ApprovedInvitationModel inv) async {
    try{

      Response response = await dio.put("/gestion-tiempo/${inv.idInvitation}/aprobar-invitacion", options: Options(headers: {"requirestoken": true}),
      data: inv.toJson());
      return response.statusCode;
    }catch (error){
      return Future.error(error);
    }
  }

  Future reject(ApprovedInvitationModel inv) async {
    try{

      Response response = await dio.put("/gestion-tiempo/${inv.idInvitation}/rechazar-invitacion", options: Options(headers: {"requirestoken": true}),
      data: inv.toJson());
      return response.statusCode;
    }catch (error){
      return Future.error(error);
    }
  }

  Future cancel(ApprovedInvitationModel inv) async {
    try{
      print(inv.toJson());

      Response response = await dio.put("/gestion-tiempo/${inv.idInvitation}/cancelar-invitacion", options: Options(headers: {"requirestoken": true}),
      data: inv.toJson());
      return response.statusCode;
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<RequestContactModel>> contacts(int idGts) async {
    try{
      Response response = await dio.get("/plan-trabajo-movil/$idGts/listado-contactos", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new RequestContactModel.fromJson(json["datos_usuario"]))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<GuestUserModel>> guestUsers(int idGts)  async {
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/$idGts/Action-listar-invidatos", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new GuestUserModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<GuestUserModel>> notificationActividadEmail(int idGts)  async {
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/$idGts/Action-listar-correos", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new GuestUserModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }


  Future<ApprovedUserActivityModel> approvedUser(int idGts)  async {
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/$idGts/estado-aprobacion", options: Options(headers: {"requirestoken": true}));
      return  new ApprovedUserActivityModel.fromJson(response.data);
    }catch (error){
      return Future.error(error);
    }
  }
}