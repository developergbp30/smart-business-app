import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/blocs/task/task_bloc.dart';
import 'package:smart_business/src/blocs/task/task_state.dart';
import 'package:smart_business/src/models/overdue_task_model.dart';
import 'package:smart_business/src/presentation/screens/bitacora/create_bitacora_page.dart';
import 'package:smart_business/src/presentation/screens/workplan/detail_activity.dart';

import '../../widget_time_line_burble.dart';

class TaskValid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   // BlocProvider.of(context).dispatch(event)
  return  BlocListener<TaskBloc, TaskState>(
    listener: (context, state){
      if(state is DetailTaskState){
         Navigator.of(context).pop();
         Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => DetailActivityPage(args: state.detail,)));
      }
    },
    child: Column(
      children: <Widget>[
        Text("Tareas vigentes", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),),
        BlocBuilder<TaskBloc, TaskState>(builder: (context, state){
           if(state is LoadedTaskValidBinnacleState){
            return  Expanded(
              child: ListView.builder(
                  itemCount: state.task.length,
                  itemBuilder: (context, i) => _itemCard2(context, state.task[i])),
            );
          }
           if(state is SendingRequestTaskState){
             return Container(
               //color: Colors.red,
                 width: MediaQuery.of(context).size.width,
                 height: MediaQuery.of(context).size.height/2,
                 child: Center(child: CircularProgressIndicator(),));
           }
          return Container(height: 50,width: 50,);
        })
      ],
    ),
  );

  }

  Widget _itemCard2(BuildContext context,TaskOverdueBinnacleModel example) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: InkWell(
        onTap: () async {

          await Navigator.push(
              context,
              new MaterialPageRoute<bool>(
                  builder: (context) => CreateBitacoraPage(idSelectWorkPlane: example.id,)));
        },
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      //margin: const EdgeInsets.only(top: 10),
                        child: TimeLineWidget(color: Color(0xFF888787),)),
                    Flexible(
                      child: Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, top: 0),
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("${example.titulo}", style: TextStyle(color: Color(0xFF888787), fontWeight: FontWeight.bold),),
                            Container(
                                margin: const EdgeInsets.only(top: 5),
                                child: Text("${example.descripcion}", style: TextStyle(color: Colors.grey[700]),)),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.calendar, size: 15,color: Colors.grey[700]),
                                  SizedBox(width: 5,),
                                  Text("${example.bFecha}", style: TextStyle(color: Colors.grey[700]),),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.clock, size: 15,color: Colors.grey[700]),
                                  SizedBox(width: 5,),
                                  Text("${example.bHoraInicio} - ${example.bHoraInicio}", style: TextStyle(color: Colors.grey[700]),),
                                ],
                              ),
                            ),

                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10,)
              ],
            ),
          ),),
      ),
    );
  }

}
