
import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/time_params_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

class WorkPlanHttp extends ApiProvider{

  Future<List<WorkPlaneModel>> getCalendar(int idUser, String yearMonth) async {
    try{
      Response response = await dio.get("/plan-trabajo-movil/$idUser/calendario?fecha=$yearMonth",
          options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => WorkPlaneModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future<ResponseRegisterActivity> registerActivity(CreateActivityModel data) async {
   data.log();
    try{
      Response response = await dio.post("/GestionTiempos/PlanTrabajo/Action-Create-Plan-Trabajo",
          options: Options(headers: {"requirestoken": true}),
          data: data.toJson()
      );
      print("${response.statusCode} \n ${response.data}");
      if(response.statusCode == 201){ // Success Full
        return new ResponseRegisterActivity( message: 'registro exitoso', type: 1);
      }else {
        if(response.data['id'] != null) return new ResponseRegisterActivity( message: 'Actualizacion exitosa', type: 1);
        return ResponseRegisterActivity(message: response.data['mensaje'], type: 2);
      }
    }catch (error){
      print("Error Http $error");
      return Future.error(error);
    }
  }

  Future<TimeParamsModel> timeParamsGts() async {
    try{
      Response response = await dio.get("/GestionTiempos/Parametros/Action-Gestion-Tiempos-Parametros",
          options: Options(headers: {"requirestoken": true}));

      return new TimeParamsModel.fromJson(response.data);

    }catch (error){
      return Future.error(error);
    }
  }

  Future approvedOrReject(ApprovedRejectModel data) async {
    try{
      Response response = await dio.put("/GestionTiempos/PlanTrabajo/Action-Aprovar-Rechazar",
          options: Options(headers: {"requirestoken": true}),
          data: data.toJson()
      );
      print("response.data : ${response.data}");
      return response.statusCode;
    }catch (error){
      print(error);
      return Future.error(error);
    }
  }

  Future cancelWorkPlane(CancelWorkPlaneModel data) async {
    try{
      Response response = await dio.put("/gestion-tiempo/${data.id}/gestion-cancelar",
          options: Options(headers: {"requirestoken": true}),
          data: data.toJson()
      );
      print("response.data : ${response.data}");
      return response.statusCode;
    }catch (error){
      print(error);
      return Future.error(error);
    }
  }

  Future<String> postponementWorkPlane(RequestPostponementModel data) async {
    try{
      Response response = await dio.put("/gestion-tiempo/${data.id}/gestion-aplazar",
          options: Options(headers: {"requirestoken": true}),
          data: data.toJson()
      );
      print("Status ${response.statusCode} | response.data : ${response.data}");
      return response.data['mensaje'];
    }catch (error){
      print(error);
      return Future.error(error);
    }
  }
}