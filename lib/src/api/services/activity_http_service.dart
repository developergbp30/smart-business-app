import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/activity_model.dart';

class ActivityHttp extends ApiProvider{

  Future<List<ResponseActivityModel>> listTypeActivity() async{
    try{

      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Action-Seleccionar-Actividad-Principal", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseActivityModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseActivityModel>> listSubTypeActivityFather(int id) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/$id/Action-Seleccionar-Sub-Actividad-Padre", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseActivityModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseActivityModel>> listSubTypeActivity(int id) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/$id/Action-Seleccionar-Sub-Actividad-Tipo", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseActivityModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}