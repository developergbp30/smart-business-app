class CommentModel {
  int id;
  String comentario;
  String adjunto;
  int idGts;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  DatosUsario datosUsario;

  CommentModel(
      {this.id,
        this.comentario,
        this.adjunto,
        this.idGts,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.datosUsario});

  CommentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comentario = json['comentario'];
    adjunto = json['adjunto'];
    idGts = json['id_gts'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    datosUsario = json['datos_usario'] != null
        ? new DatosUsario.fromJson(json['datos_usario'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['comentario'] = this.comentario;
    data['adjunto'] = this.adjunto;
    data['id_gts'] = this.idGts;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.datosUsario != null) {
      data['datos_usario'] = this.datosUsario.toJson();
    }
    return data;
  }
}

class DatosUsario {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  DatosUsario(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  DatosUsario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}

class CommentCreateModel {
   int idGts;
   String message;
  CommentCreateModel({this.idGts, this.message});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_gts'] = this.idGts;
    data['comentario'] = this.message;
    return data;
  }
}
