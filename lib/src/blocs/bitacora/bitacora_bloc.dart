import 'package:bloc/bloc.dart';
import 'package:smart_business/src/api/helper_error_dio.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_event.dart';
import 'package:smart_business/src/blocs/bitacora/bitacora_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/bitacora/bitacora_repository.dart';
import 'package:meta/meta.dart';
class BitacoraBloc extends Bloc<BitacoraEvent, BitacoraState>{
  BitacoraRepository _bitacoraRepository;
  AuthRepository _authRepository = AuthRepository();

  BitacoraBloc({@required BitacoraRepository bitacoraRepository}) :
  assert(bitacoraRepository != null), _bitacoraRepository = bitacoraRepository;
  @override
  BitacoraState get initialState => UnloadBitacoraState();

  @override
  Stream<BitacoraState> mapEventToState(BitacoraEvent event) async* {
    if(event is LoadBitacoraEvent){
      yield* _mapLoadBitacoraToState(event);
    }
    else if(event is RegisterBitacoraEvent){
      yield* _mapCreateToState(event);
    }
    if(event is CreatePendingEvent){
      yield* _mapCreatePendingToState(event);
    }
    else if(event is DeleteBinnacleEvent){
      yield* _mapDeleteBinnacleToState(event);
    }
    if(event is CreateExpenseEvent){
      yield* _mapCreateExpenseToState(event);
    }
    else if(event is UpdateExpenseEvent){
      yield* _mapUpdateExpenseToState(event);
    }
    if(event is DeleteExpenseEvent){
      yield* _mapDeleteExpenseToState(event);
    }
  }

  Stream<BitacoraState> _mapLoadBitacoraToState(LoadBitacoraEvent event) async*{
    try{
      var idUser;
      if(event.idUser == 0){
        var value = await _authRepository.userData();
        idUser = value.id;
      }else {
        idUser = event.idUser;
      }

      print("$idUser, ${event.startDate}, ${event.endDate}, ${event.year}");

      var plane = await _bitacoraRepository.getCalendar(idUser, event.startDate, event.endDate, event.year);
      print(plane);
      Map<DateTime, List> events = {};
      for(var i = 0; i < plane.length; i++){
        print(plane[i]);
        if(events[DateTime.parse(plane[i].bFecha)] == null){
          events[DateTime.parse(plane[i].bFecha)] = [plane[i]];
        }else{
          events[DateTime.parse(plane[i].bFecha)].add(plane[i]);
        }
      }
    yield LoadedBitacoraState(list: plane, events: events);
    }catch (error){
      print(error);
      var e =  HelperError().managementError(error);
      yield ErrorBitacoraState(message: e.toString());
    }
  }

  Stream<BitacoraState>  _mapCreateToState(RegisterBitacoraEvent event) async* {
    try{
      yield SendingRequestBinnacleState();
      if(event.data.idUser == null ){
        var value = await _authRepository.userData();
        event.data.idUser = value.id;
      }
      var res =  await _bitacoraRepository.create(event.data);
      if(res.type == 1){
        yield SuccesRegisterBitacoraState(response: res);
      }else {
        WarningBitacoraState(message: res.message);
      }

    }catch(error){
      print(error);
        yield ErrorBitacoraState(message: error.toString());
    }
  }

  Stream<BitacoraState> _mapCreatePendingToState(CreatePendingEvent event) async*{
    try{

      if(event.data.idUserPrincipal == null ){
        var value = await _authRepository.userData();
        event.data.idUserPrincipal = value.id;
      }

      var res =  await _bitacoraRepository.createPending(event.data);
      if(res.type == 1){
        yield SuccesRegisterBitacoraState(response: res);
      }else {
        WarningBitacoraState(message: res.message);
      }

    }catch (error){
      yield ErrorBitacoraState(message: error.toString());
    }
  }

  Stream<BitacoraState> _mapDeleteBinnacleToState(DeleteBinnacleEvent event) async*{
    try{
       await _bitacoraRepository.deleteBinnacle(event.id);
      yield SuccessDeleteBinnacleState();
    }catch (error){
      yield ErrorBitacoraState(message: error.toString());
    }
  }

  Stream<BitacoraState> _mapCreateExpenseToState(CreateExpenseEvent event) async*{
    try{
      yield SendingRequestBinnacleState();
      if(event.data.idUser ==  null ){
        var us =  await _authRepository.userData();
        event.data.idUser =  us.id;
      }
       await _bitacoraRepository.createExpense(event.data);
      yield SuccessCreateExpenseState();
    }catch (error){
      yield ErrorBitacoraState(message: error.toString());
    }
  }

  Stream<BitacoraState> _mapUpdateExpenseToState(UpdateExpenseEvent event) async*{
    try{
      if(event.data.idUser ==  null ){
        var us =  await _authRepository.userData();
        event.data.idUser =  us.id;
      }
      await _bitacoraRepository.updateExpense(event.data);
      yield SuccessUpdateExpenseState();
    }catch (error){
      yield ErrorBitacoraState(message: error.toString());
    }
  }

  Stream<BitacoraState> _mapDeleteExpenseToState(DeleteExpenseEvent event) async*{
    try{
      await _bitacoraRepository.deleteExpense(event.id);
     yield  SuccessDeleteExpenseState();
    }catch (error){

    }
  }
}