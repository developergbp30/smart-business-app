class UserSessionModel {
  int id;
  int estado;
  int tipo;
  String username;
  int superAdmin;
  int permisosConfiguracion;
  int permisosPermisos;
  String chatFotoPerfil;
  int idCliente;
  Empresa empresa;
  Rol rol;
  List<String> permisos;
  List<Menu> menu;
  List<Null> menuFavoritos;
  Mobile mobile;
  int iconos;
  int animacion;
  DatosUsuario datosUsuario;

  UserSessionModel(
      {this.id,
        this.estado,
        this.tipo,
        this.username,
        this.superAdmin,
        this.permisosConfiguracion,
        this.permisosPermisos,
        this.chatFotoPerfil,
        this.idCliente,
        this.empresa,
        this.rol,
        this.permisos,
        this.menu,
        this.menuFavoritos,
        this.mobile,
        this.iconos,
        this.animacion,
        this.datosUsuario});

  UserSessionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    tipo = json['tipo'];
    username = json['username'];
    superAdmin = json['super_admin'];
    permisosConfiguracion = json['permisos_configuracion'];
    permisosPermisos = json['permisos_permisos'];
    chatFotoPerfil = json['chat_foto_perfil'];
    idCliente = json['id_cliente'];
    empresa =
    json['empresa'] != null ? new Empresa.fromJson(json['empresa']) : null;
    rol = json['rol'] != null ? new Rol.fromJson(json['rol']) : null;
    permisos = json['permisos'].cast<String>();
    if (json['menu'] != null) {
      menu = new List<Menu>();
      json['menu'].forEach((v) {
        menu.add(new Menu.fromJson(v));
      });
    }
    if (json['menuFavoritos'] != null) {
      menuFavoritos = new List<Null>();
      json['menuFavoritos'].forEach((v) {
       // menuFavoritos.add(new Null.fromJson(v));
      });
    }
    mobile =
    json['mobile'] != null ? new Mobile.fromJson(json['mobile']) : null;
    iconos = json['iconos'];
    animacion = json['animacion'];
    datosUsuario = json['datos_usuario'] != null
        ? new DatosUsuario.fromJson(json['datos_usuario'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['tipo'] = this.tipo;
    data['username'] = this.username;
    data['super_admin'] = this.superAdmin;
    data['permisos_configuracion'] = this.permisosConfiguracion;
    data['permisos_permisos'] = this.permisosPermisos;
    data['chat_foto_perfil'] = this.chatFotoPerfil;
    data['id_cliente'] = this.idCliente;
    if (this.empresa != null) {
      data['empresa'] = this.empresa.toJson();
    }
    if (this.rol != null) {
      data['rol'] = this.rol.toJson();
    }
    data['permisos'] = this.permisos;
    if (this.menu != null) {
      data['menu'] = this.menu.map((v) => v.toJson()).toList();
    }
    if (this.menuFavoritos != null) {
    //  data['menuFavoritos'] = this.menuFavoritos.map((v) => v.toJson()).toList();
    }
    if (this.mobile != null) {
      data['mobile'] = this.mobile.toJson();
    }
    data['iconos'] = this.iconos;
    data['animacion'] = this.animacion;
    if (this.datosUsuario != null) {
      data['datos_usuario'] = this.datosUsuario.toJson();
    }
    return data;
  }
}

class Empresa {
  int id;
  int estado;
  String nombreCliente;
  String logo;
  String banner;
  int idCiudad;

  Empresa(
      {this.id,
        this.estado,
        this.nombreCliente,
        this.logo,
        this.banner,
        this.idCiudad});

  Empresa.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    nombreCliente = json['nombre_cliente'];
    logo = json['logo'];
    banner = json['banner'];
    idCiudad = json['id_ciudad'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['nombre_cliente'] = this.nombreCliente;
    data['logo'] = this.logo;
    data['banner'] = this.banner;
    data['id_ciudad'] = this.idCiudad;
    return data;
  }
}

class Rol {
  int idCliente;
  int id;
  String nombre;
  int superAdmin;
  String nombreArea;
  int idArea;

  Rol(
      {this.idCliente,
        this.id,
        this.nombre,
        this.superAdmin,
        this.nombreArea,
        this.idArea});

  Rol.fromJson(Map<String, dynamic> json) {
    idCliente = json['id_cliente'];
    id = json['id'];
    nombre = json['nombre'];
    superAdmin = json['super_admin'];
    nombreArea = json['nombre_area'];
    idArea = json['id_area'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_cliente'] = this.idCliente;
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['super_admin'] = this.superAdmin;
    data['nombre_area'] = this.nombreArea;
    data['id_area'] = this.idArea;
    return data;
  }
}

class Menu {
  int id;
  int estado;
  String nombre;
  String icono;
  String ilustrado;
  int posicion;
  dynamic idRutaConfigurar;
  dynamic idPermisoGuardian;
  int idMenuSuperior;
  int idEmpresa;
  List<SubMenus> subMenus;

  Menu(
      {this.id,
        this.estado,
        this.nombre,
        this.icono,
        this.ilustrado,
        this.posicion,
        this.idRutaConfigurar,
        this.idPermisoGuardian,
        this.idMenuSuperior,
        this.idEmpresa,
        this.subMenus});

  Menu.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    nombre = json['nombre'];
    icono = json['icono'];
    ilustrado = json['ilustrado'];
    posicion = json['posicion'];
    idRutaConfigurar = json['id_ruta_configurar'];
    idPermisoGuardian = json['id_permiso_guardian'];
    idMenuSuperior = json['id_menu_superior'];
    idEmpresa = json['id_empresa'];
    if (json['sub_menus'] != null) {
      subMenus = new List<SubMenus>();
      json['sub_menus'].forEach((v) {
        subMenus.add(new SubMenus.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['nombre'] = this.nombre;
    data['icono'] = this.icono;
    data['ilustrado'] = this.ilustrado;
    data['posicion'] = this.posicion;
    data['id_ruta_configurar'] = this.idRutaConfigurar;
    data['id_permiso_guardian'] = this.idPermisoGuardian;
    data['id_menu_superior'] = this.idMenuSuperior;
    data['id_empresa'] = this.idEmpresa;
    if (this.subMenus != null) {
      data['sub_menus'] = this.subMenus.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SubMenus {
  int id;
  int estado;
  String nombre;
  String icono;
  String ilustrado;
  int posicion;
  int idRutaConfigurar;
  String idPermisoGuardian;
  int idMenuSuperior;

  SubMenus(
      {this.id,
        this.estado,
        this.nombre,
        this.icono,
        this.ilustrado,
        this.posicion,
        this.idRutaConfigurar,
        this.idPermisoGuardian,
        this.idMenuSuperior});

  SubMenus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    nombre = json['nombre'];
    icono = json['icono'];
    ilustrado = json['ilustrado'];
    posicion = json['posicion'];
    idRutaConfigurar = json['id_ruta_configurar'];
    idPermisoGuardian = json['id_permiso_guardian'];
    idMenuSuperior = json['id_menu_superior'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['nombre'] = this.nombre;
    data['icono'] = this.icono;
    data['ilustrado'] = this.ilustrado;
    data['posicion'] = this.posicion;
    data['id_ruta_configurar'] = this.idRutaConfigurar;
    data['id_permiso_guardian'] = this.idPermisoGuardian;
    data['id_menu_superior'] = this.idMenuSuperior;
    return data;
  }
}

class Mobile {
  int id;
  int idFondo;
  int idLogo;
  int idMensaje;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  Fondo fondo;
  Fondo logo;
  Mensaje mensaje;

  Mobile(
      {this.id,
        this.idFondo,
        this.idLogo,
        this.idMensaje,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.fondo,
        this.logo,
        this.mensaje});

  Mobile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idFondo = json['id_fondo'];
    idLogo = json['id_logo'];
    idMensaje = json['id_mensaje'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    fondo = json['fondo'] != null ? new Fondo.fromJson(json['fondo']) : null;
    logo = json['logo'] != null ? new Fondo.fromJson(json['logo']) : null;
    mensaje =
    json['mensaje'] != null ? new Mensaje.fromJson(json['mensaje']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_fondo'] = this.idFondo;
    data['id_logo'] = this.idLogo;
    data['id_mensaje'] = this.idMensaje;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.fondo != null) {
      data['fondo'] = this.fondo.toJson();
    }
    if (this.logo != null) {
      data['logo'] = this.logo.toJson();
    }
    if (this.mensaje != null) {
      data['mensaje'] = this.mensaje.toJson();
    }
    return data;
  }
}

class Fondo {
  int id;
  String imagen;

  Fondo({this.id, this.imagen});

  Fondo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imagen = json['imagen'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['imagen'] = this.imagen;
    return data;
  }
}

class Mensaje {
  int id;
  String mensaje;

  Mensaje({this.id, this.mensaje});

  Mensaje.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mensaje = json['mensaje'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mensaje'] = this.mensaje;
    return data;
  }
}

class DatosUsuario {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  DatosUsuario(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  DatosUsuario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}
