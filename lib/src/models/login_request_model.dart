class LoginRequestModel {
   String username;
   String password;
   String email;

  LoginRequestModel({this.username, this.password, this.email});

  factory LoginRequestModel.fromJson(Map<String, dynamic> data) {
    return LoginRequestModel(
      username: data["username"],
      password: data["password"],
      email: data["email"]
    );
  }

  Map<String, dynamic> toJson() => {
    'username': username,
    'password':password,
    'email':email
  };
}


class LoginUserModel {
  String username;
  String token;
  String tokenType;
  int expiresIn;

  LoginUserModel({this.token, this.tokenType, this.expiresIn, this.username});

  LoginUserModel.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    return data;
  }
}

class SessionCompanyModel {
  String identificador;
  int idEmpresa;
  Manager manager;
  String imageBackground;
  String imageLogo;


  SessionCompanyModel({this.identificador, this.idEmpresa, this.manager});

  SessionCompanyModel.fromJson(Map<String, dynamic> json) {
    identificador = json['identificador'];
    idEmpresa = json['id_empresa'];
    imageBackground = json['fondo'];
    imageLogo =  json['logo'];
    manager =
    json['manager'] != null ? new Manager.fromJson(json['manager']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identificador'] = this.identificador;
    data['id_empresa'] = this.idEmpresa;
    if (this.manager != null) {
      data['manager'] = this.manager.toJson();
    }
    return data;
  }
}

class Manager {
  int id;
  String subdominio;
  String dominio;

  Manager({this.id, this.subdominio, this.dominio});

  Manager.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    subdominio = json['subdominio'];
    dominio = json['dominio'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['subdominio'] = this.subdominio;
    data['dominio'] = this.dominio;
    return data;
  }
}




