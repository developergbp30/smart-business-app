import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/contac_segment/detail_contact.dart';
import 'package:smart_business/src/presentation/screens/clients/segments/contac_segment/link_contact.dart';
class ContactsSegment extends StatelessWidget {
  final Color greenColor =  Color(0xFF11BA19);
  @override
  Widget build(BuildContext context) {
    return Container(child: Column(
      children: <Widget>[
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[Text("Contactos", style: TextStyle(fontWeight: FontWeight.w600, color: greenColor),)],),
        SizedBox(height: 20,),
        Container(
          width: MediaQuery.of(context).size.width/2,
          margin: const EdgeInsets.only(left: 20, right: 20),
          color: Colors.transparent,
          padding: const EdgeInsets.all(8),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              color: greenColor,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(50.0),
                  side: BorderSide(color: greenColor)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(FontAwesomeIcons.briefcase,color: Colors.white),
                  SizedBox(width: 6,),
                  Text(" Cargos", style: TextStyle(color: Colors.white),),
                ],
              ),
              // color: Color(0xff5D5D5D), //Colors.pink,
              onPressed: () async {}),
        ),
        SizedBox(height: 30,),
        _gridCharges(),
        Container(
          width: MediaQuery.of(context).size.width/2,
          margin: const EdgeInsets.only(left: 20, right: 20),
          color: Colors.transparent,
          padding: const EdgeInsets.all(8),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              color: greenColor,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(50.0),
                  side: BorderSide(color: greenColor)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(FontAwesomeIcons.user,color: Colors.white),
                  SizedBox(width: 6,),
                  Text(" Contactos", style: TextStyle(color: Colors.white),),
                ],
              ),
              // color: Color(0xff5D5D5D), //Colors.pink,
              onPressed: () async {}),
        ),
        SizedBox(height: 10,),
        Container(
          width: MediaQuery.of(context).size.width/2,
          margin: const EdgeInsets.only(left: 20, right: 20),
          color: Colors.transparent,
          padding: const EdgeInsets.all(8),
          child: FlatButton(
              padding: const EdgeInsets.all(0),
              color: Color(0xFFF5F5F5),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(50.0),
                  side: BorderSide(color: Color(0xFFDBDBDB))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(FontAwesomeIcons.userPlus,color: greenColor),
                  SizedBox(width: 10,),
                  Text(" Vincular contactos", style: TextStyle(color: greenColor),),
                ],
              ),
              // color: Color(0xff5D5D5D), //Colors.pink,
              onPressed: () async {
                await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => LinkContactPage()));
              }),
        ),
        SizedBox(height: 30,),
        _gridContacts(context),
      ],
    ),);
  }

  Widget _gridCharges(){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _cardUser(),
            _cardUser()
          ],
        ),
        SizedBox(height: 30,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _cardUser(),
            _cardUser()
          ],
        ),
      ],
    );
  }

  Widget _gridContacts(BuildContext context){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _cardUser(showCheck: true),
            _cardUser(showCheck: true)
          ],
        ),
        SizedBox(height: 30,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _cardUser(showCheck: true),
            InkWell(
                onTap: () async {
                  await Navigator.push(context, new MaterialPageRoute<bool>(builder: (context) => DetailContact()));
                },
                child: _cardUser(showCheck: true))
          ],
        ),
      ],
    );
  }

  Widget _cardUser({bool showCheck =  false}){
    return Stack(
        overflow: Overflow.visible,
        children: <Widget>[
      Container(
      //  color: Colors.red,
        width: 150,
        height: 150,
        child: Card(elevation: 2,),
      ),
      Positioned.fill(
        top: -20,
        child: Align(
          alignment: Alignment.center,
          child: Column(children: <Widget>[
            Container(
              height: 100,
              width: 100,
              child: CircleAvatar(backgroundImage: NetworkImage("https://cdn.pixabay.com/photo/2019/11/30/22/38/girl-4664439_960_720.jpg"),),),
            SizedBox(height: 20,),
            Text("ADMINISTRADOR",style: TextStyle(color: Color(0xFF5D5D5D), fontWeight: FontWeight.w600),),
            Text("Fransico Jose",style: TextStyle(color: Color(0xFF5D5D5D)),)
          ],),
        ),
      ),
       showCheck ? _checkedCircle() : SizedBox(),
    ]);
  }

  Widget _checkedCircle(){
    return Positioned.fill(
      top: 10,
      child: Align(
        alignment: Alignment.center,
        child: CircleAvatar(
          backgroundColor: Colors.white,
          child: Icon(FontAwesomeIcons.briefcase, color: greenColor,),),
      ),
    );
  }
}
