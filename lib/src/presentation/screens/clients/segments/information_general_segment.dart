import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/presentation/widgets/widget_circle_color.dart';
class InformationGeneralSegment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text("Infomación general", style: TextStyle(color: Colors.green),)
          ],),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: <Widget>[Text("Apple Inc.")],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
              text: "Edad: ",
              style: TextStyle(color: Colors.black87),
              children: <TextSpan>[
                TextSpan(text:"35 Años", style: TextStyle(color: Colors.grey))
              ]
            ))],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[
              Flexible(
                child: RichText(text: TextSpan(
                text: "Fecha de fundación: ",
                  style: TextStyle(color: Colors.black87),
                children: <TextSpan>[
                   TextSpan(text:"4 de Septiembre de 1998", style: TextStyle(color: Colors.grey))
                ]
            )),
              )],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[
              Icon(FontAwesomeIcons.mapMarker, color: Colors.green,),
              Text("New York, NY, Estados Unidos")
            ],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
              text: "Tamaño de la empresa: ",
                style: TextStyle(color: Colors.black87),
              children: <TextSpan>[
                TextSpan(text:"15.000", style: TextStyle(color: Colors.grey))
              ]
            ))],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
              text: "Origen de la identificación: ",
                style: TextStyle(color: Colors.black87),
              children: <TextSpan>[
                TextSpan(text:"Evento", style: TextStyle(color: Colors.grey))
              ]
            ))],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
              text: "Evento: ",
                style: TextStyle(color: Colors.black87),
              children: <TextSpan>[
                TextSpan(text:"ComicCon L.A", style: TextStyle(color: Colors.grey))
              ]
            ))],),
          ),
          /**-----------------------
           *  Classification
           * **/
          SizedBox(height: 20,),
          Row(
            children: <Widget>[
              WidgetCircleColor(color: Colors.green, height: 10, width: 10,),
              SizedBox(width: 10,),
              Text("Clasificación", style: TextStyle(color: Colors.green),)
            ],
          ),
          // Zonal
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
                text: "Zonal: ",
                style: TextStyle(color: Colors.black87),
                children: <TextSpan>[
                  TextSpan(text:"Internacional", style: TextStyle(color: Colors.grey))
                ]
            ))],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
                text: "Frecuencia de compra: ",
                style: TextStyle(color: Colors.black87),
                children: <TextSpan>[
                  TextSpan(text:"Media", style: TextStyle(color: Colors.grey))
                ]
            ))],),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: <Widget>[RichText(text: TextSpan(
                text: "Categoría: ",
                style: TextStyle(color: Colors.black87),
                children: <TextSpan>[
                  TextSpan(text:"Gold", style: TextStyle(color: Colors.grey))
                ]
            ))],),
          ),
        ],
      ),
    );
  }
}
