import 'package:smart_business/src/api/services/activity_http_service.dart';
import 'package:smart_business/src/api/services/clients_http_service.dart';
import 'package:smart_business/src/api/services/competitions_http_service.dart';
import 'package:smart_business/src/api/services/event_http_service.dart';
import 'package:smart_business/src/api/services/opportunity_http_service.dart';
import 'package:smart_business/src/api/services/pqr_http_service.dart';
import 'package:smart_business/src/api/services/project_http_service.dart';
import 'package:smart_business/src/api/services/workplan_http_service.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/client_model.dart';
import 'package:smart_business/src/models/competitions_model.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/opportunity_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';
import 'package:smart_business/src/models/project_model.dart';
import 'package:smart_business/src/models/time_params_model.dart';
import 'package:smart_business/src/models/workplane_model.dart';

class WorkPlanRepository {
  WorkPlanHttp _workPlanHttp = WorkPlanHttp();
  ActivityHttp _activityHttp = ActivityHttp();
  OpportunityHttp _opportunityHttp = OpportunityHttp();
  PqrHttp _pqrHttp = PqrHttp();
  EventHttp _eventHttp = EventHttp();
  ClientsHttp _clientsHttp = ClientsHttp();
  CompetitionsHttp _competitionsHttp = CompetitionsHttp();
  ProjectHttp _projectHttp = ProjectHttp();

  Future<List<WorkPlaneModel>> calendarGts(idUser, String yearMonth) async {
    try{
      return await _workPlanHttp.getCalendar(idUser, yearMonth);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseActivityModel>> getTypeActivity() async{
    try{
      return await _activityHttp.listTypeActivity();
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseActivityModel>> getSubTypeActivity(int id) async{
    try{
      return await _activityHttp.listSubTypeActivity(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseActivityModel>> getSubTypeActivityFather(int id) async{
    try{
      return await _activityHttp.listSubTypeActivityFather(id);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseOpportunityModel>> listOpsUser(int idUser) async{
    try{
      return await _opportunityHttp.listOpsUser(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponsePqrModel>> listPqrUser(int idUser) async{
    try{
      return await _pqrHttp.listPqrUser(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseEventModel>> listEventUser(int idUser) async{
    try{
      return await _eventHttp.listEventUser(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseClientModel>> listClientUser(int idUser) async{
    try{
      return await _clientsHttp.listClientUser(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseCompetitionModel>> listCompetitionUser(int idUser) async{
    try{
      return await _competitionsHttp.listCompetitionsUser(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<ResponseProjectModel>> listProjectUser(int idUser) async{
    try{
      return await _projectHttp.listProjectUser(idUser);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<ResponseRegisterActivity> registerActivity(CreateActivityModel data) async{
    try{
      return await _workPlanHttp.registerActivity(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<TimeParamsModel> getTimeParams() async {
    try{
      return _workPlanHttp.timeParamsGts();
    }catch (error){
      return Future.error(error);
    }
  }

  Future approved(ApprovedRejectModel data) async {
    try{
      return _workPlanHttp.approvedOrReject(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future reject(ApprovedRejectModel data) async {
    try{
      return _workPlanHttp.approvedOrReject(data);
    }catch (error){
      return Future.error(error);
    }
  }

  Future cancel(CancelWorkPlaneModel data) async {
    try{
      return _workPlanHttp.cancelWorkPlane(data);
    }catch (error){
      return Future.error(error);
    }

  }

  Future<String> postponement(RequestPostponementModel data) async {
    try{
      return _workPlanHttp.postponementWorkPlane(data);
    }catch (error){
      return Future.error(error);
    }

  }

}