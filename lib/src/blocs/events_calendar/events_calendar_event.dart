import 'package:equatable/equatable.dart';

abstract class EventsCalendarEvent extends Equatable {
  EventsCalendarEvent([List props = const []]) : super(props);
}

class ChangeDayEventsCalendarEvent extends EventsCalendarEvent {
  final List<dynamic> events;
  ChangeDayEventsCalendarEvent(this.events) : super([events]);
}