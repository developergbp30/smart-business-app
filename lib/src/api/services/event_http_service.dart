import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/event_model.dart';
import 'package:smart_business/src/models/pqr_model.dart';

class EventHttp extends ApiProvider {

  Future<List<ResponseEventModel>> listEventUser(int idUser) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Eventos/$idUser/Action-Load-Select-Dinamic", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseEventModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}