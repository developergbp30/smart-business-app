import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/bitacora_model.dart';
import 'package:smart_business/src/models/create_expenses.dart';

abstract class BitacoraEvent extends Equatable {
  BitacoraEvent([List props = const []]) : super(props);
}

class LoadBitacoraEvent extends BitacoraEvent{
  final int idUser;
  final String year;
  final String startDate;
  final String endDate;
  LoadBitacoraEvent({this.idUser, this.year, this.startDate, this.endDate}) : super([idUser, year, startDate, endDate]);

  @override
  String toString() => '$runtimeType {id: $idUser, year: $year, startDate: $startDate, endDate: $endDate} ';
}

class RegisterBitacoraEvent extends BitacoraEvent{
  final CreateBitacoraModel data;
  RegisterBitacoraEvent({this.data}) : super([data]);

  @override
  String toString() => '$runtimeType ${data.toJson()} ';
}

class CreatePendingEvent extends BitacoraEvent{
  final CreatePendingModel data;
  CreatePendingEvent({this.data}) : super([data]);

  @override
  String toString() => '$runtimeType ${data.toJson()} ';
}

class DeleteBinnacleEvent extends BitacoraEvent{
  final int id;
  DeleteBinnacleEvent({this.id}) : super([id]);
  @override
  String toString() => '$runtimeType { id => $id } ';
}

class CreateExpenseEvent extends BitacoraEvent{
  final CreateExpensesModel data;
  CreateExpenseEvent({this.data}) : super([data]);
}

class UpdateExpenseEvent extends BitacoraEvent{
  final CreateExpensesModel data;
  UpdateExpenseEvent({this.data}) : super([data]);
}

class DeleteExpenseEvent extends BitacoraEvent{
  final int id;
  DeleteExpenseEvent({this.id}) : super([id]);
  @override
  String toString() => '$runtimeType { id => $id } ';
}