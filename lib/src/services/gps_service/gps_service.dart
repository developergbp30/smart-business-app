import 'dart:async';

import 'package:location/location.dart';
import 'package:smart_business/src/repositories/gps/gps-respository.dart';
class GpsService {
  //Get repository of location
  GpsRepository _gpsRepository = GpsRepository();

  //Make this a singleton class
  GpsService._privateConstructor();
  static final GpsService instance = GpsService._privateConstructor();
  //
   Timer _intervalGps;

   _initIntervalGps() async {
      _intervalGps = new Timer.periodic(const Duration(minutes: 2), (timer) async {
        LocationData res = await _gpsRepository.getLocation();
        // Todo add logic communication whit RestApi or local Database
      });
   }
}