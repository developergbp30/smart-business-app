import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/comments_model.dart';

abstract class CommentsState extends Equatable {
  CommentsState([List props = const []]) : super(props);
}

class UnloadCommentsState extends CommentsState {}

class LoadedCommentsState extends CommentsState {
  final List<CommentModel> comments;

  LoadedCommentsState({this.comments}) : super([comments]);
}

class SuccessCreateCommentsState extends CommentsState {}

class ErrorCommentState extends CommentsState{
  final String message;
  final DateTime dateTime;

  ErrorCommentState({this.message, this.dateTime}) : super([message, dateTime]);

}
