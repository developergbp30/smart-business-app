import 'package:flutter/material.dart';
Widget globalBoxSearch(BuildContext context, {hintText, fOnchange}){
  return Container(
    height: 50,
    margin: const EdgeInsets.only(bottom: 7, top: 7),
    child: TextFormField(
      decoration: InputDecoration(
        border: OutlineInputBorder(
            borderSide: BorderSide(
                color: Colors.grey,
                width: 5.0
            ),
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        hintText: hintText,
        hintStyle: TextStyle(fontStyle: FontStyle.italic),

        // labelText: 'Actividad'
      ),
      onChanged: fOnchange,
    ),
  );
}