import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/for_views/data_screen_create_activity.dart';
import 'package:smart_business/src/models/time_params_model.dart';

abstract class SelectActivityState extends Equatable {
  SelectActivityState([List props = const []]) : super(props);
}

class EmptySelectsState extends SelectActivityState{}

class DataForSelectState extends SelectActivityState{
  final List<ResponseActivityModel> activities;
  final List<ResponseActivityModel> subTypeActivities;
  final BackDataThirdSelect thirdSelect;
  final bool showThirdSelect;
  final TimeParamsModel timeParams;


  DataForSelectState({this.activities, this.subTypeActivities,
    this.showThirdSelect, this.thirdSelect, this.timeParams})
      : super([activities, subTypeActivities,
    showThirdSelect, thirdSelect, timeParams]);
}