import 'package:equatable/equatable.dart';

abstract class BitacoraDetailEvent extends Equatable {
  BitacoraDetailEvent([List props = const []]) : super(props);
}


class FindDetailBitacoraEvent extends BitacoraDetailEvent {
  final int id;
  FindDetailBitacoraEvent({this.id}) :  super([id]);

  @override
  String toString() => '$runtimeType { id: $id }';
}

