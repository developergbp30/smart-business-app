

class CreateActivityModel {
  int id;
  int idUser;
  String title;
  String description;
  dynamic activityType;
  dynamic activitySubType;
  DateTime date;
  List<String> time;
  List<dynamic> myGuest;
  List<dynamic> myInformed;
  List<dynamic> myExternal;
  List<int> myContact;
  int idClient;
  int idPqr;
  int idOpportunity;
  int idActa;
  int idEvents;
  int idCompetitions;
  int idProject;
  int ptAtiempo;

  CreateActivityModel({
    this.id,
    this.idUser,
    this.title,
    this.description,
    this.activityType,
    this.activitySubType,
    this.date,
    this.time,
    this.myGuest,
    this.myInformed,
    this.myExternal,
    this.myContact,
    this.idClient,
    this.idPqr,
    this.idOpportunity,
    this.idActa,
    this.idEvents,
    this.idCompetitions,
    this.idProject,
    this.ptAtiempo,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] =  this.id;
    data['id_user'] =  this.idUser;
    data['fecha'] =  this.date.toString();
    data['hora'] =  this.time;
    data['titulo'] =  this.title;
    data['descripcion'] =  this.description;
    data['tipo_actividad'] =  this.activityType;
    data['sub_actividad'] =  this.activitySubType;
    data['mis_invitados'] =  this.myGuest !=  null ? this.myGuest.map((item) => item.toJson()).toList() : [];
    data['mis_informados'] =  this.myInformed !=  null ? this.myInformed.map((item) => item.toJson()).toList() : [];
    data['mis_externos'] =  this.myExternal;
    data['mis_contactos'] = this.myContact;
    data['id_cliente'] = this.idClient;
    data['id_pqr'] = this.idPqr;
    data['id_oportunidad'] = this.idOpportunity;
    data['id_acta'] = this.idActa;
    data['id_eventos'] = this.idEvents;
    data['id_competencias'] = this.idCompetitions;
    data['id_proyecto'] = this.idProject;
    data['pt_atiempo'] = this.ptAtiempo;


    return data;
  }

  void log() {
    print(" id => ${this.id} , id_user => ${this.idUser} , fecha => ${this.date}");
    print(" hora => ${this.time} , titulo => ${this.title} , descripcion => ${this.description}");
    print(" tipo_actividad => ${this.activityType} , sub_actividad => ${this.activitySubType} ");
    print(" id_cliente => ${this.idClient} , id_pqr => ${this.idPqr} , id_oportunidad => ${this.idOpportunity}");
    print(" id_acta => ${this.idActa} , id_eventos => ${this.idEvents} , id_competencias => ${this.idCompetitions}");
    print(" id_proyecto => ${this.idProject} , pt_atiempo => ${this.ptAtiempo} ");
    print("mis_contactos => ${this.myContact}");


    //data['mis_invitados'] =  this.myGuest !=  null ? this.myGuest.map((item) => item.toJson()).toList() : [];
  //  data['mis_informados'] =  this.myInformed !=  null ? this.myInformed.map((item) => item.toJson()).toList() : [];
  //  data['mis_externos'] =  this.myExternal;
  //  data['mis_contactos'] = this.myContact;
  }
}

class ResponseActivityModel {
  int id;
  String nombre;

  ResponseActivityModel({this.id, this.nombre});

  ResponseActivityModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    return data;
  }
}

class ResponseRegisterActivity {
  final String message;
  final int type;

  ResponseRegisterActivity({this.message, this.type});
}
