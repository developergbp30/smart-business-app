class TimeParamsModel {
  String inicio;
  String fin;
  int ptTiempo;
  int ptPermitido;
  int bTiempo;
  int bPermitido;

  TimeParamsModel(
      {this.inicio,
        this.fin,
        this.ptTiempo,
        this.ptPermitido,
        this.bTiempo,
        this.bPermitido});

  TimeParamsModel.fromJson(Map<String, dynamic> json) {
    inicio = json['inicio'];
    fin = json['fin'];
    ptTiempo = json['pt_tiempo'];
    ptPermitido = json['pt_permitido'];
    bTiempo = json['b_tiempo'];
    bPermitido = json['b_permitido'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['inicio'] = this.inicio;
    data['fin'] = this.fin;
    data['pt_tiempo'] = this.ptTiempo;
    data['pt_permitido'] = this.ptPermitido;
    data['b_tiempo'] = this.bTiempo;
    data['b_permitido'] = this.bPermitido;
    return data;
  }
}
