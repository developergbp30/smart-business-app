import 'package:bloc/bloc.dart';
import 'package:smart_business/src/api/helper_error_dio.dart';
import 'package:smart_business/src/blocs/postponement/postponment_event.dart';
import 'package:smart_business/src/blocs/postponement/postponment_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/postponement/postponement_repository.dart';
import 'package:meta/meta.dart';
class PostponementBloc extends Bloc<PostponementEvent, PostponementState> {

  PostponementRepository _postponementRepository;
  AuthRepository _authRepository = AuthRepository();

  PostponementBloc({@required PostponementRepository postponementRepository})
      : assert(postponementRepository !=  null), _postponementRepository = postponementRepository;

  @override
  PostponementState get initialState => UnloadPostponementState();

  @override
  Stream<PostponementState> mapEventToState(PostponementEvent event) async* {
    if(event is LoadHistoryPostponementEvent){
        yield* _mapLoadHistorySatet(event);
    }
  }

  Stream<PostponementState> _mapLoadHistorySatet(LoadHistoryPostponementEvent event) async* {
    try{
        var token  = await _authRepository.getToken();
        var res  =  await _postponementRepository.getPostponement(event.idGts, event.page);
        yield LoadedHistoryPostponementState(history: res, token: token);
    }catch (error){
      print(error);
      var e = HelperError().managementError(error);
      print(e);
      yield ErrorPostponementState(message: e.toString());
    }
  }
}