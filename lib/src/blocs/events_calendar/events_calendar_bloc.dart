import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_event.dart';
import 'package:smart_business/src/blocs/events_calendar/events_calendar_state.dart';

class EventsCalendarBloc extends Bloc<EventsCalendarEvent, EventsCalendarState> {
  @override
  EventsCalendarState get initialState => EmptyEventsCalendarState();

  @override
  Stream<EventsCalendarState> mapEventToState(EventsCalendarEvent event) async*{
    if(event is ChangeDayEventsCalendarEvent){
      yield ChangeDayEventsCalendarState(event.events);
    }
  }

}