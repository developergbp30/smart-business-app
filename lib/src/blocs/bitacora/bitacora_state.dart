import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/activity_model.dart';
import 'package:smart_business/src/models/bitacora_model.dart';

abstract class BitacoraState extends Equatable {
  BitacoraState([List props = const []]) : super(props);
}

class UnloadBitacoraState extends BitacoraState{}

class LoadedBitacoraState extends BitacoraState{
  final List<ResponseBitacoraModel> list;
  final Map<DateTime, List> events;
  LoadedBitacoraState({this.list, this.events}) : super([list, events]);

  @override
  String toString() => '$runtimeType {list: $list, events: $events}';
}

class SuccesRegisterBitacoraState extends BitacoraState{
  final ResponseRegisterActivity response;
  SuccesRegisterBitacoraState({this.response}) : super([response]);

  @override
  String toString() => '$runtimeType {response: $response}';
}

class WarningBitacoraState extends BitacoraState{
  final String message;
  WarningBitacoraState({this.message}) : super([message]);
}

class ErrorBitacoraState extends BitacoraState{
  final String message;
  ErrorBitacoraState({this.message});
}

class SendingRequestBinnacleState extends BitacoraState{}

class SuccessDeleteBinnacleState extends BitacoraState{}

class SuccessCreateExpenseState extends BitacoraState{}

class SuccessUpdateExpenseState extends BitacoraState{}

class SuccessDeleteExpenseState extends BitacoraState{}