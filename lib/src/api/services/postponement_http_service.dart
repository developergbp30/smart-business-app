import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/postponement_model.dart';

class PostponementHttp extends ApiProvider {


  Future<List<PostponementModel>> list(int idGts, int page)  async {
    try{
      Response response = await dio.get("/GestionTiempos/aplazamiento-bitacora/$idGts/listar-aplazados?page=$page", options: Options(headers: {"requirestoken": true}));
      return (response.data["data"] as List)
          .map((json) => new PostponementModel.fromJson(json))
          .toList();
    }catch (error){
      return Future.error(error);
    }
  }
}