import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/blocs/auth/auth_bloc.dart';
import 'package:smart_business/src/blocs/auth/auth_event.dart';
import 'package:smart_business/src/blocs/auth/auth_state.dart';
import 'package:smart_business/src/blocs/user_in_session/userInSession_bloc.dart';
import 'package:smart_business/src/blocs/user_in_session/userInSession_event.dart';
import 'package:smart_business/src/blocs/user_in_session/userInSession_state.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/utils/navigation.dart';
class DrawerLeft extends StatelessWidget {
  ThemeApp _themeApp = ThemeApp();
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: MultiBlocListener(
        listeners: [
          BlocListener<AuthBloc, AuthState>(
            listener: (context, state){
              if(state is CloseSessionState){
                navigateToRootApp(context);
              }
            },
          )
        ],
        child: Column(
          children: <Widget>[
           /* _infoUserLogged(context),
            _bannerModule(),*/
            Expanded(
              child: _itemsList(context),
            )
          ],
        ),
      ),
    );
  }

  Widget _infoUserLogged(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      padding: const EdgeInsets.only(left: 20, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new CircleAvatar(
            backgroundImage: new NetworkImage("https://cdn.pixabay.com/photo/2015/03/03/08/55/portrait-photography-657116_960_720.jpg"),
          ),
          Text("Adriana Lucia", style: TextStyle(fontWeight: FontWeight.bold),),
          Text("Diseñadora", style: TextStyle(fontStyle: FontStyle.italic),),

        ],
      ),
    );
  }

  Widget _bannerModule() {
    return Container(
      height: 35,
      padding: const EdgeInsets.only(left: 10, bottom: 5),
      decoration: new BoxDecoration(
          color: _themeApp.colorPink,//Colors.pink,
          borderRadius: new BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5))
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Icon(FontAwesomeIcons.calendar, color: Colors.white,),
          SizedBox(width: 10,),
          Text("Getión del tiempo", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),)
        ],
      ),
    );
  }

  Widget _itemsList(BuildContext context){
    return ListView(
      children: <Widget>[
        BlocBuilder<UserInSessionBloc, UserInSessionState>(
          builder: (context, state){
            if(state is LoadedDataUserInSession){
              return  UserAccountsDrawerHeader(
                margin: const EdgeInsets.only(bottom: 0),
                accountName: Text("${state.userSessionModel.datosUsuario.nombre}"),
                accountEmail: Text("${state.userSessionModel.datosUsuario.cargo}"),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: loadImageApi(state.userSessionModel.datosUsuario.foto, state.token),
                  /*backgroundColor:
            Theme.of(context).platform == TargetPlatform.iOS
                ? Colors.blue
                : Colors.white,
            child: Text(
              "A",
              style: TextStyle(fontSize: 40.0),
            ),*/
                ),
              );
            }
            else if(state is UnloadedDataUserInSession){
              BlocProvider.of<UserInSessionBloc>(context).dispatch(LoadDataUserInSessionEvent());
            }
            return  UserAccountsDrawerHeader(
              margin: const EdgeInsets.only(bottom: 0),
              accountName: Text("Adruiana Lucia"),
              accountEmail: Text("Diseñadora"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: new NetworkImage("https://cdn.pixabay.com/photo/2015/03/03/08/55/portrait-photography-657116_960_720.jpg"),
                /*backgroundColor:
            Theme.of(context).platform == TargetPlatform.iOS
                ? Colors.blue
                : Colors.white,
            child: Text(
              "A",
              style: TextStyle(fontSize: 40.0),
            ),*/
              ),
            );
          },
        ),

        _bannerModule(),
        ListTile(
          leading: SizedBox(width: 10,),
          title: Text("Informe de actividades", style: TextStyle(color: Colors.grey[600])), onTap: () {
          Navigator.of(context).pop();
          navigateFromLoginToHome(context);
        }),
        ListTile( leading: SizedBox(width: 10,), title: Text("Plan de Tabajo", style: TextStyle(color: Colors.grey[600]),),
          onTap: () {
            Navigator.of(context).pop();
            navigateToWorkPlan(context);
          }),
        ListTile( leading: SizedBox(width: 10,), title: Text("Invitaciones", style: TextStyle(color: Colors.grey[600]),),
        onTap: () {
          Navigator.of(context).pop();
          navigateToInvitations(context);
        },),
        ListTile( leading: SizedBox(width: 10,), title: Text("Por Programar", style: TextStyle(color: Colors.grey[600]),),
        onTap: () {
          Navigator.of(context).pop();
          navigateToForPrograming(context);
        }),
        ListTile( leading: SizedBox(width: 10,), title: Text("Bitácora", style: TextStyle(color: Colors.grey[600]),),
        onTap: ()  {
        Navigator.of(context).pop();
        navigateToBitacora(context);
        }),
        ListTile( leading: SizedBox(width: 10,), title: Text("Cambiar Cliente", style: TextStyle(color: Colors.grey[600]),),
          onTap: () {
            Navigator.of(context).pop();
            navigateToClients(context);
          },),
        ListTile( leading: SizedBox(width: 10,), title: Text("Cerrar Sesión", style: TextStyle(color: Colors.grey[600]),),
          onTap: () {
              _showAlertCloseSession(context);
          },),
        ListTile( leading: SizedBox(width: 10,), title: Text("Genrar PDF", style: TextStyle(color: Colors.grey[600]),),
          onTap: () {
            navigateToPDFPage(context);
          },),
      ],
    );
  }

  void _showAlertCloseSession(BuildContext context){
    showDialog(context: context,
    builder: (BuildContext context){
      return AlertDialog(
        title: new Text("Cerrar Sesión"),
        content: new Text("¿Seguro quieres salir?"),
        actions: <Widget>[
          new FlatButton(onPressed: (){
            Navigator.of(context).pop();
          }, child: Text("Espera No")),
          new FlatButton(onPressed: (){
            Navigator.of(context).pop();
            BlocProvider.of<AuthBloc>(context).dispatch(CloseSessionEvent());
          }, child: Text("Si", style: TextStyle(color: Colors.red),)),
        ],
      );
    }
    );
  }


}
