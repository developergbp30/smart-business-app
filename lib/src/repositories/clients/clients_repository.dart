import 'package:smart_business/src/api/services/clients_http_service.dart';

class ClientRepository {
    ClientsHttp _clientsHttp = ClientsHttp();
  Future getClients() async {
    try{
        return await _clientsHttp.getClients();
    }catch (error){
      Future.error(error);
    }
  }

  Future<dynamic> changeClient(int id) async {
    try{
        return await _clientsHttp.changeClient(id);
    }catch(error){
      Future.error(error);
    }
  }
}