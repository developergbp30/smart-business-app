import 'package:bloc/bloc.dart';
import 'package:smart_business/src/blocs/task/task_event.dart';
import 'package:smart_business/src/blocs/task/task_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
import 'package:smart_business/src/repositories/task/task_repository.dart';
import 'package:meta/meta.dart';
class TaskBloc extends Bloc<TaskEvent, TaskState> {
  TaskRepository _taskRepository;
  AuthRepository _authRepository = AuthRepository();

  TaskBloc({@required TaskRepository taskRepository}) : assert(
  taskRepository !=  null
  ), _taskRepository =  taskRepository;

  @override
  TaskState get initialState => UnloadedTaskState();

  @override
  Stream<TaskState> mapEventToState(TaskEvent event) async* {
    if(event is LoadTaskDefeatedEvent){
      yield* _mapTaskDefeated(event);
    }
    else if(event is LoadTaskValidEvent){
      yield* _mapTaskValid(event);
    }
    if(event  is LoadTaskPqrEvent){
      yield* _mapTaskPqr(event);
    }
    if(event is LoadTaskOpsEvent){
      yield* _mapTaskOps(event);
    }
    else if(event is LoadTaskEventsEvent){
      yield* _mapTaskEvent(event);
    }
    if(event is FindDetailTaskEvent){
      yield* _mapDetailTaskToState(event);
    }
    else if(event is LoadTaskActasEvent){
      yield* _mapTaskActas(event);
    }
    if(event is FindDetailActaEvent){
      yield* _mapTaskDetailActa(event);
    }
    else  if( event  is LoadTaskProjectEvent){
     yield*  _mapTaskProject(event);
    }
  }

  Stream<TaskState> _mapTaskDefeated(LoadTaskDefeatedEvent event) async*{
    try{
      yield SendingRequestTaskState();
      var us = await _authRepository.userData();
      if(event.typeTask == 1){// Find task for WorkPlane
        var res  =  await _taskRepository.defeated(us.id);
        yield LoadedTaskDefeatedState(task: res);
      }
      else if(event.typeTask == 2){// Find task for Binnacle
        var res = await _taskRepository.defeatedBinnacle(us.id);
        yield LoadedTaskDefeatedBinnacleState(task: res);
      }

    }catch (error){
      print(error);
      yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapTaskValid(LoadTaskValidEvent event) async*{
    try{
      yield SendingRequestTaskState();
      var us = await _authRepository.userData();
      if(event.typeTask == 2){// Find task for Binnacle
        var res = await _taskRepository.validBinnacle(us.id);
        yield LoadedTaskValidBinnacleState(task: res);
      }

    }catch (error){
      yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapTaskPqr(LoadTaskPqrEvent event) async*{
    try{
      yield SendingRequestTaskState();
        int userId;
      if(event.userId == 0){
       var us = await _authRepository.userData();
       userId =  us.id;
      }else{
        userId = event.userId;
      }
      switch(event.typeTask){
        case 1:
          var res  =  await _taskRepository.pqr(userId);
          yield LoadedTaskPqrState(task: res);
          break;
        case 2:
          var res  =  await _taskRepository.pqrBinnacle(userId);
          yield LoadedTaskPqrBinnacleState(task: res);
          break;
      }

    }catch (error){
      print(error);
        yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapTaskOps(LoadTaskOpsEvent event) async*{
    try{
      yield SendingRequestTaskState();
      var us = await _authRepository.userData();
      switch(event.typeTask){
        case 1:
          var res  =  await _taskRepository.ops(us.id);
          yield LoadedTaskOpsState(task: res);
          break;
        case 2:
          var res  =  await _taskRepository.opsBinnacle(us.id);
          yield LoadedTaskOpsBinnacleState(task: res);
          break;
      }

    }catch (error){
      print(error);
        yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapTaskEvent(LoadTaskEventsEvent event) async*{
    try{
      yield SendingRequestTaskState();
      switch(event.typeTask){
        case 1:
          var res  =  await _taskRepository.event();
          yield LoadedTaskEventState(task: res);
          break;
        case 2:
          var res  =  await _taskRepository.eventBinnacle();
          yield LoadedTaskEventBinnacleState(task: res);
          break;
      }

    }catch (error){
      print(error);
        yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapTaskActas(LoadTaskActasEvent event) async*{
    try{
      yield SendingRequestTaskState();
      var us = await _authRepository.userData();
      switch(event.typeTask){
        case 1:
          var res  =  await _taskRepository.actas(us.id);
          yield LoadedTaskActasState(task: res);
          break;
        case 2:
          var res  =  await _taskRepository.actas(us.id);
          yield LoadedTaskActasBinnacleState(task: res);
          break;
      }

    }catch (error){
      print(error);
        yield ErrorTaskState(message: error.toString());
    }
  }
  
  Stream<TaskState> _mapTaskProject(LoadTaskProjectEvent event) async*{
    try{
      yield SendingRequestTaskState();
      switch(event.typeTask){
        case 1:
          var res  =  await _taskRepository.project();
          yield LoadedTaskProjectState(task: res);
          break;
        case 2:
          var res  =  await _taskRepository.projectBinnacle();
          yield LoadedTaskProjectBinnacleState(task: res);
          break;
      }

    }catch (error){
      print(error);
        yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapTaskDetailActa(FindDetailActaEvent event) async*{
    try{
      //var us = await _authRepository.userData();
      yield SendingRequestTaskState();
      var res  =  await _taskRepository.detailActa(event.id);
     yield DetailActaState(detail: res);
    }catch (error){
      print(error);
        yield ErrorTaskState(message: error.toString());
    }
  }

  Stream<TaskState> _mapDetailTaskToState(FindDetailTaskEvent event) async*{
    try{
      yield SendingRequestTaskState();
      var res =  await _taskRepository.detailTask(event.id);
      yield DetailTaskState(detail: res);
    }catch (error){
      print(error);
      yield ErrorTaskState(message: error.toString());
    }
  }
}