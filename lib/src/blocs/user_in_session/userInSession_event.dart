import 'package:equatable/equatable.dart';

abstract class UserInSessionEvent extends Equatable {
  UserInSessionEvent([List props = const []]) : super(props);
}

class LoadDataUserInSessionEvent extends UserInSessionEvent {}