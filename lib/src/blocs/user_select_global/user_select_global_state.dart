import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/user_model.dart';

abstract class UserSelectGbState extends Equatable {
  UserSelectGbState([List props = const []]) : super(props);
}

class EmptyUserGlobalState  extends UserSelectGbState{}

class ChangedUserGlobalState  extends UserSelectGbState{
  final UserModel user;
  final String token;
  ChangedUserGlobalState(this.user, {this.token}) : super([user]);
  @override
  String toString() => '$runtimeType { user : ${user.id} }';
}