import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart'
;
import 'package:smart_business/src/api/helper_error_dio.dart';
import 'package:smart_business/src/blocs/auth/auth_event.dart';
import 'package:smart_business/src/blocs/auth/auth_state.dart';
import 'package:smart_business/src/repositories/auth/auth_repository.dart';
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;
  AuthBloc({@required AuthRepository authRepository}) : assert(authRepository != null), _authRepository = authRepository;

  @override
  AuthState get initialState => UnAuthenticate();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {

    if(event is VerificationCodeCompanyEvent){
       yield* _mapVerifyCompanyToSate(event);
    }
    if(event is StatusAuthEvent){
      yield* _mapStatusToState();
    }
    else if(event is LoginEvent) {
      yield*  _mapToLoginState(event);
    }

    if(event is CloseSessionEvent){
      yield* _mapCloseSessionToState();
    }
  }

  Stream<AuthState> _mapToLoginState(LoginEvent event) async* {
    yield SendingLoginState();
    try{
      final res = await _authRepository.login(event.login);
      if(res != null) {
        yield  Authenticated(res);
      }
    }catch (error){
      var e =  HelperError().managementError(error);
      yield ErrorLoginState(status: error.toString(), message: e.toString());
    }
  }

  Stream<AuthState> _mapStatusToState() async* {
    final res = await _authRepository.currentSessionDB();
    print(res);
    if(res != null){
      yield Authenticated(res);
    }else {
      yield UnAuthenticate();
    }
    //yield UnAuthenticate();
  }

  Stream<AuthState> _mapVerifyCompanyToSate(VerificationCodeCompanyEvent event) async* {
    yield SendingCodeVerifyCompanyState();
    try{
      final res =  await _authRepository.checkCodeCompany(event.codeCompany);
      yield SuccessVerificationCodeCompany(res);
    }catch (error){
      var e =  HelperError().managementError(error);
        yield ErrorCompanyState(status: error.toString(), message: e.toString());
    }
  }
  Stream<AuthState> _mapCloseSessionToState() async*{
    try{
      await _authRepository.clearSession();
      yield CloseSessionState();
    }catch (error){
        print(error);
    }
  }
}