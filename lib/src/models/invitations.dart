class InvitationReceivedModel {
  int id;
  int tipo;
  int idGts;
  int idUserEnvio;
  int idUserRecibio;
  String invitadoFecha;
  int respuesta;
  String respuestaFecha;
  String correoExterno;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  String tipoPadre;
  DatosUsuarios datosUsuarios;
  GtsTodos gtsTodos;

  InvitationReceivedModel(
      {this.id,
        this.tipo,
        this.idGts,
        this.idUserEnvio,
        this.idUserRecibio,
        this.invitadoFecha,
        this.respuesta,
        this.respuestaFecha,
        this.correoExterno,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.tipoPadre,
        this.datosUsuarios,
        this.gtsTodos});

  InvitationReceivedModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    idGts = json['id_gts'];
    idUserEnvio = json['id_user_envio'];
    idUserRecibio = json['id_user_recibio'];
    invitadoFecha = json['invitado_fecha'];
    respuesta = json['respuesta'];
    respuestaFecha = json['respuesta_fecha'];
    correoExterno = json['correo_externo'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    tipoPadre = json['tipo_padre'];
    datosUsuarios = json['datos_usuarios'] != null
        ? new DatosUsuarios.fromJson(json['datos_usuarios'])
        : null;
   gtsTodos = json['gts_todos'] != null
        ? new GtsTodos.fromJson(json['gts_todos'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['id_gts'] = this.idGts;
    data['id_user_envio'] = this.idUserEnvio;
    data['id_user_recibio'] = this.idUserRecibio;
    data['invitado_fecha'] = this.invitadoFecha;
    data['respuesta'] = this.respuesta;
    data['respuesta_fecha'] = this.respuestaFecha;
    data['correo_externo'] = this.correoExterno;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['tipo_padre'] = this.tipoPadre;
    if (this.datosUsuarios != null) {
      data['datos_usuarios'] = this.datosUsuarios.toJson();
    }
    if (this.gtsTodos != null) {
      data['gts_todos'] = this.gtsTodos.toJson();
    }
    return data;
  }
}

class DatosUsuarios {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  DatosUsuarios(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  DatosUsuarios.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}

class GtsTodos {
  int id;
  int idUserPrincipal;
  int ptTipo;
  int idPqr;
  int idActa;
  int idActaTema;
  int idBitacora;
  String ptFecha;
  String ptHoraInicio;
  String ptHoraFin;
  int ptAtiempo;
  int ptAprobado;
  int idUserAprobado;
  String ptRechazoJustificacion;
  int ptCancelada;
  String ptCancelaJustificacion;
  String ptAprobadoFecha;
  String titulo;
  String descripcion;
  int idSubtipoActividad;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idPqrRelacionada;
  int idProyecto;
  String bResultados;
  String bFecha;
  String bHoraInicio;
  String bHoraFin;
  int bAtiempo;
  dynamic bManoObra;
  int gtsInformeInfo;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  GtsTipos gtsTipos;
  ClientModel cliente;
  List<GtsContactsModel> gtsContactos;
  MyPqrModel miPqr;
  MyEventModel miEvent;
  MyProjectModel miProyecto;

  GtsTodos(
      {this.id,
        this.idUserPrincipal,
        this.ptTipo,
        this.idPqr,
        this.idActa,
        this.idActaTema,
        this.idBitacora,
        this.ptFecha,
        this.ptHoraInicio,
        this.ptHoraFin,
        this.ptAtiempo,
        this.ptAprobado,
        this.idUserAprobado,
        this.ptRechazoJustificacion,
        this.ptCancelada,
        this.ptCancelaJustificacion,
        this.ptAprobadoFecha,
        this.titulo,
        this.descripcion,
        this.idSubtipoActividad,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idPqrRelacionada,
        this.idProyecto,
        this.bResultados,
        this.bFecha,
        this.bHoraInicio,
        this.bHoraFin,
        this.bAtiempo,
        this.bManoObra,
        this.gtsInformeInfo,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.gtsTipos,
        this.cliente,
        this.gtsContactos,
        this.miPqr,
        this.miEvent,
        this.miProyecto,
      });

  GtsTodos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    ptTipo = json['pt_tipo'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idActaTema = json['id_acta_tema'];
    idBitacora = json['id_bitacora'];
    ptFecha = json['pt_fecha'];
    ptHoraInicio = json['pt_hora_inicio'];
    ptHoraFin = json['pt_hora_fin'];
    ptAtiempo = json['pt_atiempo'];
    ptAprobado = json['pt_aprobado'];
    idUserAprobado = json['id_user_aprobado'];
    ptRechazoJustificacion = json['pt_rechazo_justificacion'];
    ptCancelada = json['pt_cancelada'];
    ptCancelaJustificacion = json['pt_cancela_justificacion'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idPqrRelacionada = json['id_pqr_relacionada'];
    idProyecto = json['id_proyecto'];
    bResultados = json['b_resultados'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    bAtiempo = json['b_atiempo'];
    bManoObra = json['b_mano_obra'];
    gtsInformeInfo = json['gts_informe_info'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    gtsTipos = json['gts_tipos'] != null
        ? new GtsTipos.fromJson(json['gts_tipos'])
        : null;
    cliente = json['cliente'] != null
        ? new ClientModel.fromJson(json['cliente'])
        : null;

    gtsContactos = json['gts_contactos'] != null
     ? (json["gts_contactos"] as List )
        .map((i) => new GtsContactsModel.fromJson(i))
        .toList(): null;

    miPqr = json['mi_pqr'] != null ? new MyPqrModel.fromJson(json['mi_pqr']) :  null;
    miEvent = json['mi_evento'] != null ? new MyEventModel.fromJson(json['mi_evento']) :  null;
    miProyecto = json['mi_proyecto'] != null ? new MyProjectModel.fromJson(json['mi_proyecto']) :  null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['pt_tipo'] = this.ptTipo;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_acta_tema'] = this.idActaTema;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['pt_hora_inicio'] = this.ptHoraInicio;
    data['pt_hora_fin'] = this.ptHoraFin;
    data['pt_atiempo'] = this.ptAtiempo;
    data['pt_aprobado'] = this.ptAprobado;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_rechazo_justificacion'] = this.ptRechazoJustificacion;
    data['pt_cancelada'] = this.ptCancelada;
    data['pt_cancela_justificacion'] = this.ptCancelaJustificacion;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_pqr_relacionada'] = this.idPqrRelacionada;
    data['id_proyecto'] = this.idProyecto;
    data['b_resultados'] = this.bResultados;
    data['b_fecha'] = this.bFecha;
    data['b_hora_inicio'] = this.bHoraInicio;
    data['b_hora_fin'] = this.bHoraFin;
    data['b_atiempo'] = this.bAtiempo;
    data['b_mano_obra'] = this.bManoObra;
    data['gts_informe_info'] = this.gtsInformeInfo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.gtsTipos != null) {
      data['gts_tipos'] = this.gtsTipos.toJson();
    }
    return data;
  }
}

class ClientModel {
  int id;
  String nombreCorto;
  String logo;

  ClientModel({this.id, this.nombreCorto, this.logo});

  ClientModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombreCorto = json['nombre_corto'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre_corto'] = this.nombreCorto;
    data['logo'] = this.logo;
    return data;
  }
}


class GtsTipos {
  int id;
  String nombre;
  int idPadre;
  int tipoPadre;

  GtsTipos({this.id, this.nombre, this.idPadre, this.tipoPadre});

  GtsTipos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    idPadre = json['id_padre'];
    tipoPadre = json['tipo_padre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['id_padre'] = this.idPadre;
    data['tipo_padre'] = this.tipoPadre;
    return data;
  }
}

class GtsContactsModel {
  int idGts;
  int id;
  int idClienteContacto;
  ClientesContacto clientesContacto;

  GtsContactsModel(
      {this.idGts, this.id, this.idClienteContacto, this.clientesContacto});

  GtsContactsModel.fromJson(Map<String, dynamic> json) {
    idGts = json['id_gts'];
    id = json['id'];
    idClienteContacto = json['id_cliente_contacto'];
    clientesContacto = json['clientes_contacto'] != null
        ? new ClientesContacto.fromJson(json['clientes_contacto'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_gts'] = this.idGts;
    data['id'] = this.id;
    data['id_cliente_contacto'] = this.idClienteContacto;
    if (this.clientesContacto != null) {
      data['clientes_contacto'] = this.clientesContacto.toJson();
    }
    return data;
  }
}

class ClientesContacto {
  int id;
  int idContacto;
  int idCliente;
  Contacto contacto;
  bool selected= false;

  ClientesContacto({this.id, this.idContacto, this.idCliente, this.contacto});

  ClientesContacto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idContacto = json['id_contacto'];
    idCliente = json['id_cliente'];
    contacto = json['contacto'] != null
        ? new Contacto.fromJson(json['contacto'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_contacto'] = this.idContacto;
    data['id_cliente'] = this.idCliente;
    if (this.contacto != null) {
      data['contacto'] = this.contacto.toJson();
    }
    return data;
  }
}

class Contacto {
  int id;
  String nombreCorto;
  String foto;
  String nombre;
  int idUser;

  Contacto({this.id, this.nombreCorto, this.foto, this.nombre, this.idUser});

  Contacto.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombreCorto = json['nombre_corto'];
    foto = json['foto'];
    nombre = json['nombre'];
    idUser = json['id_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre_corto'] = this.nombreCorto;
    data['foto'] = this.foto;
    data['nombre'] = this.nombre;
    data['id_user'] = this.idUser;
    return data;
  }
}

class MyPqrModel {
  int id;
  int estado;
  String finJustificacion;
  String finFecha;
  String titulo;
  String descripcion;
  int idTipo;
  int idCliente;
  int idOportunidad;
  int idClienteContacto;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  MyPqrModel(
      {this.id,
        this.estado,
        this.finJustificacion,
        this.finFecha,
        this.titulo,
        this.descripcion,
        this.idTipo,
        this.idCliente,
        this.idOportunidad,
        this.idClienteContacto,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  MyPqrModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    finJustificacion = json['fin_justificacion'];
    finFecha = json['fin_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idTipo = json['id_tipo'];
    idCliente = json['id_cliente'];
    idOportunidad = json['id_oportunidad'];
    idClienteContacto = json['id_cliente_contacto'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['fin_justificacion'] = this.finJustificacion;
    data['fin_fecha'] = this.finFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_tipo'] = this.idTipo;
    data['id_cliente'] = this.idCliente;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente_contacto'] = this.idClienteContacto;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}

class MyEventModel {
  int id;
  int estadoFinal;
  String estadoFinalFecha;
  int estadoFinalCalificacion;
  String estadoFinalComentario;
  int tipo;
  String nombre;
  String descripcion;
  String banner;
  String version;
  int idPadre;
  String fechaInicio;
  String fechaFin;
  dynamic lugar;
  String patrocinioEstrategia;
  String invitacionAdjunto;
  int leccionesTiempo;
  int idTipoEvento;
  int idmCiudad;
  int idUserPrincipal;
  int idUserPatrocinios;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  MyEventModel(
      {this.id,
        this.estadoFinal,
        this.estadoFinalFecha,
        this.estadoFinalCalificacion,
        this.estadoFinalComentario,
        this.tipo,
        this.nombre,
        this.descripcion,
        this.banner,
        this.version,
        this.idPadre,
        this.fechaInicio,
        this.fechaFin,
        this.lugar,
        this.patrocinioEstrategia,
        this.invitacionAdjunto,
        this.leccionesTiempo,
        this.idTipoEvento,
        this.idmCiudad,
        this.idUserPrincipal,
        this.idUserPatrocinios,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  MyEventModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estadoFinal = json['estado_final'];
    estadoFinalFecha = json['estado_final_fecha'];
    estadoFinalCalificacion = json['estado_final_calificacion'];
    estadoFinalComentario = json['estado_final_comentario'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    descripcion = json['descripcion'];
    banner = json['banner'];
    version = json['version'];
    idPadre = json['id_padre'];
    fechaInicio = json['fecha_inicio'];
    fechaFin = json['fecha_fin'];
    lugar = json['lugar'];
    patrocinioEstrategia = json['patrocinio_estrategia'];
    invitacionAdjunto = json['invitacion_adjunto'];
    leccionesTiempo = json['lecciones_tiempo'];
    idTipoEvento = json['id_tipo_evento'];
    idmCiudad = json['idm_ciudad'];
    idUserPrincipal = json['id_user_principal'];
    idUserPatrocinios = json['id_user_patrocinios'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado_final'] = this.estadoFinal;
    data['estado_final_fecha'] = this.estadoFinalFecha;
    data['estado_final_calificacion'] = this.estadoFinalCalificacion;
    data['estado_final_comentario'] = this.estadoFinalComentario;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['descripcion'] = this.descripcion;
    data['banner'] = this.banner;
    data['version'] = this.version;
    data['id_padre'] = this.idPadre;
    data['fecha_inicio'] = this.fechaInicio;
    data['fecha_fin'] = this.fechaFin;
    data['lugar'] = this.lugar;
    data['patrocinio_estrategia'] = this.patrocinioEstrategia;
    data['invitacion_adjunto'] = this.invitacionAdjunto;
    data['lecciones_tiempo'] = this.leccionesTiempo;
    data['id_tipo_evento'] = this.idTipoEvento;
    data['idm_ciudad'] = this.idmCiudad;
    data['id_user_principal'] = this.idUserPrincipal;
    data['id_user_patrocinios'] = this.idUserPatrocinios;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}

class MyProjectModel {
  int id;
  int estado;
  String estadoFinalFecha;
  String nombre;
  String descripcion;
  int avanceEmpresa;
  int timelineCliente;
  int idUserResponsable;
  int avanceCliente;
  int idTipo;
  int idCliente;
  int idOportunidad;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  MyProjectModel(
      {this.id,
        this.estado,
        this.estadoFinalFecha,
        this.nombre,
        this.descripcion,
        this.avanceEmpresa,
        this.timelineCliente,
        this.idUserResponsable,
        this.avanceCliente,
        this.idTipo,
        this.idCliente,
        this.idOportunidad,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  MyProjectModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    estado = json['estado'];
    estadoFinalFecha = json['estado_final_fecha'];
    nombre = json['nombre'];
    descripcion = json['descripcion'];
    avanceEmpresa = json['avance_empresa'];
    timelineCliente = json['timeline_cliente'];
    idUserResponsable = json['id_user_responsable'];
    avanceCliente = json['avance_cliente'];
    idTipo = json['id_tipo'];
    idCliente = json['id_cliente'];
    idOportunidad = json['id_oportunidad'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['estado'] = this.estado;
    data['estado_final_fecha'] = this.estadoFinalFecha;
    data['nombre'] = this.nombre;
    data['descripcion'] = this.descripcion;
    data['avance_empresa'] = this.avanceEmpresa;
    data['timeline_cliente'] = this.timelineCliente;
    data['id_user_responsable'] = this.idUserResponsable;
    data['avance_cliente'] = this.avanceCliente;
    data['id_tipo'] = this.idTipo;
    data['id_cliente'] = this.idCliente;
    data['id_oportunidad'] = this.idOportunidad;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}

class MyOpportunityModel {
  int id;
  String nombre;
  String fechaIdentificacion;
  int idOrigen;
  int idEvento;
  int precioTotal;
  int estadoFinal;
  String estadoFinalFecha;
  int idCompetenciaGanadora;
  int idOportunidadTipo;
  int idCliente;
  int idSede;
  int idUserResponsable;
  dynamic forecast;
  int forecastDias;
  String forecastUpdated;
  int createdBy;
  String updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  MyOpportunityModel(
      {this.id,
        this.nombre,
        this.fechaIdentificacion,
        this.idOrigen,
        this.idEvento,
        this.precioTotal,
        this.estadoFinal,
        this.estadoFinalFecha,
        this.idCompetenciaGanadora,
        this.idOportunidadTipo,
        this.idCliente,
        this.idSede,
        this.idUserResponsable,
        this.forecast,
        this.forecastDias,
        this.forecastUpdated,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt});

  MyOpportunityModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    fechaIdentificacion = json['fecha_identificacion'];
    idOrigen = json['id_origen'];
    idEvento = json['id_evento'];
    precioTotal = json['precio_total'];
    estadoFinal = json['estado_final'];
    estadoFinalFecha = json['estado_final_fecha'];
    idCompetenciaGanadora = json['id_competencia_ganadora'];
    idOportunidadTipo = json['id_oportunidad_tipo'];
    idCliente = json['id_cliente'];
    idSede = json['id_sede'];
    idUserResponsable = json['id_user_responsable'];
    forecast = json['forecast'];
    forecastDias = json['forecast_dias'];
    forecastUpdated = json['forecast_updated'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['fecha_identificacion'] = this.fechaIdentificacion;
    data['id_origen'] = this.idOrigen;
    data['id_evento'] = this.idEvento;
    data['precio_total'] = this.precioTotal;
    data['estado_final'] = this.estadoFinal;
    data['estado_final_fecha'] = this.estadoFinalFecha;
    data['id_competencia_ganadora'] = this.idCompetenciaGanadora;
    data['id_oportunidad_tipo'] = this.idOportunidadTipo;
    data['id_cliente'] = this.idCliente;
    data['id_sede'] = this.idSede;
    data['id_user_responsable'] = this.idUserResponsable;
    data['forecast'] = this.forecast;
    data['forecast_dias'] = this.forecastDias;
    data['forecast_updated'] = this.forecastUpdated;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}






class ApprovedInvitationModel{
  int idInvitation;
  String date;
  int response;

  ApprovedInvitationModel({this.idInvitation, this.date, this.response});

  Map<String, dynamic>  toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fecha'] = this.date;
    data['respuesta'] = this.response;
    return data;
  }
}
