import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/approved_user_model.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/guest_user_model.dart';

abstract class GtsInvitationState extends Equatable {
  GtsInvitationState([List props = const []]) : super(props);
}


class UnloadGtsGuestUsers extends GtsInvitationState {}

class LoadedGtsGuestUsers extends GtsInvitationState {
  final List<GuestUserModel> users;
  final List<GuestUserModel> email;
  final ApprovedUserActivityModel userApproved;
  final List<RequestContactModel> contacts;
  final String token;

  LoadedGtsGuestUsers({this.users, this.email, this.userApproved, this.contacts, this.token}) : super([users, email, userApproved, contacts, token]);

  @override
  String toString() => "$runtimeType { $users }";
}

class ErrorGtsInvitation extends GtsInvitationState{
  final String message;
  ErrorGtsInvitation({this.message}) : super([message]);
}