import 'package:smart_business/src/models/user_model.dart';

class PostponementModel {
  int id;
  int idGts;
  String fechaAnterior;
  String horaInicio;
  String horaFin;
  String justificacion;
  String fechaNueva;
  int createdBy;
  Null updatedBy;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  UserModel datosUsario;

  PostponementModel(
      {this.id,
        this.idGts,
        this.fechaAnterior,
        this.horaInicio,
        this.horaFin,
        this.justificacion,
        this.fechaNueva,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.datosUsario});

  PostponementModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idGts = json['id_gts'];
    fechaAnterior = json['fecha_anterior'];
    horaInicio = json['hora_inicio'];
    horaFin = json['hora_fin'];
    justificacion = json['justificacion'];
    fechaNueva = json['fecha_nueva'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    datosUsario = json['datos_usario'] != null
        ? new UserModel.fromJson(json['datos_usario'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_gts'] = this.idGts;
    data['fecha_anterior'] = this.fechaAnterior;
    data['hora_inicio'] = this.horaInicio;
    data['hora_fin'] = this.horaFin;
    data['justificacion'] = this.justificacion;
    data['fecha_nueva'] = this.fechaNueva;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.datosUsario != null) {
      data['datos_usario'] = this.datosUsario.toJson();
    }
    return data;
  }
}


