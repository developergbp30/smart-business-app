import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/invitations.dart';

abstract class InvitationsEvent extends Equatable {
  InvitationsEvent([List props = const []]) : super(props);
}

class LoadInvitationsEvent extends InvitationsEvent{
  final int type;
  final int idUser;
  LoadInvitationsEvent({this.type, this.idUser}) : super([type, idUser]);

  @override
  String toString() => '$runtimeType {$type}';
}

class CreateInvitationsEvent extends InvitationsEvent{}

class ApprovedInvitationsEvent extends InvitationsEvent{
  final ApprovedInvitationModel approvedInvitation;
  ApprovedInvitationsEvent(this.approvedInvitation) : super([approvedInvitation]);

  @override
  String toString() => '$runtimeType {$approvedInvitation}';
}

class RejectInvitationsEvent extends InvitationsEvent{
  final ApprovedInvitationModel rejectInvitation;
  RejectInvitationsEvent(this.rejectInvitation) : super([rejectInvitation]);

  @override
  String toString() => '$runtimeType {$rejectInvitation}';
}

class CancelInvitationsEvent extends InvitationsEvent{
  final ApprovedInvitationModel rejectInvitation;
  CancelInvitationsEvent(this.rejectInvitation) : super([rejectInvitation]);

  @override
  String toString() => '$runtimeType {$rejectInvitation}';
}