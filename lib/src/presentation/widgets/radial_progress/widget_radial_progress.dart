import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' as math;
class RadialProgress extends StatefulWidget {
  double goalCompleted = 0.7;
  double $widthContainer = 200;
  double $heightContainer = 200;
  double $sizeLabel = 20;
  Color  $colorLabel = Colors.blue;
  double $strokeWidthBase = 20.0;
  double $strokeWidthProgress = 20.0;
  Color  $progressColor = Colors.blue;
  RadialProgress({this.goalCompleted, this.$widthContainer, this.$heightContainer, this.$sizeLabel, this.$colorLabel, this.$strokeWidthBase, this.$strokeWidthProgress, this.$progressColor});
  @override
  _RadialProgressState createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress> with SingleTickerProviderStateMixin {
  AnimationController _radialProgressAnimationController;
  Animation<double> _progressAnimation;
  double progressDegrees = 0;
  var count = 0;

  @override
  void initState() {
    super.initState();
    _radialProgressAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 3));
      _progressAnimation =  Tween(begin: 0.0, end: 360.0)
          .animate(CurvedAnimation(parent:  _radialProgressAnimationController, curve: Curves.decelerate))
            ..addListener((){
              setState(() {
                progressDegrees = widget.goalCompleted * _progressAnimation.value;
              });
              print(progressDegrees);
            });

    _radialProgressAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        child: Container(
          width: widget.$widthContainer,
          height: widget.$heightContainer,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text("${widget.goalCompleted}%", style: TextStyle(fontSize: widget.$sizeLabel, color: widget.$colorLabel),)
            ],
          ),
        ),
    painter: RadialPainter(progressInDegrees: progressDegrees, $strokeWidthBase: widget.$strokeWidthBase, $strokeWidthProgress: widget.$strokeWidthProgress, $progressColor: widget.$progressColor != null ? widget.$progressColor : Colors.blue),);
  }
}

class RadialPainter extends CustomPainter {
  double progressInDegrees = 100.0;
  double $strokeWidthBase = 20.0;
  double $strokeWidthProgress = 20.0;
  Color  $progressColor = Colors.blue;
  RadialPainter({this.progressInDegrees, this.$strokeWidthBase, this.$strokeWidthProgress, this.$progressColor});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
        ..color =  Colors.black12
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.stroke
        ..strokeWidth = $strokeWidthBase;
    Offset center = Offset(size.width / 2, size.height / 2);
    canvas.drawCircle(center, size.width/2, paint);

    Paint progressPaint = Paint()
    ..color = $progressColor
    ..strokeCap = StrokeCap.round
    ..style =  PaintingStyle.stroke
    ..strokeWidth = $strokeWidthProgress;

    canvas.drawArc(Rect.fromCircle(center: center, radius: size.width/2),
        math.radians(-90), math.radians(progressInDegrees), false, progressPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

}
