import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_bloc.dart';
import 'package:smart_business/src/blocs/user_select_global/user_select_global_event.dart';
import 'package:smart_business/src/blocs/users/users_bloc.dart';
import 'package:smart_business/src/blocs/users/users_event.dart';
import 'package:smart_business/src/blocs/users/users_state.dart';
import 'package:smart_business/src/models/user_model.dart';
import 'package:smart_business/src/presentation/utils/load_image_Api.dart';
import 'package:smart_business/src/presentation/widgets/box_search.dart';
import 'package:smart_business/src/repositories/users/users_repository.dart';
class ModalUsersWidget extends StatefulWidget {
  Function function;

  ModalUsersWidget({this.function});

  @override
  _ModalUsersWidgetState createState() => _ModalUsersWidgetState();
}

class _ModalUsersWidgetState extends State<ModalUsersWidget> {
  UsersBloc _usersBloc;
  String avatarUrl = "https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814_960_720.jpg";

  List<UserModel> usersList;

  @override
  void initState() {
    _usersBloc = UsersBloc(usersRepository: UsersRepository());
    _usersBloc.dispatch(LoadUsersEvent());
    super.initState();
  }

  var title = "Vacio";
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.white,
      child: Column(
        children: <Widget>[
          SizedBox(height: 20,),
          Text("Usuario",textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
          Container(
            margin: const EdgeInsets.only(left: 10, right: 10),
            child: globalBoxSearch(context, hintText: "Buscar usuario", fOnchange: (value){

             _usersBloc.dispatch(FilterUsersEvent(usersList: usersList, query: value));
            }),
          ),
          BlocBuilder(
            bloc: _usersBloc,
            builder: (context, state){
              if(state is LoadedUsersState){
                usersList = state.users;
               return  Expanded(child: ListView.builder(
                    itemCount: state.users.length,
                    itemBuilder: (context, i) => _item( state.users[i], state.token)));
              }

              if(state is FilteredUserState){
                usersList = state.usersList;
               return  Expanded(child: ListView.builder(
                    itemCount: state.filtered.length,
                    itemBuilder: (context, i) => _item( state.filtered[i], state.token)));
              }
              if(state is LoadingUsersState){
                return Container(
                    width: MediaQuery.of(context).size.width,
                   // height: MediaQuery.of(context).size.height,
                    child: Center(child: CircularProgressIndicator(),));
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  Widget _item(UserModel user, String token){
    return ListTile(
      leading: CircleAvatar(
        backgroundImage: loadImageApi(user.foto, token),
      ),
      title: Text(user.nombre),
      subtitle: Text(user.cargo),
      onTap: (){

        if(widget.function != null){
          widget.function(user);
        }
        BlocProvider.of<UserSelectGlobal>(context).dispatch(ChangeUserGlobalEvent(user));
      },
    );
  }
}
