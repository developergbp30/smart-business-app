import 'package:smart_business/src/models/documents_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/workplane_model.dart';

class ResponseBitacoraModel {
  int id;
  int idUserPrincipal;
  int ptTipo;
  int idPqr;
  int idActa;
  int idActaTema;
  int idBitacora;
  String ptFecha;
  String ptHoraInicio;
  String ptHoraFin;
  int ptAtiempo;
  dynamic ptAprobado;
  dynamic idUserAprobado;
  dynamic ptRechazoJustificacion;
  dynamic ptCancelada;
  dynamic ptCancelaJustificacion;
  dynamic ptAprobadoFecha;
  String titulo;
  String descripcion;
  int idSubtipoActividad;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idPqrRelacionada;
  int idProyecto;
  String bResultados;
  String bFecha;
  String bHoraInicio;
  String bHoraFin;
  int bAtiempo;
  String bManoObra;
  int gtsInformeInfo;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  String color;
  ActividadGts actividadGts;
  GtsTipos gtsTipos;


  ResponseBitacoraModel(
      {this.id,
        this.idUserPrincipal,
        this.ptTipo,
        this.idPqr,
        this.idActa,
        this.idActaTema,
        this.idBitacora,
        this.ptFecha,
        this.ptHoraInicio,
        this.ptHoraFin,
        this.ptAtiempo,
        this.ptAprobado,
        this.idUserAprobado,
        this.ptRechazoJustificacion,
        this.ptCancelada,
        this.ptCancelaJustificacion,
        this.ptAprobadoFecha,
        this.titulo,
        this.descripcion,
        this.idSubtipoActividad,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idPqrRelacionada,
        this.idProyecto,
        this.bResultados,
        this.bFecha,
        this.bHoraInicio,
        this.bHoraFin,
        this.bAtiempo,
        this.bManoObra,
        this.gtsInformeInfo,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.color,
        this.actividadGts,
        this.gtsTipos,
      });

  ResponseBitacoraModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    ptTipo = json['pt_tipo'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idActaTema = json['id_acta_tema'];
    idBitacora = json['id_bitacora'];
    ptFecha = json['pt_fecha'];
    ptHoraInicio = json['pt_hora_inicio'];
    ptHoraFin = json['pt_hora_fin'];
    ptAtiempo = json['pt_atiempo'];
    ptAprobado = json['pt_aprobado'];
    idUserAprobado = json['id_user_aprobado'];
    ptRechazoJustificacion = json['pt_rechazo_justificacion'];
    ptCancelada = json['pt_cancelada'];
    ptCancelaJustificacion = json['pt_cancela_justificacion'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idPqrRelacionada = json['id_pqr_relacionada'];
    idProyecto = json['id_proyecto'];
    bResultados = json['b_resultados'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    bAtiempo = json['b_atiempo'];
    bManoObra = json['b_mano_obra'];
    gtsInformeInfo = json['gts_informe_info'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    color = json['color'];
    actividadGts = json['actividad_gts'] != null
        ? new ActividadGts.fromJson(json['actividad_gts'])
        : null;
    gtsTipos = json['gts_tipos'] != null
        ? new GtsTipos.fromJson(json['gts_tipos'])
        : null;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['pt_tipo'] = this.ptTipo;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_acta_tema'] = this.idActaTema;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['pt_hora_inicio'] = this.ptHoraInicio;
    data['pt_hora_fin'] = this.ptHoraFin;
    data['pt_atiempo'] = this.ptAtiempo;
    data['pt_aprobado'] = this.ptAprobado;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_rechazo_justificacion'] = this.ptRechazoJustificacion;
    data['pt_cancelada'] = this.ptCancelada;
    data['pt_cancela_justificacion'] = this.ptCancelaJustificacion;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_pqr_relacionada'] = this.idPqrRelacionada;
    data['id_proyecto'] = this.idProyecto;
    data['b_resultados'] = this.bResultados;
    data['b_fecha'] = this.bFecha;
    data['b_hora_inicio'] = this.bHoraInicio;
    data['b_hora_fin'] = this.bHoraFin;
    data['b_atiempo'] = this.bAtiempo;
    data['b_mano_obra'] = this.bManoObra;
    data['gts_informe_info'] = this.gtsInformeInfo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['color'] = this.color;
    if (this.actividadGts != null) {
      data['actividad_gts'] = this.actividadGts.toJson();
    }
    if (this.gtsTipos != null) {
      data['gts_tipos'] = this.gtsTipos.toJson();
    }
    return data;
  }
}

class ActividadGts {
  String nombre;
  int id;
  String codigo;

  ActividadGts({this.nombre, this.id, this.codigo});

  ActividadGts.fromJson(Map<String, dynamic> json) {
    nombre = json['nombre'];
    id = json['id'];
    codigo = json['codigo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nombre'] = this.nombre;
    data['id'] = this.id;
    data['codigo'] = this.codigo;
    return data;
  }
}

class ResponseDetailBitacoraModel {
  int id;
  int idUserPrincipal;
  int ptTipo;
  Null idBitacora;
  String ptFecha;
  String ptHoraFin;
  int ptAtiempo;
  dynamic ptAprobado;
  dynamic ptCancelada;
  int idUserAprobado;
  dynamic ptRechazoJustificacion;
  String titulo;
  String descripcion;
  int idSubtipoActividad;
  int idPqr;
  int idActa;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idProyecto;
  String bResultados;
  String bFecha;
  int bAtiempo;
  dynamic ptAprobadoFecha;
  String ptHoraInicio;
  String bHoraInicio;
  String bHoraFin;
  int gtsAplazamientosCount;
  int gtsComentariosCount;
  int gtsPendientesCount;
  String tipoActividad;
  List<dynamic> invitados;
  List<dynamic> informados;
  NombreActividad nombreActividad;
  GtsTipos gtsTipos;
  List<DocumentsModel> gtsAdjuntos;
  Cliente cliente;

  ResponseDetailBitacoraModel(
      {this.id,
        this.idUserPrincipal,
        this.ptTipo,
        this.idBitacora,
        this.ptFecha,
        this.ptHoraFin,
        this.ptAtiempo,
        this.ptAprobado,
        this.ptCancelada,
        this.idUserAprobado,
        this.ptRechazoJustificacion,
        this.titulo,
        this.descripcion,
        this.idSubtipoActividad,
        this.idPqr,
        this.idActa,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idProyecto,
        this.bResultados,
        this.bFecha,
        this.bAtiempo,
        this.ptAprobadoFecha,
        this.ptHoraInicio,
        this.bHoraInicio,
        this.bHoraFin,
        this.gtsAplazamientosCount,
        this.gtsComentariosCount,
        this.gtsPendientesCount,
        this.tipoActividad,
        this.invitados,
        this.informados,
        this.nombreActividad,
        this.gtsTipos,
        this.gtsAdjuntos,
        this.cliente
      });

  ResponseDetailBitacoraModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    ptTipo = json['pt_tipo'];
    idBitacora = json['id_bitacora'];
    ptFecha = json['pt_fecha'];
    ptHoraFin = json['pt_hora_fin'];
    ptAtiempo = json['pt_atiempo'];
    ptAprobado = json['pt_aprobado'];
    ptCancelada = json['pt_cancelada'];
    idUserAprobado = json['id_user_aprobado'];
    ptRechazoJustificacion = json['pt_rechazo_justificacion'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idProyecto = json['id_proyecto'];
    bResultados = json['b_resultados'];
    bFecha = json['b_fecha'];
    bAtiempo = json['b_atiempo'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    ptHoraInicio = json['pt_hora_inicio'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    gtsAplazamientosCount = json['gts_aplazamiento_count'];
    gtsComentariosCount = json['gts_comentarios_count'];
    gtsPendientesCount = json['gts_pendientes_count'];
    tipoActividad = json['tipo_actividad'];
    if (json['invitados'] != null) {
      invitados = new List<Null>();
      json['invitados'].forEach((v) {
      //  invitados.add(new Null.fromJson(v));
      });
    }
    if (json['informados'] != null) {
      informados = new List<Null>();
      json['informados'].forEach((v) {
      //  informados.add(new Null.fromJson(v));
      });
    }
    nombreActividad = json['nombre_actividad'] != null
        ? new NombreActividad.fromJson(json['nombre_actividad'])
        : null;
    gtsTipos = json['gts_tipos'] != null
        ? new GtsTipos.fromJson(json['gts_tipos'])
        : null;
    if (json['gts_adjuntos'] != null) {
      gtsAdjuntos = new List<DocumentsModel>();
      json['gts_adjuntos'].forEach((v) {
        gtsAdjuntos.add(new DocumentsModel.fromJson(v));
      });
    }
    cliente = json['cliente'] !=  null ?  new Cliente.fromJson(json['cliente']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['pt_tipo'] = this.ptTipo;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['pt_hora_fin'] = this.ptHoraFin;
    data['pt_atiempo'] = this.ptAtiempo;
    data['pt_aprobado'] = this.ptAprobado;
    data['pt_cancelada'] = this.ptCancelada;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_rechazo_justificacion'] = this.ptRechazoJustificacion;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_proyecto'] = this.idProyecto;
    data['b_resultados'] = this.bResultados;
    data['b_fecha'] = this.bFecha;
    data['b_atiempo'] = this.bAtiempo;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    data['pt_hora_inicio'] = this.ptHoraInicio;
    data['b_hora_inicio'] = this.bHoraInicio;
    data['b_hora_fin'] = this.bHoraFin;
    data['gts_aplazamientos_count'] = this.gtsAplazamientosCount;
    data['gts_comentarios_count'] = this.gtsComentariosCount;
    data['gts_pendientes_count'] = this.gtsPendientesCount;
    data['tipo_actividad'] = this.tipoActividad;
    if (this.invitados != null) {
     // data['invitados'] = this.invitados.map((v) => v.toJson()).toList();
    }
    if (this.informados != null) {
     // data['informados'] = this.informados.map((v) => v.toJson()).toList();
    }
    if (this.nombreActividad != null) {
      data['nombre_actividad'] = this.nombreActividad.toJson();
    }
    if (this.gtsTipos != null) {
      data['gts_tipos'] = this.gtsTipos.toJson();
    }
    if (this.gtsAdjuntos != null) {
      data['gts_adjuntos'] = this.gtsAdjuntos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NombreActividad {
  String nombre;
  int id;
  String codigo;

  NombreActividad({this.nombre, this.id, this.codigo});

  NombreActividad.fromJson(Map<String, dynamic> json) {
    nombre = json['nombre'];
    id = json['id'];
    codigo = json['codigo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nombre'] = this.nombre;
    data['id'] = this.id;
    data['codigo'] = this.codigo;
    return data;
  }
}


class CreateBitacoraModel {
  int id;
  int idUser;
  String fecha;
  List<String> hora;
  String planTrabajoId;
  String tarea;
  String tipoActividadId;
  int subActividadId;
  dynamic dinamicoId;
  String descripcion;
  String resultados;
  int idCliente;
  int idPqr;
  int idOportunidad;
  int idActa;
  int idEventos;
  int idCompetencias;
  int idProyecto;
  int bTiempo;
  List<ListadoAjuntos> listadoAjuntos;

  CreateBitacoraModel(
      {this.id,
        this.idUser,
        this.fecha,
        this.hora,
        this.planTrabajoId,
        this.tarea,
        this.tipoActividadId,
        this.subActividadId,
        this.dinamicoId,
        this.descripcion,
        this.resultados,
        this.idCliente,
        this.idPqr,
        this.idOportunidad,
        this.idActa,
        this.idEventos,
        this.idCompetencias,
        this.idProyecto,
        this.bTiempo,
        this.listadoAjuntos});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user'] = this.idUser;
    data['fecha'] = this.fecha;
    data['hora'] = this.hora;
    data['plan_trabajo_id'] = this.planTrabajoId;
    data['tarea'] = this.tarea;
    data['tipo_actividad_id'] = this.tipoActividadId;
    data['sub_actividad_id'] = this.subActividadId;
    data['dinamico_id'] = this.dinamicoId;
    data['descripcion'] = this.descripcion;
    data['resultados'] = this.resultados;
    data['id_cliente'] = this.idCliente;
    data['id_pqr'] = this.idPqr;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_acta'] = this.idActa;
    data['id_eventos'] = this.idEventos;
    data['id_competencias'] = this.idCompetencias;
    data['id_proyecto'] = this.idProyecto;
    data['b_tiempo'] = this.bTiempo;
    if (this.listadoAjuntos != null) {
      data['listadoAjuntos'] =
          this.listadoAjuntos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListadoAjuntos {
  String nombre;
  String adjunto;

  ListadoAjuntos({this.nombre, this.adjunto});

  ListadoAjuntos.fromJson(Map<String, dynamic> json) {
    nombre = json['nombre'];
    adjunto = json['adjunto'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nombre'] = this.nombre;
    data['adjunto'] = this.adjunto;
    return data;
  }
}

class CreatePendingModel {
  String titulo;
  int idBitacora;
  String ptFecha;
  String descripcion;
  int idUserPrincipal;

  CreatePendingModel(
      {this.idBitacora, this.ptFecha, this.descripcion, this.idUserPrincipal});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['titulo'] = this.titulo;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['descripcion'] = this.descripcion;
    data['id_user_principal'] = this.idUserPrincipal;
    return data;
  }
}


class ResponsePendingModel {
  int id;
  int idUserPrincipal;
  dynamic ptTipo;
  int idPqr;
  int idActa;
  int idActaTema;
  int idBitacora;
  String ptFecha;
  String ptHoraInicio;
  String ptHoraFin;
  dynamic ptAtiempo;
  int ptAprobado;
  int idUserAprobado;
  dynamic ptRechazoJustificacion;
  dynamic ptCancelada;
  dynamic ptCancelaJustificacion;
  String ptAprobadoFecha;
  String titulo;
  String descripcion;
  int idSubtipoActividad;
  int idOportunidad;
  int idCliente;
  int idEvento;
  int idCompetencia;
  int idPqrRelacionada;
  int idProyecto;
  String bResultados;
  String bFecha;
  String bHoraInicio;
  String bHoraFin;
  dynamic bAtiempo;
  String bManoObra;
  int gtsInformeInfo;
  int createdBy;
  int updatedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;
  DatosUsuario datosUsuario;

  ResponsePendingModel(
      {this.id,
        this.idUserPrincipal,
        this.ptTipo,
        this.idPqr,
        this.idActa,
        this.idActaTema,
        this.idBitacora,
        this.ptFecha,
        this.ptHoraInicio,
        this.ptHoraFin,
        this.ptAtiempo,
        this.ptAprobado,
        this.idUserAprobado,
        this.ptRechazoJustificacion,
        this.ptCancelada,
        this.ptCancelaJustificacion,
        this.ptAprobadoFecha,
        this.titulo,
        this.descripcion,
        this.idSubtipoActividad,
        this.idOportunidad,
        this.idCliente,
        this.idEvento,
        this.idCompetencia,
        this.idPqrRelacionada,
        this.idProyecto,
        this.bResultados,
        this.bFecha,
        this.bHoraInicio,
        this.bHoraFin,
        this.bAtiempo,
        this.bManoObra,
        this.gtsInformeInfo,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.datosUsuario});

  ResponsePendingModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUserPrincipal = json['id_user_principal'];
    ptTipo = json['pt_tipo'];
    idPqr = json['id_pqr'];
    idActa = json['id_acta'];
    idActaTema = json['id_acta_tema'];
    idBitacora = json['id_bitacora'];
    ptFecha = json['pt_fecha'];
    ptHoraInicio = json['pt_hora_inicio'];
    ptHoraFin = json['pt_hora_fin'];
    ptAtiempo = json['pt_atiempo'];
    ptAprobado = json['pt_aprobado'];
    idUserAprobado = json['id_user_aprobado'];
    ptRechazoJustificacion = json['pt_rechazo_justificacion'];
    ptCancelada = json['pt_cancelada'];
    ptCancelaJustificacion = json['pt_cancela_justificacion'];
    ptAprobadoFecha = json['pt_aprobado_fecha'];
    titulo = json['titulo'];
    descripcion = json['descripcion'];
    idSubtipoActividad = json['id_subtipo_actividad'];
    idOportunidad = json['id_oportunidad'];
    idCliente = json['id_cliente'];
    idEvento = json['id_evento'];
    idCompetencia = json['id_competencia'];
    idPqrRelacionada = json['id_pqr_relacionada'];
    idProyecto = json['id_proyecto'];
    bResultados = json['b_resultados'];
    bFecha = json['b_fecha'];
    bHoraInicio = json['b_hora_inicio'];
    bHoraFin = json['b_hora_fin'];
    bAtiempo = json['b_atiempo'];
    bManoObra = json['b_mano_obra'];
    gtsInformeInfo = json['gts_informe_info'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    datosUsuario = json['datos_usuario'] != null
        ? new DatosUsuario.fromJson(json['datos_usuario'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_user_principal'] = this.idUserPrincipal;
    data['pt_tipo'] = this.ptTipo;
    data['id_pqr'] = this.idPqr;
    data['id_acta'] = this.idActa;
    data['id_acta_tema'] = this.idActaTema;
    data['id_bitacora'] = this.idBitacora;
    data['pt_fecha'] = this.ptFecha;
    data['pt_hora_inicio'] = this.ptHoraInicio;
    data['pt_hora_fin'] = this.ptHoraFin;
    data['pt_atiempo'] = this.ptAtiempo;
    data['pt_aprobado'] = this.ptAprobado;
    data['id_user_aprobado'] = this.idUserAprobado;
    data['pt_rechazo_justificacion'] = this.ptRechazoJustificacion;
    data['pt_cancelada'] = this.ptCancelada;
    data['pt_cancela_justificacion'] = this.ptCancelaJustificacion;
    data['pt_aprobado_fecha'] = this.ptAprobadoFecha;
    data['titulo'] = this.titulo;
    data['descripcion'] = this.descripcion;
    data['id_subtipo_actividad'] = this.idSubtipoActividad;
    data['id_oportunidad'] = this.idOportunidad;
    data['id_cliente'] = this.idCliente;
    data['id_evento'] = this.idEvento;
    data['id_competencia'] = this.idCompetencia;
    data['id_pqr_relacionada'] = this.idPqrRelacionada;
    data['id_proyecto'] = this.idProyecto;
    data['b_resultados'] = this.bResultados;
    data['b_fecha'] = this.bFecha;
    data['b_hora_inicio'] = this.bHoraInicio;
    data['b_hora_fin'] = this.bHoraFin;
    data['b_atiempo'] = this.bAtiempo;
    data['b_mano_obra'] = this.bManoObra;
    data['gts_informe_info'] = this.gtsInformeInfo;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.datosUsuario != null) {
      data['datos_usuario'] = this.datosUsuario.toJson();
    }
    return data;
  }
}

class DatosUsuario {
  int id;
  int tipo;
  String nombre;
  String foto;
  String fotoMiniatura;
  String correo;
  String cargo;

  DatosUsuario(
      {this.id,
        this.tipo,
        this.nombre,
        this.foto,
        this.fotoMiniatura,
        this.correo,
        this.cargo});

  DatosUsuario.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipo = json['tipo'];
    nombre = json['nombre'];
    foto = json['foto'];
    fotoMiniatura = json['foto_miniatura'];
    correo = json['correo'];
    cargo = json['cargo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tipo'] = this.tipo;
    data['nombre'] = this.nombre;
    data['foto'] = this.foto;
    data['foto_miniatura'] = this.fotoMiniatura;
    data['correo'] = this.correo;
    data['cargo'] = this.cargo;
    return data;
  }
}







