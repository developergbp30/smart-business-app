import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:smart_business/src/presentation/widgets/widget_circle_color.dart';
class CreateClientPage extends StatefulWidget {
  @override
  _CreateClientPageState createState() => _CreateClientPageState();
}

class _CreateClientPageState extends State<CreateClientPage> {
  TextEditingController shortNameCtrl = TextEditingController();
  TextEditingController longNameCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.grey[100],
        leading: IconButton(icon: Icon(Icons.clear, color: Colors.green,), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: Text("Crear/editar cliente", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.check, color: Colors.green,), onPressed: (){

          })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            _messageInfo(),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Formulario", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            _containerImages(),
            _imageCoverPage(heightContainer: 50),
            //---> Form
            // Name Company Short
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Nombre corto*", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              controller: shortNameCtrl,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'ingrese ..',
                // labelText: 'Actividad'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese el nombre corto';
                }
                return null;
              },
              onSaved: (value){

              },
            ),
            // Name Company Long
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Nombre largo*", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              controller: longNameCtrl,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'ingrese ...',
                // labelText: 'Actividad'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese el nombre largo';
                }
                return null;
              },
              onSaved: (value){

              },
            ),
            // Country
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Pais", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // State
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Estado", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // City
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Ciudad", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Size Company
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Tamaño de la empresa", style: TextStyle(color: Colors.grey[700]),),
            ),
            TextFormField(
              controller: longNameCtrl,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 5.0
                    )
                ),
                hintText: 'ingrese ...',
                // labelText: 'Actividad'
              ),
              validator: (value){
                if(value.isEmpty){
                  return 'Ingrese el nombre largo';
                }
                return null;
              },
              onSaved: (value){

              },
            ),
            // State to Client
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Estado del cliente", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Identification Origin
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Origen de la indentificación", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Event
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Evento", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Date Foundation
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Fecha de Fundación", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // Date Exact
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                    activeColor: Colors.green,
                    value: true, onChanged: (value){

                }),
                Text("Fecha exacta", style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600),)
              ],
            ),
            /**----------------------------
             *  Classification
             *  --------------------------**/
            SizedBox(height: 30,),
            Row(
              children: <Widget>[
                WidgetCircleColor(color: Colors.green, height: 10, width: 10,),
                SizedBox(width: 10,),
                Text("Clasificación", style: TextStyle(color: Colors.green),)
              ],
            ),
            // - zonal
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Zonal", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // - Frequency purchase
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Frecuencia de compra", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
            // - Category
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Categoria", style: TextStyle(color: Colors.grey[700]),),
            ),
            InkWell(
              child:  Container(
                height: 40,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(""),
                      Icon(Icons.expand_more)
                    ],
                  ),
                ),
              ),
              onTap: (){

              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _messageInfo(){
    return Container(
      padding: EdgeInsets.all(20),
      child: Text("Antes de crear el cliente es necesario seleccionar el tipo de formulario, este tendra los datos selceccionados", textAlign: TextAlign.justify,
      style: TextStyle(color: Colors.grey),
      ),
    );
  }

  Widget _containerImages(){
    return Container(

      //color: Colors.red,
     height: 110,
      margin: EdgeInsets.only(top: 20, bottom: 20),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          _imageCoverPage(),
          Positioned(
            top: 20,
              child: _imageProfile()),
        ],
      ),
    );
  }

  Widget _imageCoverPage({double heightContainer = 70}){
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          color: Colors.grey.withOpacity(0.2),
          height: heightContainer,
        ),
        Positioned(
          bottom: -15,
          right: 5,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text("insertar portada  ", style: TextStyle(color: Colors.green),),
              Badge(
                badgeColor: Colors.green,
                badgeContent: Icon(Icons.add_photo_alternate, color: Colors.white,),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _imageProfile(){
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          width: 80,
          height: 80,
          child: CircleAvatar(
            backgroundColor: Colors.grey.withOpacity(0.2),
          ),
        ),
        Positioned(
          bottom: -20,
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Badge(
                badgeColor: Colors.green,
                badgeContent: Icon(Icons.add_photo_alternate, color: Colors.white,),
              ),
              Text("insertar foto perfil", style: TextStyle(color: Colors.green),),
            ],
          ),
        )
      ],
    );
  }
}
