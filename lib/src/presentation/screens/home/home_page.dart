import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:smart_business/src/presentation/utils/colors_theme.dart';
import 'package:smart_business/src/presentation/widgets/change_user_widget.dart';
import 'package:smart_business/src/presentation/widgets/drawer/drawer_left.dart';
import 'package:smart_business/src/presentation/widgets/modals/modal_users.dart';
import 'package:smart_business/src/presentation/widgets/radial_progress/widget_radial_progress.dart';
import 'package:smart_business/src/presentation/widgets/radial_progress/widget_radial_progress_types.dart';
class HomePage extends StatefulWidget {
  static const String routeName = 'home_page';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _value;
  ThemeApp _themeApp = ThemeApp();

  DateTime date;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: DrawerLeft(),
      ),
      endDrawer: Drawer(),
        appBar: AppBar(
            backgroundColor: _themeApp.colorPink,
            leading: Builder(builder: (context) => IconButton(icon: Icon(Icons.menu), onPressed: () => Scaffold.of(context).openDrawer()),),
            title: Text("Informe"),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(12),
                )
            ),
            actions: <Widget>[
              InkWell(
                child: AspectRatio(
                    aspectRatio: 1.4,
                    child: ChangeUserWidget()),
                onTap: (){
                    _showUserModal(context);
                },
              )
            ]
        ),
      body: Container(
               // margin: const EdgeInsets.all(5),
                padding: const EdgeInsets.all(8.0),
                child:  SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(left: 20, bottom: 5),
                        width: MediaQuery.of(context).size.width,
                          child: Text("Hora de inicio", style: TextStyle(color: Colors.grey[700]),)),
                      InkWell(
                        child:  Container(
                          height: 40,
                          decoration: new BoxDecoration(
                              color: Colors.grey[350],
                              border: Border.all(color: Colors.grey[500], width: 1),
                              borderRadius: new BorderRadius.circular(5.0)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Flexible(child: Text("21 de Feb de 2020 - 08 de Abr de 2020")),
                                Icon(Icons.expand_more)
                              ],
                            ),
                          ),
                        ),
                        onTap: (){
                              _showDatePicker(context);
                        },
                      ),
                      _chartsRadialActivityPlaning(context),
                      _chartsRadialInvitations(context),
                      _chartsRadialActivityDone(context),
                      _chartsRadialActivityTypes(context),
                      _chartsRadialActivityExecutedTypes(context)
                    ],
                  ),
                )
      ),
    );
  }

  Widget _chartsRadialActivityPlaning(BuildContext context){
    return Container(
      margin: EdgeInsets.only(top: 60),
     /* width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,*/
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -10,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                      // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Text("Actividades Planeadas", style: TextStyle(color: Colors.white),),),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  padding: const EdgeInsets.all(5),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                      // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Row(children: <Widget>[
                    Icon(FontAwesomeIcons.clipboardCheck, color: Colors.white, size: 16,),
                    Text(" 95", style: TextStyle(color: Colors.white, fontSize: 16))
                  ],),)
              ],),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            decoration: new BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                // border: Border.all(color: Colors.grey[500], width: 1),
                borderRadius: new BorderRadius.circular(5.0)
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width/2.5,
                      child: Card(
                        elevation: 2,
                        child:  Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              RadialProgress(goalCompleted: 0.8, $widthContainer: 100, $heightContainer: 100, $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0, $progressColor: Colors.green,),
                              SizedBox(height: 15,),
                              Text("Planeado a tiempo")
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/2.5,
                      child: Card(
                        elevation: 2,
                        child:  Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              RadialProgress(goalCompleted: 0.5, $widthContainer: 100, $heightContainer: 100, $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0, $progressColor: Colors.green),
                              SizedBox(height: 15,),
                              Text("Realizadas")
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width/2.5,
                      child: Card(
                        elevation: 2,
                        child:  Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              RadialProgress(goalCompleted: 0.8, $widthContainer: 100, $heightContainer: 100, $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0,$progressColor: Colors.orangeAccent),
                              SizedBox(height: 15,),
                              Text("Aplazadas")
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/2.5,
                      child: Card(
                        elevation: 2,
                        child:  Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              RadialProgress(goalCompleted: 0.5, $widthContainer: 100, $heightContainer: 100, $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0,$progressColor: Colors.orange, $colorLabel: Colors.orangeAccent,),
                              SizedBox(height: 15,),
                              Text("Rechazadas")
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _chartsRadialInvitations(BuildContext context){
    return Container(
     // color: Colors.red,
      margin: EdgeInsets.only(top: 60),
      width: MediaQuery.of(context).size.width,
     /* height: MediaQuery.of(context).size.width,*/
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -10,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                      // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Text("Invitaciones", style: TextStyle(color: Colors.white),),),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  padding: const EdgeInsets.all(5),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                      // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Row(children: <Widget>[
                    Icon(FontAwesomeIcons.clipboardCheck, color: Colors.white, size: 16,),
                    Text(" 230", style: TextStyle(color: Colors.white, fontSize: 16))
                  ],),)
              ],),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(8),
            decoration: new BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                 //border: Border.all(color: Colors.grey[500], width: 1),
                borderRadius: new BorderRadius.circular(5.0)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 20,),
                Card(
                  elevation: 2,
                  child:  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      children: <Widget>[
                        RadialProgress(goalCompleted: 0.8, $widthContainer: 100, $heightContainer: 100,
                          $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0, $colorLabel: Colors.green, $progressColor: Colors.green,),
                        SizedBox(height: 15,),
                        Text("Aceptadas")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _chartsRadialActivityDone(BuildContext context){
    return Container(
      margin: EdgeInsets.only(top: 60),
     /* width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.width,*/
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -10,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
              Container(
                padding: const EdgeInsets.all(8),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                     // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                child: Text("Actividades Realizadas", style: TextStyle(color: Colors.white),),),
              Container(
                margin: const EdgeInsets.only(left: 5),
                padding: const EdgeInsets.all(5),
                decoration: new BoxDecoration(
                    color: _themeApp.colorPink,
                    // border: Border.all(color: Colors.grey[500], width: 1),
                    borderRadius: new BorderRadius.circular(5.0)
                ),
                child: Row(children: <Widget>[
                  Icon(FontAwesomeIcons.clipboardCheck, color: Colors.white, size: 16,),
                  Text(" 134", style: TextStyle(color: Colors.white, fontSize: 16))
              ],),)
            ],),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            decoration: new BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                //border: Border.all(color: Colors.grey[500], width: 1),
                borderRadius: new BorderRadius.circular(5.0)
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width:MediaQuery.of(context).size.width/2.5,
                      child: Card(
                        elevation: 2,
                        child:  Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              RadialProgress(goalCompleted: 0.8, $widthContainer: 100, $heightContainer: 100,
                                $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0, $colorLabel: Colors.green, $progressColor: Colors.green,),
                              SizedBox(height: 15,),
                              Text("Planeado a tiempo")
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width:MediaQuery.of(context).size.width/2.5,
                      child: Column(
                        children: <Widget>[
                          Row(children: <Widget>[
                            Icon(FontAwesomeIcons.clock, color: Colors.grey, size: 30,),
                            Text("193", style: TextStyle(color: Colors.grey, fontSize: 30, fontWeight: FontWeight.bold),)
                          ],
                          ),
                          SizedBox(height: 5,),
                          Text("Horas Planeadas"),
                          SizedBox(height: 10,),
                          Row(children: <Widget>[
                            Icon(Icons.cancel, color: Colors.grey, size: 30,),
                            Text("100", style: TextStyle(color: Colors.grey, fontSize: 30, fontWeight: FontWeight.bold),)
                          ],
                          ),
                          SizedBox(height: 5,),
                          Text("Horas No Planeadas"),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width:MediaQuery.of(context).size.width/2.5,
                      child: Card(
                        elevation: 2,
                        child:  Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              RadialProgress(goalCompleted: 0.8, $widthContainer: 100, $heightContainer: 100,
                                $strokeWidthBase: 10.0, $strokeWidthProgress: 10.0, $colorLabel: Colors.blueAccent, $progressColor: Colors.blueAccent,),
                              SizedBox(height: 15,),
                              Text("Aplazadas")
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width:MediaQuery.of(context).size.width/2.5,
                      child: Column(
                        children: <Widget>[
                          Text("193", style: TextStyle(color: Colors.grey, fontSize: 30, fontWeight: FontWeight.bold),),
                          SizedBox(height: 5,),
                          Text("Informe"),
                          SizedBox(height: 10,),
                          Text("100", style: TextStyle(color: Colors.grey, fontSize: 30, fontWeight: FontWeight.bold),),
                          SizedBox(height: 5,),
                          Text("Pendientes")
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _chartsRadialActivityTypes(BuildContext context){
    return Container(
      margin: EdgeInsets.only(top: 60),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -10,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                      // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Text("Actividades planeadas por tipo", style: TextStyle(color: Colors.white),),),
              ],),
          ),

          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(8),
            decoration: new BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                //border: Border.all(color: Colors.grey[500], width: 1),
                borderRadius: new BorderRadius.circular(5.0)
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 35,),
                RadialProgressTypes(goalCompleted: 0.7, $widthContainer: 200, $heightContainer: 200,
                  $strokeWidthBase: 20.0, $strokeWidthProgress: 20.0, $colorLabel: Colors.green, $progressColor: Colors.green,),
                SizedBox(height: 20,),
              ],
            ),
          ),

        ],
      ),
    );
  }

  Widget _chartsRadialActivityExecutedTypes(BuildContext context){
    return Container(
      margin: EdgeInsets.only(top: 60),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -10,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: new BoxDecoration(
                      color: _themeApp.colorPink,
                      // border: Border.all(color: Colors.grey[500], width: 1),
                      borderRadius: new BorderRadius.circular(5.0)
                  ),
                  child: Text("Actividades Ejecutadas por tipo", style: TextStyle(color: Colors.white),),),
              ],),
          ),

          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(8),
            decoration: new BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                //border: Border.all(color: Colors.grey[500], width: 1),
                borderRadius: new BorderRadius.circular(5.0)
            ),
            child: Column(
              children: <Widget>[
                SizedBox(height: 35,),
                RadialProgressTypes(goalCompleted: 0.7, $widthContainer: 200, $heightContainer: 200,
                  $strokeWidthBase: 20.0, $strokeWidthProgress: 20.0, $colorLabel: Colors.green, $progressColor: Colors.green,),
                SizedBox(height: 20,),
              ],
            ),
          ),

        ],
      ),
    );
  }

  void _showDatePicker(context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  child: Center(child: Text("Seleccionar el periodo", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),)),
                ),
                Container(
                  height: 200,
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (value){
                      setState(() {
                        print(date);
                        date = value;
                      });
                    },
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Cerrar", style: TextStyle(color: Colors.grey.withOpacity(0.6)),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.pop(context);
                        }),
                    SizedBox(width: 20),
                    FlatButton(
                        padding: const EdgeInsets.all(0),
                        color: Colors.transparent,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(50.0),
                            side: BorderSide(color: Colors.grey.withOpacity(0.7))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Aceptar", style: TextStyle(color: _themeApp.colorPink),),
                          ],
                        ),
                        // color: Color(0xff5D5D5D), //Colors.pink,
                        onPressed: () async {
                          Navigator.of(context).pop();
                        })
                  ],
                )
              ],
            ),
          );
        }
    );
  }
  _showUserModal(BuildContext contex){
    showModalBottomSheet(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (BuildContext bc){
          return ModalUsersWidget();
        }
    );
  }
}
