import 'package:smart_business/src/api/services/clients_http_service.dart';
import 'package:smart_business/src/api/services/users_http_service.dart';
import 'package:smart_business/src/models/contacts_model.dart';
import 'package:smart_business/src/models/invitations.dart';
import 'package:smart_business/src/models/user_model.dart';

class ContactsRepository {

  ClientsHttp _clientsHttp = ClientsHttp();
  UsersHttp _usersHttp = UsersHttp();

  static List<ContactModel> contacts = [
    ContactModel(
      name: "Diego Castro",
      company: "The Coca-Cola, Netflix, Google, Sony",
      avatarUrl: "https://cdn.pixabay.com/photo/2016/11/21/14/53/adult-1845814_960_720.jpg",
    ),
    ContactModel(
      name: "Laura Martines",
      company: "Apple Home Center, BMW",
      avatarUrl: "https://cdn.pixabay.com/photo/2015/09/02/12/58/woman-918788_960_720.jpg"
    ),
    ContactModel(
      name: "Jesus Carvajal",
      company: "Falabella, Nike",
      avatarUrl: "https://cdn.pixabay.com/photo/2015/09/02/12/58/woman-918788_960_720.jpg"
    ),
    ContactModel(
      name: "Diego Castro",
      company: "The Coca-Cola, Netflix, Google, Sony",
      avatarUrl: "https://cdn.pixabay.com/photo/2015/09/02/12/58/woman-918788_960_720.jpg"
    ),
    ContactModel(
      name: "Laura Martinez",
      company: "The Coca-Cola, Netflix, Google, Sony",
      avatarUrl: "https://cdn.pixabay.com/photo/2015/09/02/12/58/woman-918788_960_720.jpg"
    ),
    ContactModel(
      name: "Jesus Carvajal",
      company: "The Coca-Cola, Netflix, Google, Sony",
      avatarUrl: "https://cdn.pixabay.com/photo/2015/09/02/12/58/woman-918788_960_720.jpg"
    ),
  ];

  Future<List<ContactModel>> getContactsExample() async {
    Future.delayed(const Duration(seconds: 1));
    return contacts;
  }

  Future<List<ClientesContacto>> contactsClient(idClient) async {
    try{
        return _clientsHttp.contacts(idClient);
    }catch (error){
      return Future.error(error);
    }
  }

  Future<List<UserModel>> users() async {
    try{
        return _usersHttp.getUsers();
    }catch (error){
      return Future.error(error);
    }
  }
}