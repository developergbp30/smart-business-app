import 'package:dio/dio.dart';
import 'package:smart_business/src/api/Api.dart';
import 'package:smart_business/src/models/competitions_model.dart';

class CompetitionsHttp extends ApiProvider{
  Future<List<ResponseCompetitionModel>> listCompetitionsUser(int idUser) async{
    try{
      Response response = await dio.get("/GestionTiempos/PlanTrabajo/Compentencias/$idUser/Action-Load-Select-Dinamic", options: Options(headers: {"requirestoken": true}));
      return (response.data as List)
          .map((json) => new ResponseCompetitionModel.fromJson(json))
          .toList();

    }catch (error){
      return Future.error(error);
    }
  }
}