
import 'package:equatable/equatable.dart';
import 'package:smart_business/src/models/client_model.dart';

abstract class ClientsEvent extends Equatable {
  ClientsEvent([List props = const []]) : super(props);
}

class LoadClientsEvent extends ClientsEvent{}

class ChangeClientEvent extends ClientsEvent{
  final int clientID;
  ChangeClientEvent(this.clientID) : super([clientID]);
}

class FilterClientEvent extends ClientsEvent{
  final String query;
  final List<ClientModel> clients;
  FilterClientEvent({this.query, this.clients}) : super([query, clients]);
}